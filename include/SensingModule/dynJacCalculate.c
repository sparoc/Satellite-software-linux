#include "dynJacCalculate.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>


extern void calculateDynJac(float (*dynJacPt)[9], float th1, float th2, float th3){
float JB1[9][9], invJB1[9][9], JB2[9][9], d;
float dynJaCComplete[9][9];
float dynJac[9][9],dynJacT[9][9];
float pinvDynJac[9][9];
float AAT[9][9], invAAT[9][9];
float th12,th23,th123;
int i, j;

th12  = th1+th2;
th23  = th2 + th3;
th123 = th1+th2+th3;

JB1[0][0] = 60.017*cos(th123) + 1.699*cos(th123) + 2.913*cos(th1)*cos(th23) - 2.913*sin(th1)*sin(th23) + 2.646*cos(th12)*cos(th3) - 2.646*sin(th12)*sin(th3);
JB1[0][1] = - 60.017*sin(th123) - 1.699*sin(th123) - 2.913*cos(th1)*sin(th23) - 2.913*sin(th1)*cos(th23) - 2.646*cos(th12)*sin(th3) - 2.646*sin(th12)*cos(th3);
JB1[0][2] = 0.0;
JB1[0][3] = 0.0;
JB1[0][4] = 0.0;
JB1[0][5] = 21.276*sin(th123) + 0.4275*sin(th123) + 27.0016*sin(th12) + 26.9356*sin(th1) + 2.913*cos(th1)*(0.3545*sin(th23) + 0.4499*sin(th2)) + 2.646*sin(th12)*(0.3545*cos(th3) + 0.2499) + 2.913*sin(th1)*(0.3545*cos(th23) + 0.4499*cos(th2) + 0.2683) + 0.9380*cos(th12)*sin(th3) + 0.0768;
JB1[1][0] = 60.017*sin(th123) + 1.699*sin(th123) + 2.913*cos(th1)*sin(th23) + 2.913*sin(th1)*cos(th23) + 2.646*cos(th12)*sin(th3) + 2.646*sin(th12)*cos(th3);
JB1[1][1] = 60.017*cos(th123) + 1.699*cos(th123) + 2.913*cos(th1)*cos(th23) - 2.913*sin(th1)*sin(th23) + 2.646*cos(th12)*cos(th3) - 2.646*sin(th12)*sin(th3);
JB1[1][2] = 0.0;
JB1[1][3] = 0.0;
JB1[1][4] = 0.0;
JB1[1][5] = 2.913*sin(th1)*(0.3545*sin(th23) + 0.4499*sin(th2)) - 0.4275*cos(th123) - 27.0016*cos(th12) - 26.9356*cos(th1) - 21.2760*cos(th123) - 2.646*cos(th12)*(0.3545*cos(th3) + 0.2499) - 2.913*cos(th1)*(0.3545*cos(th23) + 0.4499*cos(th2) + 0.2683) + 0.9380*sin(th12)*sin(th3) - 22.1823;
JB1[2][0] = 0.0;
JB1[2][1] = 0.0;
JB1[2][2] = 67.275;
JB1[2][3] = 22.1823*sin(th123) - 0.0768*cos(th123) + 27.7172*sin(th23) + 28.9734*sin(th3);
JB1[2][4] = 22.1823*cos(th123) + 0.0768*sin(th123) + 27.7172*cos(th23) + 28.9734*cos(th3) + 23.6742;
JB1[2][5] = 0.0;
JB1[3][0] = 0.0;
JB1[3][1] = 0.0;
JB1[3][2] = 2.4758*sin(th1) + 0.1748*sin(th123) + 1.2936*sin(th12) - 0.0768;
JB1[3][3] = 0.0001*cos(th1 + th2 + th3) - 0.0284*sin(th123) - 0.0344*sin(th23) - 0.0345*sin(th3) + 0.5258*sin(th1)*(0.2683*sin(th23) + 0.4499*sin(th3)) + 0.2499*sin(th3)*(1.1875*sin(th1) + 0.5292*sin(th12));
JB1[3][4] = 0.1918*sin(th1) - 0.0001*sin(th123) - 0.0284*cos(th123) + 0.0440*sin(th123) + 0.1923*sin(th12) - 0.0344*cos(th23) - 0.0345*cos(th3) + (0.2499*cos(th3) + 0.3545)*(1.1875*sin(th1) + 0.5292*sin(th12)) + 0.5258*sin(th1)*(0.2683*cos(th23) + 0.4499*cos(th3) + 0.3545) - 0.0272;
JB1[3][5] = 0.0;
JB1[4][0] = 0.0;
JB1[4][1] = 0.0;
JB1[4][2] = 22.1823 - 0.1748*cos(th123) - 1.2936*cos(th12) - 2.4758*cos(th1);
JB1[4][3] = 8.1986*sin(th123) - 0.0284*cos(th123) + 9.9554*sin(th23) + 9.9798*sin(th3) - 0.5258*cos(th1)*(0.2683*sin(th23) + 0.4499*sin(th3)) - 0.2499*sin(th3)*(1.1875*cos(th1) + 0.5292*cos(th12));
JB1[4][4] = 8.1986*cos(th123) + 0.0284*sin(th123) - 0.1918*cos(th1) - 0.0440*cos(th123) - 0.1923*cos(th12) + 9.9554*cos(th23) + 9.9798*cos(th3) - 1.0*(1.1875*cos(th1) + 0.5292*cos(th12))*(0.2499*cos(th3) + 0.3545) - 0.5258*cos(th1)*(0.2683*cos(th23) + 0.4499*cos(th3) + 0.3545) + 7.8636;
JB1[4][5] = 0.0;
JB1[5][0] = 0.0768*cos(th123) - 22.1823*sin(th123) + 0.7644*sin(th3) + 0.7625*sin(th23) + 0.5258*sin(th23) + 1.1875*sin(th2)*cos(th3) + sin(th3)*(1.1875*cos(th2) + 0.5292);
JB1[5][1] = 0.7644*cos(th3) - 0.0768*sin(th123) - 22.1823*cos(th123) + 0.7625*cos(th23) + 0.5258*cos(th23) - 1.1875*sin(th2)*sin(th3) + cos(th3)*(1.1875*cos(th2) + 0.5292) + 0.1748;
JB1[5][2] = 0.0;
JB1[5][3] = 0.0;
JB1[5][4] = 0.0;
JB1[5][5] = 7.8636*cos(th123) + 0.0272*sin(th1 + th2 + th3) - 0.1923*cos(th3) - 0.1918*cos(th23) + 9.9798*cos(th12) - 0.1864*cos(th23) + 0.0345*sin(th12) + 9.9554*cos(th1) - 0.2365*cos(th2) + 0.0344*sin(th1) - 1.0*(0.3545*cos(th3) + 0.2499)*(1.1875*cos(th2) + 0.5292) + 0.4210*sin(th2)*sin(th3) + 10.5923;

JB2[0][0] = 21.2760*sin(th123) + 27.0016*sin(th12) + 26.9356*sin(th1) + 60.017*cos(th123)*(0.4488*sin(th23) + 0.4499*sin(th3)) - 60.017*sin(th123)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545) + 2.646*sin(th12)*(0.4488*cos(th2) + 0.3545*cos(th3) - 1.0*cos(th3)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545) - 1.0*sin(th3)*(0.4488*sin(th23) + 0.4499*sin(th3)) + 0.4499) + 2.913*cos(th1)*(0.3545*sin(th23) + 0.4499*sin(th2) + cos(th23)*(0.4488*sin(th23) + 0.4499*sin(th3)) - 1.0*sin(th23)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545)) - 2.646*cos(th12)*(0.4488*sin(th2) - 0.3545*sin(th3) + sin(th3)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545) - 1.0*cos(th3)*(0.4488*sin(th23) + 0.4499*sin(th3))) + 2.913*sin(th1)*(0.3545*cos(th23) + 0.4499*cos(th2) - 1.0*sin(th23)*(0.4488*sin(th23) + 0.4499*sin(th3)) - 1.0*cos(th23)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545) + 0.4488) + 0.0768;
JB2[0][1] = 21.2760*sin(th123) + 27.0016*sin(th12) + 26.9356*sin(th1) + 2.913*sin(th1)*(0.3545*cos(th23) + 0.4499*cos(th2) - 0.4499*sin(th23)*sin(th3) - 1.0*cos(th23)*(0.4499*cos(th3) + 0.3545) + 0.2683) + 27.0016*cos(th123)*sin(th3) + 2.913*cos(th1)*(0.3545*sin(th2 + th3) + 0.4499*sin(th2) - 1.0*sin(th23)*(0.4499*cos(th3) + 0.3545) + 0.4499*cos(th23)*sin(th3)) + 2.646*cos(th12)*(0.3545*sin(th3) + 0.4499*cos(th3)*sin(th3) - 1.0*sin(th3)*(0.4499*cos(th3) + 0.3545)) + 2.646*sin(th12)*(0.3545*cos(th3) - 0.4499*sin(th3)*sin(th3) - 1.0*cos(th3)*(0.4499*cos(th3) + 0.3545) + 0.4499) - 60.017*sin(th123)*(0.4499*cos(th3) + 0.3545) + 0.0768;
JB2[0][2] = 0.6612*sin(th12) + 27.0016*sin(th12) + 26.9356*sin(th1) + 1.3105*cos(th1)*sin(th2) + 2.913*sin(th1)*(0.4499*cos(th2) + 0.2683) + 0.0768;
JB2[1][0] = 60.017*sin(th123)*(0.4488*sin(th23) + 0.4499*sin(th3)) - 27.0016*cos(th12) - 26.9356*cos(th1) - 21.2760*cos(th123) + 60.017*cos(th123)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545) - 2.646*cos(th12)*(0.4488*cos(th2) + 0.3545*cos(th3) - 1.0*cos(th3)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545) - 1.0*sin(th3)*(0.4488*sin(th23) + 0.4499*sin(th3)) + 0.4499) + 2.913*sin(th1)*(0.3545*sin(th23) + 0.4499*sin(th2) + cos(th23)*(0.4488*sin(th23) + 0.4499*sin(th3)) - 1.0*sin(th23)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545)) - 2.913*cos(th1)*(0.3545*cos(th23) + 0.4499*cos(th2) - 1.0*sin(th23)*(0.4488*sin(th23) + 0.4499*sin(th3)) - 1.0*cos(th23)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545) + 0.4488) - 2.646*sin(th12)*(0.4488*sin(th2) - 0.3545*sin(th3) + sin(th3)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545) - 1.0*cos(th3)*(0.4488*sin(th23) + 0.4499*sin(th3))) - 22.1823;
JB2[1][1] = 27.0016*sin(th123)*sin(th3) - 27.0016*cos(th12) - 26.9356*cos(th1) - 2.913*cos(th1)*(0.3545*cos(th23) + 0.4499*cos(th2) - 0.4499*sin(th23)*sin(th3) - 1.0*cos(th2 + th3)*(0.4499*cos(th3) + 0.3545) + 0.2683) - 21.2760*cos(th123) - 2.646*cos(th12)*(0.3545*cos(th3) - 0.4499*sin(th3)*sin(th3) - 1.0*cos(th3)*(0.4499*cos(th3) + 0.3545) + 0.4499) + 2.913*sin(th1)*(0.3545*sin(th23) + 0.4499*sin(th2) - 1.0*sin(th23)*(0.4499*cos(th3) + 0.3545) + 0.4499*cos(th23)*sin(th3)) + 2.646*sin(th12)*(0.3545*sin(th3) + 0.4499*cos(th3)*sin(th3) - 1.0*sin(th3)*(0.4499*cos(th3) + 0.3545)) + 60.017*cos(th123)*(0.4499*cos(th3) + 0.3545) - 22.1823;
JB2[1][2] = 1.3105*sin(th1)*sin(th2) - 27.0016*cos(th12) - 26.9356*cos(th1) - 0.6612*cos(th12) - 2.913*cos(th1)*(0.4499*cos(th2) + 0.2683) - 22.1823;
JB2[2][0] = 0.0;
JB2[2][1] = 0.0;
JB2[2][2] = 0.0;
JB2[3][0] = 0.0;
JB2[3][1] = 0.0;
JB2[3][2] = 0.0;
JB2[4][0] = 0.0;
JB2[4][1] = 0.0;
JB2[4][2] = 0.0;
JB2[5][0] = 7.8636*cos(th123) + 0.0272*sin(th123) + 9.9798*cos(th12) - 0.1864*cos(th23) + 0.0345*sin(th12) + 9.9554*cos(th1) - 0.2365*cos(th2) + 0.0344*sin(th1) + 0.0768*cos(th123)*(0.4488*sin(th2 + th3) + 0.4499*sin(th3)) - 22.1823*sin(th123)*(0.4488*sin(th23) + 0.4499*sin(th3)) - 22.1823*cos(th123)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545) - 0.0768*sin(th123)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545) - 1.1875*sin(th2)*(0.4488*sin(th2) - 0.3545*sin(th3) + sin(th3)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545) - 1.0*cos(th3)*(0.4488*sin(th23) + 0.4499*sin(th3))) - 1.0*(1.1875*cos(th2) + 0.5292)*(0.4488*cos(th2) + 0.3545*cos(th3) - 1.0*cos(th3)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545) - 1.0*sin(th3)*(0.4488*sin(th23) + 0.4499*sin(th3)) + 0.4499) + 0.5258*sin(th23)*(0.4488*sin(th23) + 0.4499*sin(th3)) + 0.5258*cos(th23)*(0.4488*cos(th23) + 0.4499*cos(th3) + 0.3545) + 10.3467;
JB2[5][1] = 7.8636*cos(th123) + 0.0272*sin(th123) + 9.9798*cos(th12) - 0.1864*cos(th23) + 0.0345*sin(th12) + 9.9554*cos(th1) - 0.2365*cos(th2) + 0.0344*sin(th1) + 0.2365*sin(th23)*sin(th3) + 0.0345*cos(th123)*sin(th3) + 0.5258*cos(th23)*(0.4499*cos(th3) + 0.3545) - 9.9798*sin(th123)*sin(th3) + 1.1875*sin(th2)*(0.3545*sin(th3) + 0.4499*cos(th3)*sin(th3) - 1.0*sin(th3)*(0.4499*cos(th3) + 0.3545)) - 1.0*(1.1875*cos(th2) + 0.5292)*(0.3545*cos(th3) - 0.4499*sin(th3)*sin(th3) - 1.0*cos(th3)*(0.4499*cos(th3) + 0.3545) + 0.4499) - 22.1823*cos(th123)*(0.4499*cos(th3) + 0.3545) - 0.0768*sin(th123)*(0.4499*cos(th3) + 0.3545) + 10.5330;
JB2[5][2] = 9.9798*cos(th12) - 0.2968*cos(th2) + 0.0345*sin(th12) + 9.9554*cos(th1) - 0.2366*cos(th2) + 0.03445*sin(th1) + 10.4821;

d = MatrixDeterminantCC_SM(JB1, 6);
if (d == 0)
printf("\nThe matrix can not be inverted\n");
else
MatrixInverseCC_SM(JB1, 6, invJB1);

MatrixMultiplicationCC_SM(invJB1, JB2, dynJaCComplete, 6, 6, 3);

//for(i=0; i<2; i++)
//for(j=0; j<3; j++)
//dynJac[i][j] = dynJaCC_SMomplete[i][j];

//MatrixTransposeCC_SM(dynJac, dynJacT, 2, 3);
//MatrixMultiplicationCC_SM(dynJac, dynJacT, AAT, 2, 3, 2);
//MatrixInverseCC_SM(AAT, 2, invAAT);
//MatrixMultiplicationCC_SM(dynJacT, invAAT, pinvDynJac, 3, 2, 2);
memcpy(&dynJacPt, &dynJaCComplete, sizeof dynJacPt);
}
