#include "structDef.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define RAD_TO_DEG 180/M_PI
#define DEG_TO_RAD M_PI/180

KalmanFilt_9x9 TargetKF;
KalmanFilt_9x9 QuasiKF;
KalmanFilt_9x9 SatPosKF;
KalmanFilt_9x9 SatAttKF;
KalmanFilt_9x9 EndEffKF;
timeSynchronisation tSync;

void Inti_tSync()
{
	tSync.t_IMU = 0;
	tSync.t_VS = 0;
	tSync.t_ENC = 0;
}

void Init_SATATT_KF_struct()
{

  SatAttKF.previousTime = 0;
  SatAttKF.param[0] = 0.1; //da
  SatAttKF.param[1] = 0.001; //db
  SatAttKF.param[2] = 0.00001; //dvs
  SatAttKF.param[3] = 0; //
  SatAttKF.param[4] = 0.9; // Q adaptation RESTRICTED
  SatAttKF.param[5] = 0.9; // R adaptation RESTRICTED
  SatAttKF.isInput = 1;
  SatAttKF.isQAdapted = 0;
  SatAttKF.isRAdapted = 0;
  SatAttKF.isExtended = 0;
  SatAttKF.states = 2;
  SatAttKF.inputs = 1;
  SatAttKF.measurements = 1;

  float dvs_2 = SatAttKF.param[2]*SatAttKF.param[2];
  float dt = 0.1;
  float da_2 =0; //da*da
  float dt_2 = dt*dt*da_2; //dt*dt*da*da
  float dt_3 = dt_2*dt; //dt*dt*dt*da*da
  float dt_4 = dt_3*dt; //dt*dt*dt*dt*da*da

  float p[][9] = {{1,0,0,0,0,0},{0,1,0,0,0,0},{0,0,1,0,0,0},{0,0,0,1,0,0},{0,0,0,0,1,0},{0,0,0,0,0,1}};
  float x_[9] = {0,0,0,0,0,0};
  float q[][9] = {{0.25*dt_4,0,0,0.333333*dt_3,0,0},{0,0.25*dt_4,0,0,0.333333*dt_3,0},{0,0,0.25*dt_4,0,0,0.333333*dt_3},{0.333333*dt_3,0,0,dt_2,0,0},{0,0.333333*dt_3,0,0,dt_2,0},{0,0,0.333333*dt_3,0,0,dt_2}};
  float r[][9] = {{dvs_2,0,0,0,0,0},{0,dvs_2,0,0,0,0},{0,0,dvs_2,0,0,0}};
  float h[][9] = {{1,0,0,0,0,0},{0,0,0,1,0,0}};
  float b[][9] = {{dt,0,0,0,0,0},{0,1,0,0,0,0},{0,0,1,0,0,0},{0,0,0,1,0,0},{0,0,0,0,1,0},{0,0,0,0,0,1}};
  float a[][9] = {{1,-dt,0,dt,0,0},{0,1,0,0,dt,0},{0,0,1,0,0,dt},{0,0,0,1,0,0},{0,0,0,1,0},{0,0,0,0,0,1}};

  memcpy(&SatAttKF.x, &x_, sizeof SatAttKF.x);
  memcpy(&SatAttKF.P, &p, sizeof SatAttKF.P);
  memcpy(&SatAttKF.Q, &q, sizeof SatAttKF.Q);
  memcpy(&SatAttKF.R, &r, sizeof SatAttKF.R);
  memcpy(&SatAttKF.H, &h, sizeof SatAttKF.H);
  memcpy(&SatAttKF.A, &a, sizeof SatAttKF.A);
  memcpy(&SatAttKF.B, &b, sizeof SatAttKF.B);
}

void Init_SATPOS_KF_struct()
{

  SatPosKF.previousTime = 0;
  SatPosKF.param[0] = 0.5; //daxy  SatPosKF.param[0] = 0.5; //daxy			delikatne zmiany
  SatPosKF.param[1] = 0.1; //dbxy
  SatPosKF.param[2] = 0.0000001; //dvs
  SatPosKF.param[1] = 0.1; //dbxy
  SatPosKF.param[2] = 0.0000001; //dvs
  SatPosKF.param[3] = 0; //
  SatPosKF.param[4] = 0.9; // Q adaptation RESTRICTED
  SatPosKF.param[5] = 0.9; // R adaptation RESTRICTED
  SatPosKF.isInput = 1;
  SatPosKF.isQAdapted = 0;
  SatPosKF.isRAdapted = 0;
  SatPosKF.isExtended = 0;
  SatPosKF.states = 6;
  SatPosKF.inputs = 2;
  SatPosKF.measurements = 2;

  float dvs_2 = SatPosKF.param[2]*SatPosKF.param[2];
  float dt = 0.1;
  float da_2 =0; //da*da
  float dt_2 = dt*dt*da_2; //dt*dt*da*da
  float dt_3 = dt_2*dt; //dt*dt*dt*da*da
  float dt_4 = dt_3*dt; //dt*dt*dt*dt*da*da

  float p[][9] = {{1,0,0,0,0,0},{0,1,0,0,0,0},{0,0,1,0,0,0},{0,0,0,1,0,0},{0,0,0,0,1,0},{0,0,0,0,0,1}};
  float x_[9] = {0,0,0,0,0,0};
  float q[][9] = {{0.25*dt_4,0,0,0.333333*dt_3,0,0},{0,0.25*dt_4,0,0,0.333333*dt_3,0},{0,0,0.25*dt_4,0,0,0.333333*dt_3},{0.333333*dt_3,0,0,dt_2,0,0},{0,0.333333*dt_3,0,0,dt_2,0},{0,0,0.333333*dt_3,0,0,dt_2}};
  float r[][9] = {{dvs_2,0,0,0,0,0},{0,dvs_2,0,0,0,0},{0,0,dvs_2,0,0,0}};
  float h[][9] = {{1,0,0,0,0,0},{0,0,0,1,0,0}};
  float b[][9] = {{1,0,0,0,0,0},{0,1,0,0,0,0},{0,0,1,0,0,0},{0,0,0,1,0,0},{0,0,0,0,1,0},{0,0,0,0,0,1}};
  float a[][9] = {{1,0,0,dt,0,0},{0,1,0,0,dt,0},{0,0,1,0,0,dt},{0,0,0,1,0,0},{0,0,0,1,0},{0,0,0,0,0,1}};

  memcpy(&SatPosKF.P, &p, sizeof SatPosKF.P);
  memcpy(&SatPosKF.x, &x_, sizeof SatPosKF.x);
  memcpy(&SatPosKF.Q, &q, sizeof SatPosKF.Q);
  memcpy(&SatPosKF.R, &r, sizeof SatPosKF.R);
  memcpy(&SatPosKF.A, &a, sizeof SatPosKF.A);
  memcpy(&SatPosKF.H, &h, sizeof SatPosKF.H);
  memcpy(&SatPosKF.B, &b, sizeof SatPosKF.B);
}

void Init_TARGET_KF_struct()
{
  TargetKF.P;
  TargetKF.x;
  TargetKF.previousTime = 0;
  TargetKF.param[0] = 0.001; //da
  TargetKF.param[1] = 0.1; //dvs
  TargetKF.param[2] = 0; //
  TargetKF.param[3] = 0; //
  TargetKF.param[4] = 0.9; // Q adaptation RESTRICTED
  TargetKF.param[5] = 0.9; // R adaptation RESTRICTED
  TargetKF.isInput = 0;
  TargetKF.isQAdapted = 0;
  TargetKF.isRAdapted = 0;
  TargetKF.isExtended = 0;
  TargetKF.states = 6;
  TargetKF.inputs = 0;
  TargetKF.measurements = 3;

  float dvs_2 = TargetKF.param[1]*TargetKF.param[1];
  float dt = 0.1;
  float da_2 = TargetKF.param[0]*TargetKF.param[0]; //da*da
  float dt_2 = dt*dt*da_2; //dt*dt*da*da
  float dt_3 = dt_2*dt; //dt*dt*dt*da*da
  float dt_4 = dt_3*dt; //dt*dt*dt*dt*da*da

  float p[][9] = {{1,0,0,0,0,0},{0,1,0,0,0,0},{0,0,1,0,0,0},{0,0,0,1,0,0},{0,0,0,0,1,0},{0,0,0,0,0,1}};
  float x_[9] = {0,0,0,1,1,1};
  float q[][9] = {{0.25*dt_4,0,0,0.333333*dt_3,0,0},{0,0.25*dt_4,0,0,0.333333*dt_3,0},{0,0,0.25*dt_4,0,0,0.333333*dt_3},{0.333333*dt_3,0,0,dt_2,0,0},{0,0.333333*dt_3,0,0,dt_2,0},{0,0,0.333333*dt_3,0,0,dt_2}};
  float r[][9] = {{dvs_2,0,0,0,0,0},{0,dvs_2,0,0,0,0},{0,0,dvs_2,0,0,0}};
  float h[][9] = {{1,0,0,0,0,0},{0,1,0,0,0,0},{0,0,1,0,0,0}};
  float b[][9] = {{1,0,0,0,0,0},{0,1,0,0,0,0},{0,0,1,0,0,0},{0,0,0,1,0,0},{0,0,0,0,1,0},{0,0,0,0,0,1}};
  float a[][9] = {{1,0,0,dt,0,0},{0,1,0,0,dt,0},{0,0,1,0,0,dt},{0,0,0,1,0,0},{0,0,0,1,0},{0,0,0,0,0,1}};

  memcpy(&TargetKF.P, &p, sizeof TargetKF.P);
  memcpy(&TargetKF.x, &x_, sizeof TargetKF.x);
  memcpy(&TargetKF.Q, &q, sizeof TargetKF.Q);
  memcpy(&TargetKF.R, &r, sizeof TargetKF.R);
  memcpy(&TargetKF.A, &a, sizeof TargetKF.A);
  memcpy(&TargetKF.H, &h, sizeof TargetKF.H);
  memcpy(&TargetKF.B, &b, sizeof TargetKF.B);
}

void Init_ENDEFF_KF_struct()
{
  EndEffKF.P;
  EndEffKF.x;
  EndEffKF.previousTime = 0;
  EndEffKF.param[0] = 0.01; //da_enc
  EndEffKF.param[1] = 0.0001; //d_enc
  EndEffKF.param[2] = 0.001; //d_xy
  EndEffKF.param[3] = 0.001; //d_alpha
  EndEffKF.param[4] = 0.9; // Q adaptation RESTRICTED
  EndEffKF.param[5] = 0.9; // R adaptation RESTRICTED
  EndEffKF.param[6] = 0.01; //d_xy_vs
  EndEffKF.param[7] = 0.01; //d_alpha_vs
  EndEffKF.isInput = 0;
  EndEffKF.isQAdapted = 0;
  EndEffKF.isRAdapted = 0;
  EndEffKF.isExtended = 1;
  EndEffKF.states = 9;
  EndEffKF.inputs = 0;
  EndEffKF.measurements = 3;

  float d_enc = EndEffKF.param[0];

  float h[9][9] = {{1,0,0,0,0,0,0,0,0},{0,1,0,0,0,0,0,0,0},{0,0,1,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0}};
  float r[9][9] = {{d_enc*d_enc,0,0,0,0,0,0,0,0},{0,d_enc*d_enc,0,0,0,0,0,0,0},{0,0,d_enc*d_enc,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0}};
  memcpy(&EndEffKF.H, &h, sizeof EndEffKF.H);
  memcpy(&EndEffKF.R, &r, sizeof EndEffKF.R);

  float p[][9] = {{1,0,0,0,0,0,0,0,0},{0,1,0,0,0,0,0,0,0},{0,0,1,0,0,0,0,0,0},{0,0,0,1,0,0,0,0,0},{0,0,0,0,1,0,0,0,0},{0,0,0,0,0,1,0,0,0},{0,0,0,0,0,0,1,0,0},{0,0,0,0,0,0,0,1,0},{0,0,0,0,0,0,0,0,1}};
  float x_[9] = {0,0,0,0,0,0,0,0,0}; //init with VS measurements


  memcpy(&EndEffKF.P, &p, sizeof EndEffKF.P);
  memcpy(&EndEffKF.x, &x_, sizeof EndEffKF.x);
}
