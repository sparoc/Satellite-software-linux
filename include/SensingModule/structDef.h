#ifndef STRUCTDEF_H_
#define STRUCTDEF_H_

#include <stdbool.h>




extern void Inti_tSync(void);
extern void Init_TARGET_KF_struct(void);
extern void Init_SATATT_KF_struct(void);
extern void Init_SATPOS_KF_struct(void);
extern void Init_ENDEFF_KF_struct(void);

typedef struct
{
  float P[9][9];
  float x[9];
  float previousTime;
  volatile float param[9]; //kalman filter parameters, base elements of Q and R, param[4] and param[5] restricted for Q and R adaptation
  float A[9][9];
  float B[9][9];
  float Q[9][9];
  float H[9][9];
  float R[9][9];
  bool isInput;
  bool isQAdapted;
  bool isRAdapted;
  bool isExtended;
  int states;
  int inputs;
  int measurements;
}KalmanFilt_9x9;

typedef struct{
	unsigned int t_IMU;
	unsigned int t_VS;
	unsigned int t_ENC;
}timeSynchronisation;

extern timeSynchronisation tSync;
extern KalmanFilt_9x9 TargetKF;
extern KalmanFilt_9x9 SatPosKF;
extern KalmanFilt_9x9 SatAttKF;
extern KalmanFilt_9x9 EndEffKF;

#endif
