#include "matrixOp.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

void display(float matrix[9][9], int d)
{
  for(int i=0; i<d; i++)
  {
    for(int j=0; j<d; j++)
      printf("%f\t",matrix[i][j]);

    printf("\n"); // new line
  }
}
void display_(float (*matrix)[9], int m,int n)
{
  for(int i=0; i<m; i++)
  {
    for(int j=0; j<n; j++)
      {printf("%f\t",matrix[i][j]);}

    printf("\n"); // new line
  }
}

void MatrixScalarMultiplicationCC_SM(float (*matrix1)[9], float scalar, float (*result)[9],int m,int n){
  int i,k;

  for (i = 0; i < m; i++)
  {
      for (k = 0; k < n; k++)
      {
        result[i][k] = scalar*matrix1[i][k];
      }
   }
}


void MatrixVectorMultiplicationCC_SM(float (*matrix1)[9], float (*matrix2), float *result,int m,int n)
{
	int i,k;
	float sum = 0;
    for (i = 0; i < m; i++)
    {
        for (k = 0; k < n; k++)
        {

          // printf("i = %d, k = %d\n", i, k);
          // printf("matrix1[i][k] = %f, matrix2[k] = %f\n", matrix1[i][k], matrix2[k]);
          sum = sum + matrix1[i][k] * matrix2[k];
        }

        result[i] = sum;
        // printf("sum = %f\n", result[i]);
        sum = 0;
     }
}

float MatrixDeterminantCC_SM(float (*matrix)[9], float k)
{
	float s = 1;
	float det = 0;
	float b[9][9];
	int i, j, m, n, c;

	if (k == 1)
	{
		return (matrix[0][0]);
	}
	else
	{
		det = 0;
		for (c = 0; c < k; c++)
		{
			m = 0;
			n = 0;

			for (i = 0;i < k; i++)
			{
				for (j = 0 ;j < k; j++)
				{
					b[i][j] = 0;

					if (i != 0 && j != c)
					{
						b[m][n] = matrix[i][j];

						if (n < (k - 2))
						{
							n++;
						}
						else
						{
							n = 0;
							m++;
						}
					}
				}
			}

			det = det + s * (matrix[0][c] * MatrixDeterminantCC_SM(b, k - 1));
			s = -1 * s;
		}
	}

	return (det);
}

void MatrixInverseCC_SM(float (*num)[9], float f , float (*inverse)[9])
{
	float b[9][9], X[9][9];
	int p, q, m, n, i, j;
  if(f==1){
    inverse[0][0]=1/num[0][0];
  }
  else{
  	for (q = 0;q < f; q++)
  	{
  		for (p = 0;p < f; p++)
  		{
  			m = 0;
  			n = 0;

  			for (i = 0;i < f; i++)
  			{
  				for (j = 0;j < f; j++)
  				{
  					if (i != q && j != p)
  					{
  						b[m][n] = num[i][j];

  						if (n < (f - 2))
  						{
  							n++;
  						}
  						else
  						{
  							n = 0;
  							m++;
  						}
  					}
  				}
  			}

  			X[q][p] = pow(-1, q + p) * MatrixDeterminantCC_SM(b, f - 1);
  		}
  	}

  	float d = MatrixDeterminantCC_SM(num, f);

  	for (i = 0;i < f; i++)
  	{
  		for (j = 0;j < f; j++)
  		{
  			inverse[i][j] = X[j][i] / d;
  		}
  	}
  }
}

void MatrixMultiplicationCC_SM(float (*matrix1)[9], float (*matrix2)[9], float (*result)[9],int m,int n,int l)
{

	int i,j,k;
  for (i = 0; i < m; i++)
    {for (j = 0; j < l; j++)
      result[i][j] =0;}

	for (i = 0; i < m; i++)
	{
		for (j = 0; j < l; j++)
		{
			for (k = 0; k < n; k++)
			{
				result[i][j]+= matrix1[i][k] * matrix2[k][j];
        //printf("\n%d, %d =%f\t%d, %d =%f\n", i, k, matrix1[i][k],k,j,matrix2[k][j]);
			}
      //printf("\nresult %d, %d  =  %f\n",i,j,result[i][j]);
		}
	}
}

void MatrixTransposeCC_SM(float (*matrix)[9], float (*result)[9], int m, int n)
{
	int i,j;

	for(i = 0; i < n; i++)
	{
		for(j=0;j<m;j++)
		{
			result[i][j] = matrix[j][i];
		}
	}
}

void MatrixSumCC_SM(float (*matrix1)[9], float (*matrix2)[9], float (*result)[9],int m,int n, int sign)
{
	int i,j;

	for (i = 0; i < m; i++)
	{
		for (j = 0; j < n; j++)
		{
	     result[i][j] = matrix1[i][j] + sign*matrix2[i][j];
		}
	}
}


void VectorSumCC_SM(float (*matrix1), float (*matrix2), float (*result),int m, int sign)
{
	int i;

	for (i = 0; i < m; i++)
	{
    result[i] = matrix1[i] + sign*matrix2[i];
	}
}

void Vector2Matrix(float *vector, float (*matrix)[9], int n){
  int i, j;
  for (i = 0; i < 9; i++)
  {
    for (j = 0; j < 9; j++)
    {
       matrix[i][j] = 0;
     }
  }
  for (i=0; i<n; i++){
    matrix[i][0] = vector[i];
  }
}
