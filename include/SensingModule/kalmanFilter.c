#include "kalmanFilter.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "../Logger/FileLogger.h"
void predict(KalmanFilt_9x9 *filterPt, float dt, float *u)
{
  float x_[9], x_new[9], x_plus[9];
  float A_1[9][9];
  float A_T[9][9];
  float A_2[9][9];

  if(!filterPt->isExtended)  //A*x
  {
    MatrixVectorMultiplicationCC_SM(filterPt->A, filterPt->x, x_, filterPt->states, filterPt->states);
    if(filterPt->isInput)
    {
      MatrixVectorMultiplicationCC_SM(filterPt->B, u, x_plus, filterPt->states, filterPt->inputs);
      VectorSumCC_SM(x_, x_plus, x_new, filterPt->states, 1);
      memcpy(&filterPt->x, &x_new, sizeof filterPt->x);
    }
    else{
      memcpy(&filterPt->x, &x_, sizeof filterPt->x);
    }
  }
  MatrixMultiplicationCC_SM(filterPt->A, filterPt->P, A_1, filterPt->states, filterPt->states, filterPt->states);  // A*P
  MatrixTransposeCC_SM(filterPt->A, A_T, filterPt->states, filterPt->states);
  MatrixMultiplicationCC_SM(A_1, A_T, A_2, filterPt->states, filterPt->states, filterPt->states);  // A*P*A'
  MatrixSumCC_SM(A_2, filterPt->Q, filterPt->P, filterPt->states, filterPt->states, 1);  // A*P*A'+Q
}

void update(KalmanFilt_9x9 *filterPt, float *z)
{
  float H[9][9], P[9][9], x[9];
  memcpy(&x, &filterPt->x, sizeof filterPt->x);
  memcpy(&P, &filterPt->P, sizeof filterPt->P);
  memcpy(&H, &filterPt->H, sizeof filterPt->H);
  float HP[9][9];
  float HT[9][9];
  float HPHT[9][9];
  float HPHT_R[9][9];
  float invHPHT_R[9][9];
  float Hx[9];
  int i=0;
  int j=0;
  float z_Hx[9];
  float PHT[9][9];
  float K[9][9];
  float Kz_Hx[9];
  float x_[9];
  float KHP[9][9];
  float d;
// LogInfo("Measurement = [%f,%f,%f]", z[0], z[1], z[2]);

  MatrixTransposeCC_SM(H, HT, filterPt->measurements, filterPt->states);  // H*P
  MatrixMultiplicationCC_SM(H, P, HP, filterPt->measurements, filterPt->states, filterPt->states);
  MatrixMultiplicationCC_SM(HP, HT, HPHT, filterPt->measurements, filterPt->states, filterPt->measurements);  // H*P*H'
  MatrixSumCC_SM(HPHT, filterPt->R, HPHT_R, filterPt->measurements, filterPt->measurements, 1);
  d = MatrixDeterminantCC_SM(HPHT_R, filterPt->measurements);  //inv(HPH'+R)
  if (d == 0){
    printf("\nThe matrix can not be inverted, determinant = %f, %d\n", d, filterPt->measurements);
  }
  else{
    MatrixInverseCC_SM(HPHT_R, filterPt->measurements, invHPHT_R);
    MatrixMultiplicationCC_SM(filterPt->P, HT, PHT, filterPt->states, filterPt->states, filterPt->measurements);    //Hx
    MatrixMultiplicationCC_SM(PHT, invHPHT_R, K, filterPt->states, filterPt->measurements, filterPt->measurements);
    MatrixVectorMultiplicationCC_SM(H, x, Hx, filterPt->measurements, filterPt->states);
    VectorSumCC_SM(z, Hx, z_Hx, filterPt->measurements, -1);    //z-Hx

//     LogInfo("z - H*x = [%f,%f,%f]", z_Hx[0], z_Hx[1], z_Hx[2]);
    MatrixVectorMultiplicationCC_SM(K, z_Hx, Kz_Hx, filterPt->states, filterPt->measurements);    //K(z-Hx)
    VectorSumCC_SM(filterPt->x, Kz_Hx, x_, filterPt->states, 1);    //x +=K(z-Hx)
//     LogInfo("K*(z - H*x) = [%f,%f,%f]", Kz_Hx[0], Kz_Hx[1], Kz_Hx[2]);
    MatrixMultiplicationCC_SM(K, HP, KHP, filterPt->states, filterPt->measurements, filterPt->states);    //KHP
    MatrixSumCC_SM(filterPt->P, KHP, P, filterPt->states, filterPt->states, -1); //P=-KHP
    if (filterPt->isQAdapted){
        float a_d = filterPt->param[4];
        float innovation_2[9][9];//z_Hx*z_Hx_T
        float z_Hx_m[9][9];
        float z_Hx_m_T[9][9];
        float Kz_Hxz_Hx_T[9][9];
        float K_T[9][9];
        float Kz_Hxz_Hx_TK_T[9][9];
        float sum_1[9][9]; //a_d*Q
        float sum_2[9][9]; //(1-q_d)*Q
        float Q_new[9][9];

        Vector2Matrix(z_Hx, z_Hx_m, filterPt->measurements);
        MatrixTransposeCC_SM(K, K_T, filterPt->states, filterPt->measurements);
        MatrixTransposeCC_SM(z_Hx_m, z_Hx_m_T, filterPt->measurements, 1);
        MatrixMultiplicationCC_SM(z_Hx_m, z_Hx_m_T, innovation_2, filterPt->measurements, 1, filterPt->measurements);
        MatrixMultiplicationCC_SM(K, innovation_2, Kz_Hxz_Hx_T, filterPt->states, filterPt->measurements, filterPt->measurements);
        MatrixMultiplicationCC_SM(Kz_Hxz_Hx_T, K_T, Kz_Hxz_Hx_TK_T, filterPt->states, filterPt->measurements, filterPt->states);
        MatrixScalarMultiplicationCC_SM(filterPt->Q, a_d, sum_1, filterPt->states, filterPt->states);
        MatrixScalarMultiplicationCC_SM(Kz_Hxz_Hx_TK_T, (1-a_d), sum_2, filterPt->states, filterPt->states);
        MatrixSumCC_SM(sum_1, sum_2, Q_new, filterPt->states, filterPt->states, 1);
        if(Q_new[0]!=Q_new[0]) // THIS IF CHECKS IF Q WAS DEGRAGATED TO NAN!
        {
            memcpy(&filterPt->Q, &Q_new, sizeof filterPt->Q);
        }
      }
    if (filterPt->isRAdapted){
        float residual_2[9][9];
        float z_Hx__T[9][9];
        float z_Hx_[9][9];
        float Hx_[9][9];
        float sum_1[9][9];
        float sum_2[9][9];
        float res_plus_hpht[9][9];
        float a_z = filterPt->param[5];
        float R_new[9][9];
        float x_m[9][9];
        float z_m[9][9];


        Vector2Matrix(x_, x_m, filterPt->states);
        Vector2Matrix(z, z_m, filterPt->measurements);
        MatrixMultiplicationCC_SM(filterPt->H, x_m, Hx_, filterPt->measurements, filterPt->states, 1);
        MatrixSumCC_SM(z_m, Hx_, z_Hx_, filterPt->measurements, 1, -1);
        MatrixTransposeCC_SM(z_Hx_, z_Hx__T, filterPt->measurements, 1);
        MatrixMultiplicationCC_SM(z_Hx_, z_Hx__T, residual_2, filterPt->measurements, 1, filterPt->measurements);
        MatrixSumCC_SM(residual_2, HPHT, res_plus_hpht, filterPt->measurements, filterPt->measurements, 1);
        MatrixScalarMultiplicationCC_SM(filterPt->R, a_z, sum_1, filterPt->measurements, filterPt->measurements);
        MatrixScalarMultiplicationCC_SM(res_plus_hpht, (1-a_z), sum_2, filterPt->measurements, filterPt->measurements);
        MatrixSumCC_SM(sum_1, sum_2, R_new, filterPt->measurements, filterPt->measurements, 1);
        if(R_new[0]!=R_new[0]) // THIS IF CHECKS IF R WAS DEGRAGATED TO NAN!
        {
        	memcpy(&filterPt->R, &R_new, sizeof filterPt->R);
        }
      }

//     LogInfo("state[0] before update = %f", EndEffKF.x[0]);
    if(x_[0]!=x_[0]||x_[1]!=x_[1]||x_[2]!=x_[2])
    {
    	puts("state nan update");

    	LogInfo("State NAN update");

    	LogInfo("Measurement = [%f,%f,%f]", z[0], z[1], z[2]);

    	LogInfo("z - H*x = [%f,%f,%f]", z_Hx[0], z_Hx[1], z_Hx[2]);

    	LogInfo("K*(z - H*x) = [%f,%f,%f]", Kz_Hx[0], Kz_Hx[1], Kz_Hx[2]);
    }
    	// THIS IF CHECKS IF STATE WAS DEGRAGATED TO NAN! or if the angles are higher then 1.5*180deg or lwoer then 1.5*-180 deg
    {
    	 memcpy(&filterPt->P, &P, sizeof filterPt->P);
    	 memcpy(&filterPt->x, &x_, sizeof filterPt->x);
    }

//     LogInfo("state[0] after update = %f", EndEffKF.x[0]);
  }
}

void targetCalculateMatrices(float dt)
{
  float da_2 = TargetKF.param[0]*TargetKF.param[0]; //da*da
  float dt_2 = dt*dt*da_2; //dt*dt*da*da
  float dt_3 = dt_2*dt; //dt*dt*dt*da*da
  float dt_4 = dt_3*dt; //dt*dt*dt*dt*da*da

  if (!TargetKF.isQAdapted){
    float q[][9] = {{0.25*dt_4,0,0,0.333333*dt_3,0,0},{0,0.25*dt_4,0,0,0.333333*dt_3,0},{0,0,0.25*dt_4,0,0,0.333333*dt_3},{0.333333*dt_3,0,0,dt_2,0,0},{0,0.333333*dt_3,0,0,dt_2,0},{0,0,0.333333*dt_3,0,0,dt_2}};
    memcpy(&TargetKF.Q, &q, sizeof TargetKF.Q);
  }

  float a[][9] = {{1,0,0,dt,0,0},{0,1,0,0,dt,0},{0,0,1,0,0,dt},{0,0,0,1,0,0},{0,0,0,0,1,0},{0,0,0,0,0,1}};
  memcpy(&TargetKF.A, &a, sizeof TargetKF.A);
}

void satposCalculateMatrices(float dt)
{
  float cos_att = cos(SatAttKF.x[0]);
  float sin_att = sin(SatAttKF.x[0]);

  float da = SatPosKF.param[0];
  float db = SatPosKF.param[1];
  float dvs = SatPosKF.param[2];
  float da_2 = SatPosKF.param[0]*SatPosKF.param[0];
  float db_2 = SatPosKF.param[1]*SatPosKF.param[1];

  float dt_2 = dt*dt;
  float dt_3 = dt_2*dt;
  float dt_4 = dt_3*dt;

  if (!SatPosKF.isQAdapted){
    float q[][9] = {{(1/3)*da_2*dt_3,1/2*da_2*dt_2,-1/9*db*dt_3*cos_att,0,0,1/9*db*dt_2*cos_att},{1/2*da_2*dt_2,da_2*dt,1/2*db_2*dt_2*cos_att,0,0,-1/2*db_2*dt_2*sin_att},{-1/9*db_2*dt_3*cos_att,-1/2*db_2*dt_2*cos_att,db_2*dt,1/9*db_2*dt_3*sin_att,1/2*db_2*dt_2*sin_att,0},{0,0,1/9*db_2*dt_3*sin_att,1/3*da_2*dt_3,1/2*da_2*dt_2,-1/9*db_2*dt_3*cos_att},{0,0,1/2*db_2*dt_2*sin_att,1/2*da_2*dt_2,da_2*dt,-1/2*db_2*dt_2*cos_att},{-1/9*db_2*dt_3*sin_att,-1/2*db_2*dt_2*sin_att,0,-1/9*db_2*dt_3*cos_att,-1/2*db_2*dt_2*cos_att, db_2*dt}};
    memcpy(&SatPosKF.Q, &q, sizeof SatPosKF.Q);
  }

  float b[][9] = {{0,0,0,0,0,0},{cos_att,sin_att,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{-sin_att,cos_att,0,0,0,0},{0,0,0,0,0,0}};
  memcpy(&SatPosKF.B, &b, sizeof SatPosKF.B);
  float a[][9] = {{1,dt,-(1/2)*dt_2*cos_att,0,0,(-1/2)*dt_2*sin_att},{0,1,-dt*cos_att,0,0,-dt*sin_att},{0,0,1,0,0,0},{0,0,(1/2)*dt_2*sin_att,1,dt,(-1/2)*dt_2*cos_att},{0,0,dt*sin_att,0,1,-dt*cos_att},{0,0,0,0,0,1}};
  memcpy(&SatPosKF.A, &a, sizeof SatPosKF.A);
}

void satattCalculateMatrices(float dt)
{
  float da = SatAttKF.param[0];
  float db = SatAttKF.param[1];
  float dvs = SatAttKF.param[2];
  float da_2 = SatAttKF.param[0]*SatAttKF.param[0];
  float db_2 = SatAttKF.param[1]*SatAttKF.param[1];

  float dt_2 = dt*dt*db_2;
  float dt_3 = dt_2*dt;

  if (!SatAttKF.isQAdapted){
    float q[][9] = {{da_2*dt+1/3*dt_3,-1/2*dt_2,0,0,0,0},{-1/2*dt_2,dt*db_2,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0, 0}};
    memcpy(&SatAttKF.Q, &q, sizeof SatAttKF.Q);
  }

  float b[][9] = {{dt,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0}};
  memcpy(&SatAttKF.B, &b, sizeof SatAttKF.B);

  float a[][9] = {{1,-dt,0,0,0,0},{0,1,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0}};
  memcpy(&SatAttKF.A, &a, sizeof SatAttKF.A);
}

void endeffCalculateMatrices(float dt, bool isEncMeas)
{

  // if isEncMEas: the state is updated based on the measurements from encoders
  // else: the state is updated based on the measurements from vision system
  // the significance of this distinction lies in the form of the H matrix
  // it is transpose([diag([1,1,1]); zeros(6,3)]) for encoders, and
  // transpose([zeros(6,3); diag([1,1,1])]) for vision system.

  // since this filter is extended, the state is predicted here
  // only the P matrix is propagated in the predict() function

  float J[9][9];
  float R_J[9][9];
  float new_x[9] = {1,1,1,0.1,0.1,0.1,0.1,0.1,0.1};

  float thdot1 = EndEffKF.x[3];
  float thdot2 = EndEffKF.x[4];
  float thdot3 = EndEffKF.x[5];
  float angle = EndEffKF.x[8];

  float Rot[][9] = {{cos(angle), -sin(angle), 0,0,0,0}, {sin(angle), cos(angle), 0,0,0,0},{0,0,1,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0}};
  calculateDynJac(J, EndEffKF.x[0], EndEffKF.x[1], EndEffKF.x[2]);
  MatrixMultiplicationCC_SM(Rot, J, R_J, 3,3,3);
  //LogInfo("ROT*Jac = [%f, %f, %f;%f, %f, %f;%f, %f, %f]",R_J[0][0],R_J[0][1],R_J[0][2],R_J[1][0],R_J[1][1],R_J[1][2],R_J[2][0],R_J[2][1],R_J[2][2]);
  new_x[0] = EndEffKF.x[0] + 0.03*EndEffKF.x[3];
  new_x[1] = EndEffKF.x[1] + 0.03*EndEffKF.x[4];
  new_x[2] = EndEffKF.x[2] + 0.03*EndEffKF.x[5];
  new_x[3] = EndEffKF.x[3];
  new_x[4] = EndEffKF.x[4];
  new_x[5] = EndEffKF.x[5];
  new_x[6] = EndEffKF.x[6] + dt*(EndEffKF.x[3]*R_J[0][0] + EndEffKF.x[4]*R_J[0][1] + EndEffKF.x[5]*R_J[0][2]);
  new_x[7] = EndEffKF.x[7] + dt*(EndEffKF.x[3]*R_J[1][0] + EndEffKF.x[4]*R_J[1][1] + EndEffKF.x[5]*R_J[1][2]);
  new_x[8] = EndEffKF.x[8] + dt*(EndEffKF.x[3]*R_J[2][0] + EndEffKF.x[4]*R_J[2][1] + EndEffKF.x[5]*R_J[2][2]);
  //LogInfo("DTH, %f, %f, %f", dt*(EndEffKF.x[3]*R_J[0][0] + EndEffKF.x[4]*R_J[0][1] + EndEffKF.x[5]*R_J[0][2]),
	//	  dt*(EndEffKF.x[3]*R_J[1][0] + EndEffKF.x[4]*R_J[1][1] + EndEffKF.x[5]*R_J[1][2]),
	//	  dt*(EndEffKF.x[3]*R_J[2][0] + EndEffKF.x[4]*R_J[2][1] + EndEffKF.x[5]*R_J[2][2]));
  float j11 = J[0][0];
  float j12 = J[0][1];
  float j13 = J[0][2];
  float j21 = J[1][0];
  float j22 = J[1][1];
  float j23 = J[1][2];
  float j31 = J[2][0];
  float j32 = J[2][1];
  float j33 = J[2][2];

  float a[][9] = {{1,0,0,dt,0,0,0,0,0},{0,1,0,0,dt,0,0,0,0},{0,0,1,0,0,dt,0,0,0},{0,0,0,1,0,0,0,0,0},{0,0,0,1,0,0,0,0},{0,0,0,0,0,1,0,0,0},
  {0,0,0,dt*(-sin(angle)*j11 - cos(angle)*j21),dt*(-sin(angle)*j12 - cos(angle)*j22),dt*(-sin(angle)*j13 - cos(angle)*j23),1,0,(-sin(angle)*(j11*thdot1+j12*thdot2+j13*thdot3)-cos(angle)*(j21*thdot1+j22*thdot2+j23*thdot3))*dt},
  {0,0,0,dt*(cos(angle)*j11 - sin(angle)*j21),dt*(cos(angle)*j12 - sin(angle)*j22),dt*(cos(angle)*j13 - sin(angle)*j23),0,1,(cos(angle)*(j11*thdot1+j12*thdot2+j13*thdot3)-sin(angle)*(j21*thdot1+j22*thdot2+j23*thdot3))*dt},
  {0,0,0,dt*j31,dt*j32,dt*j33,0,0,1}};
  memcpy(&EndEffKF.A, &a, sizeof EndEffKF.A);

  float da_enc = EndEffKF.param[0];
  float d_enc = EndEffKF.param[1];
  float d_xy = EndEffKF.param[2];
  float d_alpha = EndEffKF.param[3];
  float d_xy_vs = EndEffKF.param[6];
  float d_alpha_vs = EndEffKF.param[7];
  float d_a_2 = da_enc*da_enc;
  float dt_2 = dt*dt*d_a_2;
  float dt_3 = dt_2*dt;
  float dt_4 = dt_3*dt;


  float q[][9] = {{0.25*dt_4,0,0,0.333333*dt_3,0,0,0,0,0},{0,0.25*dt_4,0,0,0.333333*dt_3,0,0,0,0},{0,0,0.25*dt_4,0,0,0.333333*dt_3,0,0,0},{0.333333*dt_3,0,0,dt_2,0,0,0,0,0},{0,0.333333*dt_3,0,0,dt_2,0,0,0,0},{0,0,0.333333*dt_3,0,0,dt_2,0,0,0},{0,0,0,0,0,0,d_xy,0,0},{0,0,0,0,0,0,0,d_xy,0},{0,0,0,0,0,0,0,0,d_alpha}};
  memcpy(&EndEffKF.Q, &q, sizeof EndEffKF.Q);

  float h_vs[9][9] = {{0,0,0,0,0,0,1,0,0},{0,0,0,0,0,0,0,1,0},{0,0,0,0,0,0,0,0,1},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0}};
  float r_vs[9][9] = {{d_xy_vs*d_xy_vs,0,0,0,0,0,0,0,0},{0,d_xy_vs*d_xy_vs,0,0,0,0,0,0,0},{0,0,d_alpha_vs*d_alpha_vs,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0}};
  float h_enc[9][9] = {{1,0,0,0,0,0,0,0,0},{0,1,0,0,0,0,0,0,0},{0,0,1,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0}};
  float r_enc[9][9] = {{d_enc*d_enc,0,0,0,0,0,0,0,0},{0,d_enc*d_enc,0,0,0,0,0,0,0},{0,0,d_enc*d_enc,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0},{0,0,0,0,0,0,0,0,0}};


  if (isEncMeas){
    memcpy(&EndEffKF.H, &h_enc, sizeof EndEffKF.H);
    memcpy(&EndEffKF.R, &r_enc, sizeof EndEffKF.R);
  }
  else{

    memcpy(&EndEffKF.H, &h_vs, sizeof EndEffKF.H);
    memcpy(&EndEffKF.R, &r_vs, sizeof EndEffKF.R);
  }
//   LogInfo("state[0] before predict, dt = %f, %f", EndEffKF.x[0], dt);


  //     	// THIS IF CHECKS IF STATE WAS DEGRAGATED TO NAN! or if the angles are higher then 1.5*180deg or lwoer then 1.5*-180 deg
  if(new_x[0]!=new_x[0]||new_x[1]!=new_x[1]||new_x[2]!=new_x[2])
  {
	  puts("State nan");
	   LogInfo("State NAN");
	   LogInfo("Angle = %f", angle);
	   LogInfo("Thetas = [%f,%f,%f]", EndEffKF.x[0], EndEffKF.x[1], EndEffKF.x[2]);
	   LogInfo("dThetas = [%f,%f,%f]", EndEffKF.x[3], EndEffKF.x[4], EndEffKF.x[5]);
	   LogInfo("Jac = [%f, %f, %f;%f, %f, %f;%f, %f, %f]",J[0][0],J[0][1],J[0][2],J[1][0],J[1][1],J[1][2],J[2][0],J[2][1],J[2][2]);
	   LogInfo("ROT*Jac = [%f, %f, %f;%f, %f, %f;%f, %f, %f]",R_J[0][0],R_J[0][1],R_J[0][2],R_J[1][0],R_J[1][1],R_J[1][2],R_J[2][0],R_J[2][1],R_J[2][2]);
	   float new_x_zeros[9] = {0,0,0,0,0,0,0,0,0};
	   memcpy(&EndEffKF.x, &new_x_zeros, sizeof EndEffKF.x);
  }
  else
  {
  	  memcpy(&EndEffKF.x, &new_x, sizeof EndEffKF.x);
  }
//   LogInfo("state[0] after predict = %f", EndEffKF.x[0]);
}

void test_kf(void){
	puts("include/SensingModule/kalmanFilter.c->test_kf");
}
