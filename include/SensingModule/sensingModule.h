#ifndef INCLUDE_SENSINGMODULE_SENSINGMODULE_H_
#define INCLUDE_SENSINGMODULE_SENSINGMODULE_H_


#include <math.h>
#include "structDef.h"
#include "matrixOp.h"
#include "kalmanFilter.h"
#include "include/Protocols/VisionSystemProtocol.h"
#include "include/Protocols/ImuProtocol.h"
#include "include/DataDefinition/DataStruct.h"

extern void initSensingModule(void);

extern void fuseIMU(void);
extern void fuseVS(void);
extern void fuseENC(void);

extern void getChaserPosition(float *position);
extern void getChaserAttitude(float *attitude);
extern void getChaserVelocity(float *velocity);


extern void getEndEffPosition(float *position);
extern void getEndEffAttitude(float *attitude);

extern void getTargetPosition(float *position);
extern void getTargetAttitude(float *attitude);
extern void getTargetVelocity(float *velocity);

extern void getJointPos(float *joints);
extern void getJointVel(float *djoints);

#endif /* INCLUDE_SENSINGMODULE_SENSINGMODULE_H_ */
