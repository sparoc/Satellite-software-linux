#ifndef DYNJACCALCULATE_H_
#define DYNJACCALCULATE_H_

#include "matrixOp.h"

void calculateDynJac(float (*dynJacPt)[9], float th1, float th2, float th3);
#endif
