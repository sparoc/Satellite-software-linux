#ifndef KALMANFILTER_H_
#define KALMANFILTER_H_

#include "structDef.h"
#include "matrixOp.h"
#include "dynJacCalculate.h"

void predict(KalmanFilt_9x9 *filterPt, float dt, float *u);
void update(KalmanFilt_9x9 *filterPt, float *z);

void satattCalculateMatrices(float dt);
void endeffCalculateMatrices(float dt, bool isEncMeas);
void satposCalculateMatrices(float dt);
void targetCalculateMatrices(float dt);

void test_kf(void);
#endif
