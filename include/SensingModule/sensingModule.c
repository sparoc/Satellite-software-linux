#include <stdio.h>
#include <stdlib.h>
#include "../Protocols/ImuProtocol.h"
#include "../Protocols/GCSProtocol.h"

#include "sensingModule.h"
//#include <semaphore.h>
#include <pthread.h>



#define RAD_TO_DEG 180/M_PI
#define DEG_TO_RAD M_PI/180

#define TRUE		1
#define FALSE		0

//static sem_t sem_EndEff;
//static sem_t sem_SatPos;
//static sem_t sem_SatAtt;

static pthread_mutex_t mutex_EndEff;
static pthread_mutex_t mutex_SatPos;
static pthread_mutex_t mutex_SatAtt;

int a;

void initSensingModule(void){
  Init_TARGET_KF_struct();
  Init_SATATT_KF_struct();
  Init_SATPOS_KF_struct();
  Init_ENDEFF_KF_struct();
//  sem_init(&sem_EndEff, 0, 0);
//  sem_init(&sem_SatAtt, 0, 1);
//  sem_init(&sem_SatPos, 0, 1);

  if (pthread_mutex_init(&mutex_EndEff, NULL) != 0)
  {
	  puts("mutex_EndEff init failed");
	  LogInfo("mutex_EndEff init failed");
  }
  if (pthread_mutex_init(&mutex_SatPos, NULL) != 0)
  {
	  puts("mutex_SatPos init failed");
	  LogInfo("mutex_SatPos init failed");
  }
  if (pthread_mutex_init(&mutex_SatAtt, NULL) != 0)
  {
	  puts("mutex_SatAtt init failed");
	  LogInfo("mutex_SatAtt init failed");
  }
}

void fuseIMU(void){
	float accMeasurement[2] = {sImuData.xAcc*10, sImuData.zAcc*10};
	float gyroMeasurement = sImuData.dPitch*DEG_TO_RAD;
	float dt = (float)sImuData.Time/1000 - SatAttKF.previousTime;

//	sem_wait(&sem_SatAtt);
	pthread_mutex_lock(&mutex_SatAtt);
	satattCalculateMatrices(dt);
	predict(&SatAttKF, dt, &gyroMeasurement);
	pthread_mutex_unlock(&mutex_SatAtt);
//	post(&sem_SatAtt);

//	sem_wait(&sem_SatPos);
	pthread_mutex_lock(&mutex_SatPos);
	satposCalculateMatrices(dt);
	predict(&SatPosKF, dt, accMeasurement);
	pthread_mutex_unlock(&mutex_SatPos);
//	sem_post(&sem_SatPos);

	SatAttKF.previousTime = (float)sImuData.Time/1000;
	SatPosKF.previousTime = (float)sImuData.Time/1000;
	//LogApp("IMU");
}

void fuseVS(void){
	float chaserAttitudeMeasurement = (float)sVisionSystem[VS_SATEL_MARKER].OrientationZ/10*DEG_TO_RAD;
	float chaserPositionMeasurement[2] = {(float)sVisionSystem[VS_SATEL_MARKER].PositionX/10000 + 0.05*cos(chaserAttitudeMeasurement), (float)sVisionSystem[VS_SATEL_MARKER].PositionY/10000+ 0.05*sin(chaserAttitudeMeasurement)}; //transformation to imu
	float endeffMeasurement[3] = {(float)sVisionSystem[VS_MANIP_MARKER].PositionX/10000, (float)sVisionSystem[VS_MANIP_MARKER].PositionY/10000, (float)sVisionSystem[VS_MANIP_MARKER].OrientationZ/10*DEG_TO_RAD};
	float targetMeasurement[3] = {(float)sVisionSystem[VS_TARGET_MARKER].PositionX/10000, (float)sVisionSystem[VS_TARGET_MARKER].PositionY/10000, (float)sVisionSystem[VS_TARGET_MARKER].OrientationZ/10*DEG_TO_RAD};
	float chaserTime = sVisionSystem[VS_SATEL_MARKER].Time;
	float endeffTime = sVisionSystem[VS_MANIP_MARKER].Time;
	float targetTime = sVisionSystem[VS_TARGET_MARKER].Time;
	float target_dt = 0.05;
	float endeff_dt = 0.05;
	/*oryginalne: */
	//float target_dt = (float)(targetTime - TargetKF.previousTime)/1000;
	//float endeff_dt = (float)(endeffTime - EndEffKF.previousTime)/1000;


	float Rot[][9] = {{cos(chaserAttitudeMeasurement), -sin(chaserAttitudeMeasurement), 0,0,0,0}, {sin(chaserAttitudeMeasurement), cos(chaserAttitudeMeasurement), 0,0,0,0},{0,0,1,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0},{0,0,0,0,0,0}};


	// Update satellite's state
	if (1){
//		sem_wait(&sem_SatAtt);
		pthread_mutex_lock(&mutex_SatAtt);
		update(&SatAttKF, &chaserAttitudeMeasurement);
		pthread_mutex_unlock(&mutex_SatAtt);
//		sem_post(&sem_SatAtt);
//		sem_wait(&sem_SatPos);
		pthread_mutex_lock(&mutex_SatPos);
		update(&SatPosKF, chaserPositionMeasurement);
		pthread_mutex_unlock(&mutex_SatPos);
//		sem_post(&sem_SatPos);
	}
	// Update target's state
	if (0 ){
		targetCalculateMatrices(target_dt);
		predict(&TargetKF, target_dt, NULL);
		update(&TargetKF, targetMeasurement);
	}
	// Update end-effector's state
	if (1){

//		sem_getvalue(&sem_EndEff, &a);
		pthread_mutex_lock(&mutex_EndEff);
		//LogApp("VS in");
//		sem_wait(&sem_EndEff);
//		LogInfo("state[0] before vs predict = %f, %f, %f", EndEffKF.x[0],EndEffKF.x[1],EndEffKF.x[2]);

//		sem_getvalue(&sem_EndEff, &a);
//		LogInfo("Semafor VS VALUE= %d", a);
//		LogInfo("meas vs = %f, %f, %f", endeffMeasurement[0],endeffMeasurement[1],endeffMeasurement[2]);

		endeffCalculateMatrices(endeff_dt, 0);
		predict(&EndEffKF, endeff_dt, NULL);
		update(&EndEffKF, endeffMeasurement);
		EndEffKF.previousTime = endeffTime;
//		LogInfo("state[0] after vs update = %f, %f, %f", EndEffKF.x[0],EndEffKF.x[1],EndEffKF.x[2]);
		//LogApp("VS out");
		pthread_mutex_unlock(&mutex_EndEff);
//		sem_post(&sem_EndEff);
	}

}

void fuseENC(void){
	float manip[3] = {sAppState.Joint1CurrPosition*DEG_TO_RAD, sAppState.Joint2CurrPosition*DEG_TO_RAD, sAppState.Joint3CurrPosition*DEG_TO_RAD};
	float dt = 0.1;


//	sem_wait(&sem_EndEff);
	pthread_mutex_lock(&mutex_EndEff);
	//LogApp("ENC in");
//	sem_getvalue(&sem_EndEff, &a);
//	LogInfo("Semafor ENC VALUE= %d", a);
//	LogInfo("state[0] before enc update = %f, %f, %f", EndEffKF.x[0],EndEffKF.x[1],EndEffKF.x[2]);
//	LogInfo("meas enc = %f, %f, %f", manip[0], manip[1], manip[2]);
	endeffCalculateMatrices(dt, 1);
	predict(&EndEffKF, dt, NULL);
	update(&EndEffKF, manip);
//	LogInfo("state[0] after enc update = %f, %f, %f", EndEffKF.x[0],EndEffKF.x[1],EndEffKF.x[2]);
	//LogApp("ENC out");
	pthread_mutex_unlock(&mutex_EndEff);
//	sem_post(&sem_EndEff);
}

void getChaserPosition(float *position){
	float att =0;
	getChaserAttitude(&att);

	position[0] = SatPosKF.x[0]- 0.05*cos(att*DEG_TO_RAD); //x transformation back to amrker
	position[1] = SatPosKF.x[3]- 0.05*sin(att*DEG_TO_RAD); //y transformation back to amrker
}
void getChaserAttitude(float *attitude){
	attitude[0] = SatAttKF.x[0]*RAD_TO_DEG; //theta
}

void getChaserVelocity(float *velocity){
	velocity[0] = SatPosKF.x[1]; //dx
	velocity[1] = SatPosKF.x[4]; //dy
	velocity[2] = SatAttKF.x[1]; //omega
}

void getEndEffPosition(float *position){
	position[0] = EndEffKF.x[6]; //x
	position[1] = EndEffKF.x[7]; //y
}

void getEndEffAttitude(float *attitude){
	attitude[0] = EndEffKF.x[8]*RAD_TO_DEG;
}

void getTargetPosition(float *position){
	position[0] = TargetKF.x[0]; //x
	position[1] = TargetKF.x[1]; //y
}

void getTargetAttitude(float *attitude){
	attitude[0] = TargetKF.x[2]*RAD_TO_DEG;
}

void getTargetVelocity(float *velocity){
	velocity[0] = TargetKF.x[3]; //dx
	velocity[1] = TargetKF.x[4]; //dy
	velocity[2] = TargetKF.x[5]; //omega
}

void getJointPos(float *joints){
	joints[0] = EndEffKF.x[0]*RAD_TO_DEG;
	joints[1] = EndEffKF.x[1]*RAD_TO_DEG;
	joints[2] = EndEffKF.x[2]*RAD_TO_DEG;
}

void getJointVel(float *djoints){
	djoints[0] = EndEffKF.x[3]*RAD_TO_DEG;
	djoints[1] = EndEffKF.x[4]*RAD_TO_DEG;
	djoints[2] = EndEffKF.x[5]*RAD_TO_DEG;
}
