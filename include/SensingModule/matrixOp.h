#ifndef MATRIXOP_H_
#define MATRIXOP_H_

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
void display(float (*matrix)[9], int d);
void display_(float (*matrix1)[9], int m,int n);
void MatrixVectorMultiplicationCC_SM(float (*matrix1)[9], float (*matrix2), float (*result),int m,int n);
void MatrixScalarMultiplicationCC_SM(float (*matrix1)[9], float scalar, float (*result)[9],int m,int n);
float MatrixDeterminantCC_SM(float (*matrix)[9], float k);
void MatrixInverseCC_SM(float (*num)[9], float f , float (*inverse)[9]);
void MatrixMultiplicationCC_SM(float (*matrix1)[9], float (*matrix2)[9], float (*result)[9],int m,int n,int l);
void MatrixTransposeCC_SM(float (*matrix)[9], float (*result)[9], int m, int n);
void MatrixSumCC_SM(float (*matrix1)[9], float (*matrix2)[9], float (*result)[9],int m,int n, int sign);
void VectorSumCC_SM(float (*matrix1), float (*matrix2), float (*result),int m, int sign);
void Vector2Matrix(float *vector, float (*matrix)[9], int n);
#endif
