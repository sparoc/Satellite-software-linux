/*
 * SatelliteControlLaw.h
 *
 *  Created on: Nov 26, 2018
 *      Author: ubuntu
 */

#ifndef INCLUDE_SATELLITE_SATELLITECONTROLLAW_H_
#define INCLUDE_SATELLITE_SATELLITECONTROLLAW_H_

#include "math.h"
#include "include/DataDefinition/DataStruct.h"
#include <stdio.h>
#include <stdlib.h>

#define CG_VALVE_1_ID		0
#define CG_VALVE_2_ID		1
#define CG_VALVE_3_ID		2
#define CG_VALVE_4_ID		3
#define CG_VALVE_5_ID		4
#define CG_VALVE_6_ID		5
#define CG_VALVE_7_ID		6
#define CG_VALVE_8_ID		7

#define SAT_COG_C_DISP_FROM_SAT_MARKER		(0.3011)		// [m]
#define SAT_COG_ALPHA_DISP_FROM_SAT_MARKER	(0.3386)		// [rad]
#define SAT_ORIENT_SCALE_FACTOR 			(0.00174533)	//  3.1415 / 180 / 10
#define SAT_PI								(3.14159265)
#define SAT_2_PI							(6.28318531)
#define SAT_DEG_TO_RAD						(0.01745329)
#define SAT_RAD_TO_DEG						(57.2957795)
#define SAT_MAX_CG_PWM						(90)			// Fmax = 1N * 90%
#define SAT_COLD_GAS_PWM_DEAD_ZONE			(2)

#define MANUAL_TEST_2_XY_POS_ERROR			(0.03)			// [m]
#define MANUAL_TEST_2_Z_ORIENT_ERROR		(0.0523598775)	// [rad]

#define MANUAL_TEST_2_RA_XY_POS_ERROR		(0.1)			// [m]

typedef struct
{
	float KpPositionX;
	float KdPositionX;
	float PrevEPositionX;
	float KpPositionY;
	float KdPositionY;
	float PrevEPositionY;
	float KpOrientationZ;
	float KdOrientationZ;
	float PrevEOrientationZ;
	float OutFx;
	float OutFy;
	float OutMz;
	short Delta;
}SatellitPID;

extern SatellitPID sSatellitPid;

extern void InitSatelliteMembers(void);
void ProceedSatellitePID(float a_errorX, float a_errorY, float a_errorZ);
extern void CalculateSattelitePWMToStartPosition(float ff_forcex, float ff_forcey, float ff_momentz);  // bylo void
extern void load_forces_and_moments(float FF_forcex[1000], float FF_forcey[1000], float FF_momentz[1000]);                    // tego nie bylo


#endif /* INCLUDE_SATELLITE_SATELLITECONTROLLAW_H_ */
