/*
 * SatelliteControlLaw.c
 *
 *  Created on: Nov 26, 2018
 *      Author: ubuntu
 */


#include "SatelliteControlLaw.h"
#include "include/Protocols/VisionSystemProtocol.h"
#include "include/Logger/FileLogger.h"


SatellitPID sSatellitPid;
char gSatBuffer[256];

//
void ProceedSatellitePID(float a_errorX, float a_errorY, float a_errorZ)
{
	sSatellitPid.OutFx = sSatellitPid.KpPositionX * a_errorX + sSatellitPid.KdPositionX * (((a_errorX - sSatellitPid.PrevEPositionX) / sSatellitPid.Delta) * 1000);
	sSatellitPid.PrevEPositionX = a_errorX;

	sSatellitPid.OutFy = sSatellitPid.KpPositionY * a_errorY + sSatellitPid.KdPositionY * (((a_errorY - sSatellitPid.PrevEPositionY) / sSatellitPid.Delta) * 1000);
	sSatellitPid.PrevEPositionY = a_errorY;

	sSatellitPid.OutMz = sSatellitPid.KpOrientationZ * a_errorZ + sSatellitPid.KdOrientationZ * (((a_errorZ - sSatellitPid.PrevEOrientationZ) / sSatellitPid.Delta) * 1000);
	sSatellitPid.PrevEOrientationZ = a_errorZ;
}

void CalculateSattelitePWMToStartPosition(float ff_forcex, float ff_forcey, float ff_momentz)
{
	float x_base, y_base, rot_base;
	float x_baseTf, y_baseTf;
	float rot_baseTf;
	float ex;
	float ey;
	float erot;
	float x_ref, y_ref, rot_ref;
	int i = 0;


	/*get measurements*/
	x_base = (float)sVisionSystem[VS_SATEL_MARKER].PositionX / 10000.0; // [m]
	y_base = (float)sVisionSystem[VS_SATEL_MARKER].PositionY / 10000.0; // [m]
	rot_base = (float)sVisionSystem[VS_SATEL_MARKER].OrientationZ * SAT_ORIENT_SCALE_FACTOR; // [rad]

	/*transformation to ref rotation base, similar to simulation */
	rot_baseTf = rot_base + SAT_PI / 2.0;

	// Modification 14.11.2019 FLB
	// if(rot_baseTf > SAT_PI)
		// rot_baseTf =  rot_baseTf - 2 * SAT_2_PI;

	if(rot_baseTf > SAT_2_PI)
		rot_baseTf =  rot_baseTf - SAT_2_PI;
	if(rot_baseTf < -SAT_2_PI)
			rot_baseTf =  rot_baseTf + SAT_2_PI;
	// Modification End

	/*tranformation to center of mass */
	// 0.2462 [m] - distance between marker and center of mass
	x_baseTf = x_base + SAT_COG_C_DISP_FROM_SAT_MARKER * cos(rot_baseTf + SAT_COG_ALPHA_DISP_FROM_SAT_MARKER);
	// 0.4182 [rad] - angle between center of mass and marker mounting point
	y_baseTf = y_base + SAT_COG_C_DISP_FROM_SAT_MARKER * sin(rot_baseTf + SAT_COG_ALPHA_DISP_FROM_SAT_MARKER);

	/*count error pos*/
	x_ref = sSatCtrl.PositionXReq; //[m]
	ex = x_ref - x_baseTf;

	y_ref = sSatCtrl.PositionYReq;
	ey = y_ref - y_baseTf;

	rot_ref = sSatCtrl.OrientationZReq;	// [rad]
	erot = rot_ref - rot_baseTf;

	if(erot > SAT_PI)
		erot = erot - SAT_2_PI;

	// Modification START (FLB)
	if(erot > SAT_PI)
			erot = erot - SAT_2_PI;
	if(erot < -SAT_PI)
		erot = erot + SAT_2_PI;
	// Modification END

	sVisionSystem[VS_SATEL_MARKER].PosXcog = x_baseTf;
	sVisionSystem[VS_SATEL_MARKER].PosYcog = y_baseTf;
	sVisionSystem[VS_SATEL_MARKER].OrientZcog = rot_baseTf;


	// If we want to use a PD controller to calculate the PWM, part A must be unchecked
	// If we want to use a feedforward forces and moment  to calculate the PWM, part B must be unchecked

	// Part A
	ProceedSatellitePID(ex, ey, erot);

	// Part B
	// sSatellitPid.OutFx = ff_forcex;
	// sSatellitPid.OutFy = ff_forcey;
	// sSatellitPid.OutMz = ff_momentz;


	/* Jesli wszystkie silniki dzialaja */
	/*
	sSatCtrl.PWM[CG_VALVE_1_ID] = 100 * (1.25 * sSatellitPid.OutMz - 0.4984 * sSatellitPid.OutFx * cos(rot_baseTf) + 0.0004425 * sSatellitPid.OutFy * cos(rot_baseTf)
    			- 0.0004425 * sSatellitPid.OutFx * sin(rot_baseTf) - 0.4984 * sSatellitPid.OutFy * sin(rot_baseTf));

	sSatCtrl.PWM[CG_VALVE_2_ID] = 100 * (0.50044 * sSatellitPid.OutFx * sin(rot_baseTf) - 0.00159874 * sSatellitPid.OutFx * cos(rot_baseTf)
				- 0.50044 * sSatellitPid.OutFy * cos(rot_baseTf) - 1.25 * sSatellitPid.OutMz - 0.00159874 * sSatellitPid.OutFy * sin(rot_baseTf));

	sSatCtrl.PWM[CG_VALVE_3_ID] = 100 * (1.25 * sSatellitPid.OutMz + 0.00159874 * sSatellitPid.OutFx * cos(rot_baseTf) - 0.49956 * sSatellitPid.OutFy * cos(rot_baseTf)
    			+ 0.49956 * sSatellitPid.OutFx * sin(rot_baseTf) + 0.00159874 * sSatellitPid.OutFy * sin(rot_baseTf));

	sSatCtrl.PWM[CG_VALVE_4_ID] = 100 * (0.4984 * sSatellitPid.OutFx * cos(rot_baseTf) - 1.25 * sSatellitPid.OutMz - 0.0004425 * sSatellitPid.OutFy * cos(rot_baseTf)
    			+ 0.0004425 * sSatellitPid.OutFx * sin(rot_baseTf) + 0.4984 * sSatellitPid.OutFy * sin(rot_baseTf));

	sSatCtrl.PWM[CG_VALVE_5_ID] = 100 * (1.25 * sSatellitPid.OutMz + 0.5016 * sSatellitPid.OutFx * cos(rot_baseTf) + 0.0004425 * sSatellitPid.OutFy * cos(rot_baseTf)
    			- 0.0004425 * sSatellitPid.OutFx * sin(rot_baseTf) + 0.5016 * sSatellitPid.OutFy * sin(rot_baseTf));

	sSatCtrl.PWM[CG_VALVE_6_ID] = 100 * (0.49956 * sSatellitPid.OutFy * cos(rot_baseTf) - 0.00159874 * sSatellitPid.OutFx * cos(rot_baseTf) - 1.25 * sSatellitPid.OutMz
    			- 0.49956 * sSatellitPid.OutFx * sin(rot_baseTf) - 0.00159874 * sSatellitPid.OutFy * sin(rot_baseTf));

	sSatCtrl.PWM[CG_VALVE_7_ID] = 100 * (1.25 * sSatellitPid.OutMz + 0.00159874 * sSatellitPid.OutFx * cos(rot_baseTf) + 0.50044 * sSatellitPid.OutFy * cos(rot_baseTf)
    			- 0.50044 * sSatellitPid.OutFx * sin(rot_baseTf) + 0.00159874 * sSatellitPid.OutFy * sin(rot_baseTf));

	sSatCtrl.PWM[CG_VALVE_8_ID] = 100 * (0.0004425 * sSatellitPid.OutFx * sin(rot_baseTf) - 0.5016 * sSatellitPid.OutFx * cos(rot_baseTf)
    			- 0.0004425 * sSatellitPid.OutFy * cos(rot_baseTf) - 1.25 * sSatellitPid.OutMz - 0.5016 * sSatellitPid.OutFy * sin(rot_baseTf));

	*/


	// Z dnia 12.03.2020 dla niedzialajacego jednego silnika nr 4
		sSatCtrl.PWM[CG_VALVE_1_ID] = 100*(2*sSatellitPid.OutMz - (4033*cos(rot_baseTf)*sSatellitPid.OutFx)/5000 - (209*cos(rot_baseTf)*sSatellitPid.OutFy)/5000 + (209*sin(rot_baseTf)*sSatellitPid.OutFx)/5000 -(4033*sin(rot_baseTf)*sSatellitPid.OutFy)/5000);
		sSatCtrl.PWM[CG_VALVE_2_ID] = 100*((2099*cos(rot_baseTf)*sSatellitPid.OutFx)/20000 -(3*sSatellitPid.OutMz)/2 -(9373*cos(rot_baseTf)*sSatellitPid.OutFy)/20000 +(9373*sin(rot_baseTf)*sSatellitPid.OutFx)/20000 +(2099*sin(rot_baseTf)*sSatellitPid.OutFy)/20000);
		sSatCtrl.PWM[CG_VALVE_3_ID] = 100*((3*sSatellitPid.OutMz)/2 -(2099*cos(rot_baseTf)*sSatellitPid.OutFx)/20000 -(10627*cos(rot_baseTf)*sSatellitPid.OutFy)/20000 +(10627*sin(rot_baseTf)*sSatellitPid.OutFx)/20000 -(2099*sin(rot_baseTf)*sSatellitPid.OutFy)/20000);
		sSatCtrl.PWM[CG_VALVE_4_ID] = 0;
		sSatCtrl.PWM[CG_VALVE_5_ID] = 100*(sSatellitPid.OutMz +(5967*cos(rot_baseTf)*sSatellitPid.OutFx)/10000 -(209*cos(rot_baseTf)*sSatellitPid.OutFy)/10000 +(209*sin(rot_baseTf)*sSatellitPid.OutFx)/10000 +(5967*sin(rot_baseTf)*sSatellitPid.OutFy)/10000);
		sSatCtrl.PWM[CG_VALVE_6_ID] = 100*((2099*cos(rot_baseTf)*sSatellitPid.OutFx)/20000 -(3*sSatellitPid.OutMz)/2 +(10627*cos(rot_baseTf)*sSatellitPid.OutFy)/20000 -(10627*sin(rot_baseTf)*sSatellitPid.OutFx)/20000 +(2099*sin(rot_baseTf)*sSatellitPid.OutFy)/20000);
		sSatCtrl.PWM[CG_VALVE_7_ID] = 100*((3*sSatellitPid.OutMz)/2 -(2099*cos(rot_baseTf)*sSatellitPid.OutFx)/20000 +(9373*cos(rot_baseTf)*sSatellitPid.OutFy)/20000 -(9373*sin(rot_baseTf)*sSatellitPid.OutFx)/20000 -(2099*sin(rot_baseTf)*sSatellitPid.OutFy)/20000);
		sSatCtrl.PWM[CG_VALVE_8_ID] = 100*((209*cos(rot_baseTf)*sSatellitPid.OutFy)/10000 -(5967*cos(rot_baseTf)*sSatellitPid.OutFx)/10000 - sSatellitPid.OutMz -(209*sin(rot_baseTf)*sSatellitPid.OutFx)/10000 -(5967*sin(rot_baseTf)*sSatellitPid.OutFy)/10000);


    /* measured force from coldgas is aprox. 1N */
    for(i=0; i < 8; i++)
    {
    	if(sSatCtrl.PWM[i] < 0)
    	{
    		sSatCtrl.PWM[i] = 0;
    	}
    	else if(sSatCtrl.PWM[i] != 0)
    	{
    		sSatCtrl.PWM[i] = sSatCtrl.PWM[i] + SAT_COLD_GAS_PWM_DEAD_ZONE;

    		if(sSatCtrl.PWM[i] > SAT_MAX_CG_PWM)
			{
				sSatCtrl.PWM[i] = SAT_MAX_CG_PWM;
			}
    	}
    }
}


void load_forces_and_moments(float FF_forcex[1000], float FF_forcey[1000], float FF_momentz[1000])
{
	FILE *plik;
	float x1,x2,x3;
	plik = fopen("/usr/bin/scenariusz1.txt", "r");
	int i=0,OK=0;

	while(1)
	{
    	OK=(fscanf(plik, "%f%f%f", &x1,&x2, &x3)==3);
	    FF_forcex[i] = x1;
	    FF_forcey[i] = x2;
	    FF_momentz[i] = x3;
	    LogInfo("Read: %d\t%f,%f,%f -> %d\n",i,x1,x2,x3,OK);
	    i=i+1;
	    if( !OK)
	    	break;
	}
	LogInfo("Read forces: %d\n",i);
}


void InitSatelliteMembers(void)
{
	sSatellitPid.KpPositionX = 10.0;
	sSatellitPid.KdPositionX = 28.0;
	sSatellitPid.PrevEPositionX = 0;
	sSatellitPid.KpPositionY = 10.0;
	sSatellitPid.KdPositionY = 24.0;
	sSatellitPid.PrevEPositionY = 0;
	sSatellitPid.KpOrientationZ = 1.3;
	sSatellitPid.KdOrientationZ = 5.0;
	sSatellitPid.PrevEOrientationZ = 0;
	sSatellitPid.OutFx = 0;
	sSatellitPid.OutFy = 0;
	sSatellitPid.OutMz = 0;
	sSatellitPid.Delta = 100;
}
