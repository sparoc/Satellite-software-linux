/*
 * SocketCAN.h
 *
 *  Created on: Apr 8, 2016
 *      Author: ubuntu
 */

#ifndef INCLUDE_SOCKETS_SOCKETCAN_H_
#define INCLUDE_SOCKETS_SOCKETCAN_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <linux/can.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <linux/can/raw.h>
#include <linux/can/error.h>

#define CAN_STANDARD_ID			0x00
#define CAN_EXTENDED_ID			0x01
#define MAX_CAN_DATA_BUFFER		8

#define BROADCAST_ID			0x00
#define COLD_GAS_BOARD_ID		0x01
#define JOINT_1_BOARD_ID		0x02
#define JOINT_2_BOARD_ID		0x03
#define JOINT_3_BOARD_ID		0x04
#define MOMENTUM_BOARD_ID		0x05
#define GRIPPER_BOARD_ID		0x06

extern int gCan0SocketDesc;
extern int gCan1SocketDesc;

// First 32 bytes
// bit 0-28	: CAN identifier (11/29 bit)
// bit 29	: error message frame flag (0 = data frame, 1 = error message)
// bit 30	: remote transmission request flag (1 = rtr frame)
// bit 31	: frame format flag (0 = standard 11 bit, 1 = extended 29 bit)
typedef struct
{
	//unsigned char IDE;			// 1 - extended 29 bit, 0 - standard 11 bit
	//unsigned char RTR;			// Remote transmission request
	//unsigned char ID;				// 11 or 28 bits
	unsigned char MsgId;		// 7 bits
	unsigned char SourceId;		// 4 bits
	unsigned char ReceiverId;	// 4 bits
	unsigned short ExtraArg;	// 14 bits
}CbkCanId;

typedef struct
{
	int CanHardwareNumber;
	CbkCanId CanId;
	unsigned char DLC;			// 8 bits
	unsigned char TxData[MAX_CAN_DATA_BUFFER];
	struct can_frame RxFrame;
	unsigned int RxCounter;
	unsigned int TxCounter;
}CbkCanProtocol;

extern CbkCanProtocol sCan0Frame;
extern CbkCanProtocol sCan1Frame;

void ParseCanError(CbkCanProtocol *sOrcoCanFrame);
extern int StartCanSocket(char *canName);
extern void GetCanId(CbkCanId *canId, canid_t can_id);
extern void BuildCan0TxFrame(CbkCanProtocol *scanFrame);
extern void BuildCan1TxFrame(CbkCanProtocol *scanFrame);
extern void SendCan0TxFrame(void);
extern void SendCan1TxFrame(void);

canid_t CreateCanId(CbkCanProtocol *canFrame);



#endif /* INCLUDE_SOCKETS_SOCKETCAN_H_ */
