/*
 * Sockets.h
 *
 *  Created on: Apr 6, 2016
 *      Author: ubuntu
 */

#ifndef INCLUDE_SOCKETS_SOCKETSERVERLAN_H_
#define INCLUDE_SOCKETS_SOCKETSERVERLAN_H_

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>

// The maximum number of clients the server will accept
#define SOCKET_MAXIMUM_CLIENTS		3

extern int gLanSocketDesc;

extern int StartLanSocket(char *ip, uint16_t port);

#endif /* INCLUDE_SOCKETS_SOCKETSERVERLAN_H_ */
