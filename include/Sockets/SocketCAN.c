/*
 * SocketCAN.c
 *
 *  Created on: Apr 8, 2016
 *      Author: ubuntu
 */

#include "SocketCAN.h"
#include <termios.h> /* POSIX terminal control definitions */

int gCan0SocketDesc;
int gCan1SocketDesc;
struct can_frame sCan0TxFrame;
struct can_frame sCan1TxFrame;
CbkCanProtocol sCan0Frame;
CbkCanProtocol sCan1Frame;

int StartCanSocket(char *canName)
{
	int socketDesc;
	struct ifreq nameToIndex;
	struct sockaddr_can socadr;

	socketDesc = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	//printf("%d \n",&socketDesc);
	if(socketDesc == -1)
	{
		perror("SocketCAN.c - StartCanSocket(): Unable to create socket - ");
		return -1;
	}

	//Prepare the sockaddr_in structure
	socadr.can_family = PF_CAN;
	// Convert can bus interface name to index
	strcpy(nameToIndex.ifr_name, canName);
	ioctl(socketDesc, SIOCGIFINDEX, &nameToIndex);
	socadr.can_ifindex = nameToIndex.ifr_ifindex;

	// We want to wait for a data to process them
	//fcntl(socketDesc, F_SETFL, O_NONBLOCK);

	// Enable/Disable the loopback
	int loopback = 0;
	setsockopt(socketDesc, SOL_CAN_RAW, CAN_RAW_LOOPBACK, &loopback, sizeof(loopback));

	// Enable/Disable reception of theCAN sent msg
	int recv_own_msgs = 0;
	setsockopt(socketDesc, SOL_CAN_RAW, CAN_RAW_RECV_OWN_MSGS, &recv_own_msgs, sizeof(recv_own_msgs));

	// Enable reception of the CAN errors
	can_err_mask_t err_mask = CAN_ERR_MASK;
	setsockopt(socketDesc, SOL_CAN_RAW, CAN_RAW_ERR_FILTER, &err_mask, sizeof(err_mask));

	// Set RX buffer
	//int rxBufferSize = 1000;
	//if (setsockopt(socketDesc, SOL_SOCKET, SO_RCVBUF, &rxBufferSize, sizeof(int)) == -1)
	//{
	//    perror("Error setting socket SO_RCVBUF - ");
	//}

	// Set TX buffer
	//int txBufferSize = 1000;
	//if (setsockopt(socketDesc, SOL_SOCKET, SO_SNDBUF, &txBufferSize, sizeof(int)) == -1)
	//{
	//	perror("Error setting socket SO_SNDBUF - ");
	//}

	// Bind given address to socket
	if(bind(socketDesc, (struct sockaddr *)&socadr, sizeof(socadr)) < 0)
	{
		perror("SocketCAN.c - StartCanSocket(): Binding address to socket failed - ");
		return -1;
	}

	return socketDesc;
}

canid_t CreateCanId(CbkCanProtocol *canFrame)
{
	//unsigned int canId = 0x80000000 & canFrame->CanId.IDE;		// Standard or Extended ID
	//canId |= 0x40000000 & canFrame->CanId.RTR;					// Remote transmission request
	//canId |= (int)canFrame->CanId.ID;							// 11 or 28 bits

	unsigned int canId = 0x80000000;
	canId |= (((int)canFrame->CanId.MsgId) << 22);
	canId |= (((int)canFrame->CanId.SourceId) << 18);
	canId |= (((int)canFrame->CanId.ReceiverId) << 14);
	canId |= (int)canFrame->CanId.ExtraArg;

	return (canid_t)canId;
}

void GetCanId(CbkCanId *canId, canid_t can_id)
{
	//canId->ID = (unsigned char)(can_id & 0x1FFFFFFF);

	canId->MsgId = (unsigned char)((can_id & 0x1FC00000) >> 22);
	canId->SourceId = (unsigned char)((can_id & 0x003C0000) >> 18);
	canId->ReceiverId = (unsigned char)((can_id & 0x0003C000) >> 14);
	canId->ExtraArg = (unsigned short)(can_id & 0x00003FFF);
}

void BuildCan0TxFrame(CbkCanProtocol *canFrame)
{
	sCan0TxFrame.can_id = CreateCanId(canFrame);
	sCan0TxFrame.can_dlc = canFrame->DLC;
	int i;
	for(i = 0; i < canFrame->DLC; i++)
		sCan0TxFrame.data[i] = canFrame->TxData[i];
}

void BuildCan1TxFrame(CbkCanProtocol *canFrame)
{
	sCan1TxFrame.can_id = CreateCanId(canFrame);
	sCan1TxFrame.can_dlc = canFrame->DLC;
	int i;
	for(i = 0; i < canFrame->DLC; i++)
		sCan1TxFrame.data[i] = canFrame->TxData[i];
}

void SendCan0TxFrame(void)
{
	int txSize = sizeof(struct can_frame);
	int sent = write(gCan0SocketDesc, &sCan0TxFrame, txSize);

	if(sent == -1)
		perror("SendCan0TxFrame(): writing to CAN0 failed - ");

	if(sent != txSize)
		puts("SendCan0TxFrame(): wrote less bytes.");

	sCan0Frame.TxCounter += sent;
}

void SendCan1TxFrame(void)
{
	int txSize = sizeof(struct can_frame);
	int sent = write(gCan1SocketDesc, &sCan1TxFrame, txSize);

	if(sent == -1)
		perror("SendCan1TxFrame(): writing to CAN1 failed - ");

	if(sent != txSize)
		puts("SendCan1TxFrame(): wrote less bytes.");

	sCan1Frame.TxCounter += sent;
}

void ParseCanError(CbkCanProtocol *sCanFrame)
{
	if(sCanFrame->RxFrame.can_id & CAN_ERR_ACK)
	{
		printf("CAN%d received no ACK on transmission.\n", sCanFrame->CanHardwareNumber);
	}
	if(sCanFrame->RxFrame.can_id & CAN_ERR_BUSOFF)
	{
		printf("CAN%d bus off.\n", sCanFrame->CanHardwareNumber);
	}
	if(sCanFrame->RxFrame.can_id & CAN_ERR_LOSTARB)
	{
		printf("CAN%d lost arbitration.\n", sCanFrame->CanHardwareNumber);
	}
	if(sCanFrame->RxFrame.can_id & CAN_ERR_CRTL)
	{
		printf("CAN%d controller problems.\n", sCanFrame->CanHardwareNumber);
	}
	if(sCanFrame->RxFrame.can_id & CAN_ERR_TX_TIMEOUT)
	{
		printf("CAN%d TX timeout (by net device driver).\n", sCanFrame->CanHardwareNumber);
	}
	if(sCanFrame->RxFrame.can_id & CAN_ERR_BUSERROR)
	{
		printf("CAN%d bus error (may flood!).", sCanFrame->CanHardwareNumber);
	}
	if(sCanFrame->RxFrame.can_id & CAN_ERR_TX_TIMEOUT)
	{
		printf("CAN%d TX timeout (by net device driver).\n", sCanFrame->CanHardwareNumber);
	}
	if(sCanFrame->RxFrame.can_id & CAN_ERR_CRTL)
	{
		if (sCanFrame->RxFrame.data[1] & CAN_ERR_CRTL_RX_OVERFLOW)
			printf("CAN%d_ERR_CRTL_RX_OVERFLOW\n", sCanFrame->CanHardwareNumber);
		if (sCanFrame->RxFrame.data[1] & CAN_ERR_CRTL_TX_OVERFLOW)
			printf("CAN%d_ERR_CRTL_TX_OVERFLOW\n", sCanFrame->CanHardwareNumber);
		if (sCanFrame->RxFrame.data[1] & CAN_ERR_CRTL_RX_WARNING)
			printf("CAN%d_ERR_CRTL_RX_WARNING\n", sCanFrame->CanHardwareNumber);
		if (sCanFrame->RxFrame.data[1] & CAN_ERR_CRTL_TX_WARNING)
			printf("CAN%d_ERR_CRTL_TX_WARNING\n", sCanFrame->CanHardwareNumber);
	}
	else
		puts("CAN - new error !!!");

	fflush(stdout);
}

