/*
 * Sockets.c
 *
 *  Created on: Apr 6, 2016
 *      Author: ubuntu
 */

#include "../Sockets/SocketServerLAN.h"

int gLanSocketDesc;

extern int StartLanSocket(char *ip, uint16_t port)
{
	int socketDesc;
	struct sockaddr_in server;

	//Create socket
	socketDesc = socket(AF_INET, SOCK_STREAM , 0);

	if (socketDesc == -1)
	{
		perror("SocketServerLAN.c - StartLanSocket(): Unable to create socket - ");
	}

	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = inet_addr(ip);
	server.sin_port = htons(port);

	// Bind IP address to socket
	if(bind(socketDesc,(struct sockaddr *)&server , sizeof(server)) < 0)
	{
		char comBuffer[128];
		sprintf(comBuffer, "SocketServerLAN.c - StartLanSocket(): Binding IP address: %s to socket: %d failed - ", ip, port);
		perror(comBuffer);
		//perror("SocketServerLAN.c - StartLanSocket(): Binding IP address to socket failed - ");
	}

	//Listen
	listen(socketDesc, SOCKET_MAXIMUM_CLIENTS);

	puts("LAN Server is running...");

	return socketDesc;
}
