/*
 * ManualMode.h
 *
 *  Created on: Oct 26, 2018
 *      Author: ubuntu
 */

#ifndef INCLUDE_MANIPULATOR_MANUALMODE_H_
#define INCLUDE_MANIPULATOR_MANUALMODE_H_

#include "../DataDefinition/DataStruct.h"
#include "../Algorithm/TrajectoryPlanning.h"

#define JOINT_POS_SLEW_LOMIT		(0.00872664626)	// 0.5 [deg]

extern unsigned int gSatTrajectoryIndex;
/* Number of trajectory elements */
extern unsigned int gSatTrajectorySize;
extern unsigned int gJointTrajectoryIndex;
/* Number of trajectory elements */
extern unsigned int gJointTrajectorySize;


/* Manual 1 */
extern void Manual1ReadJointPosition(void);
extern void ReadJointPosition(void);
int GenerateJointTrajectory(int jointIndex, float q_ini, float q_fin, float Tr, float Tp, float Th, int freq);
int GenerateSatelliteTrajectory(int index, float q_ini, float q_fin, float Tr, float Tp, float Th, int freq);
int InitSatelliteFileTrajectory(int size);
int InitQuasiFileTrajectory(int size);
void UploadSatelliteFileTrajectory(float x, float y, float orient);
void UploadQuasiFileTrajectory(float x, float y, float z);
/* Manual 2 */
void ManualGoToRequiredPositionCartesian(void);





#endif /* INCLUDE_MANIPULATOR_MANUALMODE_H_ */
