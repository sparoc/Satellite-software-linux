/*
 * ManualMode.c
 *
 *  Created on: Oct 26, 2018
 *      Author: ubuntu
 */

#include "../Manipulator/ManualMode.h"

#include "../Protocols/JointProtocol_v2.h"

unsigned int gSatTrajectoryIndex = 0;
unsigned int gSatTrajectorySize = 0;
unsigned int gJointTrajectoryIndex = 0;
unsigned int gJointTrajectorySize = 0;

extern void ReadJointPosition(void)
{
	sAppState.JointResponseState = 0;

//	GetJointState(JOINT_1_ID, s_E1val);
//	usleep(50);
//	GetJointState(JOINT_2_ID, s_E1val);
//	usleep(50);
//	GetJointState(JOINT_3_ID, s_E1val);
//	usleep(50);
}

void Manual1ReadJointPosition(void)
{
	sAppState.JointResponseState = 0;

//	GetJointState(JOINT_1_ID, s_E1val);
//	usleep(50);
//	GetJointState(JOINT_2_ID, s_E1val);
//	usleep(50);
//	GetJointState(JOINT_3_ID, s_E1val);
//	usleep(50);
//
//	GetJointParam(JOINT_1_ID, p_E0);
//	usleep(50);
//	GetJointParam(JOINT_2_ID, p_E0);
//	usleep(50);
//	GetJointParam(JOINT_3_ID, p_E0);
//	usleep(50);
//
//	GetJointState(JOINT_1_ID, s_E1stat1);
//	usleep(50);
//	GetJointState(JOINT_2_ID, s_E1stat1);
//	usleep(50);
//	GetJointState(JOINT_3_ID, s_E1stat1);
//	usleep(50);
}

int InitSatelliteFileTrajectory(int size)
{
	if(gSatTrajectory == NULL)
	{
		gSatTrajectoryIndex = 0;
		gSatTrajectorySize = (unsigned int)(size);

		gSatTrajectory = (float**)malloc(3 * sizeof(float*));
		gSatTrajectory[0] = (float*)calloc(gSatTrajectorySize, sizeof(float));
		gSatTrajectory[1] = (float*)calloc(gSatTrajectorySize, sizeof(float));
		gSatTrajectory[2] = (float*)calloc(gSatTrajectorySize, sizeof(float));

		if(gSatTrajectory != NULL)
			return 1;
	}

	return 0;
}

int InitQuasiFileTrajectory(int size)
{
	if(gJointTrajectoryQuasi == NULL)
	{
		gJointTrajectoryIndex = 0;
		gJointTrajectorySize = (unsigned int)(size);

		gJointTrajectoryQuasi = (float**)malloc(3 * sizeof(float*));
		gJointTrajectoryQuasi[0] = (float*)calloc(gJointTrajectorySize, sizeof(float));
		gJointTrajectoryQuasi[1] = (float*)calloc(gJointTrajectorySize, sizeof(float));
		gJointTrajectoryQuasi[2] = (float*)calloc(gJointTrajectorySize, sizeof(float));

		if(gJointTrajectoryQuasi != NULL)
			return 1;
	}

	return 0;
}

void UploadSatelliteFileTrajectory(float x, float y, float theta)
{
	gSatTrajectory[0][gSatTrajectoryIndex] = x;
	gSatTrajectory[1][gSatTrajectoryIndex] = y;
	gSatTrajectory[2][gSatTrajectoryIndex] = theta;

	gSatTrajectoryIndex++;
}

void UploadQuasiFileTrajectory(float x, float y, float z)
{
	gJointTrajectoryQuasi[0][gJointTrajectoryIndex] = x;
	gJointTrajectoryQuasi[1][gJointTrajectoryIndex] = y;
	gJointTrajectoryQuasi[2][gJointTrajectoryIndex] = z;

	gJointTrajectoryIndex++;
}

// Generate satellite trajectory for given index (0, 1, 2)
int GenerateSatelliteTrajectory(int index, float q_ini, float q_fin, float Tr, float Tp, float Th, int freq)
{
	if(gSatTrajectory == NULL)
	{
		gSatTrajectoryIndex = 0;
		gSatTrajectorySize = (unsigned int)(Tr * freq);

		gSatTrajectory = (float**)malloc(3 * sizeof(float*));

		if(gSatTrajectory == NULL)
	    	return 0;
	}

	gSatTrajectory[index] = (float*)calloc(gSatTrajectorySize, sizeof(float));

	return q_TrajGenerationCartesian(gSatTrajectory[index], NULL, q_ini, q_fin, Tr, Tp, Th, freq);
}

// Generate joint trajectory for given index (0, 1, 2)
int GenerateJointTrajectory(int jointIndex, float q_ini, float q_fin, float Tr, float Tp, float Th, int freq)
{
	if(gJointTrajectoryQuasi == NULL)
	{
		gJointTrajectoryIndex = 0;
		gJointTrajectorySize = (unsigned int)(Tr * freq);

		gJointTrajectoryQuasi = (float**)malloc(3 * sizeof(float*));

		if(gJointTrajectoryQuasi == NULL)
	    	return 0;
	}

	gJointTrajectoryQuasi[jointIndex] = (float*)calloc(gJointTrajectorySize, sizeof(float));

	return q_TrajGeneration(gJointTrajectoryQuasi[jointIndex], NULL, q_ini, q_fin, Tr, Tp, Th, freq);
}




