#ifndef GCS_COM_RX_PROTOCOL_H
#define GCS_COM_RX_PROTOCOL_H

#include "GCSProtocol.h"
#include "../drilibs/CRC/CRC16.h"

extern void ParseGCSFrame(unsigned char a_bByte);

#endif
