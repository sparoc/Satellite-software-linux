#ifndef GCS_COM_TX_PROTOCOL_H
#define GCS_COM_TX_PROTOCOL_H

#include "GCSProtocol.h"
#include "../drilibs/CRC/CRC16.h"

int GetGCSFrameToSend(int a_InputSize);
int AddCrcToGCSFrame(int a_InputSize);
int CountOfGCSEscapeByte(int a_size);
unsigned char GetGCSEscapeByte(unsigned char a_bValue);

#endif
