#include "ImuProtocol.h"
#include "GCSProtocol.h"

unsigned char gImuCommand;
ImuData sImuData;
ImuFrame sImuFrame;

void ProcessImuCommand(void)
{
	gImuCommand = sImuFrame.RxCmdBuffer[0];

	if(gImuCommand == DEVICE_IMU_DATA)
	{
		sImuData.Time = ConvertToLSBInt(sImuFrame.RxCmdBuffer, 1);
		// Raw IMU data [LSB]
		sImuData.dRoll = ConvertToLSBShort(sImuFrame.RxCmdBuffer, 5)  * TO_GYRO_REAL_UNIT;
		sImuData.dPitch = ConvertToLSBShort(sImuFrame.RxCmdBuffer, 7) * TO_GYRO_REAL_UNIT;
		sImuData.dYaw = ConvertToLSBShort(sImuFrame.RxCmdBuffer, 9)   * TO_GYRO_REAL_UNIT;
		sImuData.xAcc = ConvertToLSBShort(sImuFrame.RxCmdBuffer, 11)  * TO_ACCEL_REAL_UNIT;
		sImuData.yAcc = ConvertToLSBShort(sImuFrame.RxCmdBuffer, 13)  * TO_ACCEL_REAL_UNIT;
		sImuData.zAcc = ConvertToLSBShort(sImuFrame.RxCmdBuffer, 15)  * TO_ACCEL_REAL_UNIT;

		//fuseIMU();
		//LogInfo("dRoll: %f\tdPitch: %f\tdYaw: %f\txAcc: %f\tyAcc: %f\tzAcc: %f",
		//		sImuData.dRoll, sImuData.dPitch, sImuData.dYaw, sImuData.xAcc, sImuData.yAcc, sImuData.zAcc);
	}
	else if(gImuCommand == ADIS_ABC_PROGRESS_MB)
	{
		// IMU calibration in progress, it will take about 90 sec
		if(sImuFrame.RxCmdBuffer[1] == ADIS_ABC_PROGRESS)
		{
			sprintf(gProtBuffer, "IMU auto bias correction step: %d\n", sImuFrame.RxCmdBuffer[2]);
			perror(gProtBuffer);
		}
		else if(sImuFrame.RxCmdBuffer[1] == ADIS_ABC_DONE)
		{
			puts("IMU ready\n");
			SendCmdToGCS(IMU_READY);
		}
	}
}


void InitImuProtocolMembers(void)
{
	sImuFrame.RxCmdBufferLenght = 0;
	sImuFrame.IsFrameReady = 0;
}
