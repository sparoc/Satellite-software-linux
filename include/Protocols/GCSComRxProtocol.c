#include "GCSComRxProtocol.h"

int gRxGCSByteCounter = 0;
int gRxGCSSynchByteCounter = 0;		// 0x7E (Hex), 126 (Dec)
unsigned char m_arrGCSStripedPackage[GCS_MAX_FRAME_SIZE] __attribute__ ((aligned (4)));

void ParseGCSFrame(unsigned char a_bByte)
{
	// 0x7E (Hex) = 126 (Dec)
	if( a_bByte == 126 )
	{
		sGCSFrame.RxRawFrame[gRxGCSByteCounter] = a_bByte;
		gRxGCSByteCounter++;
		gRxGCSSynchByteCounter++;
	}
	else if( gRxGCSByteCounter != 0 )
	{
		sGCSFrame.RxRawFrame[gRxGCSByteCounter] = a_bByte;
		gRxGCSByteCounter++;
		if(gRxGCSByteCounter > GCS_MAX_FRAME_SIZE)
		{
			gRxGCSByteCounter = 0;
			gRxGCSSynchByteCounter = 0;
		}
	}
	else if( gRxGCSByteCounter == 0 )
	{
		return;
	}
	if( gRxGCSSynchByteCounter == 2 )
	{
		if(gRxGCSByteCounter >= GCS_MIN_FRAME_SIZE)
		{
			// Strip the frame from synch bytes(0x7E) and un-escap bytes(7D 5E or 7D 5D).
			//First we have to strip package because CRC also can be masked
			int nSynchByte = 0;		// 0x7E
			int nEscapeByte = 0;	// 0x7D
			int nRealSize = 0;
			int nIndex = 0;
			int i;
			for(i = 0; i < gRxGCSByteCounter; i++ )
			{
				if( nSynchByte == 2 )
					continue;
				if( sGCSFrame.RxRawFrame[i] == 0x7E )
					nSynchByte++;
				if( sGCSFrame.RxRawFrame[i] == 0x7D )
					nEscapeByte++;
				nRealSize++;
			}

			for(i = 0; i < nRealSize; i++ )
			{
				if( sGCSFrame.RxRawFrame[i] != 0x7E )
				{
					if( sGCSFrame.RxRawFrame[i] == 0x7D && sGCSFrame.RxRawFrame[i+1] == 0x5E )
					{
						m_arrGCSStripedPackage[nIndex] = 0x7E;
						nIndex++;
					}
					else if( sGCSFrame.RxRawFrame[i] == 0x7D && sGCSFrame.RxRawFrame[i+1] == 0x5D )
					{
						m_arrGCSStripedPackage[nIndex] = 0x7D;
						nIndex++;
					}
					else if( sGCSFrame.RxRawFrame[i-1] != 0x7E && sGCSFrame.RxRawFrame[i-1] != 0x7D )
					{
						m_arrGCSStripedPackage[nIndex] = sGCSFrame.RxRawFrame[i];
						nIndex++;
					}
					else if( i == 1  )
					{
						m_arrGCSStripedPackage[nIndex] = sGCSFrame.RxRawFrame[i];
						nIndex++;
					}
				}
			}

			if(CheckCrc(m_arrGCSStripedPackage, 0, nIndex - 2)) // '-2' because 2 bytes are CRC bytes
			{
				for (i = 0; i < nIndex - 2; i++)
					sGCSFrame.RxCmdBuffer[i] = m_arrGCSStripedPackage[i];

				sGCSFrame.RxCmdBufferLenght = nIndex - 2;

				gRxGCSByteCounter = 0;
				gRxGCSSynchByteCounter = 0;
				sGCSFrame.IsFrameReady = 1;
			}
			else
			{
				sGCSFrame.RxRawFrame[0] = SYNCH_BYTE;
				gRxGCSByteCounter = 1;
				gRxGCSSynchByteCounter = 1;
				puts("CRC failed !!!");
			}
		}
		else
		{
			sGCSFrame.RxRawFrame[0] = SYNCH_BYTE;
			gRxGCSByteCounter = 1;
			gRxGCSSynchByteCounter = 1;
		}
	}
}

