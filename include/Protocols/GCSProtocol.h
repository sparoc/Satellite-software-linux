#ifndef GCS_PROTOCOL_H_
#define GCS_PROTOCOL_H_

#include "../drilibs/BitConvert/Convert.h"
#include "../Gpio/Gpio.h"
#include "../Serials/Serials.h"
#include "GCSComTxProtocol.h"
#include "include/Sockets/SocketCAN.h"
#include "JointProtocol_v2.h"
#include "GripperProtocol.h"
#include "VisionSystemProtocol.h"
#include "include/Satellite/SatelliteControlLaw.h"
#include "../../MainComputer.h"
#include "../DataDefinition/DataStruct.h"
#include "../Manipulator/ManualMode.h"
#include "../Logger/FileLogger.h"
#include "../SensingModule/sensingModule.h"

#define GCS_MAX_FRAME_SIZE 		1024
#define GCS_MIN_FRAME_SIZE 		5
#define MAX_LAN_CLIENTS			3

extern int gLanSocket;
extern volatile unsigned int g10msClock;
extern char gProtBuffer[256];

extern void InitGCSMembers(void);
extern void ProcessGCSCommand(void);
void SendTest1ToGCS(int position, int current, unsigned char extraArg);
void RunDrive(void);
extern void SendDataToVS(float a_fValue1, float a_fValue2, float a_fValue3, float a_fValue4);
extern void SendRS0DiagnosticData(CbkCanId *canId);
extern void SendCurrentJointPosition(unsigned char a_JointId, float a_Pos1, unsigned char a_EncError);
extern void SendManual1TestDataToGCS(void);
void SendManual2TestDataToGCS(void);
void RunColdGasValve(unsigned char valveId, unsigned char pwm, unsigned short period);
void ParseFileUploadFrame(unsigned char a_Cmd);
void SendFileUploadMsgToGCS(unsigned char a_eCmd);
void SendFirmwareProcessStatus(void);
extern void SendCmdToGCS(unsigned char a_eCmd);

// Test commands
void RunColdGasValveTestCmd(void);


typedef struct
{
	// Buffer used by frame builder
    unsigned char RxRawFrame[GCS_MAX_FRAME_SIZE];
    // Buffer used by user code
    unsigned char RxCmdBuffer[GCS_MAX_FRAME_SIZE];
    // Buffer used by frame builder
    unsigned char TxRawBuffer[GCS_MAX_FRAME_SIZE];
    // Buffer used by user code
    unsigned char TxCmdBuffer[GCS_MAX_FRAME_SIZE];
    int  RxCmdBufferLenght;
    int IsFrameReady;
}GCSFrame;

extern GCSFrame sGCSFrame;
extern unsigned char gGCSCommand;

typedef struct
{
	unsigned int ModeType;
	unsigned int AllDataReceived;
	unsigned int FileSizeInLines;
	unsigned int ReceivedLines;
}FileUpload;

extern FileUpload sFileUpload;

enum ProtocolCommand
{
    // Basic commands: 0 - 60
    ACKNOWLEDGE = 0,
    COMPLETION = 1,
    WIFI_CONNECTION = 2,
    RS232_CONNECTION = 3,
	TRIGGER_PD30_ENABLE = 4,
	TRIGGER_PD30_STATE = 5,
	TRIGGER_PD30_DISABLE = 6,
	FILE_UPLOAD = 7,
	VS_DATA_FROM_ONBOARD_CAMERAS = 8,
	IMU_READY = 9,
	// Manual modes
	MANUAL_1_REQUEST_JOINTS_POSITION = 10,
	MANUAL_1_RESPONSE_JOINTS_POSITION  = 11,
	MANUAL_1_RUN_JOINT_1_TEST = 12,
	MANUAL_1_RUN_JOINT_2_TEST = 13,
	MANUAL_1_RUN_JOINT_3_TEST = 14,
	MANUAL_DIRECT_PWM_CONTROL = 15,
	MANUAL_2_REQUEST_SATELLITE_POSITION = 16,
	MANUAL_2_SAT_GO_TO_REQUESTED_POS = 17,
	MANUAL_2_SAT_GO_TO_REQUESTED_POS_STOP = 18,
	MANUAL_3_SAT_EXECUTE_TRAJECTORY = 19,
	MANUAL_3_SAT_EXECUTE_TRAJECTORY_STOP = 20,
    MANUAL_4_SAT_EXECUTE_FILE_TRAJECTORY = 21,
    MANUAL_4_SAT_EXECUTE_FILE_TRAJECTORY_STOP = 22,
	MANUAL_2_ROBOTIC_ARM_GO_TO_REQUESTED_POS = 23,
	MANUAL_2_ROBOTIC_ARM_GO_TO_REQUESTED_POS_STOP = 24,
	MANUAL_3_ROBOTIC_ARM_EXECUTE_TRAJECTORY = 25,
	MANUAL_3_ROBOTIC_ARM_EXECUTE_TRAJECTORY_STOP = 26,
	MANUAL_4_ROBOTIC_ARM_EXECUTE_FILE_TRAJECTORY = 27,
	MANUAL_4_ROBOTIC_ARM_EXECUTE_FILE_TRAJECTORY_STOP = 28,
	MANUAL_1_RUN_ALL_JOINT_TEST = 29,
	// Vision system commands
	VS_SATELLITE_MARKER_DATA = 30,
	START_READ_ENC2_FROM_ELASTIC_JOINT = 31,
	STOP_READ_ENC2_FROM_ELASTIC_JOINT = 32,
    MANUAL_1_QUASI_TRAJECTORY_START = 33,
    MANUAL_1_QUASI_TRAJECTORY_STOP = 34,
	//Gripper commands
	GRIPPER_COMMAND = 40,
	//Sense Module
	SENSE_MODULE_ON_CMD = 41,
	SENSE_MODULE_OFF_CMD = 42,
	//Arm test type
	KINEMATIC_JAC_WITHOUT_F = 43,
	DYNAMIC_JAC = 44,
	KINEMATIC_JAC_WITH_F = 45,
	OVF_ALGORITHM = 46,
	SATELLITE_CONTROL = 47,
	ADAPTIVE_BACK = 48,
	TORQUE_TRAJECTORY = 49,
	// Test Commands
	COLD_GAS_VALVE_TEST_MSG = 50,
    // Diagnostic commands: 150 - 180
	DIAGNOSTIC_TTYS1_PORT = 150,
    DIAGNOSTICK_DATA = 151,
    RUN_DRIVE_RIGHT = 152,
    RUN_DRIVE_LEFT = 153,

    LOGINFO_STRING = 160,
    // System self-test commands: 181-210
    INTERNAL_ERROR = 181,
    HARDWARE_HEALTH_CHECK_DONE = 185,
    // Init cmd
    SYSTEM_INIT_CMD = 255
};

enum FileUploadCmd
{
	// Basic commands
	FILE_UPLOAD_ACK = 0,
	FILE_UPLOAD_MODE_ENABLE = 1,
	FILE_UPLOAD_DATA_TRANSFER = 2,
	FILE_UPLOAD_PROCESS_STATUS = 3,
	FILE_UPLOAD_DONE = 4,
};

enum FileModeTypEnum
{
	CARTESIAN = 0,
	QUASI = 1,
};



#endif /* GCS_PROTOCOL_H_ */
