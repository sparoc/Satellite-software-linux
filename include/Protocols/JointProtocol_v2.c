/*
 * JointProtocol.c
 *
 *  Created on: May 30, 2016
 *      Author: ubuntu
 */

#include "JointProtocol_v2.h"

CanFeadback sJoint_1_state;
CanFeadback sJoint_2_state;
CanFeadback sJoint_3_state;

extern void ProcessJointCommand(void) {
	CbkCanId canId;
	GetCanId(&canId, sCan0Frame.RxFrame.can_id);

	if (canId.MsgId == CAN_FB_CURRENT) {
		if (canId.SourceId == JOINT_1_ID) {
			sJoint_1_state.Current = unpackFloat(sCan0Frame.RxFrame.data);
		} else if (canId.SourceId == JOINT_2_ID) {
			sJoint_2_state.Current = unpackFloat(sCan0Frame.RxFrame.data);
		} else if (canId.SourceId == JOINT_3_ID) {
			sJoint_3_state.Current = unpackFloat(sCan0Frame.RxFrame.data);
		}
	} else if (canId.MsgId == CAN_FB_ENCODER_1) {
		if (canId.SourceId == JOINT_1_ID) {
			sJoint_1_state.Encoder_1 = unpackFloat(sCan0Frame.RxFrame.data);
			sAppState.Joint1CurrPosition = sJoint_1_state.Encoder_1;
			sAppState.JointResponseState |= JOINT_1_RESPONDED;
		} else if (canId.SourceId == JOINT_2_ID) {
			sJoint_2_state.Encoder_1 = unpackFloat(sCan0Frame.RxFrame.data);
			sAppState.Joint2CurrPosition = sJoint_2_state.Encoder_1;
			sAppState.JointResponseState |= JOINT_2_RESPONDED;
		} else if (canId.SourceId == JOINT_3_ID) {
			sJoint_3_state.Encoder_1 = unpackFloat(sCan0Frame.RxFrame.data);
			sAppState.Joint3CurrPosition = sJoint_3_state.Encoder_1;
			sAppState.JointResponseState |= JOINT_3_RESPONDED;
		}
	} else if (canId.MsgId == CAN_HK_JOINT) {
		if (canId.SourceId == JOINT_1_ID) {
			sJoint_1_state.Encoder_1 = unpackFloat(sCan0Frame.RxFrame.data);
			sAppState.Joint1CurrPosition = sJoint_1_state.Encoder_1;
			sAppState.JointResponseState |= JOINT_1_RESPONDED;
			//printf("HK read: %f\n", sJoint_1_state.Encoder_1);
			//puts("HK read1");
		} else if (canId.SourceId == JOINT_2_ID) {
			sJoint_2_state.Encoder_1 = unpackFloat(sCan0Frame.RxFrame.data);
			sAppState.Joint2CurrPosition = sJoint_2_state.Encoder_1;
			sAppState.JointResponseState |= JOINT_2_RESPONDED;
		} else if (canId.SourceId == JOINT_3_ID) {
			sJoint_3_state.Encoder_1 = unpackFloat(sCan0Frame.RxFrame.data);
			sAppState.Joint3CurrPosition = sJoint_3_state.Encoder_1;
			sAppState.JointResponseState |= JOINT_3_RESPONDED;
		}
	} else if (canId.MsgId == CAN_FB_ENCODER_2) {
		if (canId.SourceId == JOINT_1_ID) {
			sJoint_1_state.Encoder_2 = unpackFloat(sCan0Frame.RxFrame.data);
		} else if (canId.SourceId == JOINT_2_ID) {
			sJoint_2_state.Encoder_2 = unpackFloat(sCan0Frame.RxFrame.data);
		} else if (canId.SourceId == JOINT_3_ID) {
			sJoint_3_state.Encoder_2 = unpackFloat(sCan0Frame.RxFrame.data);
		}
	} else if (canId.MsgId == CAN_FB_SPEED) {
		if (canId.SourceId == JOINT_1_ID) {
			sJoint_1_state.Speed = unpackFloat(sCan0Frame.RxFrame.data);
		} else if (canId.SourceId == JOINT_2_ID) {
			sJoint_2_state.Speed = unpackFloat(sCan0Frame.RxFrame.data);
		} else if (canId.SourceId == JOINT_3_ID) {
			sJoint_3_state.Speed = unpackFloat(sCan0Frame.RxFrame.data);
		}
	} else if (canId.MsgId == CAN_FB_TORQUE) {
		if (canId.SourceId == JOINT_1_ID) {
			sJoint_1_state.Torque = unpackFloat(sCan0Frame.RxFrame.data);
		} else if (canId.SourceId == JOINT_2_ID) {
			sJoint_2_state.Torque = unpackFloat(sCan0Frame.RxFrame.data);
		} else if (canId.SourceId == JOINT_3_ID) {
			sJoint_3_state.Torque = unpackFloat(sCan0Frame.RxFrame.data);
		}
	} else if (canId.MsgId == CAN_FB_DRIVE_SIGNAL) {
		if (canId.SourceId == JOINT_1_ID) {
			sJoint_1_state.Drive_signal = (sCan0Frame.RxFrame.data[0])
					| ((int16_t) sCan0Frame.RxFrame.data[1] << 8);
		} else if (canId.SourceId == JOINT_2_ID) {
			sJoint_2_state.Speed = (sCan0Frame.RxFrame.data[0])
					| ((int16_t) sCan0Frame.RxFrame.data[1] << 8);
		} else if (canId.SourceId == JOINT_3_ID) {
			sJoint_3_state.Speed = (sCan0Frame.RxFrame.data[0])
					| ((int16_t) sCan0Frame.RxFrame.data[1] << 8);
		}
	} else if (canId.MsgId == CAN_FB_STATE) {
		if (canId.SourceId == JOINT_1_ID) {
			sJoint_1_state.State = (sCan0Frame.RxFrame.data[0])
					| ((int16_t) sCan0Frame.RxFrame.data[1] << 8);
		} else if (canId.SourceId == JOINT_2_ID) {
			sJoint_2_state.State = (sCan0Frame.RxFrame.data[0])
					| ((int16_t) sCan0Frame.RxFrame.data[1] << 8);
		} else if (canId.SourceId == JOINT_3_ID) {
			sJoint_3_state.State = (sCan0Frame.RxFrame.data[0])
					| ((int16_t) sCan0Frame.RxFrame.data[1] << 8);
		}
	}
}

void canRequestFeedback(unsigned char id, unsigned char can_msg_type,
		unsigned char feedback_type) {
	sCan0Frame.CanId.MsgId = can_msg_type;
	sCan0Frame.CanId.SourceId = 0x00;
	sCan0Frame.CanId.ReceiverId = id;
	sCan0Frame.CanId.ExtraArg = 0;
	sCan0Frame.DLC = 1;

	sCan0Frame.TxData[0] = feedback_type;

	BuildCan0TxFrame(&sCan0Frame);
	SendCan0TxFrame();
}

void canMotorEnable(unsigned char id, unsigned char can_msg_type) {
	sCan0Frame.CanId.MsgId = can_msg_type;
	sCan0Frame.CanId.SourceId = 0x00;
	sCan0Frame.CanId.ReceiverId = id;
	sCan0Frame.CanId.ExtraArg = 0;
	sCan0Frame.DLC = 0;

	BuildCan0TxFrame(&sCan0Frame);
	SendCan0TxFrame();
}

void canSetReferencValue(unsigned char id, unsigned char can_msg_type, float value) {
	sCan0Frame.CanId.MsgId = can_msg_type;
	sCan0Frame.CanId.SourceId = 0x00;
	sCan0Frame.CanId.ReceiverId = id;
	sCan0Frame.CanId.ExtraArg = 0;
	sCan0Frame.DLC = 4;

	unsigned char temp[4];
	packFloat(temp,value);

	sCan0Frame.TxData[0] = temp[0];
	sCan0Frame.TxData[1] = temp[1];
	sCan0Frame.TxData[2] = temp[2];
	sCan0Frame.TxData[3] = temp[3];

	BuildCan0TxFrame(&sCan0Frame);
	SendCan0TxFrame();
}

void setMode(unsigned char id, unsigned char arg, float pos, float vel){
	;
}

void InitJointProtocolMembers(void) {
// For future use

}

float unpackFloat(uint8_t *buf) {
	union {
		float a;
		unsigned char bytes[4];
	} thing;

	thing.bytes[0] = buf[0];
	thing.bytes[1] = buf[1];
	;
	thing.bytes[2] = buf[2];
	;
	thing.bytes[3] = buf[3];
	;

	return thing.a;
}

void packFloat(uint8_t *buf, float x) {
	union {
		float a;
		unsigned char bytes[4];
	} thing;
	thing.a = x;

	buf[0] = thing.bytes[0];
	buf[1] = thing.bytes[1];
	buf[2] = thing.bytes[2];
	buf[3] = thing.bytes[3];
}

