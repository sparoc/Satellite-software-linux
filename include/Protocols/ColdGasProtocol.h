/*
 * ColdGasProtocol.h
 *
 *  Created on: Sep 25, 2018
 *      Author: ubuntu
 */

#ifndef INCLUDE_PROTOCOLS_COLDGASPROTOCOL_H_
#define INCLUDE_PROTOCOLS_COLDGASPROTOCOL_H_


enum ColdGasMode
{
	SINGLE_PULSE = 0x00,
	MULTIPLE_PULSES = 0x01,
	CONTINOUS_PULSES = 0x02,
};


#endif /* INCLUDE_PROTOCOLS_COLDGASPROTOCOL_H_ */
