#include "GCSComTxProtocol.h"

unsigned char m_arrGCSSenderTempArray[GCS_MAX_FRAME_SIZE] __attribute__ ((aligned (4)));

int GetGCSFrameToSend(int a_InputSize)
{
	a_InputSize += AddCrcToGCSFrame(a_InputSize);
	int nEscapeByte = CountOfGCSEscapeByte(a_InputSize);

	if (nEscapeByte == 0)
	{
		a_InputSize += 2; // +2 because 2 SYNCH_BYTE
		m_arrGCSSenderTempArray[0] = SYNCH_BYTE;
		int i;
		for (i = 0; i < a_InputSize; i++)
			m_arrGCSSenderTempArray[i + 1] = sGCSFrame.TxRawBuffer[i];

		m_arrGCSSenderTempArray[a_InputSize - 1] = SYNCH_BYTE;

		for(i = 0; i < a_InputSize; i++)
			sGCSFrame.TxRawBuffer[i] = m_arrGCSSenderTempArray[i];
	}
	else
	{
		int nEscByteCount = 0;

		int i;
		for (i = 0; i < a_InputSize; i++)
		{
			if( sGCSFrame.TxRawBuffer[i] == SYNCH_BYTE || sGCSFrame.TxRawBuffer[i] == ESCAPE_BYTE )
			{
				m_arrGCSSenderTempArray[i + nEscByteCount] = ESCAPE_BYTE;
				m_arrGCSSenderTempArray[i + 1 + nEscByteCount] = GetGCSEscapeByte(sGCSFrame.TxRawBuffer[i]);
				nEscByteCount++;
			}
			else
			{
				m_arrGCSSenderTempArray[i + nEscByteCount] = sGCSFrame.TxRawBuffer[i];
			}
		}
		a_InputSize += nEscByteCount;

		a_InputSize += 2; // +2 because 2 SYNCH_BYTE
		sGCSFrame.TxRawBuffer[0] = SYNCH_BYTE;
		for (i = 0; i < a_InputSize; i++) // 2 SYNCH_BYTE
			sGCSFrame.TxRawBuffer[i + 1] = m_arrGCSSenderTempArray[i];

		sGCSFrame.TxRawBuffer[a_InputSize - 1] = SYNCH_BYTE;
	}

	return a_InputSize;
}

int AddCrcToGCSFrame(int a_InputSize)
{
     int nCrcValue = CalculateCrc(sGCSFrame.TxCmdBuffer, 0, a_InputSize);

     sGCSFrame.TxRawBuffer[a_InputSize] = nCrcValue >> 0;
     sGCSFrame.TxRawBuffer[a_InputSize + 1] = nCrcValue >> 8;

     int i;
     for (i = 0; i < a_InputSize; i++)
    	 sGCSFrame.TxRawBuffer[i] = sGCSFrame.TxCmdBuffer[i];

     return 2; // We added 2 CRC bytes to the frame
}

int CountOfGCSEscapeByte(int a_size)
{
    int nEscapeByte = 0;
    int i;
    for (i = 0; i < a_size; i++)
    {
        if( sGCSFrame.TxRawBuffer[i] == SYNCH_BYTE || sGCSFrame.TxRawBuffer[i] == ESCAPE_BYTE )
            nEscapeByte++;
    }
    return nEscapeByte;
}

unsigned char GetGCSEscapeByte(unsigned char a_bValue)
{
     if (a_bValue == SYNCH_BYTE)
         return SYNCH_BYTE ^ 0x20;
     else
         return ESCAPE_BYTE ^ 0x20;
}

