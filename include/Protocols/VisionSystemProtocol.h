/*
 * VisionSystemProtocol.h
 *
 *  Created on: Nov 26, 2018
 *      Author: ubuntu
 */

#ifndef INCLUDE_PROTOCOLS_VISIONSYSTEMPROTOCOL_H_
#define INCLUDE_PROTOCOLS_VISIONSYSTEMPROTOCOL_H_

#define VS_ONBOARD_POINTS	30
#define VS_MARKER_NUMBER	3
#define VS_SATEL_MARKER		0
#define VS_MANIP_MARKER		1
#define VS_TARGET_MARKER	2


typedef struct
{
	int PositionX;		// 10000 * [m]
	int PositionY;		// 10000 * [m]
	int OrientationZ;	// 10 * [deg]
	float PosXcog;		// [m]
	float PosYcog;		// [m]
	float OrientZcog;	// [deg]
	short MarkerId;
	short Time;			// Not used

}VisionSystem;

typedef struct
{
	float X;		// [m]
	float Y;		// [m]
	float Z;		// [m]
}OnBoardVisionSystem;

extern VisionSystem sVisionSystem[VS_MARKER_NUMBER];
extern OnBoardVisionSystem sOnBoardVS[VS_ONBOARD_POINTS];

#endif /* INCLUDE_PROTOCOLS_VISIONSYSTEMPROTOCOL_H_ */
