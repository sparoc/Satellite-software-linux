#include "ImuComReceiver.h"

int gRxImuByteCounter = 0;
int gRxImuSynchByteCounter = 0;		// 0x7E (Hex), 126 (Dec)
unsigned char gImuStripedPackage[INS_MAX_FRAME_SIZE];

void ParseImuFrame(unsigned char a_bByte)
{
	// 0x7E (Hex) = 126 (Dec)
	if( a_bByte == 126 )
	{
		sImuFrame.RxRawFrame[gRxImuByteCounter] = a_bByte;
		gRxImuByteCounter++;
		gRxImuSynchByteCounter++;
	}
	else if( gRxImuByteCounter != 0 )
	{
		sImuFrame.RxRawFrame[gRxImuByteCounter] = a_bByte;
		gRxImuByteCounter++;
		if(gRxImuByteCounter > INS_MAX_FRAME_SIZE)
		{
			gRxImuByteCounter = 0;
			gRxImuSynchByteCounter = 0;
		}
	}
	else if( gRxImuByteCounter == 0 )
	{
		return;
	}
	if( gRxImuSynchByteCounter == 2 )
	{
		if(gRxImuByteCounter >= INS_MIN_FRAME_SIZE)
		{
			//StripGCSPackage(gRxImuByteCounter);
			// Strip the frame from synch bytes(0x7E) and un-escap bytes(7D 5E or 7D 5D).
			//First we have to strip package because CRC also can be masked
			int nSynchByte = 0;		// 0x7E
			int nEscapeByte = 0;	// 0x7D
			int nRealSize = 0;
			int nIndex = 0;
			int i;
			for(i = 0; i < gRxImuByteCounter; i++ )
			{
				if( nSynchByte == 2 )
					continue;
				if( sImuFrame.RxRawFrame[i] == 0x7E )
					nSynchByte++;
				if( sImuFrame.RxRawFrame[i] == 0x7D )
					nEscapeByte++;
				nRealSize++;
			}

			for(i = 0; i < nRealSize; i++ )
			{
				if( sImuFrame.RxRawFrame[i] != 0x7E )
				{
					if( sImuFrame.RxRawFrame[i] == 0x7D && sImuFrame.RxRawFrame[i+1] == 0x5E )
					{
						gImuStripedPackage[nIndex] = 0x7E;
						nIndex++;
					}
					else if( sImuFrame.RxRawFrame[i] == 0x7D && sImuFrame.RxRawFrame[i+1] == 0x5D )
					{
						gImuStripedPackage[nIndex] = 0x7D;
						nIndex++;
					}
					else if( sImuFrame.RxRawFrame[i-1] != 0x7E && sImuFrame.RxRawFrame[i-1] != 0x7D )
					{
						gImuStripedPackage[nIndex] = sImuFrame.RxRawFrame[i];
						nIndex++;
					}
					else if( i == 1  )
					{
						gImuStripedPackage[nIndex] = sImuFrame.RxRawFrame[i];
						nIndex++;
					}
				}
			}

			if(CheckCrc(gImuStripedPackage, 0, nIndex - 2)) // '-2' because 2 bytes are CRC bytes
			{
				for (i = 0; i < nIndex - 2; i++)
					sImuFrame.RxCmdBuffer[i] = gImuStripedPackage[i];

				sImuFrame.RxCmdBufferLenght = nIndex - 2;

				gRxImuByteCounter = 0;
				gRxImuSynchByteCounter = 0;
				sImuFrame.IsFrameReady = 1;
			}
			else
			{
				sImuFrame.RxRawFrame[0] = SYNCH_BYTE;
				gRxImuByteCounter = 1;
				gRxImuSynchByteCounter = 1;
			}
		}
		else
		{
			sImuFrame.RxRawFrame[0] = SYNCH_BYTE;
			gRxImuByteCounter = 1;
			gRxImuSynchByteCounter = 1;
		}
	}
}

