#include "GCSProtocol.h"

GCSFrame sGCSFrame;
FileUpload sFileUpload;

int gLanSocket;
unsigned char gGCSCommand;
int gRxCount = 0;
char gProtBuffer[256];

void ProcessGCSCommand(void) {
	gGCSCommand = sGCSFrame.RxCmdBuffer[0];

	if (gGCSCommand == DIAGNOSTICK_DATA) {
		gRxCount += ConvertToLSBInt(sGCSFrame.RxCmdBuffer, 1);

		int startIndex = 1;
		sGCSFrame.TxCmdBuffer[0] = DIAGNOSTICK_DATA;
		startIndex = PutIntIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, gRxCount);

		short size = GetGCSFrameToSend(startIndex);
		RS0WriteBuffer(gRS0fd, sGCSFrame.TxRawBuffer, size);
	} else if (gGCSCommand == TRIGGER_PD30_ENABLE) {
		EnableGpio(sGCSFrame.RxCmdBuffer[1], sGCSFrame.RxCmdBuffer[2]);
	} else if (gGCSCommand == TRIGGER_PD30_STATE) {
		SetGpioState(sGCSFrame.RxCmdBuffer[1], sGCSFrame.RxCmdBuffer[2]);
	} else if (gGCSCommand == TRIGGER_PD30_DISABLE) {
		DisableGpio(sGCSFrame.RxCmdBuffer[1]);
	} else if (gGCSCommand == VS_SATELLITE_MARKER_DATA) {
		int size = sGCSFrame.RxCmdBuffer[1];

		if (size == 18) {
			// Marker #1
			sVisionSystem[VS_SATEL_MARKER].MarkerId = sGCSFrame.RxCmdBuffer[2];
			sVisionSystem[VS_SATEL_MARKER].PositionX = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 3);
			sVisionSystem[VS_SATEL_MARKER].PositionY = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 5);
			sVisionSystem[VS_SATEL_MARKER].OrientationZ = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 7);
			sVisionSystem[VS_SATEL_MARKER].Time = g10msClock * 10;
			// Marker #2
			sVisionSystem[VS_MANIP_MARKER].MarkerId = sGCSFrame.RxCmdBuffer[9];
			sVisionSystem[VS_MANIP_MARKER].PositionX = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 10);
			sVisionSystem[VS_MANIP_MARKER].PositionY = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 12);
			sVisionSystem[VS_MANIP_MARKER].OrientationZ = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 14);
			sVisionSystem[VS_MANIP_MARKER].Time = g10msClock * 10;
			/*LogInfo("M1: %d\tPX: %d\tPY: %d\tR1: %d",
			 sVisionSystem[VS_SATEL_MARKER].MarkerId,
			 sVisionSystem[VS_SATEL_MAsVisionSystemRKER].PositionX,
			 sVisionSystem[VS_SATEL_MARKER].PositionY,
			 sVisionSystem[VS_SATEL_MARKER].OrientationZ);
			 LogInfo("M2: %d\tPX: %d\tPY: %d\tR2: %d",
			 sVisionSystem[VS_MANIP_MARKER].MarkerId,
			 sVisionSystem[VS_MANIP_MARKER].PositionX,
			 sVisionSystem[VS_MANIP_MARKER].PositionY,
			 sVisionSystem[VS_MANIP_MARKER].OrientationZ);*/
		} else if (size == 27) {
			// Marker #1
			sVisionSystem[VS_SATEL_MARKER].MarkerId = sGCSFrame.RxCmdBuffer[2];
			sVisionSystem[VS_SATEL_MARKER].PositionX = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 3);
			sVisionSystem[VS_SATEL_MARKER].PositionY = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 5);
			sVisionSystem[VS_SATEL_MARKER].OrientationZ = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 7);
			sVisionSystem[VS_SATEL_MARKER].Time = g10msClock * 10;
			// Marker #2
			sVisionSystem[VS_MANIP_MARKER].MarkerId = sGCSFrame.RxCmdBuffer[9];
			sVisionSystem[VS_MANIP_MARKER].PositionX = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 10);
			sVisionSystem[VS_MANIP_MARKER].PositionY = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 12);
			sVisionSystem[VS_MANIP_MARKER].OrientationZ = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 14);
			sVisionSystem[VS_MANIP_MARKER].Time = g10msClock * 10;
			// Marker #3
			sVisionSystem[VS_TARGET_MARKER].MarkerId = sGCSFrame.RxCmdBuffer[16];
			sVisionSystem[VS_TARGET_MARKER].PositionX = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 17);
			sVisionSystem[VS_TARGET_MARKER].PositionY = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 19);
			sVisionSystem[VS_TARGET_MARKER].OrientationZ = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 21);
			sVisionSystem[VS_TARGET_MARKER].Time = g10msClock * 10;
		}
		//fuseVS();
	} else if (gGCSCommand == VS_DATA_FROM_ONBOARD_CAMERAS) {
		for (int i = 0; i < 30; i++) {
			sOnBoardVS[i].X = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 1 + (i * 12));
			sOnBoardVS[i].Y = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 5 + (i * 12));
			sOnBoardVS[i].Z = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 9 + (i * 12));

			if (sOnBoardVS[i].X == 255 && sOnBoardVS[i].Y == 255 && sOnBoardVS[i].Z == 255)
				return;
		}
	} else if (gGCSCommand == MANUAL_1_REQUEST_JOINTS_POSITION) {
		Manual1ReadJointPosition();
		sAppState.JointResponseTimeout = (g10msClock + JOINT_RESPONDED_TIMEOUT);
		//sAppState.JointRequestSent = 1;
	} else if (gGCSCommand == MANUAL_1_RUN_JOINT_1_TEST) {
		sAppState.RunManualTest1 = 0;
		sAppState.RunManualJointIndex = 0;

		float qIni = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 1);	// [deg]
		float qFin = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 5);	// [deg]
		float Tr = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 9);
		float Tp = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 13);
		float Th = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 17);
		int freq = ConvertToLSBInt(sGCSFrame.RxCmdBuffer, 21);

		sAppState.RunManualTest1 = GenerateJointTrajectory(sAppState.RunManualJointIndex, qIni, qFin, Tr, Tp, Th, freq);
		canMotorEnable(JOINT_1_ID, CAN_MSG_START);
		puts("SR: Trajectory started");
	} else if (gGCSCommand == MANUAL_1_RUN_JOINT_2_TEST) {
		sAppState.RunManualTest1 = 0;
		sAppState.RunManualJointIndex = 1;

		float qIni = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 1);	// [deg]
		float qFin = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 5);	// [deg]
		float Tr = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 9);
		float Tp = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 13);
		float Th = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 17);
		int freq = ConvertToLSBInt(sGCSFrame.RxCmdBuffer, 21);

		sAppState.RunManualTest1 = GenerateJointTrajectory(sAppState.RunManualJointIndex, qIni, qFin, Tr, Tp, Th, freq);
		canMotorEnable(JOINT_2_ID, CAN_MSG_START);
		puts("SR: Trajectory started");
	} else if (gGCSCommand == MANUAL_1_RUN_JOINT_3_TEST) {
		sAppState.RunManualTest1 = 0;
		sAppState.RunManualJointIndex = 2;

		float qIni = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 1);	// [deg]
		float qFin = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 5);	// [deg]
		float Tr = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 9);
		float Tp = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 13);
		float Th = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 17);
		int freq = ConvertToLSBInt(sGCSFrame.RxCmdBuffer, 21);

		sAppState.RunManualTest1 = GenerateJointTrajectory(sAppState.RunManualJointIndex, qIni, qFin, Tr, Tp, Th, freq);
		canMotorEnable(JOINT_3_ID, CAN_MSG_START);
		puts("SR: Trajectory started");
	} else if (gGCSCommand == MANUAL_1_RUN_ALL_JOINT_TEST) {
		sAppState.RunManualTest1 = 0;

		float qIniJ1 = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 1);	// [deg]
		float qFinJ1 = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 5);	// [deg]
		float qIniJ2 = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 9);	// [deg]
		float qFinJ2 = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 13);	// [deg]
		float qIniJ3 = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 17);	// [deg]
		float qFinJ3 = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 21);	// [deg]
		float Tr = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 25);
		float Tp = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 29);
		float Th = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 33);
		int freq = ConvertToLSBInt(sGCSFrame.RxCmdBuffer, 37);

		printf("Init1 %f \n", qIniJ1);
		printf("Fin1 %f \n", qFinJ1);

		printf("Init2 %f \n", qIniJ2);
		printf("Fin2 %f \n", qFinJ2);

		printf("Init3 %f \n", qIniJ3);
		printf("Fin3 %f \n", qFinJ3);

		int res1 = GenerateJointTrajectory(0, qIniJ1, qFinJ1, Tr, Tp, Th, freq);
		int res2 = GenerateJointTrajectory(1, qIniJ2, qFinJ2, Tr, Tp, Th, freq);
		int res3 = GenerateJointTrajectory(2, qIniJ3, qFinJ3, Tr, Tp, Th, freq);

		if (res1 && res2 && res3) {
			sAppState.RunManualTest1All = 1;
			canMotorEnable(JOINT_ALL, CAN_MSG_START);
			puts("SR: Trajectory for all joints started");
		} else {
			LogError("%d. Joint trajectories for joints failed", 1);
		}
	} else if (gGCSCommand == MANUAL_2_REQUEST_SATELLITE_POSITION) {
		SendManual2TestDataToGCS();
	} else if (gGCSCommand == MANUAL_2_SAT_GO_TO_REQUESTED_POS) {
		sSatCtrl.PositionXReq = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 1);		// [m]
		sSatCtrl.PositionYReq = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 5);		// [m]
		sSatCtrl.OrientationZReq = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 9) * SAT_DEG_TO_RAD;	// [rad]

		puts("SR: Satellite requested trajectory started");
		sAppState.RunManualTest2 = 1;
	} else if (gGCSCommand == MANUAL_2_SAT_GO_TO_REQUESTED_POS_STOP) {
		puts("SR: Satellite requested trajectory manually stopped");
		sAppState.RunManualTest2 = 0;
	} else if (gGCSCommand == MANUAL_2_ROBOTIC_ARM_GO_TO_REQUESTED_POS) {
		sSatCtrl.PositionXReq = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 1);		// [m]
		sSatCtrl.PositionYReq = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 5);		// [m]
		// Not used
		sSatCtrl.OrientationZReq = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 9);	// [deg]

		int size = 300;
		InitSatelliteFileTrajectory(size);

		for (int i = 0; i < size; i++) {
			UploadSatelliteFileTrajectory(sSatCtrl.PositionXReq, sSatCtrl.PositionYReq, sSatCtrl.OrientationZReq);
		}

		gSatTrajectoryIndex = 0;

		ReadJointPosition();
		sAppState.JointResponseTimeout = (g10msClock + JOINT_RESPONDED_TIMEOUT);
		canMotorEnable(JOINT_ALL, CAN_MSG_START);
		puts("SR: Robotic arm cartesian trajectory started.");
		sAppState.RunManualTest2RA = 1;
	} else if (gGCSCommand == MANUAL_2_ROBOTIC_ARM_GO_TO_REQUESTED_POS_STOP) {
		canMotorEnable(JOINT_ALL, CAN_MSG_STOP);
		puts("SR: Robotic arm requested trajectory manually stopped");
		sAppState.RunManualTest2RA = 0;
	} else if (gGCSCommand == COLD_GAS_VALVE_TEST_MSG) {
		RunColdGasValveTestCmd();
	} else if (gGCSCommand == MANUAL_DIRECT_PWM_CONTROL) {
		unsigned char jointID = sGCSFrame.RxCmdBuffer[1];
		short pwm = ConvertToLSBShort(sGCSFrame.RxCmdBuffer, 2);
		float pwm_f = ((float) pwm) * 0.04;
		canMotorEnable(JOINT_ALL, CAN_MSG_START);
		canSetReferencValue(jointID, CAN_SET_CURRENT, pwm_f);
		printf("PWM %f\n", pwm_f);
		puts("WinApp: Direct PWM control.");
	} else if (gGCSCommand == MANUAL_3_SAT_EXECUTE_TRAJECTORY) {
		sAppState.RunManualTest3 = 0;

		float xIni = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 1);			// [m]
		float xFin = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 5);			// [m]
		float yIni = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 9);			// [m]
		float yFin = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 13);			// [m]
		float orientIni = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 17);	// [deg]
		float orientFin = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 21);	// [deg]
		float Tr = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 25);
		float Tp = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 29);
		float Th = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 33);
		int freq = sGCSFrame.RxCmdBuffer[37];								// [Hz]

		int xResult = GenerateSatelliteTrajectory(0, xIni, xFin, Tr, Tp, Th, freq);		// 0 - trajectory for x position
		int yResult = GenerateSatelliteTrajectory(1, yIni, yFin, Tr, Tp, Th, freq);		// 1 - trajectory for y position
		int orientResult = GenerateSatelliteTrajectory(2, orientIni, orientFin, Tr, Tp, Th, freq);		// 2 - trajectory for orientation

		if (xResult && yResult && orientResult) {
			sAppState.RunManualTest3 = 1;
			canMotorEnable(JOINT_ALL, CAN_MSG_START);
			puts("SR: Satellite trajectory started");
		}
	} else if (gGCSCommand == MANUAL_3_SAT_EXECUTE_TRAJECTORY_STOP) {
		sAppState.RunManualTest3 = 0;
		canMotorEnable(JOINT_ALL, CAN_MSG_STOP);
	} else if (gGCSCommand == MANUAL_3_ROBOTIC_ARM_EXECUTE_TRAJECTORY) {
		sAppState.RunManualTest3RA = 0;

		float xIni = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 1);			// [m]
		float xFin = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 5);			// [m]
		float yIni = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 9);			// [m]
		float yFin = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 13);			// [m]
		float orientIni = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 17);	// [deg]
		float orientFin = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 21);	// [deg]
		float Tr = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 25);
		float Tp = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 29);
		float Th = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 33);
		int freq = sGCSFrame.RxCmdBuffer[37];								// [Hz]

		int xResult = GenerateSatelliteTrajectory(0, xIni, xFin, Tr, Tp, Th, freq);		// 0 - trajectory for x position
		int yResult = GenerateSatelliteTrajectory(1, yIni, yFin, Tr, Tp, Th, freq);		// 1 - trajectory for y position
		int orientResult = GenerateSatelliteTrajectory(2, orientIni, orientFin, Tr, Tp, Th, freq);		// 2 - trajectory for orientation

		if (xResult && yResult && orientResult) {
			Manual1ReadJointPosition();
			sAppState.JointResponseTimeout = (g10msClock + JOINT_RESPONDED_TIMEOUT);
			sAppState.RunManualTest3RA = 1;
			canMotorEnable(JOINT_ALL, CAN_MSG_START);
			puts("SR: Robotic ARM trajectory started");
		}
	} else if (gGCSCommand == MANUAL_3_ROBOTIC_ARM_EXECUTE_TRAJECTORY_STOP) {
		sAppState.RunManualTest3RA = 0;
		canMotorEnable(JOINT_ALL, CAN_MSG_STOP);
	} else if (gGCSCommand == MANUAL_4_SAT_EXECUTE_FILE_TRAJECTORY) {
		if (sFileUpload.AllDataReceived) {
			sAppState.RunManualTest4 = 1;
			canMotorEnable(JOINT_ALL, CAN_MSG_START);
			puts("SR: Satellite trajectory from file started");
		} else {
			puts("SR: Satellite trajectory from file - file not received!");
		}
	} else if (gGCSCommand == MANUAL_4_SAT_EXECUTE_FILE_TRAJECTORY_STOP) {
		sAppState.RunManualTest4 = 0;
		canMotorEnable(JOINT_ALL, CAN_MSG_STOP);
	} else if (gGCSCommand == MANUAL_4_ROBOTIC_ARM_EXECUTE_FILE_TRAJECTORY) {
		if (sFileUpload.AllDataReceived) {
			Manual1ReadJointPosition();
			sAppState.JointResponseTimeout = (g10msClock + JOINT_RESPONDED_TIMEOUT);

			sAppState.RunManualTest4RA = 1;
			canMotorEnable(JOINT_ALL, CAN_MSG_START);
			puts("SR: Robotic Arm trajectory from file started");
		} else {
			puts("SR: Robotic Arm trajectory from file - file not received!");
		}
	} else if (gGCSCommand == MANUAL_4_ROBOTIC_ARM_EXECUTE_FILE_TRAJECTORY_STOP) {
		sAppState.RunManualTest4RA = 0;
		canMotorEnable(JOINT_ALL, CAN_MSG_STOP);
	} else if (gGCSCommand == FILE_UPLOAD) {
		ParseFileUploadFrame(sGCSFrame.RxCmdBuffer[1]);
	} else if (gGCSCommand == START_READ_ENC2_FROM_ELASTIC_JOINT) {
		sAppState.ReadEnc2FromElasticJoint = 1;
	} else if (gGCSCommand == STOP_READ_ENC2_FROM_ELASTIC_JOINT) {
		sAppState.ReadEnc2FromElasticJoint = 0;
	} else if (gGCSCommand == MANUAL_1_QUASI_TRAJECTORY_START) {
		puts("SR: Quasi trajectory started");
		sAppState.RunManualQuasiTraj = 1;
		canMotorEnable(JOINT_ALL, CAN_MSG_START);
	} else if (gGCSCommand == MANUAL_1_QUASI_TRAJECTORY_STOP) {
		puts("SR: Quasi trajectory stopped");
		canMotorEnable(JOINT_ALL, CAN_MSG_STOP);
		sAppState.RunManualQuasiTraj = 0;
	} else if (gGCSCommand == GRIPPER_COMMAND) {
		unsigned char gripper_mode = sGCSFrame.RxCmdBuffer[1];
		SetGripperState(gripper_mode);
	} else if (gGCSCommand == SENSE_MODULE_ON_CMD) {
		sAppState.SenseModule = SENSE_MODULE_ON;
		LogApp("Sense Module: ON");
	} else if (gGCSCommand == SENSE_MODULE_OFF_CMD) {
		sAppState.SenseModule = SENSE_MODULE_OFF;
		LogApp("Sense Module: OFF");
	} else if (gGCSCommand == KINEMATIC_JAC_WITHOUT_F) {
		sAppState.ControlAlgorithm = CONTROL_KINEMATIC_WITHOUT_FB;
		LogApp("Algorithm: Jakobian Kinematyczny bez sprzezenia");
	} else if (gGCSCommand == KINEMATIC_JAC_WITH_F) {
		sAppState.ControlAlgorithm = CONTROL_KINEMATIC_WITH_FB;
		LogApp("Algorithm: Jakobian Kinematyczny ze sprzezeniem");
	} else if (gGCSCommand == DYNAMIC_JAC) {
		sAppState.ControlAlgorithm = CONTROL_DYNAMIC;
		LogApp("Algorithm: Jakobian dynamiczny");
	} else if (gGCSCommand == OVF_ALGORITHM) {
		sAppState.ControlAlgorithm = CONTROL_OVF;
		LogApp("Algorithm: OVF");
	} else if (gGCSCommand == ADAPTIVE_BACK) {
		sAppState.ControlAlgorithm = CONTROL_ADAPTIVE_BACK;
		LogApp("Algorithm: Adaptive Back");
	}	 else if (gGCSCommand == TORQUE_TRAJECTORY) {
		sAppState.ControlAlgorithm = CONTROL_TORQUE_TRAJECTORY;
		LogApp("Algorithm: Torque Trajectory");
	}

}

char comBufferCan[256];
void RunColdGasValveTestCmd(void) {
	unsigned char mode = sGCSFrame.RxCmdBuffer[1];
	unsigned short period = ConvertToLSBUShort(sGCSFrame.RxCmdBuffer, 2);
	unsigned char pwmDuty = sGCSFrame.RxCmdBuffer[4];
	unsigned char impulses = sGCSFrame.RxCmdBuffer[5];
	unsigned char motor = sGCSFrame.RxCmdBuffer[6];

	sCan0Frame.CanId.MsgId = 0;
	sCan0Frame.CanId.SourceId = 0;
	sCan0Frame.CanId.ReceiverId = COLD_GAS_BOARD_ID;
	sCan0Frame.CanId.ExtraArg = 0;
	sCan0Frame.DLC = 8;

	sCan0Frame.DLC = 6;
	sCan0Frame.TxData[0] = motor;			// Motor: 1, 2, 4, 8, 16, 32, 64 or 128
	sCan0Frame.TxData[1] = mode;			// Mode: 0 - single pulse, 1 - number of pulse, 2 - continuous pulses
	sCan0Frame.TxData[2] = period >> 8;		// High byte of period [ms]
	sCan0Frame.TxData[3] = period;			// Low byte of period [ms]
	sCan0Frame.TxData[4] = pwmDuty;			// PWM duty: [%], Ranee: 0-90
	sCan0Frame.TxData[5] = impulses;		// Number of pulse: required number of pulse - used only in Mode: 1

	BuildCan0TxFrame(&sCan0Frame);
	SendCan0TxFrame();

	sprintf(comBufferCan, "CanId: %d\t[%d]: %d %d %d %d %d %d\n", sCan0Frame.CanId.ExtraArg, sCan0Frame.DLC, sCan0Frame.TxData[0], sCan0Frame.TxData[1], sCan0Frame.TxData[2], sCan0Frame.TxData[3], sCan0Frame.TxData[4], sCan0Frame.TxData[5]);
	perror(comBufferCan);
}

void RunColdGasValve(unsigned char valveId, unsigned char pwm, unsigned short period) {
	sCan0Frame.CanId.MsgId = 0;
	sCan0Frame.CanId.SourceId = 0;
	sCan0Frame.CanId.ReceiverId = COLD_GAS_BOARD_ID;
	sCan0Frame.CanId.ExtraArg = 0;
	sCan0Frame.DLC = 8;

	sCan0Frame.DLC = 6;
	sCan0Frame.TxData[0] = (unsigned char) pow(2, valveId);			// Motor: 1, 2, 4, 8, 16, 32, 64 or 128
	sCan0Frame.TxData[1] = 1;				// Mode: 0 - single pulse, 1 - number of pulse, 2 - continuous pulses
	sCan0Frame.TxData[2] = period >> 8;		// High byte of period [ms]
	sCan0Frame.TxData[3] = period;			// Low byte of period [ms]
	sCan0Frame.TxData[4] = pwm;				// PWM duty: [%], Ranee: 0-90
	sCan0Frame.TxData[5] = 1;				// Number of pulse: required number of pulse - used only in Mode: 1

	BuildCan0TxFrame(&sCan0Frame);
	SendCan0TxFrame();
	usleep(100);
}

void RunDrive(void) {
	static unsigned int data = 0;
	//sCan0Frame.CanId.IDE = CAN_STANDARD_ID;
	//sCan0Frame.CanId.ID = JOINT_1_ID;
	//sCan0Frame.CanId.RTR = 0x00;

	sCan0Frame.CanId.MsgId = 0x00;
	sCan0Frame.CanId.SourceId = 0x00;
	sCan0Frame.CanId.ReceiverId = 0x00;
	sCan0Frame.CanId.ExtraArg = JOINT_1_BOARD_ID;

	sCan0Frame.DLC = 8;
	sCan0Frame.TxData[0] = data;
	sCan0Frame.TxData[1] = data >> 8;
	sCan0Frame.TxData[2] = data >> 16;
	sCan0Frame.TxData[3] = data >> 24;
	sCan0Frame.TxData[4] = 0x00;
	sCan0Frame.TxData[5] = 0x00;
	sCan0Frame.TxData[6] = 0x00;
	sCan0Frame.TxData[7] = 0x00;

	data++;

	BuildCan0TxFrame(&sCan0Frame);
	SendCan0TxFrame();
}

/*void RunTest1(int position, int current, unsigned char extraArg)
 {
 sCan0Frame.CanId.MsgId = 0x10;
 sCan0Frame.CanId.SourceId = 0x07;
 sCan0Frame.CanId.ReceiverId = 0x03;
 sCan0Frame.CanId.ExtraArg = extraArg;
 sCan0Frame.DLC = 8;
 sCan0Frame.TxData[0] = position;
 sCan0Frame.TxData[1] = position >> 8;
 sCan0Frame.TxData[2] = position >> 16;
 sCan0Frame.TxData[3] = position >> 24;
 sCan0Frame.TxData[4] = current;
 sCan0Frame.TxData[5] = current >> 8;
 sCan0Frame.TxData[6] = current >> 16;
 sCan0Frame.TxData[7] = current >> 24;

 BuildCan0TxFrame(&sCan0Frame);
 SendCan0TxFrame();
 }*/

void SendDataToVS(float a_fValue1, float a_fValue2, float a_fValue3, float a_fValue4) {
	int startIndex = 1;
	sGCSFrame.TxCmdBuffer[0] = DIAGNOSTICK_DATA;

	startIndex = PutSingleIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, a_fValue1);
	startIndex = PutSingleIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, a_fValue2);
	startIndex = PutSingleIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, a_fValue3);
	startIndex = PutSingleIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, a_fValue4);

	short size = GetGCSFrameToSend(startIndex);
	RS0WriteBuffer(gRS0fd, sGCSFrame.TxRawBuffer, size);
}

void SendRS0DiagnosticData(CbkCanId *canId) {
	sGCSFrame.TxCmdBuffer[0] = DIAGNOSTIC_TTYS1_PORT;
	sGCSFrame.TxCmdBuffer[1] = (*canId).MsgId;
	sGCSFrame.TxCmdBuffer[2] = 0x00;
	sGCSFrame.TxCmdBuffer[3] = sCan0Frame.RxFrame.can_dlc;
	sGCSFrame.TxCmdBuffer[4] = sCan0Frame.RxFrame.data[0];
	sGCSFrame.TxCmdBuffer[5] = sCan0Frame.RxFrame.data[1];
	sGCSFrame.TxCmdBuffer[6] = sCan0Frame.RxFrame.data[2];
	sGCSFrame.TxCmdBuffer[7] = sCan0Frame.RxFrame.data[3];
	sGCSFrame.TxCmdBuffer[8] = sCan0Frame.RxFrame.data[4];
	sGCSFrame.TxCmdBuffer[9] = sCan0Frame.RxFrame.data[5];
	sGCSFrame.TxCmdBuffer[10] = sCan0Frame.RxFrame.data[6];
	sGCSFrame.TxCmdBuffer[11] = sCan0Frame.RxFrame.data[7];

	short size = GetGCSFrameToSend(12);
	RS0WriteBuffer(gRS0fd, sGCSFrame.TxRawBuffer, size);
}

void SendManual1TestDataToGCS(void) {
	int startIndex = 1;
	sGCSFrame.TxCmdBuffer[0] = MANUAL_1_RESPONSE_JOINTS_POSITION;

	// Position range" -180 : +180
	startIndex = PutSingleIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, sAppState.Joint1CurrPosition);
	startIndex = PutSingleIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, sAppState.Joint2CurrPosition);
	startIndex = PutSingleIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, sAppState.Joint3CurrPosition);
	// Warning. Error, CRC
	sGCSFrame.TxCmdBuffer[startIndex] = sJoint1Status.State.Estat1;
	sGCSFrame.TxCmdBuffer[++startIndex] = sJoint2Status.State.Estat1;
	sGCSFrame.TxCmdBuffer[++startIndex] = sJoint3Status.State.Estat1;

	short size = GetGCSFrameToSend(++startIndex);
	// Send a message to the LAN client
	send(gLanSocket, sGCSFrame.TxRawBuffer, size, 0);
	//RS0WriteBuffer(gRS0fd, sGCSFrame.TxRawBuffer, size);
}

extern float EEXPose, EEYPose, EEOrient;
extern float SatXPose, SatYPose, SatOrient;

void SendManual2TestDataToGCS(void) {
	int startIndex = 1;
	sGCSFrame.TxCmdBuffer[0] = MANUAL_2_REQUEST_SATELLITE_POSITION;

	startIndex = PutSingleIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, SatXPose);		// [m]
	startIndex = PutSingleIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, SatYPose);		// [m]
	startIndex = PutSingleIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, SatOrient);	// [rad]

	startIndex = PutSingleIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, EEXPose);		// [m]
	startIndex = PutSingleIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, EEYPose);		// [m]
	startIndex = PutSingleIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, EEOrient);	// [rad]

	short size = GetGCSFrameToSend(startIndex);

	// Send a message to the LAN client
	send(gLanSocket, sGCSFrame.TxRawBuffer, size, 0);
	//RS0WriteBuffer(gRS0fd, sGCSFrame.TxRawBuffer, size);
}

void SendCurrentJointPosition(unsigned char a_JointId, float a_Pos1, unsigned char a_EncError) {

}

void ParseFileUploadFrame(unsigned char a_Cmd) {
	// Step 1
	if (a_Cmd == FILE_UPLOAD_MODE_ENABLE) {
		sFileUpload.AllDataReceived = 0;
		sFileUpload.ReceivedLines = 0;
		sFileUpload.ModeType = sGCSFrame.RxCmdBuffer[2];
		sFileUpload.FileSizeInLines = ConvertToLSBInt(sGCSFrame.RxCmdBuffer, 3);

		if (sFileUpload.ModeType == QUASI) {
			InitQuasiFileTrajectory(sFileUpload.FileSizeInLines);	// 3 data * 4 bytes
		} else if (sFileUpload.ModeType == CARTESIAN) {
			InitSatelliteFileTrajectory(sFileUpload.FileSizeInLines);	// 3 data * 4 bytes
		}

		SendFileUploadMsgToGCS(FILE_UPLOAD_ACK);
	}
	// Step 2
	else if (a_Cmd == FILE_UPLOAD_DATA_TRANSFER) {
		float x, y, z;
		int receivedLines = (sGCSFrame.RxCmdBufferLenght - 2) / 12;
		sFileUpload.ReceivedLines += receivedLines;

		for (int i = 0; i < receivedLines; i++) {
			x = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 2 + (i * 12));
			y = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 6 + (i * 12));
			z = ConvertToLSBSingle(sGCSFrame.RxCmdBuffer, 10 + (i * 12));

			// x *= 186413.51111;
			// y *= 186413.51111;
			// z *= 186413.51111;

			if (sFileUpload.ModeType == QUASI) {
//				x *= 186413.51111;
//				y *= 186413.51111;
//				z *= 186413.51111;
				UploadQuasiFileTrajectory(x, y, z);
			} else if (sFileUpload.ModeType == CARTESIAN) {
				UploadSatelliteFileTrajectory(x, y, z);
			}
		}

		SendFileUploadMsgToGCS(FILE_UPLOAD_ACK);
	}
	// Step 3
	else if (a_Cmd == FILE_UPLOAD_DONE) {
		if (sFileUpload.FileSizeInLines == sFileUpload.ReceivedLines) {
			gSatTrajectoryIndex = 0;
			gJointTrajectoryIndex = 0;
			sFileUpload.AllDataReceived = 1;
			SendFileUploadMsgToGCS(FILE_UPLOAD_ACK);

			puts("SR: File upload succeed.");
		} else {
			// If we are here, that means file data are corrupted and we didn't send FILE_UPLOAD_ACK.
			// The host application waits 5000ms for receiving the FILE_UPLOAD_ACK command after the host sent FILE_UPLOAD_DONE command.
			// After that time host application will request FILE_UPLOAD_PROCESS_STATUS command.
		}
	}
	// Step 4 only if FILE_UPLOAD_ACK command was not sent after the FILE_UPLOAD_DONE command
	else if (a_Cmd == FILE_UPLOAD_PROCESS_STATUS) {
		SendFirmwareProcessStatus();
		perror("File upload failed");
	}
}

void SendFirmwareProcessStatus(void) {
	int startIndex = 3;
	sGCSFrame.TxCmdBuffer[0] = FILE_UPLOAD;
	sGCSFrame.TxCmdBuffer[1] = FILE_UPLOAD_PROCESS_STATUS;
	sGCSFrame.TxCmdBuffer[2] = sFileUpload.AllDataReceived;
	startIndex = PutIntIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, sFileUpload.FileSizeInLines);
	startIndex = PutIntIntoLSBArray(sGCSFrame.TxCmdBuffer, startIndex, sFileUpload.ReceivedLines);
	int size = GetGCSFrameToSend(startIndex);
	send(gLanSocket, sGCSFrame.TxRawBuffer, size, 0);
}

void SendFileUploadMsgToGCS(unsigned char a_eCmd) {
	sGCSFrame.TxCmdBuffer[0] = FILE_UPLOAD;
	sGCSFrame.TxCmdBuffer[1] = a_eCmd;
	short size = GetGCSFrameToSend(2);
	// Send a message to the LAN client
	send(gLanSocket, sGCSFrame.TxRawBuffer, size, 0);
}

void SendCmdToGCS(unsigned char a_eCmd) {
	sGCSFrame.TxCmdBuffer[0] = a_eCmd;
	short size = GetGCSFrameToSend(1);
	// Send a message to the LAN client
	send(gLanSocket, sGCSFrame.TxRawBuffer, size, 0);
}

void InitGCSMembers(void) {
	sGCSFrame.RxCmdBufferLenght = 0;
	sGCSFrame.IsFrameReady = 0;
}
