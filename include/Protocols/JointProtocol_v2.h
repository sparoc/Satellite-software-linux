/*
 * JointProtocol.h
 *
 *  Created on: May 30, 2016
 *      Author: ubuntu
 */

#ifndef INCLUDE_PROTOCOLS_JOINTPROTOCOL_V2_H_
#define INCLUDE_PROTOCOLS_JOINTPROTOCOL_V2_H_

#include "../drilibs/BitConvert/Convert.h"
#include "../Algorithm/AngleCalculation.h"
#include "../DataDefinition/DataStruct.h"
#include "../Sockets/SocketCAN.h"

#define JOINT_SET_MODE_MSG			0x10
#define JOINT_GET_STATE_MSG			0x42
#define JOINT_STATE_RESPONSE_MSG	0x43
#define JOINT_GET_PARAM_MSG			0x46
#define JOINT_PARAM_RESPONSE_MSG	0x47
#define JOINT_POS_RESPONDED			0x07
#define JOINT_POS_STATE_RESPONDED	0x3F
#define JOINT_RESPONDED_TIMEOUT		10		// 10 * 10ms == 100ms

typedef struct {
	int16_t Drive_signal;
	float Encoder_1;
	float Encoder_2;
	float Speed;
	float Current;
	float Torque;
	uint16_t State;
} CanFeadback;

enum FEEDBACK_TYPE {
	CAN_FEEDBACK_NONE, CAN_FEEDBACK_SINGLE, CAN_FEEDBACK_CONTINUOUS,
};

enum CAN_MSG_TYPE {
	CAN_MSG_STOP,
	CAN_MSG_START,
	CAN_SET_CURRENT,
	CAN_SET_POSITION,
	CAN_SET_SPEED,
	CAN_SET_TORQUE,
	CAN_GET_DRIVE_SIGNAL,
	CAN_GET_ENCODER_1,
	CAN_GET_ENCODER_2,
	CAN_GET_SPEED,
	CAN_GET_CURRENT,
	CAN_GET_TORQUE,
	CAN_GET_STATE,
	CAN_HK_JOINT,
	CAN_FB_DRIVE_SIGNAL,
	CAN_FB_ENCODER_1,
	CAN_FB_ENCODER_2,
	CAN_FB_SPEED,
	CAN_FB_CURRENT,
	CAN_FB_TORQUE,
	CAN_FB_STATE
};

enum JointsIdentification {
	JOINT_1_ID = JOINT_1_BOARD_ID,
	JOINT_2_ID = JOINT_2_BOARD_ID,
	JOINT_3_ID = JOINT_3_BOARD_ID,
	JOINT_ALL = BROADCAST_ID,
};

enum JointModes {
	CS_JM_PASSIVE = 0x00,
	CS_JM_INIT = 0x01,
	CS_JM_MANUAL = 0x02,
	CS_JM_NOMINAL = 0x03,
};

enum JointResponseEnum
{
	JOINT_1_RESPONDED = 0x01,
	JOINT_2_RESPONDED = 0x02,
	JOINT_3_RESPONDED = 0x04,
	JOINT_ENC_1_STATUS = 0x08,
	JOINT_ENC_2_STATUS = 0x10,
	JOINT_ENC_3_STATUS = 0x20,
};


extern CanFeadback sJoint_1_state;
extern CanFeadback sJoint_2_state;
extern CanFeadback sJoint_3_state;

extern void ProcessJointCommand(void);

extern void canMotorEnable(unsigned char id, unsigned char can_msg_type);
extern void canSetReferencValue(unsigned char id, unsigned char can_msg_type, float value);
extern void canRequestFeedback(unsigned char id, unsigned char can_msg_type,
		unsigned char feedback_type);

extern float unpackFloat(uint8_t *buf);
extern void packFloat(uint8_t *buf, float x);

extern void InitJointProtocolMembers(void);

extern void setMode(unsigned char id, unsigned char arg, float pos, float vel);

#endif /* INCLUDE_PROTOCOLS_JOINTPROTOCOL_H_ */
