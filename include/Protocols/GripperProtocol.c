/*
 * GripperProtocol.c
 *
 *  Created on: Jan 5, 2022
 *      Author: Konrad
 */

#include "GripperProtocol.h"


void SetGripperState(unsigned char arg)
{
	sCan0Frame.CanId.MsgId = 0;
	sCan0Frame.CanId.SourceId = 0x00;
	sCan0Frame.CanId.ReceiverId = GRIPPER_BOARD_ID;
	sCan0Frame.CanId.ExtraArg = 0;
	sCan0Frame.DLC = 1;
	sCan0Frame.TxData[0] = arg;

	BuildCan0TxFrame(&sCan0Frame);
	SendCan0TxFrame();
};
