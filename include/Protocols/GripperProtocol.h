/*
 * GripperProtocol.h
 *
 *  Created on: Jan 5, 2022
 *      Author: Konrad
 */

#ifndef INCLUDE_PROTOCOLS_GRIPPERPROTOCOL_H_
#define INCLUDE_PROTOCOLS_GRIPPERPROTOCOL_H_

#include "../drilibs/BitConvert/Convert.h"
#include "../Algorithm/AngleCalculation.h"
#include "../DataDefinition/DataStruct.h"
#include "../Sockets/SocketCAN.h"

enum GripperFunction
{
    GRIP_ARMED = 0,
	GRIP_CLOSE = 1,
	GRIP_OPEN = 2,
	GRIP_STOP = 5,
};

extern void SetGripperState(unsigned char arg);

#endif /* INCLUDE_PROTOCOLS_GRIPPERPROTOCOL_H_ */
