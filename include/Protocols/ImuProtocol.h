#ifndef INS_PROTOCOL_H_
#define INS_PROTOCOL_H_

#include "../drilibs/BitConvert/Convert.h"
#include "../SensingModule/sensingModule.h"

#define INS_MAX_FRAME_SIZE 			1024
#define INS_MIN_FRAME_SIZE 			5

// Convert raw gyro reading to real unit [deg/s]
#define TO_GYRO_REAL_UNIT 			(300.0 / 6000.0)   		// 20 LSB = 1 deg at gyro rate = 300deg/s
// Convert raw accelerometer reading to real unit [g]
#define TO_ACCEL_REAL_UNIT 			(0.001)		            // 1LSB == 1mg, 1000LSB == 1g,

typedef struct
{
    unsigned int Time;		// [ms]
    float dRoll;			// [deg/s]
    float dPitch;			// [deg/s]
    float dYaw;				// [deg/s]
    float xAcc;				// [g]
    float yAcc;				// [g]
    float zAcc;				// [g]
}ImuData;

typedef struct
{
    unsigned char RxRawFrame[INS_MAX_FRAME_SIZE];
    unsigned char RxCmdBuffer[INS_MAX_FRAME_SIZE];
    unsigned char TxBuffer[INS_MAX_FRAME_SIZE];
    unsigned char TxCmdBuffer[INS_MAX_FRAME_SIZE];
    volatile int  RxCmdBufferLenght;
    volatile int  IsFrameReady;
}ImuFrame;


extern ImuData sImuData;
extern ImuFrame sImuFrame;

extern void InitImuProtocolMembers(void);
extern void ProcessImuCommand(void);

enum ImuCommand
{
	ENABLE_CONFIG_MODE = 0,
	SET_INS_CONFIG = 1,
	GET_INS_CONFIG = 2,
	SET_IMU_KALMAN_FILTER_TERMS = 3,
	GET_IMU_KALMAN_FILTER_TERMS = 4,
	ENABLE_COMPASS_CALIBRATION = 5,
	SET_DISPLACEMENT_OF_SENSORS = 6,
	GET_DISPLACEMENT_OF_SENSORS = 7,
	DEVICE_IMU_DATA = 20,
	CAMERA_CORRECTION = 25,
	DEBUG_MODE = 30,
	FIRMWARE_UPGRADE = 100,
	UPLOAD_ARM7_BIN_FILE_INFO = 101,
	UPLOAD_ARM7_BIN_FILE_DATA = 102,
	UPLOAD_ARM7_BIN_FILE_DONE = 103,
	UPLOAD_BIN_FILE_CORRUPTED = 104,
	INS_ERROR = 200,
	// Diagnostic
	PERFORMANCE_COUNTERS = 240,
	ADIS_ABC_PROGRESS_MB = 241,
	TESTING_MODE_SHORT = 253,
	TESTING_MODE_SINGLE = 254,
	INS_NONE_CMD = 255,
};

enum ImuOutputFormat
{
	RPY_KALMAN_IN_RADIANS = 0,
	RPY_KALMAN_IN_DEGREES = 1,
	IMU_RAW_DATA_IN_LSB = 2,
	NO_IMU_DATA = 3,
	IMU_VACUM_TEST = 4,
	IMU_DISTANCE_TEST = 5,
	HEADING_DISTANCE = 6,
};

enum BaudRate
{
	BR_9600 = 0,
	BR_19200 = 1,
	BR_38400 = 2,
	BR_57600 = 3,
	BR_115200 = 4,
};

enum UpdateRateHz
{
	UPDATE_RATE_05_HZ = 5,		// 0
	UPDATE_RATE_10_HZ = 10,		// 1
	UPDATE_RATE_15_HZ = 15,		// 2
	UPDATE_RATE_30_HZ = 30,		// 3
	UPDATE_RATE_60_HZ = 60,		// 4
	UPDATE_RATE_90_HZ = 90,		// 5
	UPDATE_RATE_180_HZ = 180,	// 6
	UPDATE_RATE_KF_STOP = 255,	// 7
};

enum InitializationState
{
	ADIS_ABC_PROGRESS = 0,
	ADIS_ABC_DONE = 1,
	SENSORS_INIT_DONE = 2,
};

#endif /* INS_PROTOCOL_H_ */
