/*
 * JointProtocol.h
 *
 *  Created on: May 30, 2016
 *      Author: ubuntu
 */

#ifndef INCLUDE_PROTOCOLS_JOINTPROTOCOL_H_
#define INCLUDE_PROTOCOLS_JOINTPROTOCOL_H_

#include "../drilibs/BitConvert/Convert.h"
#include "../Algorithm/AngleCalculation.h"
#include "../DataDefinition/DataStruct.h"
#include "../Sockets/SocketCAN.h"

#define JOINT_SET_MODE_MSG			0x10
#define JOINT_GET_STATE_MSG			0x42
#define JOINT_STATE_RESPONSE_MSG	0x43
#define JOINT_GET_PARAM_MSG			0x46
#define JOINT_PARAM_RESPONSE_MSG	0x47
#define JOINT_POS_RESPONDED			0x07
#define JOINT_POS_STATE_RESPONDED	0x3F
#define JOINT_RESPONDED_TIMEOUT		10		// 10 * 10ms == 100ms

enum JointsIdentification
{
    JOINT_1_ID = JOINT_1_BOARD_ID,
	JOINT_2_ID = JOINT_2_BOARD_ID,
	JOINT_3_ID = JOINT_3_BOARD_ID,
	JOINT_ALL = 0xFF,
};

enum JointModes
{
	CS_JM_PASSIVE = 0x00,
	CS_JM_INIT = 0x01,
	CS_JM_MANUAL = 0x02,
	CS_JM_NOMINAL = 0x03,
};

enum IdentifyArgumentStatus
{
	// JointLimitState in C code
	s_l_HL = 0x00,			// Hardware left limit switch, Range: from 0 to 1, Scale: 1
	s_l_HR = 0x01,
	s_l_ML = 0x02,
	s_l_MR = 0x03,
	s_Ext = 0x04,
	// EncoderState in C code
	s_E1val = 0x08,			// Current Encoder 1 position, Range: 0 : 2^16
	s_E1stat1 = 0x0C,		// Encoder 1 Error				|
	s_E1stat2 = 0x0D,		// Encoder 1 Warning			| Are read only together
	s_E1stat3 = 0x0E,		// Encoder 1 CRC (check sum)	|
	// MotorState in C code
	s_Mstat1 = 0x10,
	s_Mstat2 = 0x11,
	s_Mstat3 = 0x12,
	s_Mcurr = 0x13,
	// ControlState in C code
	// inputs_t
	s_x0 = 0x18,			// Position
	s_x1 = 0x1C,			// Velocity
	s_lHL = 0x20,
	s_lHR = 0x21,
	s_lML = 0x22,
	s_lMR = 0x23,
	s_ext_error = 0x24,
	s_newRequest = 0x25,
	s_preq = 0x29,			// For interpolation use
	s_pwm = 0x2D,			// For torque control use
	s_rm = 0x31,
	// ControlState in C code
	// outputs_t
	s_u = 0x34,
	s_i	= 0x38,
	s_s	= 0x3C,
	s_xr0 = 0x3D,
	s_xr1 = 0x41,
	s_preqp	= 0x45,
	s_preqc	= 0x49,
	s_iter = 0x4D,
	// CAN_StateTypeDef in C code
	s_can_res = 0x54,
	// EncoderState in C code
	s_E2val = 0x5C,			// Current Encoder 2 position, Range: , Scale:
	s_E2stat1 = 0x60,		// Encoder 2 Error
	s_E2stat2 = 0x61,		// Encoder 2 Warning
	s_E2stat3 = 0x62,		// Encoder 2 CRC (check sum)
};

enum IdentifyArgumentParameters
{
	p_loopts = 0x00,
	p_inpts = 0x04,
	p_k0 = 0x08,
	p_k1 = 0x0C,
	p_k2 = 0x10,
	p_k3 = 0x14,
	p_k4 = 0x18,
	p_k5 = 0x1C,
	p_k6 = 0x20,
	p_k7 = 0x24,
	p_iL0 = 0x28,
	p_iL1 = 0x2C,
	p_uL0 = 0x30,
	p_uL1 = 0x34,
	p_pL0 = 0x38,
	p_pL1 = 0x3C,
	p_Esig = 0x40,
	p_E0 = 0x44,		// Current Encoder 1 offset position, Range: 0 : 2^16
	p_Mlsig = 0x48,
	p_ID = 0x4C,
	//p_codeVersion
	//p_enableEnc2
};

enum ValidateErrorCodes
{
	ERROR_NONE = 0x00,
	ERROR_EVAL = 0x01,
};

enum JointResponseEnum
{
	JOINT_1_RESPONDED = 0x01,
	JOINT_2_RESPONDED = 0x02,
	JOINT_3_RESPONDED = 0x04,
	JOINT_ENC_1_STATUS = 0x08,
	JOINT_ENC_2_STATUS = 0x10,
	JOINT_ENC_3_STATUS = 0x20,
};


void ReportJointResponse(unsigned char aJoint);
void DataRequestAndInit(void);
extern unsigned char ValidateEval(float eval);
float CalculatePreqX(unsigned char id, float arg);
extern unsigned char GetStateLength(unsigned char arg);
extern unsigned char GetParameterLength(unsigned char arg);
extern float CalculateAngleToSetMode(unsigned char jointId, float angle);
extern void setMode(unsigned char id, unsigned char arg, float pos, float vel);
extern void ProcessJointCommand(void);
//void SetMode(int position, int current);
extern void GetJointState(unsigned char id, unsigned char arg);
extern void GetJointParam(unsigned char id, unsigned char arg);
extern void InitJointProtocolMembers(void);



#endif /* INCLUDE_PROTOCOLS_JOINTPROTOCOL_H_ */
