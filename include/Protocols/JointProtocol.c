/*
 * JointProtocol.c
 *
 *  Created on: May 30, 2016
 *      Author: ubuntu
 */

#include "JointProtocol.h"




extern void ProcessJointCommand(void)
{
	CbkCanId canId;
	GetCanId(&canId, sCan0Frame.RxFrame.can_id);

	if(canId.MsgId == JOINT_STATE_RESPONSE_MSG && canId.ExtraArg == s_l_HL)
	{
		if(canId.SourceId == JOINT_1_ID)
		{
			sJoint1Status.State.l_HL = sCan0Frame.RxFrame.data[0];
			sJoint1Status.State.l_HR = sCan0Frame.RxFrame.data[1];
			sJoint1Status.State.l_ML = sCan0Frame.RxFrame.data[2];
			sJoint1Status.State.l_MR = sCan0Frame.RxFrame.data[3];
		}
		else if(canId.SourceId == JOINT_2_ID)
		{
			sJoint2Status.State.l_HL = sCan0Frame.RxFrame.data[0];
			sJoint2Status.State.l_HR = sCan0Frame.RxFrame.data[1];
			sJoint2Status.State.l_ML = sCan0Frame.RxFrame.data[2];
			sJoint2Status.State.l_MR = sCan0Frame.RxFrame.data[3];
		}
		else if(canId.SourceId == JOINT_3_ID)
		{
			sJoint3Status.State.l_HL = sCan0Frame.RxFrame.data[0];
			sJoint3Status.State.l_HR = sCan0Frame.RxFrame.data[1];
			sJoint3Status.State.l_ML = sCan0Frame.RxFrame.data[2];
			sJoint3Status.State.l_MR = sCan0Frame.RxFrame.data[3];
		}
		//else if(canId.SourceId == JOINT_4_ID)
		//{
		//	sJoint4Status.State.l_HL = sCan0Frame.RxFrame.data[0];
		//	sJoint4Status.State.l_HR = sCan0Frame.RxFrame.data[1];
		//	sJoint4Status.State.l_ML = sCan0Frame.RxFrame.data[2];
		//	sJoint4Status.State.l_MR = sCan0Frame.RxFrame.data[3];
		//}
	}
	else if(canId.MsgId == JOINT_STATE_RESPONSE_MSG && canId.ExtraArg == s_E1val)
	{
		float position = (360.0 / 67108864.0) * ConvertToLSBInt(sCan0Frame.RxFrame.data, 0);

		if(canId.SourceId == JOINT_1_ID)
		{
			//elastic joint required encoder sign reverse
			sJoint1Status.State.Eval = position;
			ValidateEval(sJoint1Status.State.Eval);
			ReportJointResponse(JOINT_1_RESPONDED);
		}
		else if(canId.SourceId == JOINT_2_ID)
		{
			sJoint2Status.State.Eval = position;
			ValidateEval(sJoint2Status.State.Eval);
			ReportJointResponse(JOINT_2_RESPONDED);
		}
		else if(canId.SourceId == JOINT_3_ID)
		{
			sJoint3Status.State.Eval = position;
			ValidateEval(sJoint3Status.State.Eval);
			ReportJointResponse(JOINT_3_RESPONDED);
		}
	}
	else if(canId.MsgId == JOINT_STATE_RESPONSE_MSG && canId.ExtraArg == s_E2val)
	{
		sAppState.Joint1Enc2Position = (360.0 / 67108864.0) * ConvertToLSBInt(sCan0Frame.RxFrame.data, 0);
	}
	else if(canId.MsgId == JOINT_STATE_RESPONSE_MSG && canId.ExtraArg == s_E1stat1)
	{
		if(canId.SourceId == JOINT_1_ID)
		{
			// Warning
			sJoint1Status.State.Estat1 = sCan0Frame.RxFrame.data[0];
			// Error
			sJoint1Status.State.Estat2 = sCan0Frame.RxFrame.data[1];
			// CRC
			sJoint1Status.State.Estat3 = sCan0Frame.RxFrame.data[2];

			ReportJointResponse(JOINT_ENC_1_STATUS);
		}
		else if(canId.SourceId == JOINT_2_ID)
		{
			sJoint2Status.State.Estat1 = sCan0Frame.RxFrame.data[0];
			sJoint2Status.State.Estat2 = sCan0Frame.RxFrame.data[1];
			sJoint2Status.State.Estat3 = sCan0Frame.RxFrame.data[2];

			ReportJointResponse(JOINT_ENC_2_STATUS);
		}
		else if(canId.SourceId == JOINT_3_ID)
		{
			sJoint3Status.State.Estat1 = sCan0Frame.RxFrame.data[0];
			sJoint3Status.State.Estat2 = sCan0Frame.RxFrame.data[1];
			sJoint3Status.State.Estat3 = sCan0Frame.RxFrame.data[2];

			ReportJointResponse(JOINT_ENC_3_STATUS);
		}
	}
	else if(canId.MsgId == JOINT_STATE_RESPONSE_MSG && canId.ExtraArg == s_preq)
	{
		float temp = (360.0 / 67108864.0) * ConvertToLSBInt(sCan0Frame.RxFrame.data, 0);

		if(canId.SourceId == JOINT_1_ID)
		{
			sJoint1Status.State.preq = CalculatePreqX(canId.SourceId, temp);
			sJoint1Status.State.uff = tc_tqhdw2tqsoft(ConvertToLSBInt(sCan0Frame.RxFrame.data, 4));
		}
		else if(canId.SourceId == JOINT_2_ID)
		{
			sJoint2Status.State.preq = CalculatePreqX(canId.SourceId, temp);
			sJoint2Status.State.uff = tc_tqhdw2tqsoft(ConvertToLSBInt(sCan0Frame.RxFrame.data, 4));
		}
		else if(canId.SourceId == JOINT_3_ID)
		{
			sJoint3Status.State.preq = CalculatePreqX(canId.SourceId, temp);
			sJoint3Status.State.uff = tc_tqhdw2tqsoft(ConvertToLSBInt(sCan0Frame.RxFrame.data, 4));
		}
		//else if(canId.SourceId == JOINT_4_ID)
		//{
		//	sJoint4Status.State.preq = CalculatePreqX(canId.SourceId, temp);
		//	sJoint4Status.State.uff = tc_tqhdw2tqsoft(ConvertToLSBInt(sCan0Frame.RxFrame.data, 4));
		//}
	}
	else if(canId.MsgId == JOINT_STATE_RESPONSE_MSG && canId.ExtraArg == s_x0)
	{
		float x0 = (360.0 / 67108864.0) * ConvertToLSBInt(sCan0Frame.RxFrame.data, 0);
		float temp = (360.0 / 67108864.0) * ConvertToLSBInt(sCan0Frame.RxFrame.data, 4);

		if(canId.SourceId == JOINT_1_ID)
		{
			sJoint1Status.State.x0 = x0;
			sJoint1Status.State.x1 = CalculatePreqX(canId.SourceId, temp);
		}
		else if(canId.SourceId == JOINT_2_ID)
		{
			sJoint2Status.State.x0 = x0;
			sJoint2Status.State.x1 = CalculatePreqX(canId.SourceId, temp);
		}
		else if(canId.SourceId == JOINT_3_ID)
		{
			sJoint3Status.State.x0 = x0;
			sJoint3Status.State.x1 = CalculatePreqX(canId.SourceId, temp);
		}
		//else if(canId.SourceId == JOINT_4_ID)
		//{
		//	sJoint4Status.State.x0 = x0;
		//	sJoint4Status.State.x1 = CalculatePreqX(canId.SourceId, temp);
		//}
	}
	else if(canId.MsgId == JOINT_STATE_RESPONSE_MSG && canId.ExtraArg == s_xr0)
	{
		if(canId.SourceId == JOINT_1_ID)
			sJoint1Status.State.xr0 = (360.0 / 67108864.0) * ConvertToLSBInt(sCan0Frame.RxFrame.data, 0);
		else if(canId.SourceId == JOINT_2_ID)
			sJoint2Status.State.xr0 = (360.0 / 67108864.0) * ConvertToLSBInt(sCan0Frame.RxFrame.data, 0);
		else if(canId.SourceId == JOINT_3_ID)
			sJoint3Status.State.xr0 = (360.0 / 67108864.0) * ConvertToLSBInt(sCan0Frame.RxFrame.data, 0);
		//else if(canId.SourceId == JOINT_4_ID)
		//	sJoint4Status.State.xr0 = (360.0 / 67108864.0) * ConvertToLSBInt(sCan0Frame.RxFrame.data, 0);
	}
	else if(canId.MsgId == JOINT_STATE_RESPONSE_MSG && canId.ExtraArg == s_xr1)
	{
		float temp = (360.0 / 67108864.0) * ConvertToLSBInt(sCan0Frame.RxFrame.data, 0);

		if(canId.SourceId == JOINT_1_ID)
		{
			sJoint1Status.State.xr1 = CalculatePreqX(canId.SourceId, temp);
			sJoint1Status.State.ur = tc_tqhdw2tqsoft(ConvertToLSBInt(sCan0Frame.RxFrame.data, 4));
		}
		else if(canId.SourceId == JOINT_2_ID)
		{
			sJoint2Status.State.xr1 = CalculatePreqX(canId.SourceId, temp);
			sJoint2Status.State.ur = tc_tqhdw2tqsoft(ConvertToLSBInt(sCan0Frame.RxFrame.data, 4));
		}
		else if(canId.SourceId == JOINT_3_ID)
		{
			sJoint3Status.State.xr1 = CalculatePreqX(canId.SourceId, temp);
			sJoint3Status.State.ur = tc_tqhdw2tqsoft(ConvertToLSBInt(sCan0Frame.RxFrame.data, 4));
		}
		//else if(canId.SourceId == JOINT_4_ID)
		//{
		//	sJoint4Status.State.xr1 = CalculatePreqX(canId.SourceId, temp);
		//	sJoint4Status.State.ur = tc_tqhdw2tqsoft(ConvertToLSBInt(sCan0Frame.RxFrame.data, 4));
		//}
	}
	else if(canId.MsgId == JOINT_STATE_RESPONSE_MSG && canId.ExtraArg == s_u)
	{
		if(canId.SourceId == JOINT_1_ID)
			sJoint1Status.State.u = tc_tqhdw2tqsoft(ConvertToLSBInt(sCan0Frame.RxFrame.data, 0));
		else if(canId.SourceId == JOINT_2_ID)
			sJoint2Status.State.u = tc_tqhdw2tqsoft(ConvertToLSBInt(sCan0Frame.RxFrame.data, 0));
		else if(canId.SourceId == JOINT_3_ID)
			sJoint3Status.State.u = tc_tqhdw2tqsoft(ConvertToLSBInt(sCan0Frame.RxFrame.data, 0));
		//else if(canId.SourceId == JOINT_4_ID)
		//	sJoint4Status.State.u = tc_tqhdw2tqsoft(ConvertToLSBInt(sCan0Frame.RxFrame.data, 0));
	}
	else if(canId.MsgId == JOINT_PARAM_RESPONSE_MSG && canId.ExtraArg == p_E0)
	{
		float offset = (360.0 / 67108864.0) * ConvertToLSBInt(sCan0Frame.RxFrame.data, 0);

		if(canId.SourceId == JOINT_1_ID)
			sJoint1Status.Param.E0 = offset;
		else if(canId.SourceId == JOINT_2_ID)
			sJoint2Status.Param.E0 = offset;
		else if(canId.SourceId == JOINT_3_ID)
			sJoint3Status.Param.E0 = offset;
	}
}

float CalculatePreqX(unsigned char id, float arg)
{
	float encZero = 0;
	float result = 0;

	if(id == JOINT_1_ID)
	{
		encZero = ac_CAD2enc(sJoint1Status.Param.E0, 1);
		result = ac_enc2CAD(arg + encZero, 1);
	}
	else if(id == JOINT_2_ID)
	{
		encZero = ac_CAD2enc(sJoint2Status.Param.E0, 2);
		result = ac_enc2CAD(arg + encZero, 2);
	}
	else if(id == JOINT_3_ID)
	{
		encZero = ac_CAD2enc(sJoint3Status.Param.E0, 3);
		result = ac_enc2CAD(arg + encZero, 3);
	}
	//else if(id == JOINT_4_ID)
	//{
	//	encZero = ac_CAD2enc(sJoint4Status.Param.E0, 4);
	//	result = ac_enc2CAD(arg + encZero, 4);
	//}

	return result;
}



float CalculateAngleToSetMode(unsigned char jointId, float angle)
{
	unsigned char id = 0;
	float eO = 0;

	if(jointId == JOINT_1_ID)
	{
		id = 1;
		eO = sJoint1Status.Param.E0;
	}
	else if(jointId == JOINT_2_ID)
	{
		id = 2;
		eO = sJoint2Status.Param.E0;
	}
	else if(jointId == JOINT_3_ID)
	{
		id = 3;
		eO = sJoint3Status.Param.E0;
	}
	//else if(jointId == JOINT_4_ID)
	//{
	//	id = 4;
	//	eO = sJoint4Status.Param.E0;
	//}

	float angle1 = ac_CAD2enc(angle, id);
	float angle2 = ac_enc2control(angle1, eO);
	return (angle2 / (360.0 / 67108864.0));
}

void setMode(unsigned char id, unsigned char arg, float pos, float pwm)
{
	sCan0Frame.CanId.MsgId = JOINT_SET_MODE_MSG;
	sCan0Frame.CanId.SourceId = 0x00;
	sCan0Frame.CanId.ReceiverId = id;
	sCan0Frame.CanId.ExtraArg = arg;
	sCan0Frame.DLC = 8;

	int position  = (int)pos;
	int direcPWM  = (int)pwm;

	sCan0Frame.TxData[0] = position;
	sCan0Frame.TxData[1] = position >> 8;
	sCan0Frame.TxData[2] = position >> 16;
	sCan0Frame.TxData[3] = position >> 24;
	sCan0Frame.TxData[4] = direcPWM;
	sCan0Frame.TxData[5] = direcPWM >> 8;
	sCan0Frame.TxData[6] = direcPWM >> 16;
	sCan0Frame.TxData[7] = direcPWM >> 24;

	BuildCan0TxFrame(&sCan0Frame);
	SendCan0TxFrame();
}

void GetJointState(unsigned char id, unsigned char arg)
{
	sCan0Frame.CanId.MsgId = JOINT_GET_STATE_MSG;
	sCan0Frame.CanId.SourceId = 0x00;
	sCan0Frame.CanId.ReceiverId = id;
	sCan0Frame.CanId.ExtraArg = arg;
	sCan0Frame.DLC = 1;
	sCan0Frame.TxData[0] = GetStateLength(arg);

	BuildCan0TxFrame(&sCan0Frame);
	SendCan0TxFrame();
}

void GetJointParam(unsigned char id, unsigned char arg)
{
	sCan0Frame.CanId.MsgId = JOINT_GET_PARAM_MSG;
	sCan0Frame.CanId.SourceId = 0x00;
	sCan0Frame.CanId.ReceiverId = id;
	sCan0Frame.CanId.ExtraArg = arg;
	sCan0Frame.DLC = 1;
	sCan0Frame.TxData[0] = GetParameterLength(arg);

	BuildCan0TxFrame(&sCan0Frame);
	SendCan0TxFrame();
}


unsigned char GetStateLength(unsigned char arg)
{
	unsigned char result = 0;


	if(arg == 12)
	{
		result = 3;	// Encoder Warning + Error + CRC
	}
	else if((arg < 20 && arg != 8) || arg == 32 || arg == 33 || arg == 34 || arg == 35 || arg == 36 || arg == 48 || arg == 60)
	{
		result = 1;
	}
	else if(arg == 8 || arg >= 64 || arg == 24 || arg == 28 || arg == 40 || arg == 44 || arg == 52 || arg == 56)
	{
		result = 4;
	}

	return result;
}

unsigned char GetParameterLength(unsigned char arg)
{
	unsigned char result =  0;

	if(arg < 64 || arg == 68)
	{
		result = 4;
	}
	else if(arg == 64 || arg == 72 || arg == 73 || arg == 76)
	{
		result = 1;
	}

	return result;
}

unsigned char ValidateEval(float eval)
{
	unsigned char errorCoder = ERROR_NONE;

	if(eval >= 360 || eval < 0)
	{
		errorCoder = ERROR_EVAL;
		// Send error code to Service Computer
	}

	return errorCoder;

}

void ReportJointResponse(unsigned char aJoint)
{
	sAppState.JointResponseState |= aJoint;
}


void DataRequestAndInit(void)
{
	GetJointState(JOINT_1_ID, s_E1val);
	GetJointState(JOINT_2_ID, s_E1val);
	GetJointState(JOINT_3_ID, s_E1val);
	//GetJointState(JOINT_4_ID, s_E1val);

	GetJointParam(JOINT_1_ID, p_E0);
	GetJointParam(JOINT_2_ID, p_E0);
	GetJointParam(JOINT_3_ID, p_E0);
	//GetJointParam(JOINT_4_ID, p_E0);
}

void InitJointProtocolMembers(void)
{
	sJoint1Status.Param.E0 = 0;
	sJoint2Status.Param.E0 = 41.9;
	sJoint3Status.Param.E0 = 0;
	sJoint4Status.Param.E0 = 0;
}

