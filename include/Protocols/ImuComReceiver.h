#ifndef GCS_COM_RECEIVER_H
#define GCS_COM_RECEIVER_H

#include "../drilibs/CRC/CRC16.h"
#include "ImuProtocol.h"

extern void ParseImuFrame(unsigned char a_bByte);

#endif
