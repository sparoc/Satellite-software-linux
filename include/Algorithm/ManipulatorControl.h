/*
 * ManipulatorControl.h
 *
 *  Created on: Oct 7, 2016
 *      Author: ubuntu
 */

#ifndef INCLUDE_ALGORITHM_MANIPULATORCONTROL_H_
#define INCLUDE_ALGORITHM_MANIPULATORCONTROL_H_

#include "../DataDefinition/DataStruct.h"

void CalculateRefJointTraj(LemurData *lemur, ManipulatorParams *manParams, NomJointTrajectory *nom, Satellite *sat, RefJointTrajectory *out);


#endif /* INCLUDE_ALGORITHM_MANIPULATORCONTROL_H_ */
