/*
 * ManipulatorCartesianControl.h
 *
 *  Created on: Jan 9, 2019
 *      Author: ubuntu
 */

#ifndef INCLUDE_ALGORITHM_MANIPULATORCARTESIANCONTROL_H_
#define INCLUDE_ALGORITHM_MANIPULATORCARTESIANCONTROL_H_

#include <stdio.h>
#include <math.h>

float MatrixDeterminantCC(float (*)[9], float);
void MatrixInverseCC(float (*)[9], float, float (*)[9]);
void MatrixMultiplicationCC(float (*)[9], float (*)[9], float (*)[9], int, int, int);
void MatrixTransposeCC(float (*)[9], float (*)[9], int, int);
//void Antisym(float vector[9], float (*matrix)[9]);
//void CrossProd(float vector1[3], float vector2[3], float result[3]);
//void VecSubtract(float vector1[3], float vector2[3], float result[3]);
void ValMatMultiplicationCC(float value, float (*matrix)[9], float (*result)[9],int m,int n);
void MatrixSum4CC(float (*matrix1)[9], float (*matrix2)[9], float (*matrix3)[9], float (*matrix4)[9], float (*result)[9],int m,int n);
void MatrixSum2CC(float (*matrix1)[9], float (*matrix2)[9], float (*result)[9],int m,int n);
void MatrixSubtract2CC(float (*matrix1)[9], float (*matrix2)[9], float (*result)[9],int m,int n);


#endif /* INCLUDE_ALGORITHM_MANIPULATORCARTESIANCONTROL_H_ */
