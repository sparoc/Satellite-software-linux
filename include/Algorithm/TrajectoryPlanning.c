/*
 * TrajectoryPlanning.c
 *
 *  Created on: Nov 6, 2018
 *      Author: ubuntu
 */

#include "TrajectoryPlanning.h"

float **gJointTrajectoryQuasi;
float **gSatTrajectory;

/*
 * q_traj - output trajectory
 */
unsigned int q_TrajGeneration(float *q_traj, float* qprim_traj, float q_ini, float q_fin, float Tr, float Tp, float Th, int freq)
{
	// Validate
	if(q_ini < -120 && q_ini > 120)
		return 0;
	//else if()
	//{
	//
	//}

	float L;
	float Vmax;
	float ap,bp,cp,dp,ep;
	float as,bs;
	float ah,bh,ch,dh,eh;
	float dt;
	int i;
	float time;
	int n = freq * Tr;

	L  = q_fin - q_ini;
	Vmax = (-2*L)/(-2*Tr+Th+Tp);

	if(Tp==0)
	{
      ap = 0;
      bp = 0;
	}
	else
	{
	  ap = (-Vmax)/(2*pow(Tp,3));
	  bp = (Vmax)/(pow(Tp,2));
    }
    cp = 0;
    dp = 0;
    ep = q_ini;
    as = Vmax;
    bs = -0.5*Vmax*Tp+q_ini;
    ah = (Vmax)/(2*pow(Th,3));
    bh = (-Vmax)*(2*Tr-Th)/(pow(Th,3));
    ch = (3*Vmax)*Tr*(Tr-Th)/(pow(Th,3));
    dh = (-2*pow(Tr,3)*Vmax+3*pow(Tr,2)*Th*Vmax)/(pow(Th,3));
    eh = (2*Tr*pow(Th,3)*Vmax-Vmax*pow(Th,4)-Tp*Vmax*pow(Th,3)+pow(Tr,4)*Vmax-2*pow(Tr,3)*Th*Vmax)/(2*pow(Th,3))+q_ini;

 	time = 0;

    dt = 1.0 / (float)freq;

	for(i = 0; i < n; i++)
	{

	  if(time<Tp)
	  {
        q_traj[i] = ap*pow(time,4) + bp*pow(time,3) + cp*pow(time,2) + dp*time+ep;
        if(qprim_traj != NULL)
        	qprim_traj[i] = 4 * ap * pow(time, 3) + 3 * bp * pow(time,2) + 2 * cp * time + dp;

	  }
      else if(time<=Tr-Th)
      {
        q_traj[i]=as*time+bs;
        if(qprim_traj != NULL)
        	qprim_traj[i] = as;
      }
      else
      {
        q_traj[i]=ah*pow(time,4)+bh*pow(time,3)+ch*pow(time,2)+dh*time+eh;
        if(qprim_traj != NULL)
        	qprim_traj[i] = 4 * ah * pow(time, 3) + 3 * bh * pow(time,2) + 2 * ch * time + dh;
      }

//	  // Convert position in [deg] to encoder units
//	  q_traj[i] *= 186413.51111111111;
//
//	  // Convert velocity in [deg] to encoder units
//	  if(qprim_traj != NULL)
//		  qprim_traj[i] *= 186413.51111111111;

	  time += dt;
	}

	return 1;
}


unsigned int q_TrajGenerationCartesian(float *q_traj, float* qprim_traj, float q_ini, float q_fin, float Tr, float Tp, float Th, int freq)
{
	float L;
	float Vmax;
	float ap,bp,cp,dp,ep;
	float as,bs;
	float ah,bh,ch,dh,eh;
	float dt;
	int i;
	float time;
	int n = freq * Tr;

	L  = q_fin - q_ini;
	Vmax = (-2*L)/(-2*Tr+Th+Tp);

	if(Tp==0)
	{
      ap = 0;
      bp = 0;
	}
	else
	{
	  ap = (-Vmax)/(2*pow(Tp,3));
	  bp = (Vmax)/(pow(Tp,2));
    }
    cp = 0;
    dp = 0;
    ep = q_ini;
    as = Vmax;
    bs = -0.5*Vmax*Tp+q_ini;
    ah = (Vmax)/(2*pow(Th,3));
    bh = (-Vmax)*(2*Tr-Th)/(pow(Th,3));
    ch = (3*Vmax)*Tr*(Tr-Th)/(pow(Th,3));
    dh = (-2*pow(Tr,3)*Vmax+3*pow(Tr,2)*Th*Vmax)/(pow(Th,3));
    eh = (2*Tr*pow(Th,3)*Vmax-Vmax*pow(Th,4)-Tp*Vmax*pow(Th,3)+pow(Tr,4)*Vmax-2*pow(Tr,3)*Th*Vmax)/(2*pow(Th,3))+q_ini;

 	time = 0;

    dt = 1.0 / (float)freq;

	for(i = 0; i < n; i++)
	{

	  if(time<Tp)
	  {
        q_traj[i] = ap*pow(time,4) + bp*pow(time,3) + cp*pow(time,2) + dp*time+ep;
        if(qprim_traj != NULL)
        	qprim_traj[i] = 4 * ap * pow(time, 3) + 3 * bp * pow(time,2) + 2 * cp * time + dp;

	  }
      else if(time<=Tr-Th)
      {
        q_traj[i]=as*time+bs;
        if(qprim_traj != NULL)
        	qprim_traj[i] = as;
      }
      else
      {
        q_traj[i]=ah*pow(time,4)+bh*pow(time,3)+ch*pow(time,2)+dh*time+eh;
        if(qprim_traj != NULL)
        	qprim_traj[i] = 4 * ah * pow(time, 3) + 3 * bh * pow(time,2) + 2 * ch * time + dh;
      }


	  time += dt;
	}

	return 1;
}

