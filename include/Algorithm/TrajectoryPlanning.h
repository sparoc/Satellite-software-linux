/*
 * TrajectoryPlanning.h
 *
 *  Created on: Nov 6, 2018
 *      Author: ubuntu
 */

#ifndef INCLUDE_ALGORITHM_TRAJECTORYPLANNING_H_
#define INCLUDE_ALGORITHM_TRAJECTORYPLANNING_H_

#include <stdlib.h>
#include <math.h>

extern float **gJointTrajectoryQuasi;
extern float **gSatTrajectory;

extern unsigned int q_TrajGeneration(float *q_traj, float* qprim_traj, float q_ini, float q_fin, float Tr, float Tp, float Th, int freq);
extern unsigned int q_TrajGenerationCartesian(float *q_traj, float* qprim_traj, float q_ini, float q_fin, float Tr, float Tp, float Th, int freq);


#endif /* INCLUDE_ALGORITHM_TRAJECTORYPLANNING_H_ */
