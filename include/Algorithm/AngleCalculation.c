/*
 * AngleCalculation.c
 *
 *  Created on: Oct 5, 2016
 *      Author: ubuntu
 */

#include "AngleCalculation.h"

float ac_CAD2enc(float a_CAD, unsigned char joint_no)
{

	//float hr = 180; //must be change if used other then deg representation
	float r = 360; //must be change if used other then deg representation


	float delta_a[4] = {50.7, -218.1, 30.5, -16.3};

	float a_enc = 0;

	switch (joint_no) {

	    case 1:

	        a_enc = a_CAD + delta_a[0];
	        break;
	    case 2:
	        a_enc = a_CAD + delta_a[1];
	        break;
	    case 3:
	        a_enc = a_CAD + delta_a[2];
	        break;
	    case 4:
	        a_enc = a_CAD + delta_a[3];
	        break;
	}

	if(a_enc < 0)
	{
	    a_enc=a_enc+r;
	}
	else if (a_enc > r)
		{
	    a_enc=a_enc-r;
		}

	return a_enc;
}

float ac_enc2CAD(float a_enc, unsigned char joint_no)
{
	//float hr = 180; //must be change if used other then deg representation
	float r = 360; //must be change if used other then deg representation

	//float delta_a[4] = {50.7, -218.1, 30.5, -16.3};
	float delta_a[4] = {330.3, -218.1, 30.5, -16.3};
	float a_CAD = 0;

	switch (joint_no)
	{
	    case 1:
	        a_CAD = a_enc - delta_a[0];
	        break;
	    case 2:
	        a_CAD = a_enc - delta_a[1];
	        break;
	    case 3:
	        a_CAD = a_enc - delta_a[2];
	        break;
	    case 4:
	        a_CAD = a_enc - delta_a[3];
	        break;
	}

	if(a_CAD < 0)
	{
	    a_CAD = a_CAD + r;
	    //a_CAD += r;
	}
	else if (a_CAD > r)
	{
	    a_CAD = a_CAD - r;
	}

	return a_CAD;
}

float ac_control2enc(float a_con, float enc_zero)
{
	//float hr = 180; //must be change if used other then deg representation
	float r = 360; //must be change if used other then deg representation

	float a_enc = 0;
	if (a_con<=0)
	{
	    a_enc=a_con+r;
	}
	else
	{
	    a_enc=a_con;
	}

	a_enc = a_enc+enc_zero;
	return a_enc;
}

float ac_enc2control(float a_enc, float enc_zero)
{
	float hr = 180; //must be change if used other then deg representation
	float r = 360; //must be change if used other then deg representation

	float z = enc_zero - hr;
	float a_con = a_enc - z;
	if(a_con<0)
	{
		a_con = a_con + r;
	}
	else if(a_con>r)
	{
	    a_con = a_con - r;
	}

	a_con = a_con - hr;
	return a_con;
}

float tc_tqsoft2tqhdw(float t_inp)
{
	float t_out= -t_inp;
	return t_out;
}

float tc_tqhdw2tqsoft(float t_inp)
{
	float t_out= -t_inp;
	return t_out;
}


