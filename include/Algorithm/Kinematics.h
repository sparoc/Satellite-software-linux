/*
 * Kinematics.h
 *
 *  Created on: Oct 7, 2016
 *      Author: ubuntu
 */

#ifndef INCLUDE_ALGORITHM_KINEMATICS_H_
#define INCLUDE_ALGORITHM_KINEMATICS_H_

#include "../DataDefinition/DataStruct.h"

void FWDKinematics(LemurData *lemur, float th1, float th2, float th3, float th4, EEPositionLFF *eePosLFF);
void INVKinematics(LemurData *lemur, float targetX, float targetY, float targetZ, Satellite *sat, NomJointTrajectory *nomTraj);

#endif /* INCLUDE_ALGORITHM_KINEMATICS_H_ */
