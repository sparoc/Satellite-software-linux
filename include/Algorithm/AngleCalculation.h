/*
 * AngleCalculation.h
 *
 *  Created on: Oct 5, 2016
 *      Author: ubuntu
 */

#ifndef INCLUDE_ALGORITHM_ANGLECALCULATION_H_
#define INCLUDE_ALGORITHM_ANGLECALCULATION_H_

float ac_CAD2enc(float a_CAD, unsigned char joint_no);
float ac_enc2CAD(float a_enc, unsigned char joint_no);
float ac_control2enc(float a_con, float enc_zero);
float ac_enc2control(float a_enc, float enc_zero);
float tc_tqsoft2tqhdw(float t_inp);
float tc_tqhdw2tqsoft(float t_inp);

#endif /* INCLUDE_ALGORITHM_ANGLECALCULATION_H_ */
