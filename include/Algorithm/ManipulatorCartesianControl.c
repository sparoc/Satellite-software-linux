/*
 * ManipulatorCartesianControl.c
 *
 *  Created on: Jan 9, 2019
 *      Author: ubuntu
 */

#include "ManipulatorCartesianControl.h"


float MatrixDeterminantCC(float (*matrix)[9], float k)
{
	float s = 1;
	float det = 0;
	float b[9][9];
	int i, j, m, n, c;

	if (k == 1)
	{
		return (matrix[0][0]);
	}
	else
	{
		det = 0;
		for (c = 0; c < k; c++)
		{
			m = 0;
			n = 0;

			for (i = 0;i < k; i++)
			{
				for (j = 0 ;j < k; j++)
				{
					b[i][j] = 0;

					if (i != 0 && j != c)
					{
						b[m][n] = matrix[i][j];

						if (n < (k - 2))
						{
							n++;
						}
						else
						{
							n = 0;
							m++;
						}
					}
				}
			}

			det = det + s * (matrix[0][c] * MatrixDeterminantCC(b, k - 1));
			s = -1 * s;
		}
	}

	return (det);
}

void MatrixInverseCC(float (*num)[9], float f , float (*inverse)[9])
{
	float b[9][9], X[9][9];
	int p, q, m, n, i, j;

	for (q = 0;q < f; q++)
	{
		for (p = 0;p < f; p++)
		{
			m = 0;
			n = 0;

			for (i = 0;i < f; i++)
			{
				for (j = 0;j < f; j++)
				{
					if (i != q && j != p)
					{
						b[m][n] = num[i][j];

						if (n < (f - 2))
						{
							n++;
						}
						else
						{
							n = 0;
							m++;
						}
					}
				}
			}

			X[q][p] = pow(-1, q + p) * MatrixDeterminantCC(b, f - 1);
		}
	}

	float d = MatrixDeterminantCC(num, f);

	for (i = 0;i < f; i++)
	{
		for (j = 0;j < f; j++)
		{
			inverse[i][j] = X[j][i] / d;
		}
	}
}

void MatrixMultiplicationCC(float (*matrix1)[9], float (*matrix2)[9], float (*result)[9],int m,int n,int l)
{
	int i,j,k;
	float sum = 0;

	for (i = 0; i < m; i++)
	{
		for (j = 0; j < l; j++)
		{
			for (k = 0; k < n; k++)
			{
				sum = sum + matrix1[i][k] * matrix2[k][j];
			}

			result[i][j] = sum;
			sum = 0;
		}
	}
}

void MatrixTransposeCC(float (*matrix)[9], float (*result)[9], int m, int n)
{
	int i,j;

	for(i = 0; i < n; i++)
	{
		for(j=0;j<m;j++)
		{
			result[i][j] = matrix[j][i];
		}
	}
}
//
//void Antisym(float vector[9], float (*matrix)[9])
//{
//	matrix[0][0] = 0;
//	matrix[0][1] = -vector[2];
//	matrix[0][2] = vector[1];
//	matrix[1][0] = vector[2];
//	matrix[1][1] = 0;
//	matrix[1][2] = -vector[0];
//	matrix[2][0] = -vector[1];
//	matrix[2][1] = vector[0];
//	matrix[2][2] = 0;
//}
//
//void CrossProd(float vector1[3], float vector2[3], float result[3])
//{
//	result[0] = vector1[1]*vector2[2] - vector1[2]*vector2[1];
//	result[1] = -(vector1[0]*vector2[2] - vector1[2]*vector2[0]);
//	result[2] = vector1[0]*vector2[1] - vector1[1]*vector2[0];
//}
//
//void VecSubtract(float vector1[3], float vector2[3], float result[3])
//{
//	result[0] = vector1[0] - vector2[0];
//	result[1] = vector1[1] - vector2[1];
//	result[2] = vector1[2] - vector2[2];
//}

void ValMatMultiplicationCC(float value, float (*matrix)[9], float (*result)[9],int m,int n)
{
	int i,j;
	for(i=0;i<m;i++)
	 for(j=0;j<n;j++)
	   result[i][j] = value * matrix[i][j];
}

void MatrixSum4CC(float (*matrix1)[9], float (*matrix2)[9], float (*matrix3)[9], float (*matrix4)[9], float (*result)[9],int m,int n)
{
	int i,j;
	for(i=0;i<m;i++)
	 for(j=0;j<n;j++)
	   result[i][j] = matrix1[i][j] + matrix2[i][j] + matrix3[i][j] + matrix4[i][j];
}

void MatrixSum2CC(float (*matrix1)[9], float (*matrix2)[9], float (*result)[9],int m,int n)
{
	int i,j;
	for(i=0;i<m;i++)
	 for(j=0;j<n;j++)
	   result[i][j] = matrix1[i][j] + matrix2[i][j];
}

void MatrixSubtract2CC(float (*matrix1)[9], float (*matrix2)[9], float (*result)[9],int m,int n)
{
	int i,j;
	for(i=0;i<m;i++)
	 for(j=0;j<n;j++)
	   result[i][j] = matrix1[i][j] - matrix2[i][j];
}

