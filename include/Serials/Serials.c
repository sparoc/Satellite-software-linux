/*
 * serial.c

 *
 *  Created on: Mar 31, 2016
 *      Author: ubuntu
 */

#include "../Serials/Serials.h"

#define RS0_BAUD_RATE		B115200
#define UA0_BAUD_RATE		B115200

int gRS0fd;
int gUA0fd;

int RS0Open(void)
{
	struct termios options;

	// SAM5AD3 XPLAINED
	// ttyS0: RS1 label at PCB board
	// TXD1 - pin 18 at eval board
	// RXD1 - pin 19 at eval board
	int fd = open("/dev/ttyS1", O_RDWR | O_NOCTTY | O_NDELAY);

	if(fd == -1)
	{
		perror("Serials.c - RS0Open(): Unable to open /dev/ttyS1 (RS0)  - ");
	}
	else
	{
		// The FNDELAY option causes the read function to return 0 if no characters are available on the port.
		//fcntl(fd, F_SETFL, FNDELAY);
		fcntl(fd, F_SETFL, 0);

		tcgetattr(fd, &options);
		// Baudrate
		cfsetispeed(&options, RS0_BAUD_RATE);
		cfsetospeed(&options, RS0_BAUD_RATE);
		// Enable the receiver and set local mode
		options.c_cflag |= (CLOCAL | CREAD);
		// Mask the character size bits
		options.c_cflag &= ~CSIZE;
		// Setting the raw input mode
		options.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHONL | IEXTEN | ISIG);

		// No parity (8N1):
		options.c_cflag &= ~PARENB;
		options.c_cflag &= ~CSTOPB;
		options.c_cflag &= ~CSIZE;
		// Select 8 data bits
		options.c_cflag |= CS8;
		// Disable hardware flow control:
	    options.c_cflag &= ~CRTSCTS;
	    options.c_lflag |= FLUSHO;
	    // Raw mode
	    // http://manpages.ubuntu.com/manpages/xenial/man3/termios.3.html
	    // GCH 06092018
	    options.c_oflag &= ~OPOST;
	    options.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);

		tcsetattr(fd, TCSANOW, &options);
	}

	return fd;
}

void RS0Close(int fd)
{
	close(fd);
}

void RS0WriteBuffer(int fd, unsigned char *address, int length)
{
	int n = write(fd, address, length);

	if(n == -1)
		perror("Serials.c - RS0WriteBuffer(): writing to ttyS1 (RS0) failed - ");
}

void RS0Write(int fd, unsigned char *address)
{
	RS0WriteBuffer(fd, address, strlen((char*)address));
}

int UA0Open(void)
{
	struct termios options;

	// SAM5AD3 XPLAINED
	// ttyS5: UA0 label at PCB board
	// UTXD0 - pin PC30 at eval board
	// URXD0 - pin PC29 at eval board
	int fd = open("/dev/ttyS5", O_RDWR | O_NOCTTY | O_NDELAY);

	if(fd == -1)
	{
		perror("Serials.c - UA0Open(): Unable to open /dev/ttyS5 (UA0)  - ");
	}
	else
	{
		// The FNDELAY option causes the read function to return 0 if no characters are available on the port.
		//fcntl(fd, F_SETFL, FNDELAY);
		fcntl(fd, F_SETFL, 0);

		tcgetattr(fd, &options);
		// Baudrate
		cfsetispeed(&options, UA0_BAUD_RATE);
		cfsetospeed(&options, UA0_BAUD_RATE);
		// Enable the receiver and set local mode
		options.c_cflag |= (CLOCAL | CREAD);
		// Mask the character size bits
		options.c_cflag &= ~CSIZE;
		// Setting the raw input mode
		options.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHONL | IEXTEN | ISIG);

		// No parity (8N1):
		options.c_cflag &= ~PARENB;
		options.c_cflag &= ~CSTOPB;
		options.c_cflag &= ~CSIZE;
		// Select 8 data bits
		options.c_cflag |= CS8;
		// Disable hardware flow control:
	    options.c_cflag &= ~CRTSCTS;
	    options.c_lflag |= FLUSHO;
	    // Raw mode
	    // http://manpages.ubuntu.com/manpages/xenial/man3/termios.3.html
	    // GCH 06092018
	    options.c_oflag &= ~OPOST;
	    options.c_iflag &= ~(IGNBRK | BRKINT | PARMRK | ISTRIP | INLCR | IGNCR | ICRNL | IXON);

		tcsetattr(fd, TCSANOW, &options);
	}

	return fd;
}

void UA0Close(int fd)
{
	close(fd);
}

void UA0WriteBuffer(int fd, unsigned char *address, int length)
{
	int n = write(fd, address, length);

	if(n == -1)
		perror("Serials.c - UA0WriteBuffer(): writing to ttyS5 (UA0) failed - ");
}

void UA0Write(int fd, unsigned char *address)
{
	UA0WriteBuffer(fd, address, strlen((char*)address));
}

