/*
 * serial.h
 *
 *  Created on: Mar 31, 2016
 *      Author: ubuntu
 */

#ifndef INCLUDE_SERIALS_SERIALS_H_
#define INCLUDE_SERIALS_SERIALS_H_

#include <stdio.h>   /* Standard input/output definitions */
#include <string.h>  /* String function definitions */
#include <unistd.h>  /* UNIX standard function definitions */
#include <fcntl.h>   /* File control definitions */
#include <errno.h>   /* Error number definitions */
#include <termios.h> /* POSIX terminal control definitions */

extern int gRS0fd;
extern int gUA0fd;

// RS0
extern int RS0Open(void);
extern void RS0Close(int fd);
extern void RS0WriteBuffer(int fd, unsigned char *address, int length);
extern void RS0Write(int fd, unsigned char *address);

// UA0
extern int UA0Open(void);
extern void UA0Close(int fd);
extern void UA0WriteBuffer(int fd, unsigned char *address, int length);
extern void UA0Write(int fd, unsigned char *address);

#endif /* INCLUDE_SERIALS_SERIALS_H_ */
