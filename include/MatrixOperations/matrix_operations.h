/*
 * matrix_operations.h
 *
 *  Created on: 25 pa� 2021
 *      Author: FLB
 */

#ifndef MATRIX_OPERATIONS_H_
#define MATRIX_OPERATIONS_H_

#include <stdio.h>
#include <math.h>

float MatrixDeterminant(float (*)[9], float);
void MatrixInverse(float (*)[9], float, float (*)[9]);
void MatrixMultiplication(float (*)[9], float (*)[9], float (*)[9], int, int, int);
void MatrixTranspose(float (*)[9], float (*)[9], int, int);
void Antisym(float vector[3], float (*matrix)[9]);
void CrossProd(float vector1[3], float vector2[3], float result[3]);
void VecSubtract(float vector1[3], float vector2[3], float result[3]);
void ValMatMultiplication(float value, float (*matrix)[9], float (*result)[9], int m, int n);
void MatrixSum(float (*matrix1)[9], float (*matrix2)[9], float (*result)[9], int m, int n);
void MatrixSubtract(float (*matrix1)[9], float (*matrix2)[9], float (*result)[9], int m, int n);

#endif /* MATRIX_OPERATIONS_H_ */
