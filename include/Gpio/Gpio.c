/*
 * GpioProtocol.c
 *
 *  Created on: Jun 14, 2016
 *      Author: ubuntu
 */

#include "Gpio.h"

void EnableGpio(unsigned char a_gpio, unsigned char a_direction)
{
	char buffer[64];

	// Reserve (export) the GPIO
	int fd = open("/sys/class/gpio/export", O_WRONLY);

	if(fd == -1)
	{
		perror("Gpio.c - EnableGpio(): Unable to open /sys/class/gpio/export - ");
		return;
	}

	sprintf(buffer, "%d", a_gpio);
	write(fd, buffer, strlen(buffer));
	close(fd);

	// Set the direction in the GPIO at D (96) section
	sprintf(buffer, "/sys/class/gpio/pioD%d/direction", a_gpio - 96);

	fd = open(buffer, O_WRONLY);

	if(a_direction == GPIO_IN)
		write(fd, "in", 2);
	else
		write(fd, "out", 3);

	close(fd);
}

extern void SetGpioState(unsigned char a_gpio, unsigned char a_state)
{
	char buffer[64];

	sprintf(buffer, "/sys/class/gpio/pioD%d/value", a_gpio - 96);

	int fd = open(buffer, O_WRONLY);
	write(fd, (a_state == GPIO_HIGH ? "1" : "0"), 1);
	close(fd);
}

extern void DisableGpio(unsigned char a_gpio)
{
	char buffer[8];

	int fd = open("/sys/class/gpio/unexport", O_WRONLY);

	sprintf(buffer, "%d", a_gpio);
	write(fd, buffer, strlen(buffer));
	close(fd);
}
