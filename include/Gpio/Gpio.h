/*
 * GpioProtocol.h
 *
 *  Created on: Jun 14, 2016
 *      Author: ubuntu
 */

#ifndef INCLUDE_PROTOCOLS_GPIOPROTOCOL_H_
#define INCLUDE_PROTOCOLS_GPIOPROTOCOL_H_

#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>

extern void EnableGpio(unsigned char a_gpio, unsigned char a_direction);
extern void SetGpioState(unsigned char a_gpio, unsigned char a_state);
extern void DisableGpio(unsigned char a_gpio);

enum GpioDirection
{
	GPIO_IN = 0,
	GPIO_OUT = 1
};

enum GpioState
{
	GPIO_LOW = 0,
	GPIO_HIGH = 1
};

enum GpioMapping
{
	GPIO_PE11 = 139,
	GPIO_PE12 = 140,
	GPIO_PE13 = 141,
	GPIO_PE14 = 142,
	GPIO_PE15 = 143,
	GPIO_PE16 = 144,
	GPIO_PE17 = 145
};

#endif /* INCLUDE_PROTOCOLS_GPIOPROTOCOL_H_ */
