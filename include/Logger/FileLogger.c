/*
 * FileLogger.c
 *
 *  Created on: Jan 14, 2019
 *      Author: ubuntu
 */

#include "FileLogger.h"

FILE *gFileLogger = NULL;
char m_strTimestamp[15];

void LogInApp(char *str,short length){

	sGCSFrame.TxCmdBuffer[0] = LOGINFO_STRING;
	strcpy(&sGCSFrame.TxCmdBuffer[1],str);
	short size = GetGCSFrameToSend(++length);
	send(gLanSocket, sGCSFrame.TxRawBuffer, size, 0);

}

char *LogTimestamp(char *str)
{
    struct timeval te;

    gettimeofday(&te, NULL); // get current time

    unsigned int milliseconds = te.tv_sec * 1000LL + te.tv_usec / 1000; // milliseconds

    sprintf(str, "%u", milliseconds);
    return str;
}

int InitFileLogger(void)
{
	gFileLogger = fopen("CBK_SR.log", "w");

	check(gFileLogger != NULL, "Error opening file!");

	return 0;

	error:
		return -1;
}

void FlushFileLogger(void)
{
	fflush(gFileLogger);
}

void CloseFileLogger(void)
{
	fclose(gFileLogger);
	gFileLogger = NULL;
}

