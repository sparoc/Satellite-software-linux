/*
 * FileLogger.h
 *
 *  Created on: Jan 14, 2019
 *      Author: ubuntu
 */

#ifndef INCLUDE_LOGGER_FILELOGGER_H_
#define INCLUDE_LOGGER_FILELOGGER_H_

#include <stdio.h>
#include <sys/time.h>
#include "dbg.h"
#include "../Protocols/GCSProtocol.h"

extern FILE *gFileLogger;
extern char m_strTimestamp[15];

#define LogError(M, ...) fprintf(gFileLogger, "[%s] [ERROR] " M "\n", LogTimestamp(m_strTimestamp), ##__VA_ARGS__)
#define LogWarning(M, ...) fprintf(gFileLogger, "[%s] [WARN] " M "\n", LogTimestamp(m_strTimestamp), ##__VA_ARGS__)
#define LogInfo(M, ...) fprintf(gFileLogger, "[%s] [INFO] " M "\n", LogTimestamp(m_strTimestamp), ##__VA_ARGS__)

#define LogApp(M, ...) {char temp_buffer[600];\
						int temp_len=sprintf(temp_buffer, "%s" M "\n", LogTimestamp(m_strTimestamp), ##__VA_ARGS__);\
						LogInApp(temp_buffer,temp_len);}

#define PrintInfo(M, ...)	{char temp_buffer[200];\
							int temp_len=sprintf(temp_buffer, "[%s] " M "\n", LogTimestamp(m_strTimestamp), ##__VA_ARGS__);\
							puts(temp_buffer);}


void LogInApp(char *str,short length);
char *LogTimestamp(char *str);
int InitFileLogger(void);
void FlushFileLogger(void);
void CloseFileLogger(void);



#endif /* INCLUDE_LOGGER_FILELOGGER_H_ */
