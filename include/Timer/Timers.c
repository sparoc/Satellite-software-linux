/*
 * Timers.c
 *
 *  Created on: Apr 1, 2016
 *      Author: ubuntu
 */

#include "Timers.h"

timer_t gMainTimer;

volatile unsigned int gTc1HzFlag = 0;
volatile unsigned int gTc10HzFlag = 0;
volatile unsigned int gTc100HzFlag = 0;
volatile unsigned int g10msClock = 1;

void TimerSignal (int signum)
{
	static int localCounter = 1;

	gTc100HzFlag = 1;
	g10msClock++;

	if((localCounter % TIMER_10Hz_INTERVAL) == 0)
	{
		gTc10HzFlag = 1;
	}

	if((localCounter % TIMER_1Hz_INTERVAL) == 0)
	{
		localCounter = 0;
		gTc1HzFlag = 1;

	}

	localCounter++;

	int overrun = timer_getoverrun (gMainTimer);

	if(overrun > 0)
	{
		//char buffer [64];
		//sprintf (buffer, "Main timer overrun counts = %d\n", overrun);
		//write (STDOUT_FILENO, buffer, strlen (buffer));
	}
}

void StartTimer(void)
{
	struct itimerspec timerInterval;
	struct sigaction action;

	memset (&action, 0, sizeof (struct sigaction));
	action.sa_handler = TimerSignal;
	if (sigaction (SIGALRM, &action, NULL) == -1)
		perror ("Timers.c - CreateTimer(): sigaction() - ");

	if (timer_create (CLOCK_REALTIME, NULL, &gMainTimer) == -1)
		perror ("Timers.c - CreateTimer(): timer_create() - ");

	timerInterval.it_interval.tv_sec = 0;
	timerInterval.it_interval.tv_nsec = MAIN_TIMER_INTERVAL_NS;
	timerInterval.it_value.tv_sec = 0;
	timerInterval.it_value.tv_nsec = MAIN_TIMER_INTERVAL_NS;

	if (timer_settime (gMainTimer, 0, &timerInterval, NULL) == -1)
		perror ("Timers.c - CreateTimer(): timer_settime() - ");
}
