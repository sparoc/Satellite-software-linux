/*
 * Timers.h
 *
 *  Created on: Apr 1, 2016
 *      Author: ubuntu
 */

#ifndef INCLUDE_TIMER_TIMERS_H_
#define INCLUDE_TIMER_TIMERS_H_

#include <signal.h>
#include <time.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>

#define MAIN_TIMER_INTERVAL_NS		10000000	// [ns], 100Hz

#define TIMER_1Hz_INTERVAL			100
#define TIMER_10Hz_INTERVAL			10

extern volatile unsigned int gTc1HzFlag;
extern volatile unsigned int gTc10HzFlag;
extern volatile unsigned int gTc100HzFlag;
extern volatile unsigned int g10msClock;

void TimerSignal (int signum);
extern void StartTimer(void);

#endif /* INCLUDE_TIMER_TIMERS_H_ */
