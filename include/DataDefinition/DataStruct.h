/*
 * DataStruct.h
 *
 *  Created on: Oct 7, 2016
 *      Author: ubuntu
 */

#ifndef INCLUDE_DATADEFINITION_DATASTRUCT_H_
#define INCLUDE_DATADEFINITION_DATASTRUCT_H_

#define ENCODER_UNKNOWN_STATUS	255;

extern void InitDataStructMembers(void);

typedef struct
{
	int WorkingMode;
	int InitProcessDone;
	int LoopCycles100Hz;
	int LoopCycles10Hz;
	int LoopCycles1Hz;
	int RunManualTest1;				// Run single joint
	int RunManualTest1All;			// Run three joint
	int RunManualTest2;				// Move satellite to required position
	int RunManualTest2RA;		// Move Robotic Arm to required position (Dynamic Jacobian)
	//int RunManualTest2RAKinJac;		// Move Robotic Arm to required position (Kinematic Jacobian)
	int RunManualTest3;				// Move satellite through generated trajectory
	int RunManualTest3RA;		// Move Robotic Arm through generated trajectory (Dynamic Jacobian)
	//int RunManualTest3RAKinJac;		// Move Robotic Arm through generated trajectory (Kinematic Jacobian)
	int RunManualTest4;				// Move satellite through trajectory from file
	int RunManualTest4RA;		// Move Robotic Arm through trajectory from file (Dynamic Jacobian)
	//int RunManualTest4RAKinJac;		// Move Robotic Arm through trajectory from file (Kinematic Jacobian)
	int RunManualJointIndex;		// Range: 0 - 2
	int ReadEnc2FromElasticJoint;	// Read encoder 2 from elastic joint
	int RunManualQuasiTraj;			// Run trajectory for all joints based on the received file
	float Joint1Enc2Position;		// Encoder position from second encoder [deg]
	int DisableGCSCommands;
	//int JointRequestSent;
	float Joint1CurrPosition;		// Range: -180 : +180 deg
	float Joint2CurrPosition;		// Range: -180 : +180 deg
	float Joint3CurrPosition;		// Range: -180 : +180 deg
	unsigned int JointResponded;
	unsigned int JointResponseTimeout;
	unsigned char JointResponseState;	// Bit field: ex. 0000 0010 means second joint responded
	int ControlAlgorithm;
	int SenseModule;
}AppState;

enum ControlAlgorithm
{
    CONTROL_KINEMATIC_WITHOUT_FB = 0,
	CONTROL_KINEMATIC_WITH_FB = 1,
	CONTROL_DYNAMIC = 2,
	CONTROL_OVF = 3,
	CONTROL_ADAPTIVE_BACK = 4,
	CONTROL_TORQUE_TRAJECTORY = 5,
};

enum SenseModule
{
    SENSE_MODULE_OFF = 0,
	SENSE_MODULE_ON = 1,
};

typedef struct
{
	float PositionXReq;				// [m]
	float PositionYReq;				// [m]
	float OrientationZReq;			// [deg]
	unsigned char PWM[8];
}SatelliteControl;



typedef struct
{
	float PositionXReq;				// [m]
	float PositionYReq;				// [m]
	float OrientationZReq;			// [deg]
}ManipulatorControl;

typedef struct
{
	float Pa1[3];
	float Pa2[3];
	float Pa3[3];
	float Pa4[3];
	float O1[3];
	float O2[3];
	float O3[3];
	float O4[3];
	float Ax1[3];
	float Ax2[3];
	float Ax3[3];
	float Ax4[3];
	float Rq[3];
	float Z1;
	float Z2;
	float Z3;
	float Z4;
	float Z5;
}LemurData;

typedef struct
{
	int ManeuverTime;
	float Force1x;
	float Force1y;
	float Force1z;
	float Force2x;
	float Force2y;
	float Force2z;
	int ContactData1;
	unsigned char SparData1;
	float SparData2x;
	float SparData2y;
	float SparData2z;
	unsigned char SparData3;
}ManipulatorParams;

typedef struct
{
	unsigned int loopts;
	unsigned int inpts;
	int	k0;
	int	k1;
	int	k2;
	int	k3;
	int	k4;
	int	k5;
	int	k6;
	int	k7;
	int	iL0;
	int	iL1;
	int	uL0;
	int	uL1;
	int	pL0;
	int	pL1;
	unsigned char Esig;
	float E0;
	int	Mlsig;
	unsigned char ID;
}JoinParamData;

typedef struct
{
	unsigned char	l_HL;
	unsigned char	l_HR;
	unsigned char	l_ML;
	unsigned char	l_MR;
	unsigned char	Ext;
	float	Eval;				// Position in [deg]
	unsigned char	Estat1;
	unsigned char	Estat2;
	unsigned char	Estat3;
	unsigned char	Mstat1;
	unsigned char	Mstat2;
	unsigned char	Mstat3;
	int	Mcurr;
	float	x0;
	float	x1;
	unsigned char	lHL;
	unsigned char	lHR;
	unsigned char	lML;
	unsigned char	lMR;
	unsigned char	ext_error;
	float	preq;
	float	uff;
	unsigned char	rm;
	float	u;
	int	i;
	unsigned char	s;
	float	xr0;
	float	xr1;
	float	ur;
	float	preqp;
	float	preqc;
	float	uffp;
	float	uffc;
	unsigned int	iter;
	int zeroOffset;
}JoinStateData;

typedef struct
{
	float Th1Traj;	// deg
	float Th2Traj;	// deg
	float Th3Traj;	// deg
	float Th4Traj;	// deg
}NomJointTrajectory;

typedef struct
{
	float xF; // N
	float yF; // N
	float zF; // N
	float xT; // Nm
	float yT; // Nm
	float zT; // Nm
	float q1; // 1
	float q2; // 1
	float q3; // 1
	float q4; // 1
	float xW; // deg/s
	float yW; // deg/s
	float zW; // deg/s
	float xP; // m
	float yP; // m
	float zP; // m
	float xV; // m/s
	float yV; // m/s
	float zV; // m/s
}Satellite;

typedef struct
{
	unsigned char ProcessDone;	// 0-1
	float Th1Ref;	// deg
	float Th2Ref;	// deg
	float Th3Ref;	// deg
	float Th4Ref;	// deg
}RefJointTrajectory;

typedef struct
{
	float X;
	float Y;
	float Z;
}EEPositionLFF;

typedef struct
{
	int Sequence;
	float TargetX;
	float TargetY;
	float TargetZ;
	float TargetQ1;
	float TargetQ2;
	float TargetQ3;
	float TargetQ4;
	float TargetVx;
	float TargetVy;
	float TargetVz;
	float TargetWx;
	float TargetWy;
	float TargetWz;
	unsigned char SpaceParam;	// 0 - cartesian, 1- configuration
}NomCartesianTrajectory;

typedef struct
{
	int TrajCounter;
	NomCartesianTrajectory NomCartTraj;
	NomJointTrajectory ThTraj;
}Trajectory;

typedef struct
{
	JoinStateData State;
	JoinParamData Param;
}JointStatus;

extern SatelliteControl sSatCtrl;
extern AppState sAppState;
extern LemurData sLemur;
extern EEPositionLFF sEEPosLFF;
extern Trajectory *sTraj;
extern ManipulatorParams sManParams;
extern Satellite sSatState;
extern NomJointTrajectory sNomJointTraj;
extern RefJointTrajectory sRefJointTraj;
extern NomCartesianTrajectory sNomCartTraj;
extern JointStatus sJoint1Status;
extern JointStatus sJoint2Status;
extern JointStatus sJoint3Status;
extern JointStatus sJoint4Status;

#endif /* INCLUDE_DATADEFINITION_DATASTRUCT_H_ */
