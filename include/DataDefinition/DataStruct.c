/*
 * DataStruct.c
 *
 *  Created on: Oct 7, 2016
 *      Author: ubuntu
 */

#include "DataStruct.h"

AppState sAppState;
SatelliteControl sSatCtrl;
ManipulatorControl sManipCtrl;
LemurData sLemur;
Trajectory *sTraj;
EEPositionLFF sEEPosLFF;
ManipulatorParams sManParams;
Satellite sSatState;
NomJointTrajectory sNomJointTraj;
RefJointTrajectory sRefJointTraj;
NomCartesianTrajectory sNomCartTraj;
JointStatus sJoint1Status;
JointStatus sJoint2Status;
JointStatus sJoint3Status;
JointStatus sJoint4Status;


void InitDataStructMembers()
{
	sLemur.Pa1[0] = 1.001;
	sLemur.Pa1[1] = 1.002;
	sLemur.Pa1[2] = 1.003;

	sLemur.Pa2[0] = 2.001;
	sLemur.Pa2[1] = 2.002;
	sLemur.Pa2[2] = 2.003;

	sLemur.Pa3[0] = 3.001;
	sLemur.Pa3[1] = 3.002;
	sLemur.Pa3[2] = 3.003;

	sLemur.Pa4[0] = 4.001;
	sLemur.Pa4[1] = 4.002;
	sLemur.Pa4[2] = 4.003;

	sJoint1Status.State.Estat1 = ENCODER_UNKNOWN_STATUS;
	sJoint2Status.State.Estat1 = ENCODER_UNKNOWN_STATUS;
	sJoint3Status.State.Estat1 = ENCODER_UNKNOWN_STATUS;

	sJoint1Status.State.Eval = 0;
	sJoint2Status.State.Eval = 0;
	sJoint3Status.State.Eval = 0;

	sJoint1Status.Param.E0 = 0;
	sJoint2Status.Param.E0 = 0;
	sJoint3Status.Param.E0 = 0;
}
