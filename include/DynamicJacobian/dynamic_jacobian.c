/*
 * dynamic_jacobian.c
 *
 *  Created on: 1 lis 2021
 *      Author: FLB
 */

#include "dynamic_jacobian.h"
#include "../MatrixOperations/matrix_operations.h"

void DynamicJacobian(float qs, float q1, float q2, float q3, float (*DynJac)[9])
{
	float Jb_0ee[9][9],Jb_00[9][9], Jb_01[9][9], Jb_02[9][9], Jb_03[9][9];
	float Adg03_1[9][9], Adg02_1[9][9], Adg01_1[9][9], Adg00_1[9][9];
	float Adg3ee[9][9], Adg2ee[9][9], Adg1ee[9][9], Adg0ee[9][9];
	float M0[9][9], M1[9][9], M2[9][9], M3[9][9];
	float JB1[9][9], JB2[9][9], Jac_TR[9][9], Jac_TR_final[9][9];
	float resultA[9][9], resultB[9][9], result0A[9][9], result1A[9][9], result2A[9][9], result3A[9][9];
	float sum1A[9][9], sum2A[9][9];
    float resultC[9][9], resultD[9][9];
 	float result0B[9][9], result1B[9][9], result2B[9][9], result3B[9][9];
    float sum1B[9][9], sum2B[9][9];
	float q = qs + q1 + q2 + q3;
	float rot_matrix[9][9];

	rot_matrix[0][0] = cos(q);
	rot_matrix[0][1] = -sin(q);
	rot_matrix[0][2] = 0;
	rot_matrix[1][0] = sin(q);
	rot_matrix[1][1] = cos(q);
	rot_matrix[1][2] = 0;
	rot_matrix[2][0] = 0;
	rot_matrix[2][1] = 0;
	rot_matrix[2][2] = 1;

	const float error_param = 1.2;

	float L1 = 0.4490;
	float L2 = 0.4490;
	float L3 = 0.3103;
	float c1 = 0.1362;
	float c2 = 0.1340;
	float c3 = 0.1511;
	float b1 = -0.0017;
	float b2 = -0.0005;
	float b3 = 0.0004;
	float sx = -0.3770;
	float sy = 0.001;
	float m1 = 2.8098*error_param;
	float m2 = 2.8200*error_param;
	float m3 = 4.64*error_param;
	float ms = 58.6858*error_param;
	float I1 = 0.0637*error_param;
	float I2 = 0.0635*error_param;
	float I3 = 0.0515*error_param;
	float Is = 2.4172*error_param;

    for(int i=0; i<6; i+=1)
    	for(int j=0; j<3; j+=1)
    	{
    		Jb_0ee[i][j] = 0;
    		Jb_00[i][j] = 0;
			Jb_01[i][j] = 0;
			Jb_02[i][j] = 0;
			Jb_03[i][j] = 0;
        }

    for(int i=0; i<6; i=i+1)
    	for(int j=0; j<6; j=j+1)
    	{
    		Adg03_1[i][j] = 0;
    		Adg02_1[i][j] = 0;
			Adg01_1[i][j] = 0;
			Adg00_1[i][j] = 0;
			Adg3ee[i][j] = 0;
			Adg2ee[i][j] = 0;
			Adg1ee[i][j] = 0;
			Adg0ee[i][j] = 0;
			M0[i][j] = 0;
			M1[i][j] = 0;
			M2[i][j] = 0;
			M3[i][j] = 0;
        }

	Jb_0ee[0][0] = L1*sin(q2+q3) + L2*sin(q3);
    Jb_0ee[0][1] = L2*sin(q3);
    Jb_0ee[1][0] = L1*cos(q2+q3) + L2*cos(q3) + L3;
    Jb_0ee[1][1] = L2*cos(q3) + L3;
    Jb_0ee[1][2] = L3;
    Jb_0ee[5][0] = 1;
    Jb_0ee[5][1] = 1;
    Jb_0ee[5][2] = 1;

    Adg03_1[0][0] = cos(q1+q2+q3);
    Adg03_1[0][1] = sin(q1+q2+q3);
    Adg03_1[0][5] = (L1*sin(q2+q3)+L2*sin(q3)-b3);
    Adg03_1[1][0] = -sin(q1+q2+q3);
    Adg03_1[1][1] = cos(q1+q2+q3);
    Adg03_1[1][5] = (L1*cos(q2+q3)+L2*cos(q3)+c3);
    Adg03_1[2][2] = 1;
    Adg03_1[2][3] = (L1*sin(q1)+L2*sin(q1+q2)+c3*sin(q1+q2+q3)+b3*cos(q1+q2+q3));
    Adg03_1[2][4] = -(L1*cos(q1)+L2*cos(q1+q2)+c3*cos(q1+q2+q3)-b3*sin(q1+q2+q3));
    Adg03_1[3][3] = cos(q1+q2+q3);
    Adg03_1[3][4] = sin(q1+q2+q3);
    Adg03_1[4][3] = -sin(q1+q2+q3);
    Adg03_1[4][4] = cos(q1+q2+q3);
    Adg03_1[5][5] = 1;

    Adg02_1[0][0] = cos(q1 + q2);
    Adg02_1[0][1] = sin(q1 + q2);
    Adg02_1[0][5] = L1*sin(q2)-b2;
    Adg02_1[1][0] = -sin(q1 + q2);
    Adg02_1[1][1] = cos(q1 + q2);
    Adg02_1[1][5] = c2 + L1*cos(q2);
    Adg02_1[2][2] = 1;
    Adg02_1[2][3] = c2*sin(q1 + q2) + L1*sin(q1) + b2*cos(q1+q2);
    Adg02_1[2][4] = -c2*cos(q1 + q2) - L1*cos(q1) + b2*sin(q1+q2);
    Adg02_1[3][3] = cos(q1 + q2);
    Adg02_1[3][4] = sin(q1 + q2);
    Adg02_1[4][3] = -sin(q1 + q2);
    Adg02_1[4][4] = cos(q1 + q2);
    Adg02_1[5][5] = 1;

    Adg01_1[0][0] = cos(q1);
    Adg01_1[0][1] = sin(q1);
    Adg01_1[0][5] = -b1;
    Adg01_1[1][0] = -sin(q1);
    Adg01_1[1][1] = cos(q1);
    Adg01_1[1][5] = c1;
    Adg01_1[2][2] = 1;
    Adg01_1[2][3] = c1*sin(q1) + b1*cos(q1);
    Adg01_1[2][4] = -c1*cos(q1) + b1*sin(q1);
    Adg01_1[3][3] = cos(q1);
    Adg01_1[3][4] = sin(q1);
    Adg01_1[4][3] = -sin(q1);
    Adg01_1[4][4] =  cos(q1);
    Adg01_1[5][5] = 1;

    Adg00_1[0][0] = 1;
    Adg00_1[0][5] = -sy;
    Adg00_1[1][1] = 1;
    Adg00_1[1][5] = sx;
    Adg00_1[2][2] = 1;
    Adg00_1[2][3] = sy;
    Adg00_1[2][4] = -sx;
    Adg00_1[3][3] = 1;
    Adg00_1[4][4] = 1;
    Adg00_1[5][5] = 1;

    Adg3ee[0][0] = 1;
    Adg3ee[0][5] = -b3;
    Adg3ee[1][1] = 1;
    Adg3ee[1][5] = -(L3 - c3);
    Adg3ee[2][2] = 1;
    Adg3ee[2][3] = b3;
    Adg3ee[2][4] = L3 - c3;
    Adg3ee[3][3] = 1;
    Adg3ee[4][4] = 1;
    Adg3ee[5][5] = 1;

    Adg2ee[0][0] = cos(q3);
    Adg2ee[0][1] = -sin(q3);
    Adg2ee[0][5] = L3*sin(q3)-b2;
    Adg2ee[1][0] = sin(q3);
    Adg2ee[1][1] = cos(q3);
    Adg2ee[1][5] = -(L2-c2+L3*cos(q3));
    Adg2ee[2][2] = 1;
    Adg2ee[2][3] = sin(q3)*(L2-c2)+b2*cos(q3);
    Adg2ee[2][4] = L3+(L2-c2)*cos(q3)-b2*sin(q3);
    Adg2ee[3][3] = cos(q3);
    Adg2ee[3][4] = -sin(q3);
    Adg2ee[4][3] = sin(q3);
    Adg2ee[4][4] = cos(q3);
    Adg2ee[5][5] = 1;

    Adg1ee[0][0] = cos(q2+q3);
    Adg1ee[0][1] = -sin(q2+q3);
    Adg1ee[0][5] = L2*sin(q2)+L3*sin(q2+q3)-b1;
    Adg1ee[1][0] = sin(q2+q3);
    Adg1ee[1][1] = cos(q2+q3);
    Adg1ee[1][5] = -(L1-c1+L2*cos(q2)+L3*cos(q2+q3));
    Adg1ee[2][2] = 1;
    Adg1ee[2][3] = L2*sin(q3)+(L1-c1)*sin(q2+q3)+b1*cos(q2+q3);
    Adg1ee[2][4] = L2*cos(q3)+(L1-c1)*cos(q2+q3)+L3-b1*sin(q2+q3);
    Adg1ee[3][3] = cos(q2+q3);
    Adg1ee[3][4] = -sin(q2+q3);
    Adg1ee[4][3] = sin(q2+q3);
    Adg1ee[4][4] = cos(q2+q3);
    Adg1ee[5][5] = 1;

    Adg0ee[0][0] = cos(q1+q2+q3);
    Adg0ee[0][1] = -sin(q1+q2+q3);
    Adg0ee[0][5] = -sy+L1*sin(q1)+L2*sin(q1+q2)+L3*sin(q1+q2+q3);
    Adg0ee[1][0] = sin(q1+q2+q3);
    Adg0ee[1][1] = cos(q1+q2+q3);
    Adg0ee[1][5] = -(-sx+L1*cos(q1)+L2*cos(q1+q2)+L3*cos(q1+q2+q3));
    Adg0ee[2][2] = 1;
    Adg0ee[2][3] = sy*cos(q1+q2+q3)-sx*sin(q1+q2+q3)+L1*sin(q2+q3)+L2*sin(q3);
    Adg0ee[2][4] = -sy*sin(q1+q2+q3)-sx*cos(q1+q2+q3)+L1*cos(q2+q3)+L2*cos(q3)+L3;
    Adg0ee[3][3] = cos(q1+q2+q3);
    Adg0ee[3][4] = -sin(q1+q2+q3);
    Adg0ee[4][3] = sin(q1+q2+q3);
    Adg0ee[4][4] = cos(q1+q2+q3);
    Adg0ee[5][5] = 1;

    M0[0][0] = ms;
    M0[1][1] = ms;
    M0[2][2] = ms;
    M0[5][5] = Is;

    M1[0][0] = m1;
    M1[1][1] = m1;
    M1[2][2] = m1;
    M1[5][5] = I1;

    M2[0][0] = m2;
    M2[1][1] = m2;
    M2[2][2] = m2;
    M2[5][5] = I2;

    M3[0][0] = m3;
    M3[1][1] = m3;
    M3[2][2] = m3;
    M3[5][5] = I3;

    Jb_01[0][0] = -b1;
    Jb_01[1][0] = c1;
    Jb_01[5][0] = 1;

    Jb_02[0][0] = -b2 + L1*sin(q2);
    Jb_02[0][1] = -b2;
    Jb_02[1][0] = c2 + L1*cos(q2);
    Jb_02[1][1] = c2;
    Jb_02[5][0] = 1;
    Jb_02[5][1] = 1;

    Jb_03[0][0] = L1*sin(q2+q3)+L2*sin(q3)-b3;
    Jb_03[0][1] = L2*sin(q3)-b3;
    Jb_03[0][2] = -b3;
    Jb_03[1][0] = L1*cos(q2+q3)+L2*cos(q3)+c3;
    Jb_03[1][1] = L2*cos(q3)+c3;
    Jb_03[1][2] = c3;
    Jb_03[5][0] = 1;
    Jb_03[5][1] = 1;
    Jb_03[5][2] = 1;

    MatrixTranspose(Adg00_1, resultA, 6, 6);
    MatrixMultiplication(resultA, M0, resultB, 6, 6, 6);
    MatrixMultiplication(resultB, Adg0ee, result0A, 6, 6, 6);

    MatrixTranspose( Adg01_1, resultA, 6, 6);
    MatrixMultiplication(resultA, M1, resultB, 6, 6, 6);
    MatrixMultiplication(resultB, Adg1ee, result1A, 6, 6, 6);

    MatrixTranspose( Adg02_1, resultA, 6, 6);
    MatrixMultiplication(resultA, M2, resultB, 6, 6, 6);
    MatrixMultiplication(resultB, Adg2ee, result2A, 6, 6, 6);

    MatrixTranspose( Adg03_1, resultA, 6, 6);
    MatrixMultiplication(resultA, M3, resultB, 6, 6, 6);
    MatrixMultiplication(resultB, Adg3ee, result3A, 6, 6, 6);

    MatrixSum(result0A, result1A, sum1A, 6, 6);
    MatrixSum(sum1A, result2A, sum2A, 6, 6);
    MatrixSum(sum2A, result3A, JB1, 6, 6);

    MatrixTranspose(Adg00_1, resultA, 6, 6);
    MatrixMultiplication(resultA, M0, resultB, 6, 6, 6);
    MatrixMultiplication(Adg0ee, Jb_0ee, resultC, 6, 6, 3);
    MatrixSubtract(resultC, Jb_00, resultD, 6, 3);
    MatrixMultiplication(resultB, resultD, result0B, 6, 6, 3);

    MatrixTranspose(Adg01_1, resultA, 6, 6);
    MatrixMultiplication(resultA, M1, resultB, 6, 6, 6);
    MatrixMultiplication(Adg1ee, Jb_0ee, resultC, 6, 6, 3);
    MatrixSubtract(resultC, Jb_01, resultD, 6, 3);
    MatrixMultiplication(resultB, resultD, result1B, 6, 6, 3);

    MatrixTranspose(Adg02_1, resultA, 6, 6);
    MatrixMultiplication(resultA, M2, resultB, 6, 6, 6);
    MatrixMultiplication(Adg2ee, Jb_0ee, resultC, 6, 6, 3);
    MatrixSubtract(resultC, Jb_02, resultD, 6, 3);
    MatrixMultiplication(resultB, resultD, result2B, 6, 6, 3);

    MatrixTranspose(Adg03_1, resultA, 6, 6);
    MatrixMultiplication(resultA, M3, resultB, 6, 6, 6);
    MatrixMultiplication(Adg3ee, Jb_0ee, resultC, 6, 6, 3);
    MatrixSubtract(resultC, Jb_03, resultD, 6, 3);
    MatrixMultiplication(resultB, resultD, result3B, 6, 6, 3);

    MatrixSum(result0B, result1B, sum1B, 6, 3);
    MatrixSum(sum1B, result2B, sum2B, 6, 3);
    MatrixSum(sum2B, result3B, JB2, 6, 3);

    MatrixInverse(JB1, 6, resultA);
    MatrixMultiplication(resultA, JB2, Jac_TR, 6, 6, 3);

    for(int i=0;i<2;i++)
    	for(int j=0; j<3; j++)
    		Jac_TR_final[i][j] = Jac_TR[i][j];

    for(int j=0; j<3; j++)
    	Jac_TR_final[2][j] = Jac_TR[5][j];

    MatrixMultiplication(rot_matrix, Jac_TR_final, DynJac, 3, 3, 3);

}
