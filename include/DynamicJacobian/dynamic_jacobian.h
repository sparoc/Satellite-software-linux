/*
 * dynamic_jacobian.h
 *
 *  Created on: 1 lis 2021
 *      Author: FLB
 */

#ifndef DYNAMIC_JACOBIAN_H_
#define DYNAMIC_JACOBIAN_H_

void DynamicJacobian(float q0, float q1, float q2, float q3, float (*DynJac)[9]);

#endif /* DYNAMIC_JACOBIAN_H_ */
