#include "OVF.h"

#include "firas/repulse_vecFIRAS.h"
#include "jacobian_ovf/dynamic_jacobian.h"
#include "../MatrixOperations/matrix_operations.h"

// xActualPosition Actual position of the EE
extern void OVF(strObstacle *obstacles, strPoint *xDesiredPosition,
		strPoint *xActualPosition, strPoint *xThirdLinkActualPosition,
		double fDesiredPhi, double xActualPhi, double fTh1, double fTh2,
		double fTh3, double (*u_total)[9]) {

	double fEEDist = norm(xDesiredPosition, xActualPosition);

	double fPhiDist = fDesiredPhi - xActualPhi;

	double xVectKk[9][9];
	xVectKk[0][0] = (xDesiredPosition->x - xActualPosition->x);
	xVectKk[1][0] = (xDesiredPosition->y - xActualPosition->y);
	xVectKk[2][0] = fDesiredPhi - xActualPhi;

	//Parameters of the FIRAS function:
	double alg_par_FIRAS_A = 0.005;
	double alg_par_FIRAS_r0 = 10;

	// Parameters of the OVF vector field
	double alg_par_OVF_sightdist = 0.02;
	double alg_par_obst_dist_min = 0.1;
	double alg_par_OVF_direction = 1.35;
	double alg_par_obst_dir = -1;
	double alg_par_f1 = -30;
	double alg_par_f2 = 1;
	double alg_par_f3 = -70;
	double alg_par_f4 = 1 / 2;
	double alg_par_f5 = -50;
	double alg_par_f6 = 1;

	// Parameters that affect the forces when the end-effector is close to the desired position
	double alg_par_G_att_multip = 20;
	double alg_par_G_rep_multip = 1;

	double fDynJac[9][9];

	DynamicJacobianOVF(fTh1, fTh2, fTh3, fDynJac);

	double alg_par_G_att = alg_par_G_att_multip * exp(alg_par_f1 * fEEDist)
			+ alg_par_f2;
	double alg_par_G_att_phi = alg_par_G_att_multip * exp(alg_par_f3 * fPhiDist)
			+ alg_par_f4;
	double alg_par_G_rep = alg_par_G_rep_multip
			* (-exp(alg_par_f5 * fEEDist) + alg_par_f6);

	//strVector xSize;
	//	strVector xPosition;
	//	double dRotation;
	//[PP_link, PP_obst] = close_link_obst(J3, JEE, obstacles);

	strPoint points[] = { *xThirdLinkActualPosition, *xActualPosition };

	strPoint PP_link, PP_obst;

	vCloseLinkObst(points, obstacles, &PP_link, &PP_obst);

	//strVector Pobst;
	strVector PO[5];
	double dTempMatrix;

	for (int i = 1; i < 6; i++) {
		///TODO: W innej kolejności sa wartosci w PO?
		///

		double Pobst[9][9];

		Pobst[0][0] = pow(-1, floor(1.5 + 0.5 * i)) * obstacles->xSize.x / 2;
		Pobst[0][1] = pow(-1, floor(2 + 0.5 * i)) * obstacles->xSize.y / 2;

		//Pobst.x =
		//Pobst.y = pow(-1, floor(2 + 0.5 * i)) * obstacles.xSize.y / 2;
///TODO: Dlaczego w OVF_main.m w linijce 56 jest macierz [1 0;0 1]? Dla podanych danych
/// zawsze bedzie taka sama i w 56 linijce nastepuje sumowanie dwóch wektorów

		double dRotMatrix[9][9];

		vRotMatrix(obstacles->dRotation, dRotMatrix);

		double dFinalMatrix[9][9];

		MatrixMultiplication(dRotMatrix, Pobst, dFinalMatrix, 2, 1, 2);

		PO[i - 1].x = obstacles->xPosition.x + dFinalMatrix[0][0];
		PO[i - 1].y = obstacles->xPosition.y + dFinalMatrix[0][1];
	}

	bool intersec1, intersec2, intersec3, intersec4;
	strLine xTempLineRobot, xTempLineObstacle;

	///TODO: Przerobic na wektor!!!
	xTempLineRobot.xLineBeginning.x = xActualPosition->x;
	xTempLineRobot.xLineBeginning.y = xActualPosition->y;
	xTempLineRobot.xLineEnd.x = xDesiredPosition->x;
	xTempLineRobot.xLineEnd.y = xDesiredPosition->y;

	xTempLineObstacle.xLineBeginning.x = PO[0].x;
	xTempLineObstacle.xLineBeginning.y = PO[0].y;
	xTempLineObstacle.xLineEnd.x = PO[1].x;
	xTempLineObstacle.xLineEnd.y = PO[1].y;
	intersec1 = bLineIntersection(&xTempLineRobot, &xTempLineObstacle);
	intersec2 = bLineIntersection(&xTempLineRobot, &xTempLineObstacle);
	intersec3 = bLineIntersection(&xTempLineRobot, &xTempLineObstacle);
	intersec4 = bLineIntersection(&xTempLineRobot, &xTempLineObstacle);

	double inter_test = intersec1 + intersec2 + intersec3 + intersec4;
	double inter_dist = norm(&PP_link, &PP_obst);

	if (inter_test > 0 && inter_dist < alg_par_OVF_sightdist * 7 / 4) {
		//% If the desired final position is obscured by the k-th obstacle and the manipulator is sufficiently close to that obstacle...
		alg_par_G_att = 0; // In such case there is no attractive potential of the desired final position
	}

	//= pot_FIRAS(obstacles, PP_link(1), PP_link(2), alg_par_FIRAS_A, alg_par_FIRAS_r0);

	double pot_obst = fPotFIRAS(obstacles, &PP_link, alg_par_FIRAS_A,
			alg_par_FIRAS_r0);

	// Computation of the repulsive force direction
	// Transpozycja moze nie jest konieczna
	strVector L1cg = { .x = (xActualPosition->x - xThirdLinkActualPosition->x)
			/ 2 + xThirdLinkActualPosition->x, .y = (xActualPosition->y
			- xThirdLinkActualPosition->y) / 2 + xThirdLinkActualPosition->y };

	//((JEE - J3) / 2 + J3)
	// Point at the centre of the manipulator link

	double vect_obstL3[2] = { 0 };
	//tordir_test1 = [];
	double rep_u_obst[3] = { 0 };
	double dynJac_L3obs_red[2][3] = { { 0, 0 } };
	double rep_dir[2] = { 0, 0 };
	double tordir_obstL3[3] = { 0 };

	double temp = norm(&PP_link, &PP_obst);

	if (norm(&PP_link, &PP_obst) < alg_par_obst_dist_min) {
		//[vect_par, vect_rep, vect_sum]

		double vect_sum[2], vect_parEE[9][9], dGarb_vect_parEE[9][9],
				dGarbvect_sum[2];
		repulse_vectFIRAS(obstacles, &PP_link, alg_par_obst_dir,
				alg_par_OVF_direction, alg_par_FIRAS_A, alg_par_FIRAS_r0,
				alg_par_OVF_sightdist,
				((inter_test > 0) ? 1 : ((inter_test < 0) ? -1 : 0)), vect_sum,
				dGarb_vect_parEE); //% Directions from the vector field (modified OVF method)

		repulse_vectFIRAS(obstacles, xActualPosition, alg_par_obst_dir,
				alg_par_OVF_direction, alg_par_FIRAS_A, alg_par_FIRAS_r0,
				alg_par_OVF_sightdist,
				((inter_test > 0) ? 1 : ((inter_test < 0) ? -1 : 0)),
				dGarbvect_sum, vect_parEE);

		//DODAC TE EELEMENTY!!!!!
		//rep_dir = vect_sum;
		//vect_obstL3 = pot_obst*rep_dir;

		double vect_obstL3[9][9];

		vect_obstL3[0][0] = pot_obst * vect_sum[0] * alg_par_G_rep;
		vect_obstL3[1][0] = pot_obst * vect_sum[1] * alg_par_G_rep;

		double dist_J3L3obs = norm(xThirdLinkActualPosition, &PP_link);

		double dynJac_L3obs[9][9];

		double dOldvalue;

		dOldvalue = vModifyL(2, dist_J3L3obs);

		DynamicJacobianOVF(fTh1, fTh2, fTh3, dynJac_L3obs);

		vModifyL(2, dOldvalue);

		double tordir_obstL3[3], dVector1[3], dVector2[3];		//={}

		dVector1[0] = xActualPosition->x - xThirdLinkActualPosition->x;
		dVector1[1] = xActualPosition->y - xThirdLinkActualPosition->y;
		dVector1[2] = 0;

		dVector2[0] = vect_parEE[0][0];
		dVector2[1] = vect_parEE[1][0];
		dVector2[2] = 0;

		CrossProd(dVector1, dVector2, tordir_obstL3);
		//tordir_obstL3 = cross([JEE - J3;0], [vect_parEE;0]);

		if (fEEDist >= 0.025) {
			double dMatrixRot[9][9], dInvertedMatrixRot[9][9],
					dFirstVector[9][9], dFinalVector[9][9],
					dInvdynJac_L3obs[9][9], dMultirep_u_obst[9][9];

			vRotMatrix(xActualPhi, dMatrixRot);

			MatrixInverse(dMatrixRot, 2, dInvertedMatrixRot);

			MatrixMultiplication(dInvertedMatrixRot, vect_obstL3, dFirstVector,
					2, 2, 1);

			dFinalVector[0][0] = dFirstVector[0][0];
			dFinalVector[1][0] = dFirstVector[1][0];
			dFinalVector[2][0] = (
					(tordir_obstL3[2] > 0) ?
							1 : ((tordir_obstL3[2] < 0) ? -1 : 0))
					* (sqrt(
							vect_obstL3[0][0] * vect_obstL3[0][0]
									+ vect_obstL3[1][0] * vect_obstL3[1][0]));

			MatrixInverse(dynJac_L3obs, 3, dInvdynJac_L3obs);

			MatrixMultiplication(dInvdynJac_L3obs, dFinalVector, dMultirep_u_obst,
					3, 3, 1);

			rep_u_obst[0]=dMultirep_u_obst[0][0];
			rep_u_obst[1]=dMultirep_u_obst[1][0];
			rep_u_obst[2]=dMultirep_u_obst[2][0];

			//int a = 0;

			//a++;
			//rep_u_obst = inv(dynJac_L3obs)*[inv([cos(phi), -sin(phi);sin(phi), cos(phi)])*(alg_par_G_rep*vect_obstL3(:,end));   1*sign(tordir_obstL3(3))*(alg_par_G_rep*norm(vect_obstL3(:,end)))];// % Computation of repulsive torques in manipulator joints
		} else {
			int a;
			//dynJac_L3obs_red = dynJac_L3obs(1:2,1:3);
			//rep_u_obst = pinv(dynJac_L3obs_red)*[inv([cos(phi), -sin(phi);sin(phi), cos(phi)])*(alg_par_G_rep*vect_obstL3(:,end))];// % Computation of repulsive torques in manipulator joints
		}

	}

	double rep_u_total[3];

	rep_u_total[0] = rep_u_obst[0];
	rep_u_total[1] = rep_u_obst[1];
	rep_u_total[2] = rep_u_obst[2];

	double att_u_targ[9][9], rotMatrixTotal[9][9], inverserotMatrixTotal[9][9],
			dFirstArgument[9][9], dMatrixToMultiplication[9][9],
			inverseDynJac[9][9];

	vRotMatrix(xActualPhi, rotMatrixTotal);

	xVectKk[0][0] = xVectKk[0][0] * 5 * alg_par_G_att;
	xVectKk[1][0] = xVectKk[1][0] * 5 * alg_par_G_att;
	xVectKk[2][0] = xVectKk[2][0] * 5 * alg_par_G_att_phi;

	MatrixInverse(rotMatrixTotal, 2, inverserotMatrixTotal);
	MatrixInverse(fDynJac, 3, inverseDynJac);

	MatrixMultiplication(inverserotMatrixTotal, xVectKk, dFirstArgument, 2, 2,
			1);

	dMatrixToMultiplication[0][0] = dFirstArgument[0][0];
	dMatrixToMultiplication[1][0] = dFirstArgument[1][0];
	dMatrixToMultiplication[2][0] = xVectKk[2][0];

	MatrixMultiplication(inverseDynJac, dMatrixToMultiplication, att_u_targ, 3,
			3, 1);

	u_total[0][0] = att_u_targ[0][0] + rep_u_total[0];
	u_total[1][0] = att_u_targ[1][0] + rep_u_total[1];
	u_total[2][0] = att_u_targ[2][0] + rep_u_total[2];
}

