/*
 * dynamic_jacobian.h
 *
 *  Created on: 1 lis 2021
 *      Author: FLB
 */

#ifndef DYNAMIC_JACOBIAN_H_
#define DYNAMIC_JACOBIAN_H_

extern void DynamicJacobianOVF(double q1, double q2, double q3, double (*DynJac)[9]);
extern double vModifyL(int dValue, double dLValue);

#endif /* DYNAMIC_JACOBIAN_H_ */
