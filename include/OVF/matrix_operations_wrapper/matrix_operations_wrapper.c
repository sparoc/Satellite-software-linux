/**
 * Wrapper file has been written to add new functionality to the old
 * matrix operations library. The new library is based on structures
 * defined in header file. To use any matrix operation function, the
 * wrapper should be used without including matrix_operations.h!
 *
 * @{
 */

/*--------------------------------------------------------------*/
/*--- #includes of type <...> ----------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- #includes of type "..." ----------------------------------*/
/*--------------------------------------------------------------*/

#include "matrix_operations_wrapper.h"
#include "../../MatrixOperations/matrix_operations.h"

/*--------------------------------------------------------------*/
/*--- #define-constants and macros -----------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- data types (typedef) -------------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- global constants -----------------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- global variables -----------------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- local constants ------------------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- local variables ------------------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- prototypes local functions--------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- function definitions -------------------------------------*/
/*--------------------------------------------------------------*/

/* Subtract two vectors */
extern void vVectorSubtract(strVector *xResultVector, strVector *xVector1,
		strVector *xVector2) {
	//float fVecor1[3], fVecor2[3], fVecor3[3];

	//fVecor1[0] = xVector1->x;
	//fVecor1[1] = xVector1->y;

	//fVecor2[0] = xVector2->x;
	//fVecor2[1] = xVector2->y;

	//VecSubtract(fVecor1, fVecor2, fVecor3);

	xResultVector->x = xVector1->x - xVector2->x;	//fVecor3[0];
	xResultVector->y = xVector1->y - xVector2->y;	//fVecor3[1];
	xResultVector->z = xVector1->z - xVector2->z;	//fVecor3[2];
}

/* Inverse the matrix */
extern void vMatrixInverseCC(float (*fMatrixToInverse)[9], int iMatrixSize,
		float (*fMatrixInverted)[9]) {
	MatrixInverse(fMatrixToInverse, (float) iMatrixSize, fMatrixInverted);
}

/* Insert vector to the first matrix's column */
extern void vInsertVector2Matrix(strVector *xVectorToPutToMatrix,
		float (*fResultMatrix)[9]) {
	fResultMatrix[0][0] = xVectorToPutToMatrix->x;
	fResultMatrix[1][0] = xVectorToPutToMatrix->y;
	fResultMatrix[2][0] = xVectorToPutToMatrix->z;
}

/* Matrixes multiplication */
extern void vMatrixMultiplication(float (*fMatrix1)[9], float (*xMatrix2)[9],
		float (*fResultMatrix)[9], int iMatrixHeight, int iMatrixWidthMax,
		int iMatrixWidth) {
	MatrixMultiplication(fMatrix1, xMatrix2, fResultMatrix, iMatrixHeight,
			iMatrixWidthMax, iMatrixWidth);
}

/* Cutting vector from matrix; cutting first column of the matrix */
extern void vCutVectorFromMatrix(float (*fMatrix)[9], strVector *xVector) {
	xVector->x = fMatrix[0][0];
	xVector->y = fMatrix[1][0];
	xVector->z = fMatrix[2][0];
}

/* Matrix determinant counting */
extern float fMatrixDeterminant(float (*matrix)[9], int iMatrixSize) {
	return MatrixDeterminant(matrix, (float) iMatrixSize);
}

extern strVector vVecSubtract(strVector *p1, strVector *p2) {
	float vec_1[3], vec_2[3], vec_3[3];
	vec_1[0] = p1->x;
	vec_1[1] = p1->y;

	vec_2[0] = p2->x;
	vec_2[1] = p2->y;

	VecSubtract(vec_1, vec_2, vec_3);

	strVector point_to_return = { vec_3[0], vec_3[1], 0 };

	return point_to_return;
}

extern void vRotMatrix(double dAngleValue, double (*matrix)[9]) {
	matrix[0][0] = cos(dAngleValue);
	matrix[0][1] = -sin(dAngleValue);
	matrix[1][0] = sin(dAngleValue);
	matrix[1][1] = cos(dAngleValue);
}

//! @}
