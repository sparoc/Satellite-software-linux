#ifndef INC_MATRIX_OPERATIONS_WRAPPER_MATRIX_OPERATIONS_WRAPPER_H_
#define INC_MATRIX_OPERATIONS_WRAPPER_MATRIX_OPERATIONS_WRAPPER_H_

/*--------------------------------------------------------------*/
/*--- #includes of type <...> ----------------------------------*/
/*--------------------------------------------------------------*/

#include "stdio.h"

/*--------------------------------------------------------------*/
/*--- #includes of type "..." ----------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- #define-constants and macros -----------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- data types (typedef) -------------------------------------*/
/*--------------------------------------------------------------*/

/* Vector structure */
typedef struct strVector {
	double x, y, z;
} strVector;

/*--------------------------------------------------------------*/
/*--- global constants (extern const) --------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- global variables (extern) --------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- prototypes global functions ------------------------------*/
/*--------------------------------------------------------------*/

/* Subtract two vectors */
extern void vVectorSubtract(strVector *xResultVector, strVector *xVector1,
		strVector *xVector2);

/* Inverse the matrix */
extern void vMatrixInverseCC(float (*fMatrixToInverse)[9], int iMatrxSize,
		float (*iMatrixSize)[9]);

/* Insert vector to the first matrix's column */
extern void vInsertVector2Matrix(strVector *xVectorToPutToMatrix,
		float (*fResultMatrix)[9]);

/* Matrixes multiplication */
extern void vMatrixMultiplication(float (*fMatrix1)[9], float (*xMatrix2)[9],
		float (*fResultMatrix)[9], int iMatrixHeight, int iMatrixWidth,
		int iMatrixWidth2);

/* Cutting vector from matrix; cutting first column of the matrix */
extern void vCutVectorFromMatrix(float(*fMatrix)[9], strVector *xVector);

/* Matrix determinant counting */
extern float fMatrixDeterminant(float(*matrix)[9], int iMatrixSize);

/* Vector subtract */
extern strVector vVecSubtract(strVector *p1, strVector *p2);

/* Rot matrix */
extern void vRotMatrix(double dAngleValue, double (*matrix)[9]);

#endif /* INC_MATRIX_OPERATIONS_WRAPPER_MATRIX_OPERATIONS_WRAPPER_H_ */
