//function [vect_par, vect_rep, vect_sum] = repulse_vectFIRAS(obst, pos_pot, obst_dir, dir_prop, A, r0, sightdist, vis_des)
#include <math.h>
#include "repulse_vecFIRAS.h"
#include "../../MatrixOperations/matrix_operations.h"
#include "../matrix_operations_wrapper/matrix_operations_wrapper.h"

void repulse_vectFIRAS(strObstacle *obst, strPoint *pos_pot, double obst_dir,
		double dir_prop, double A, double r0, double sightdist, double vis_des,
		double *vect_sum, double (*vect_par)[9]) {

	/*
	 % repulse_vectFIRAS
	 %
	 % This function calculates vectors of forces in the Obstacle Vector
	 % Field (OVF) method. Thes4 vectors are calculated for the point that is
	 % located on the manipulator link and is closest to the obstacle.
	 % Potential field of the obstacle is described by the FIRAS function.
	 %
	 % INPUT:
	 % obst - vector containing definition of one obstacle
	 % pos_pot - point for which the OVF vectors are calculated
	 % obst_dir - direction of the obstacle vector field: -1 (clockwise) or +1 (counter-clockwise)
	 % dir_prop - constant parameter used for determination of the direction of the main OVF vector (vect_sum)
	 %           (very low value = vector poining away from the obstacle, very high value = vector parallel to the obstacle)
	 % A - parameter of the FIRAS potential function (A/2*(1/r - 1/r0)^2)
	 % r0 - parameter of the FIRAS potential function
	 % sightdist - constant parameter that described the distance from the obstacle at which the repulsive force changes its direction
	 % vis_des - information about the visibility of the desired position from the point on the manipulator that is closest to the obstacles:
	 %           0 (desired position is visible) or 1, 2, 3 (desired position is obscured by the obstacle)
	 %
	 % OUTPUT:
	 % vect - vector parallel to the obstacle
	 % vect_rep - vector poiting away from the centre of the obstacle
	 % vect_sum - main vector of the OVF method, this vector determines the direction of the force acting on the manipulator
	 %           (vect_sum = vect_rep + vect*dir_prop)
	 %
	 % Tomasz Rybus ver 1.1 (Created 17.05.2019, Modified 21.05.2019). Based on repulse_vect.m by Tomasz Rybus.
	 */
	double h = 0.0001; //% fixed step used for numerical calculation of derivative of the potential function

	strPoint xTempPoint;

	xTempPoint.x = pos_pot->x + h;
	xTempPoint.y = pos_pot->y;
//% Calculate gradient of the potential function (in pos_pot):
	double dfdx = fPotFIRAS(obst, &xTempPoint, A, r0);
	xTempPoint.x = pos_pot->x - h;
	dfdx -= fPotFIRAS(obst, &xTempPoint, A, r0);
	dfdx = dfdx / (2 * h);

	xTempPoint.x = pos_pot->x;
	xTempPoint.y = pos_pot->y + h;
	double dfdy = fPotFIRAS(obst, &xTempPoint, A, r0);
	xTempPoint.y = pos_pot->y - h;
	dfdy -= fPotFIRAS(obst, &xTempPoint, A, r0);

	dfdy = dfdy / (2 * h);

	//% Direction of the repulsive vector is determined by the direction of the potential field gradient:
	double vect_rep[9][9];



	vect_rep[0][0] = -dfdx;
	vect_rep[1][0] = -dfdy;

	double dDl = sqrt(
				vect_rep[0][0] * vect_rep[0][0] + vect_rep[1][0] * vect_rep[1][0]);

	vect_rep[0][0] = vect_rep[0][0] / dDl;
	vect_rep[1][0] = vect_rep[1][0] / dDl;

	vRotMatrix((double)(obst_dir * (M_PI / 2.0)), vect_par);

	double vect_result[9][9];

	MatrixMultiplication(vect_par, vect_rep, vect_result, 2, 2, 1);
	//rot(obst_dir*M_PI/2)*vect_rep;

	double r = fDistObst(obst, pos_pot);

	vect_par[0][0]=vect_result[0][0];
	vect_par[1][0]=vect_result[1][0];

	vect_sum[0] = pow(-atan(pow(10, 4) * (r - sightdist)) / (M_PI / 2), vis_des)
			* vect_rep[0][0] + vect_par[0][0] * dir_prop; //% Main vector of the OVF method
	vect_sum[1] = pow(-atan(pow(10, 4) * (r - sightdist)) / (M_PI / 2), vis_des)
			* vect_rep[1][0] + vect_par[1][0] * dir_prop;


	double dDl2= sqrt(vect_sum[0] * vect_sum[0] + vect_sum[1] * vect_sum[1]);

	vect_sum[0] = vect_sum[0]
			/ dDl2;
	vect_sum[1] = vect_sum[1]
			/ dDl2;

	//return 2;
}
#ifdef AA




	r = dist_obst(obst, pos_pot); //% Distance from the given point  pos_pot to the closest point on the obstacle
			vect_sum = (-atan(10^4*(r - sightdist))/(pi/2))^vis_des*vect_rep + vect_par*dir_prop;//% Main vector of the OVF method

			vect_sum = vect_sum / norm(vect_sum);
#endif

