#ifndef INC_OBSTACLES_OBSTACLE_GENERATOR_H_
#define INC_OBSTACLES_OBSTACLE_GENERATOR_H_

/*--------------------------------------------------------------*/
/*--- #includes of type <...> ----------------------------------*/
/*--------------------------------------------------------------*/

#include <stdio.h>
#include <stdbool.h>

/*--------------------------------------------------------------*/
/*--- #includes of type "..." ----------------------------------*/
/*--------------------------------------------------------------*/

#include "../matrix_operations_wrapper/matrix_operations_wrapper.h"

/*--------------------------------------------------------------*/
/*--- #define-constants and macros -----------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- data types (typedef) -------------------------------------*/
/*--------------------------------------------------------------*/

/* Line structure */
typedef struct strLine {
	strVector xLineBeginning;
	strVector xLineEnd;
} strLine;

/* Point structure */
typedef struct{
	double x, y;
} strPoint;

/* Obstacle structure */
typedef struct strObstacle {
	strVector xSize;
	strVector xPosition;
	double dRotation;
} strObstacle;

/*--------------------------------------------------------------*/
/*--- global constants (extern const) --------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- global variables (extern) --------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- prototypes global functions ------------------------------*/
/*--------------------------------------------------------------*/

///TODO: Add descriptions
extern bool bLineIntersection(strLine *xLine1, strLine *xLine2);

extern void vCloseLinkObst(strPoint xPoints[], strObstacle *xObs,
		strPoint *xPP_link, strPoint *xPP_obst);

extern strPoint xClosePoint2(strPoint *P1, strPoint *P2, strPoint *P3);

extern void count_PO(strObstacle *obs, strPoint PO[]);

extern void vCountPPLinkObst(strPoint point_temp, strPoint pp_l, strPoint pp_ob,
		float *dist, strPoint *PP_link, strPoint *PP_obst, strPoint dist_point);

extern float norm(strPoint *xPoint1, strPoint *xPoint2);

#endif /* INC_OBSTACLES_OBSTACLE_GENERATOR_H_ */
