/*--------------------------------------------------------------*/
/*--- #includes of type <...> ----------------------------------*/
/*--------------------------------------------------------------*/

#include <math.h>

/*--------------------------------------------------------------*/
/*--- #includes of type "..." ----------------------------------*/
/*--------------------------------------------------------------*/

#include "obstacle_generator.h"

/*--------------------------------------------------------------*/
/*--- #define-constants and macros -----------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- data types (typedef) -------------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- global constants -----------------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- global variables -----------------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- local constants ------------------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- local variables ------------------------------------------*/
/*--------------------------------------------------------------*/

/*--------------------------------------------------------------*/
/*--- prototypes local functions--------------------------------*/
/*--------------------------------------------------------------*/

static strPoint vClosePoint2(strPoint P1, strPoint P2, strPoint P3);
//static void count_PO(strObstacle *obs, strPoint PO[]);

/*--------------------------------------------------------------*/
/*--- function definitions -------------------------------------*/
/*--------------------------------------------------------------*/

extern bool bLineIntersection(strLine *xLine1, strLine *xLine2) {
	//(float vector1[3], float vector2[3], float result[3]);

	float A_matrix[9][9]; //method works only for 2 dimensional space
	float A_matrix_inv[9][9];
	//float matrix_zeros[9][9] = { 0 };
	float vector_in_matrix[9][9];
	//float B_matrix[3];
	//float x_vector[3];
	float multiplication_matrix[9][9];
	//float subtract_result_line_1[3];
	//float subtract_result_line_2[3];
	//int i, j;

	//VecSubtract(line_1.line_end, line_1.line_beginning, subtract_result_line_1);
	//VecSubtract(line_2.line_beginning, line_2.line_end, subtract_result_line_2);

	strVector xResultVector1, xResultVector2;

	vVectorSubtract(&xResultVector1, &xLine1->xLineEnd,
			&xLine1->xLineBeginning);
	vVectorSubtract(&xResultVector2, &xLine2->xLineBeginning,
			&xLine2->xLineEnd);

	A_matrix[0][0] = xResultVector1.x;
	A_matrix[1][0] = xResultVector1.y;

	A_matrix[0][1] = xResultVector2.x;
	A_matrix[1][1] = xResultVector2.y;

	strVector xBMatrix;

	vVectorSubtract(&xBMatrix, &xLine2->xLineBeginning,
			&xLine1->xLineBeginning);

	vMatrixInverseCC(&A_matrix[0], 2, A_matrix_inv);

	vInsertVector2Matrix(&xBMatrix, vector_in_matrix);

	vMatrixMultiplication(A_matrix_inv, vector_in_matrix, multiplication_matrix,
			3, 3, 1);

	strVector xVector;

	vCutVectorFromMatrix(multiplication_matrix, &xVector);

	if (fMatrixDeterminant(A_matrix, 2)) {
		if (0 <= xVector.x && xVector.x <= 1 && 0 <= xVector.y
				&& xVector.y <= 1) {
			return true;
		} else {
			return false;
		}
	} else {
		return false;
	}
}

/*
 INPUT:
 P1 - position of the beginning of the manipulator link (manipulator joint)
 P2 - position of the end of the manipulator link (manipulator end-effector or joint located between the manipulator links)
 obst - structure containing information about the obstacle

 OUTPUT:
 PP_link - point on the manipulator link that is closest to the obstacle
 PP_obst - point on the obstacle that is closest to the manipulator link
 */
extern void vCloseLinkObst(strPoint xPoints[], strObstacle *xObs,
		strPoint *xPP_link, strPoint *xPP_obst) {
	strPoint xPO[5];

	//point PP_link, PP_obst;

	count_PO(xObs, xPO);

	float fDist = 10000;

	//To find point on the link that is closest to the obstacle and point on the obstacle that is closest to the link we consider 3 cases
	// Case 1: manipulator link and each corner of the rectangle

	for (int i = 0; i < 4; i++) {
		strPoint point_temp = vClosePoint2(xPoints[0], xPoints[1], xPO[i]);

		//count_PP_link_obst(point_temp,PO[i],&dist,PP_link, PP_obst, PO[i]);
		vCountPPLinkObst(point_temp, point_temp, xPO[i], &fDist, xPP_link,
				xPP_obst, xPO[i]);
	}

	for (int i = 0; i < 4; i++) {

		strPoint point_temp = vClosePoint2(xPO[i], xPO[i + 1], xPoints[0]);
		vCountPPLinkObst(point_temp, xPoints[0], point_temp, &fDist, xPP_link,
				xPP_obst, xPoints[0]);
		//count_PP_link_obst(point_temp, PO[i], &dist, PP_link, PP_obst);

		point_temp = vClosePoint2(xPO[i], xPO[i + 1], xPoints[1]);
		vCountPPLinkObst(point_temp, xPoints[1], point_temp, &fDist, xPP_link,
				xPP_obst, xPoints[1]);
		//count_PP_link_obst(points_temp, PO[i], &dist, PP_link, PP_obst);
		/*
		 count_PP_link_obst(points, PO[i], &dist, PP_link, PP_obst);

		 point point_temp = close_point_2(PO[i], PO[i+1], points[0]);
		 float dist_temp = norm(point_temp, points[0]);

		 if (dist_temp < dist)
		 {
		 *PP_link = points[0];
		 *PP_obst = point_temp;
		 dist = dist_temp;
		 }

		 //Case 3: point located at the end of the manipulator link and each side of the rectangle
		 //point point_temp;
		 point_temp=close_point_2(PO[i], PO[i + 1], points[1]);
		 dist_temp = norm(point_temp, points[1]);

		 if (dist_temp < dist)
		 {
		 *PP_link = points[1];
		 *PP_obst = point_temp;
		 dist = dist_temp;
		 }
		 */
	}
}

extern void vCountPPLinkObst(strPoint xPointTemp, strPoint xPPL, strPoint xPPOb,
		float *dist, strPoint *PP_link, strPoint *PP_obst, strPoint xDistPoint) {

	float fDistTemp = norm(&xPointTemp, &xDistPoint);

	if (fDistTemp < *dist) {
		*PP_link = xPPL;
		*PP_obst = xPPOb;
		*dist = fDistTemp;
	}
}

extern float norm(strPoint *xPoint1, strPoint *xPoint2) {
	return sqrt(
			pow(fabs(xPoint1->x - xPoint2->x), 2)
					+ pow(fabs(xPoint1->y - xPoint2->y), 2));
}

static strPoint vClosePoint2(strPoint P1, strPoint P2, strPoint P3) {
	float eps = pow(10., -10.);
	strPoint result;
	/* Special case 1: line segment is parallel to the Y axis */
	if (fabs(P1.x - P2.x) < eps) {
		result.x = P1.x;
		result.y = P3.y;
	} else {
		/* Special case 2: line segment is parallel to the X axis */
		if (fabs(P1.y - P2.y) < eps) {
			result.x = P3.x;
			result.y = P1.y;
		} else {
			// Line "1" that goes through point P1 and P2(this is the line that contains the given line segment)
			float a1 = (P2.y - P1.y) / (P2.x - P1.x);
			float b1 = (P1.y * P2.x - P2.y * P1.x) / (P2.x - P1.x);

			// Line "2" that is perpendicular to line "1" and goes through point P3
			float a2 = -1 / a1;
			float b2 = P3.y - a2 * P3.x;

			// Point of intersection between line "1" ane line "2"
			result.x = (b2 - b1) / (a1 - a2);
			result.y = a2 * result.x + b2;
		}
	}

	// We need to check if point PP(point of inersection) lies on the the given line segment
	float dotproduct = (result.x - P1.x) * (P2.x - P1.x)
			+ (result.y - P1.y) * (P2.y - P1.y);
	float squaredlengthba = (P2.x - P1.x) * (P2.x - P1.x)
			+ (P2.y - P1.y) * (P2.y - P1.y);

	// If not, then the beginning or the end of the line segment is selected as the closest point
	if (dotproduct < 0 || dotproduct > squaredlengthba) {
		result.x = P1.x;
		result.y = P1.y;

		if (norm(&P2, &P3) < norm(&result, &P3)) {
			result.x = P2.x;
			result.y = P2.y;
		}
	}

	return result;
}


extern void count_PO(strObstacle *obs, strPoint PO[]) {
	float pobst[2] = { 0 };

	for (int i = 0; i < 5; i++) {
		pobst[0] = (float) pow(-1., (double) (floor(1.5 + 0.5 * (i + 1))))
				* obs->xSize.x / 2;
		pobst[1] = (float) pow(-1., (double) (floor(2 + 0.5 * (i + 1))))
				* obs->xSize.y / 2;
		PO[i].x = obs->xPosition.x
				+ (cos(obs->xPosition.z) - sin(obs->xPosition.z)) * pobst[0];
		PO[i].y = obs->xPosition.y
				+ (sin(obs->xPosition.z) + cos(obs->xPosition.z)) * pobst[1];
	}
}

extern strPoint xClosePoint2(strPoint *P1, strPoint *P2, strPoint *P3)
{
	float eps = pow(10., -10.);
	strPoint result;
	/* Special case 1: line segment is parallel to the Y axis */
	if (fabs(P1->x - P2->x) < eps) {
		result.x = P1->x;
		result.y = P3->y;
	}
	else
	{
		/* Special case 2: line segment is parallel to the X axis */
		if (fabs(P1->y - P2->y) < eps) {
			result.x = P3->x;
			result.y = P1->y;
		}
		else
		{
			// Line "1" that goes through point P1 and P2(this is the line that contains the given line segment)
			float a1 = (P2->y - P1->y) / (P2->x - P1->x);
			float b1 = (P1->y * P2->x - P2->y * P1->x) / (P2->x - P1->x);

			// Line "2" that is perpendicular to line "1" and goes through point P3
			float a2 = -1 / a1;
			float b2 = P3->y - a2 * P3->x;

			// Point of intersection between line "1" ane line "2"
			result.x = (b2 - b1) / (a1 - a2);
			result.y = a2 * result.x + b2;
		}
	}

	// We need to check if point PP(point of inersection) lies on the the given line segment
	float dotproduct = (result.x - P1->x)*(P2->x - P1->x) + (result.y - P1->y)*(P2->y - P1->y);
	float squaredlengthba = (P2->x - P1->x)*(P2->x - P1->x) + (P2->y - P1->y)*(P2->y - P1->y);

	// If not, then the beginning or the end of the line segment is selected as the closest point
	if (dotproduct < 0 || dotproduct > squaredlengthba) {
		result.x = P1->x;
		result.y = P1->y;

		if (norm(P2, P3) < norm(&result, P3)) {
			result.x = P2->x;
			result.y = P2->y;
		}
	}

	return result;
}

//! @}

