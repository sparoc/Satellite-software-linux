/*
 * OVF.h
 *
 *  Created on: 1 gru 2021
 *      Author: Jacek
 */

#ifndef INC_OVF_H_
#define INC_OVF_H_

#include <stdio.h>
#include <math.h>
#include "matrix_operations_wrapper/matrix_operations_wrapper.h"
#include "obstacles/obstacle_generator.h"

// xActualPosition actual position of the EE
extern void OVF(strObstacle *obstacles, strPoint *xDesiredPosition,
		strPoint *xActualPosition, strPoint *xThirdLinkActualPosition,
		double fDesiredPhi, double xActualPhi, double fTh1, double fTh2,
		double fTh3, double (*u_total)[9]);

#endif /* INC_OVF_H_ */
