/*
 * kinematic_jacobian.c
 *
 *  Created on: 20 gru 2021
 *      Author: FLB
 */

#include "kinematic_jacobian.h"

void KinematicJacobian(float q1, float q2, float q3, float (*KinJac)[9])
{

	float L1 = 0.4490;
	float L2 = 0.4490;
	float L3 = 0.3103;

	KinJac[0][0] = - L1*sin(q1) - L3*cos(q3)*(cos(q1)*sin(q2) + cos(q2)*sin(q1)) - L3*sin(q3)*(cos(q1)*cos(q2) - sin(q1)*sin(q2)) - L2*cos(q1)*sin(q2) -L2*cos(q2)*sin(q1);
	KinJac[0][1] = - L3*cos(q3)*(cos(q1)*sin(q2) + cos(q2)*sin(q1)) - L3*sin(q3)*(cos(q1)*cos(q2) - sin(q1)*sin(q2)) - L2*cos(q1)*sin(q2) -L2*cos(q2)*sin(q1);
	KinJac[0][2] = - L3*cos(q3)*(cos(q1)*sin(q2) + cos(q2)*sin(q1)) - L3*sin(q3)*(cos(q1)*cos(q2) - sin(q1)*sin(q2));

	KinJac[1][0] = L1*cos(q1) + L3*cos(q3)*(cos(q1)*cos(q2) - sin(q1)*sin(q2)) -L3*sin(q3)*(cos(q1)*sin(q2) + cos(q2)*sin(q1)) + L2*cos(q1)*cos(q2) -L2*sin(q1)*sin(q2);
	KinJac[1][1] = L3*cos(q3)*(cos(q1)*cos(q2) - sin(q1)*sin(q2)) -L3*sin(q3)*(cos(q1)*sin(q2) + cos(q2)*sin(q1)) + L2*cos(q1)*cos(q2) -L2*sin(q1)*sin(q2);
	KinJac[1][2] = L3*cos(q3)*(cos(q1)*cos(q2) - sin(q1)*sin(q2)) -L3*sin(q3)*(cos(q1)*sin(q2) + cos(q2)*sin(q1));

    KinJac[2][0] = 1;
    KinJac[2][1] = 1;
    KinJac[2][2] = 1;

}
