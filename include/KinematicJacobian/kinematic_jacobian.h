/*
 * kinematic_jacobian.h
 *
 *  Created on: 20 gru 2021
 *      Author: FLB
 */

#include <math.h>

#ifndef KINEMATIC_JACOBIAN_H_
#define KINEMATIC_JACOBIAN_H_

void KinematicJacobian(float q1, float q2, float q3, float (*KinJac)[9]);

#endif /* KINEMATIC_JACOBIAN_H_ */
