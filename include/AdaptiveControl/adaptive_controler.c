/*
 * adaptive_controler.c
 *
 *  Created on: 5 sty 2024
 *      Author: Mateusz Wojtunik
 */

#include "adaptive_controler.h"
#include "../MatrixOperations/matrix_operations.h"

// Calculating kinematic Jacobians of the system:
// satellite jacobian and its derivative; manipulator jacobian and its derivative
void KinematicJacobians(float q0, float q1, float q2, float q3,
						float dq0, float dq1, float dq2, float dq3,
						float (*kinJac)[9], float (*derKinJac)[9],
						float (*satJac)[9], float (*derSatJac)[9]) {

	float L1, L2, L3, pmx, pmy;
	float q01, q012, q0123, dq01, dq012, dq0123;
	// Auxiliary variables
	float r_see_x, r_see_y, dr_see_x, dr_see_y;

	L1 = 0.4490;
	L2 = 0.4490;
	L3 = 0.31025;

	pmx = 0.3770;
	pmy = -0.0010;

	q01 = q0+q1;
	q012 = q0+q1+q2;
	q0123 = q0+q1+q2+q3;

	dq01 = dq0+dq1;
	dq012 = dq0+dq1+dq2;
	dq0123 = dq0+dq1+dq2+dq3;


	InitiateMatrixWithZeros(3,3,kinJac);

	kinJac[0][0] = - L1*sin(q01) - L2*sin(q012) - L3*sin(q0123);
	kinJac[0][1] = - L2*sin(q012) - L3*sin(q0123);
	kinJac[0][2] = - L3*sin(q0123);

	kinJac[1][0] = L1*cos(q01) + L2*cos(q012) + L3*cos(q0123);
	kinJac[1][1] = L2*cos(q012) + L3*cos(q0123);
	kinJac[1][2] = L3*cos(q0123);

	kinJac[2][0] = 1;
	kinJac[2][1] = 1;
	kinJac[2][2] = 1;


	InitiateMatrixWithZeros(3,3,derKinJac);

	derKinJac[0][0] = - dq01*L1*cos(q01) - dq012*L2*cos(q012) - dq0123*L3*cos(q0123);
	derKinJac[0][1] = - dq012*L2*cos(q012) - dq0123*L3*cos(q0123);
	derKinJac[0][2] = - dq0123*L3*cos(q0123);

	derKinJac[1][0] = - dq01*L1*sin(q01) - dq012*L2*sin(q012) - dq0123*L3*sin(q0123);
	derKinJac[1][1] = - dq012*L2*sin(q012) - dq0123*L3*sin(q0123);
	derKinJac[1][2] = - dq0123*L3*sin(q0123);


	InitiateMatrixWithZeros(3,3,satJac);

	r_see_x = pmx*cos(q0) - pmy*sin(q0) + L1*cos(q01) + L2*cos(q012) + L3*cos(q0123);
	r_see_y = pmx*sin(q0) + pmy*cos(q0) + L1*sin(q01) + L2*sin(q012) + L3*sin(q0123);

	satJac[0][0] = 1;
	satJac[1][1] = 1;
	satJac[2][2] = 1;
	satJac[0][2] = -r_see_y;
	satJac[1][2] = r_see_x;


	InitiateMatrixWithZeros(3,3,derSatJac);

	dr_see_x = - dq0*pmx*sin(q0) - dq0*pmy*cos(q0) - dq01*L1*sin(q01) - dq012*L2*sin(q012) - dq0123*L3*sin(q0123);
	dr_see_y = dq0*pmx*cos(q0) - dq0*pmy*sin(q0) + dq01*L1*cos(q01) + dq012*L2*cos(q012) + dq0123*L3*cos(q0123);

	derSatJac[0][2] = -dr_see_y;
	derSatJac[1][2] = dr_see_x;

}


// Calculating partial Jacobians of the system and its derivatives
void PartialJacobians(float q0, float q1, float q2, float q3,
					  float dxs, float dys, float dq0, float dq1, float dq2, float dq3,
					  float (*A)[9][9], float (*B)[9][9],
					  float (*Adot)[9][9], float (*Bdot)[9][9],
					  float (*dAdx)[9][9], float (*dBdx)[9][9]) {

	float L1, L2, rx1, ry1, rx2, ry2, rx3, ry3, pmx, pmy;
	// Partial Jacobians and their derivatives
	float JT0[9][9], JR0[9][9], JT1[9][9], JR1[9][9], JT2[9][9], JR2[9][9], JT3[9][9], JR3[9][9];
	float dJT0[9][9], dJR0[9][9], dJT1[9][9], dJR1[9][9], dJT2[9][9], dJR2[9][9], dJT3[9][9], dJR3[9][9];
	float JT0T[9][9], JR0T[9][9], JT1T[9][9], JR1T[9][9], JT2T[9][9], JR2T[9][9], JT3T[9][9], JR3T[9][9];
	float dJT0T[9][9], dJR0T[9][9], dJT1T[9][9], dJR1T[9][9], dJT2T[9][9], dJR2T[9][9], dJT3T[9][9], dJR3T[9][9];
	// Auxiliary variables
	float a1, a2, a3, a4, a5, a6, a7, a8, a9;
	float a10, a11, a12, a13, a14, a15, a16, a17, a18;
	float da1, da2, da3, da4, da5, da6, da7, da8, da9;
	float da10, da11, da12, da13, da14, da15, da16, da17, da18;
	float A0[9][9], A1[9][9], A2[9][9], A3[9][9];
	float B0[9][9], B1[9][9], B2[9][9], B3[9][9];
	float Adot0_1[9][9], Adot1_1[9][9], Adot2_1[9][9], Adot3_1[9][9];
	float Adot0_2[9][9], Adot1_2[9][9], Adot2_2[9][9], Adot3_2[9][9];
	float Bdot0_1[9][9], Bdot1_1[9][9], Bdot2_1[9][9], Bdot3_1[9][9];
	float Bdot0_2[9][9], Bdot1_2[9][9], Bdot2_2[9][9], Bdot3_2[9][9];
	float Adot0[9][9], Adot1[9][9], Adot2[9][9], Adot3[9][9];
	float Bdot0[9][9], Bdot1[9][9], Bdot2[9][9], Bdot3[9][9];

	L1 = 0.4490;
	L2 = 0.4490;

	rx1 = 0.1362;
	ry1 = -0.0017;
	rx2 = 0.1340;
	ry2 = -0.0005;
	rx3 = 0.1511;
	ry3 = 0.0004;

	pmx = 0.3770;
	pmy = -0.0010;

	// Calculating auxiliary variables
	a1 = - pmx*sin(q0) - pmy*cos(q0) - rx1*sin(q0+q1) - ry1*cos(q0+q1);
	a2 = - rx1*sin(q0+q1) - ry1*cos(q0+q1);
	a3 = pmx*cos(q0) - pmy*sin(q0) + rx1*cos(q0+q1) - ry1*sin(q0+q1);
	a4 = rx1*cos(q0+q1) - ry1*sin(q0+q1);

	a5 = -pmx*sin(q0) - pmy*cos(q0) - L1*sin(q0+q1) - rx2*sin(q0+q1+q2) - ry2*cos(q0+q1+q2);
	a6 = - L1*sin(q0+q1) - rx2*sin(q0+q1+q2) - ry2*cos(q0+q1+q2);
	a7 = - rx2*sin(q0+q1+q2) - ry2*cos(q0+q1+q2);
	a8 = pmx*cos(q0) - pmy*sin(q0) + L1*cos(q0+q1) + rx2*cos(q0+q1+q2) - ry2*sin(q0+q1+q2);
	a9 = L1*cos(q0+q1) + rx2*cos(q0+q1+q2) - ry2*sin(q0+q1+q2);
	a10 = rx2*cos(q0+q1+q2) - ry2*sin(q0+q1+q2);

	a11 = -pmx*sin(q0) - pmy*cos(q0) - L1*sin(q0+q1) - L2*sin(q0+q1+q2) - rx3*sin(q0+q1+q2+q3) - ry3*cos(q0+q1+q2+q3);
	a12 = -L1*sin(q0+q1) - L2*sin(q0+q1+q2) - rx3*sin(q0+q1+q2+q3) - ry3*cos(q0+q1+q2+q3);
	a13 = - L2*sin(q0+q1+q2) - rx3*sin(q0+q1+q2+q3) - ry3*cos(q0+q1+q2+q3);
	a14 = - rx3*sin(q0+q1+q2+q3) - ry3*cos(q0+q1+q2+q3);
	a15 = pmx*cos(q0) - pmy*sin(q0) + L1*cos(q0+q1) + L2*cos(q0+q1+q2) + rx3*cos(q0+q1+q2+q3) - ry3*sin(q0+q1+q2+q3);
	a16 = L1*cos(q0+q1) + L2*cos(q0+q1+q2) + rx3*cos(q0+q1+q2+q3) - ry3*sin(q0+q1+q2+q3);
	a17 = L2*cos(q0+q1+q2) + rx3*cos(q0+q1+q2+q3) - ry3*sin(q0+q1+q2+q3);
	a18 = rx3*cos(q0+q1+q2+q3) - ry3*sin(q0+q1+q2+q3);

	da1 = - pmx*dq0*cos(q0) + pmy*dq0*sin(q0) - rx1*(dq0+dq1)*cos(q0+q1) + ry1*(dq0+dq1)*sin(q0+q1);
	da2 = - rx1*(dq0+dq1)*cos(q0+q1) + ry1*(dq0+dq1)*sin(q0+q1);
	da3 = - pmx*dq0*sin(q0) - pmy*dq0*cos(q0) - rx1*(dq0+dq1)*sin(q0+q1) - ry1*(dq0+dq1)*cos(q0+q1);
	da4 = - rx1*(dq0+dq1)*sin(q0+q1) - ry1*(dq0+dq1)*cos(q0+q1);

	da5 = - pmx*dq0*cos(q0) + pmy*dq0*sin(q0) - L1*(dq0+dq1)*cos(q0+q1) - rx2*(dq0+dq1+dq2)*cos(q0+q1+q2) + ry2*(dq0+dq1+dq2)*sin(q0+q1+q2);
	da6 = - L1*(dq0+dq1)*cos(q0+q1) - rx2*(dq0+dq1+dq2)*cos(q0+q1+q2) + ry2*(dq0+dq1+dq2)*sin(q0+q1+q2);
	da7 = - rx2*(dq0+dq1+dq2)*cos(q0+q1+q2) + ry2*(dq0+dq1+dq2)*sin(q0+q1+q2);
	da8 = - pmx*dq0*sin(q0) - pmy*dq0*cos(q0) - L1*(dq0+dq1)*sin(q0+q1) - rx2*(dq0+dq1+dq2)*sin(q0+q1+q2) - ry2*(dq0+dq1+dq2)*cos(q0+q1+q2);
	da9 = - L1*(dq0+dq1)*sin(q0+q1) - rx2*(dq0+dq1+dq2)*sin(q0+q1+q2) - ry2*(dq0+dq1+dq2)*cos(q0+q1+q2);
	da10 = - rx2*(dq0+dq1+dq2)*sin(q0+q1+q2) - ry2*(dq0+dq1+dq2)*cos(q0+q1+q2);

	da11 = - pmx*dq0*cos(q0) + pmy*dq0*sin(q0) - L1*(dq0+dq1)*cos(q0+q1) - L2*(dq0+dq1+dq2)*cos(q0+q1+q2) - rx3*(dq0+dq1+dq2+dq3)*cos(q0+q1+q2+q3) + ry3*(dq0+dq1+dq2+dq3)*sin(q0+q1+q2+q3);
	da12 = - L1*(dq0+dq1)*cos(q0+q1) - L2*(dq0+dq1+dq2)*cos(q0+q1+q2) - rx3*(dq0+dq1+dq2+dq3)*cos(q0+q1+q2+q3) + ry3*(dq0+dq1+dq2+dq3)*sin(q0+q1+q2+q3);
	da13 = - L2*(dq0+dq1+dq2)*cos(q0+q1+q2) - rx3*(dq0+dq1+dq2+dq3)*cos(q0+q1+q2+q3) + ry3*(dq0+dq1+dq2+dq3)*sin(q0+q1+q2+q3);
	da14 = - rx3*(dq0+dq1+dq2+dq3)*cos(q0+q1+q2+q3) + ry3*(dq0+dq1+dq2+dq3)*sin(q0+q1+q2+q3);
	da15 = - pmx*dq0*sin(q0) - pmy*dq0*cos(q0) - L1*(dq0+dq1)*sin(q0+q1) - L2*(dq0+dq1+dq2)*sin(q0+q1+q2) - rx3*(dq0+dq1+dq2+dq3)*sin(q0+q1+q2+q3) - ry3*(dq0+dq1+dq2+dq3)*cos(q0+q1+q2+q3);
	da16 = - L1*(dq0+dq1)*sin(q0+q1) - L2*(dq0+dq1+dq2)*sin(q0+q1+q2) - rx3*(dq0+dq1+dq2+dq3)*sin(q0+q1+q2+q3) - ry3*(dq0+dq1+dq2+dq3)*cos(q0+q1+q2+q3);
	da17 = - L2*(dq0+dq1+dq2)*sin(q0+q1+q2) - rx3*(dq0+dq1+dq2+dq3)*sin(q0+q1+q2+q3) - ry3*(dq0+dq1+dq2+dq3)*cos(q0+q1+q2+q3);
	da18 = - rx3*(dq0+dq1+dq2+dq3)*sin(q0+q1+q2+q3) - ry3*(dq0+dq1+dq2+dq3)*cos(q0+q1+q2+q3);

	// Partial Jacobians
	InitiateMatrixWithZeros(2,6,JT0);
	JT0[0][0] = 1;
	JT0[1][1] = 1;

	InitiateMatrixWithZeros(1,6,JR0);
	JR0[0][2] = 1;

	InitiateMatrixWithZeros(2,6,JT1);
	JT1[0][0] = 1;
	JT1[0][2] = a1;
	JT1[0][3] = a2;
	JT1[1][1] = 1;
	JT1[1][2] = a3;
	JT1[1][3] = a4;

	InitiateMatrixWithZeros(1,6,JR1);
	JR1[0][2] = 1;
	JR1[0][3] = 1;

	InitiateMatrixWithZeros(2,6,JT2);
	JT2[0][0] = 1;
	JT2[0][2] = a5;
	JT2[0][3] = a6;
	JT2[0][4] = a7;
	JT2[1][1] = 1;
	JT2[1][2] = a8;
	JT2[1][3] = a9;
	JT2[1][4] = a10;

	InitiateMatrixWithZeros(1,6,JR2);
	JR2[0][2] = 1;
	JR2[0][3] = 1;
	JR2[0][4] = 1;

	InitiateMatrixWithZeros(2,6,JT3);
	JT3[0][0] = 1;
	JT3[0][2] = a11;
	JT3[0][3] = a12;
	JT3[0][4] = a13;
	JT3[0][5] = a14;
	JT3[1][1] = 1;
	JT3[1][2] = a15;
	JT3[1][3] = a16;
	JT3[1][4] = a17;
	JT3[1][5] = a18;

	InitiateMatrixWithZeros(1,6,JR3);
	JR3[0][2] = 1;
	JR3[0][3] = 1;
	JR3[0][4] = 1;
	JR3[0][5] = 1;

	// Time derivatives of partial Jacobians
	InitiateMatrixWithZeros(2,6,dJT0);

	InitiateMatrixWithZeros(1,6,dJR0);

	InitiateMatrixWithZeros(2,6,dJT1);
	dJT1[0][2] = da1;
	dJT1[0][3] = da2;
	dJT1[1][2] = da3;
	dJT1[1][3] = da4;

	InitiateMatrixWithZeros(1,6,dJR1);

	InitiateMatrixWithZeros(2,6,dJT2);
	dJT2[0][2] = da5;
	dJT2[0][3] = da6;
	dJT2[0][4] = da7;
	dJT2[1][2] = da8;
	dJT2[1][3] = da9;
	dJT2[1][4] = da10;

	InitiateMatrixWithZeros(1,6,dJR2);

	InitiateMatrixWithZeros(2,6,dJT3);
	dJT3[0][2] = da11;
	dJT3[0][3] = da12;
	dJT3[0][4] = da13;
	dJT3[0][5] = da14;
	dJT3[1][2] = da15;
	dJT3[1][3] = da16;
	dJT3[1][4] = da17;
	dJT3[1][5] = da18;

	InitiateMatrixWithZeros(1,6,dJR3);

	// Transpositions of partial Jacobians and their derivatives
	MatrixTranspose(JT0,JT0T,2,6);
	MatrixTranspose(JT1,JT1T,2,6);
	MatrixTranspose(JT2,JT2T,2,6);
	MatrixTranspose(JT3,JT3T,2,6);
	MatrixTranspose(JR0,JR0T,1,6);
	MatrixTranspose(JR1,JR1T,1,6);
	MatrixTranspose(JR2,JR2T,1,6);
	MatrixTranspose(JR3,JR3T,1,6);

	MatrixTranspose(dJT0,dJT0T,2,6);
	MatrixTranspose(dJT1,dJT1T,2,6);
	MatrixTranspose(dJT2,dJT2T,2,6);
	MatrixTranspose(dJT3,dJT3T,2,6);
	MatrixTranspose(dJR0,dJR0T,1,6);
	MatrixTranspose(dJR1,dJR1T,1,6);
	MatrixTranspose(dJR2,dJR2T,1,6);
	MatrixTranspose(dJR3,dJR3T,1,6);

	// Components of regression matrix (basis functions of mass matrix)
	Initiate3DMatrixWithZeros(6,6,4,A);

	MatrixMultiplication(JT0T,JT0,A0,6,2,6);
	MatrixMultiplication(JT1T,JT1,A1,6,2,6);
	MatrixMultiplication(JT2T,JT2,A2,6,2,6);
	MatrixMultiplication(JT3T,JT3,A3,6,2,6);

	Assign2DMatrixTo3DMatrix(6,6,0,A0,A);
	Assign2DMatrixTo3DMatrix(6,6,1,A1,A);
	Assign2DMatrixTo3DMatrix(6,6,2,A2,A);
	Assign2DMatrixTo3DMatrix(6,6,3,A3,A);


	Initiate3DMatrixWithZeros(6,6,4,B);

	MatrixMultiplication(JR0T,JR0,B0,6,1,6);
	MatrixMultiplication(JR1T,JR1,B1,6,1,6);
	MatrixMultiplication(JR2T,JR2,B2,6,1,6);
	MatrixMultiplication(JR3T,JR3,B3,6,1,6);

	Assign2DMatrixTo3DMatrix(6,6,0,B0,B);
	Assign2DMatrixTo3DMatrix(6,6,1,B1,B);
	Assign2DMatrixTo3DMatrix(6,6,2,B2,B);
	Assign2DMatrixTo3DMatrix(6,6,3,B3,B);

	// Time derivatives of components of regression matrix (basis functions of mass matrix)
	Initiate3DMatrixWithZeros(6,6,4,Adot);

	MatrixMultiplication(dJT0T,JT0,Adot0_1,6,2,6);
	MatrixMultiplication(dJT1T,JT1,Adot1_1,6,2,6);
	MatrixMultiplication(dJT2T,JT2,Adot2_1,6,2,6);
	MatrixMultiplication(dJT3T,JT3,Adot3_1,6,2,6);

	MatrixMultiplication(JT0T,dJT0,Adot0_2,6,2,6);
	MatrixMultiplication(JT1T,dJT1,Adot1_2,6,2,6);
	MatrixMultiplication(JT2T,dJT2,Adot2_2,6,2,6);
	MatrixMultiplication(JT3T,dJT3,Adot3_2,6,2,6);

	MatrixSum(Adot0_1,Adot0_2,Adot0,6,6);
	MatrixSum(Adot1_1,Adot1_2,Adot1,6,6);
	MatrixSum(Adot2_1,Adot2_2,Adot2,6,6);
	MatrixSum(Adot3_1,Adot3_2,Adot3,6,6);

	Assign2DMatrixTo3DMatrix(6,6,0,Adot0,Adot);
	Assign2DMatrixTo3DMatrix(6,6,1,Adot1,Adot);
	Assign2DMatrixTo3DMatrix(6,6,2,Adot2,Adot);
	Assign2DMatrixTo3DMatrix(6,6,3,Adot3,Adot);


	Initiate3DMatrixWithZeros(6,6,4,Bdot);

	MatrixMultiplication(dJR0T,JR0,Bdot0_1,6,2,6);
	MatrixMultiplication(dJR1T,JR1,Bdot1_1,6,2,6);
	MatrixMultiplication(dJR2T,JR2,Bdot2_1,6,2,6);
	MatrixMultiplication(dJR3T,JR3,Bdot3_1,6,2,6);

	MatrixMultiplication(JR0T,dJR0,Bdot0_2,6,2,6);
	MatrixMultiplication(JR1T,dJR1,Bdot1_2,6,2,6);
	MatrixMultiplication(JR2T,dJR2,Bdot2_2,6,2,6);
	MatrixMultiplication(JR3T,dJR3,Bdot3_2,6,2,6);

	MatrixSum(Bdot0_1,Bdot0_2,Bdot0,6,6);
	MatrixSum(Bdot1_1,Bdot1_2,Bdot1,6,6);
	MatrixSum(Bdot2_1,Bdot2_2,Bdot2,6,6);
	MatrixSum(Bdot3_1,Bdot3_2,Bdot3,6,6);

	Assign2DMatrixTo3DMatrix(6,6,0,Bdot0,Bdot);
	Assign2DMatrixTo3DMatrix(6,6,1,Bdot1,Bdot);
	Assign2DMatrixTo3DMatrix(6,6,2,Bdot2,Bdot);
	Assign2DMatrixTo3DMatrix(6,6,3,Bdot3,Bdot);

	// State derivatives of partial Jacobians
	Initiate3DMatrixWithZeros(6,6,4,dAdx);
	Initiate3DMatrixWithZeros(6,6,4,dBdx);

	// d/dx(JT1'*JT1*dx)
	dAdx[0][2][1] = - dq1*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1)) - dq0*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1) + pmx*cos(q0) - pmy*sin(q0));
	dAdx[1][2][1] = - dq1*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1)) - dq0*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1) + pmy*cos(q0) + pmx*sin(q0));
	dAdx[2][2][1] = dq0*(2*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1) + pmy*cos(q0) + pmx*sin(q0))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1) + pmx*cos(q0) - pmy*sin(q0)) - 2*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1) + pmy*cos(q0) + pmx*sin(q0))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1) + pmx*cos(q0) - pmy*sin(q0))) - dys*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1) + pmy*cos(q0) + pmx*sin(q0)) - dxs*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1) + pmx*cos(q0) - pmy*sin(q0)) + dq1*((rx1*cos(q0 + q1) - ry1*sin(q0 + q1))*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1) + pmy*cos(q0) + pmx*sin(q0)) + (ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1) + pmx*cos(q0) - pmy*sin(q0)) - (rx1*cos(q0 + q1) - ry1*sin(q0 + q1))*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1) + pmy*cos(q0) + pmx*sin(q0)) - (ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1) + pmx*cos(q0) - pmy*sin(q0)));
	dAdx[3][2][1] = dq1*(2*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1)) - 2*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1))) - dys*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1)) - dxs*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1)) + dq0*((rx1*cos(q0 + q1) - ry1*sin(q0 + q1))*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1) + pmy*cos(q0) + pmx*sin(q0)) + (ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1) + pmx*cos(q0) - pmy*sin(q0)) - (rx1*cos(q0 + q1) - ry1*sin(q0 + q1))*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1) + pmy*cos(q0) + pmx*sin(q0)) - (ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1) + pmx*cos(q0) - pmy*sin(q0)));

	dAdx[0][3][1] = - dq0*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1)) - dq1*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1));
	dAdx[1][3][1] = - dq0*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1)) - dq1*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1));
	dAdx[2][3][1] = dq0*(2*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1))*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1) + pmy*cos(q0) + pmx*sin(q0)) - 2*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1) + pmx*cos(q0) - pmy*sin(q0))) - dxs*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1)) - dys*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1)) + dq1*((ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1)) - (ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1)) + (rx1*cos(q0 + q1) - ry1*sin(q0 + q1))*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1) + pmy*cos(q0) + pmx*sin(q0)) - (ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1) + pmx*cos(q0) - pmy*sin(q0)));
	dAdx[3][3][1] = dq0*((ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1)) - (ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1)) + (rx1*cos(q0 + q1) - ry1*sin(q0 + q1))*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1) + pmy*cos(q0) + pmx*sin(q0)) - (ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1) + pmx*cos(q0) - pmy*sin(q0))) - dys*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1)) - dxs*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1)) + dq1*(2*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1)) - 2*(ry1*cos(q0 + q1) + rx1*sin(q0 + q1))*(rx1*cos(q0 + q1) - ry1*sin(q0 + q1)));

	// d/dx(JT2'*JT2*dx)
	dAdx[0][2][2] = - dq2*(rx2*cos(q0 + q1 + q2) - ry2*sin(q0 + q1 + q2)) - dq1*(L1*cos(q0 + q1) + rx2*cos(q0 + q1 + q2) - ry2*sin(q0 + q1 + q2)) - dq0*(L1*cos(q0 + q1) + pmx*cos(q0) - pmy*sin(q0) + rx2*cos(q0 + q1 + q2) - ry2*sin(q0 + q1 + q2));
	dAdx[1][2][2] = - dq2*(ry2*cos(q0 + q1 + q2) + rx2*sin(q0 + q1 + q2)) - dq1*(L1*sin(q0 + q1) + ry2*cos(q0 + q1 + q2) + rx2*sin(q0 + q1 + q2)) - dq0*(L1*sin(q0 + q1) + pmy*cos(q0) + pmx*sin(q0) + ry2*cos(q0 + q1 + q2) + rx2*sin(q0 + q1 + q2));
	dAdx[2][2][2] = dxs*pmy*sin(q0) - dys*pmy*cos(q0) - dxs*pmx*cos(q0) - dys*pmx*sin(q0) - dxs*rx2*cos(q0 + q1 + q2) - dys*ry2*cos(q0 + q1 + q2) + dxs*ry2*sin(q0 + q1 + q2) - dys*rx2*sin(q0 + q1 + q2) - L1*dxs*cos(q0 + q1) - L1*dys*sin(q0 + q1);
	dAdx[3][2][2] = dxs*ry2*sin(q0 + q1 + q2) - dys*ry2*cos(q0 + q1 + q2) - dxs*rx2*cos(q0 + q1 + q2) - dys*rx2*sin(q0 + q1 + q2) - L1*dxs*cos(q0 + q1) - L1*dys*sin(q0 + q1);
	dAdx[4][2][2] = dxs*ry2*sin(q0 + q1 + q2) - dys*ry2*cos(q0 + q1 + q2) - dxs*rx2*cos(q0 + q1 + q2) - dys*rx2*sin(q0 + q1 + q2);

	dAdx[0][3][2] = - dq2*(rx2*cos(q0 + q1 + q2) - ry2*sin(q0 + q1 + q2)) - dq0*(L1*cos(q0 + q1) + rx2*cos(q0 + q1 + q2) - ry2*sin(q0 + q1 + q2)) - dq1*(L1*cos(q0 + q1) + rx2*cos(q0 + q1 + q2) - ry2*sin(q0 + q1 + q2));
	dAdx[1][3][2] = - dq2*(ry2*cos(q0 + q1 + q2) + rx2*sin(q0 + q1 + q2)) - dq0*(L1*sin(q0 + q1) + ry2*cos(q0 + q1 + q2) + rx2*sin(q0 + q1 + q2)) - dq1*(L1*sin(q0 + q1) + ry2*cos(q0 + q1 + q2) + rx2*sin(q0 + q1 + q2));
	dAdx[2][3][2] = dxs*ry2*sin(q0 + q1 + q2) - dys*ry2*cos(q0 + q1 + q2) - dxs*rx2*cos(q0 + q1 + q2) - dys*rx2*sin(q0 + q1 + q2) - L1*dxs*cos(q0 + q1) - L1*dys*sin(q0 + q1) + 2*dq0*pmy*rx2*cos(q1 + q2) + dq1*pmy*rx2*cos(q1 + q2) + dq2*pmy*rx2*cos(q1 + q2) - 2*dq0*pmx*ry2*cos(q1 + q2) - dq1*pmx*ry2*cos(q1 + q2) - dq2*pmx*ry2*cos(q1 + q2) + 2*L1*dq0*pmy*cos(q1) + L1*dq1*pmy*cos(q1) - 2*dq0*pmx*rx2*sin(q1 + q2) - dq1*pmx*rx2*sin(q1 + q2) - dq2*pmx*rx2*sin(q1 + q2) - 2*dq0*pmy*ry2*sin(q1 + q2) - dq1*pmy*ry2*sin(q1 + q2) - dq2*pmy*ry2*sin(q1 + q2) - 2*L1*dq0*pmx*sin(q1) - L1*dq1*pmx*sin(q1);
	dAdx[3][3][2] = dxs*ry2*sin(q0 + q1 + q2) - dys*ry2*cos(q0 + q1 + q2) - dxs*rx2*cos(q0 + q1 + q2) - dys*rx2*sin(q0 + q1 + q2) - L1*dxs*cos(q0 + q1) - L1*dys*sin(q0 + q1) + dq0*pmy*rx2*cos(q1 + q2) - dq0*pmx*ry2*cos(q1 + q2) + L1*dq0*pmy*cos(q1) - dq0*pmx*rx2*sin(q1 + q2) - dq0*pmy*ry2*sin(q1 + q2) - L1*dq0*pmx*sin(q1);
	dAdx[4][3][2] = dxs*ry2*sin(q0 + q1 + q2) - dys*ry2*cos(q0 + q1 + q2) - dxs*rx2*cos(q0 + q1 + q2) - dys*rx2*sin(q0 + q1 + q2) + dq0*pmy*rx2*cos(q1 + q2) - dq0*pmx*ry2*cos(q1 + q2) - dq0*pmx*rx2*sin(q1 + q2) - dq0*pmy*ry2*sin(q1 + q2);

	dAdx[0][4][2] = - dq0*(rx2*cos(q0 + q1 + q2) - ry2*sin(q0 + q1 + q2)) - dq1*(rx2*cos(q0 + q1 + q2) - ry2*sin(q0 + q1 + q2)) - dq2*(rx2*cos(q0 + q1 + q2) - ry2*sin(q0 + q1 + q2));
	dAdx[1][4][2] = - dq0*(ry2*cos(q0 + q1 + q2) + rx2*sin(q0 + q1 + q2)) - dq1*(ry2*cos(q0 + q1 + q2) + rx2*sin(q0 + q1 + q2)) - dq2*(ry2*cos(q0 + q1 + q2) + rx2*sin(q0 + q1 + q2));
	dAdx[2][4][2] = dxs*ry2*sin(q0 + q1 + q2) - dys*ry2*cos(q0 + q1 + q2) - dxs*rx2*cos(q0 + q1 + q2) - dys*rx2*sin(q0 + q1 + q2) + 2*dq0*pmy*rx2*cos(q1 + q2) + dq1*pmy*rx2*cos(q1 + q2) + dq2*pmy*rx2*cos(q1 + q2) - 2*dq0*pmx*ry2*cos(q1 + q2) - dq1*pmx*ry2*cos(q1 + q2) - dq2*pmx*ry2*cos(q1 + q2) - 2*L1*dq0*ry2*cos(q2) - 2*L1*dq1*ry2*cos(q2) - L1*dq2*ry2*cos(q2) - 2*dq0*pmx*rx2*sin(q1 + q2) - dq1*pmx*rx2*sin(q1 + q2) - dq2*pmx*rx2*sin(q1 + q2) - 2*dq0*pmy*ry2*sin(q1 + q2) - dq1*pmy*ry2*sin(q1 + q2) - dq2*pmy*ry2*sin(q1 + q2) - 2*L1*dq0*rx2*sin(q2) - 2*L1*dq1*rx2*sin(q2) - L1*dq2*rx2*sin(q2);
	dAdx[3][4][2] = dxs*ry2*sin(q0 + q1 + q2) - dys*ry2*cos(q0 + q1 + q2) - dxs*rx2*cos(q0 + q1 + q2) - dys*rx2*sin(q0 + q1 + q2) + dq0*pmy*rx2*cos(q1 + q2) - dq0*pmx*ry2*cos(q1 + q2) - 2*L1*dq0*ry2*cos(q2) - 2*L1*dq1*ry2*cos(q2) - L1*dq2*ry2*cos(q2) - dq0*pmx*rx2*sin(q1 + q2) - dq0*pmy*ry2*sin(q1 + q2) - 2*L1*dq0*rx2*sin(q2) - 2*L1*dq1*rx2*sin(q2) - L1*dq2*rx2*sin(q2);
	dAdx[4][4][2] = dxs*ry2*sin(q0 + q1 + q2) - dys*ry2*cos(q0 + q1 + q2) - dxs*rx2*cos(q0 + q1 + q2) - dys*rx2*sin(q0 + q1 + q2) + dq0*pmy*rx2*cos(q1 + q2) - dq0*pmx*ry2*cos(q1 + q2) - L1*dq0*ry2*cos(q2) - L1*dq1*ry2*cos(q2) - dq0*pmx*rx2*sin(q1 + q2) - dq0*pmy*ry2*sin(q1 + q2) - L1*dq0*rx2*sin(q2) - L1*dq1*rx2*sin(q2);

	// d/dx(JT3'*JT3*dx)
	dAdx[0][2][3] = dq0*pmy*sin(q0) - dq0*pmx*cos(q0) - L2*dq0*cos(q0 + q1 + q2) - L2*dq1*cos(q0 + q1 + q2) - L2*dq2*cos(q0 + q1 + q2) - L1*dq0*cos(q0 + q1) - L1*dq1*cos(q0 + q1) - dq0*rx3*cos(q0 + q1 + q2 + q3) - dq1*rx3*cos(q0 + q1 + q2 + q3) - dq2*rx3*cos(q0 + q1 + q2 + q3) - dq3*rx3*cos(q0 + q1 + q2 + q3) + dq0*ry3*sin(q0 + q1 + q2 + q3) + dq1*ry3*sin(q0 + q1 + q2 + q3) + dq2*ry3*sin(q0 + q1 + q2 + q3) + dq3*ry3*sin(q0 + q1 + q2 + q3);
	dAdx[1][2][3] = - dq0*pmy*cos(q0) - dq0*pmx*sin(q0) - L2*dq0*sin(q0 + q1 + q2) - L2*dq1*sin(q0 + q1 + q2) - L2*dq2*sin(q0 + q1 + q2) - dq0*ry3*cos(q0 + q1 + q2 + q3) - dq1*ry3*cos(q0 + q1 + q2 + q3) - dq2*ry3*cos(q0 + q1 + q2 + q3) - dq3*ry3*cos(q0 + q1 + q2 + q3) - L1*dq0*sin(q0 + q1) - L1*dq1*sin(q0 + q1) - dq0*rx3*sin(q0 + q1 + q2 + q3) - dq1*rx3*sin(q0 + q1 + q2 + q3) - dq2*rx3*sin(q0 + q1 + q2 + q3) - dq3*rx3*sin(q0 + q1 + q2 + q3);
	dAdx[2][2][3] = dxs*pmy*sin(q0) - dys*pmy*cos(q0) - dxs*pmx*cos(q0) - dys*pmx*sin(q0) - L2*dxs*cos(q0 + q1 + q2) - L2*dys*sin(q0 + q1 + q2) - L1*dxs*cos(q0 + q1) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - L1*dys*sin(q0 + q1) + dxs*ry3*sin(q0 + q1 + q2 + q3) - dys*rx3*sin(q0 + q1 + q2 + q3);
	dAdx[3][2][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - L2*dys*sin(q0 + q1 + q2) - L1*dxs*cos(q0 + q1) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - L1*dys*sin(q0 + q1) - L2*dxs*cos(q0 + q1 + q2) - dys*rx3*sin(q0 + q1 + q2 + q3);
	dAdx[4][2][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - L2*dys*sin(q0 + q1 + q2) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - L2*dxs*cos(q0 + q1 + q2) - dys*rx3*sin(q0 + q1 + q2 + q3);
	dAdx[5][2][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*rx3*sin(q0 + q1 + q2 + q3);

	dAdx[0][3][3] = dq0*ry3*sin(q0 + q1 + q2 + q3) - L2*dq1*cos(q0 + q1 + q2) - L2*dq2*cos(q0 + q1 + q2) - L1*dq0*cos(q0 + q1) - L1*dq1*cos(q0 + q1) - dq0*rx3*cos(q0 + q1 + q2 + q3) - dq1*rx3*cos(q0 + q1 + q2 + q3) - dq2*rx3*cos(q0 + q1 + q2 + q3) - dq3*rx3*cos(q0 + q1 + q2 + q3) - L2*dq0*cos(q0 + q1 + q2) + dq1*ry3*sin(q0 + q1 + q2 + q3) + dq2*ry3*sin(q0 + q1 + q2 + q3) + dq3*ry3*sin(q0 + q1 + q2 + q3);
	dAdx[1][3][3] = - L2*dq0*sin(q0 + q1 + q2) - L2*dq1*sin(q0 + q1 + q2) - L2*dq2*sin(q0 + q1 + q2) - dq0*ry3*cos(q0 + q1 + q2 + q3) - dq1*ry3*cos(q0 + q1 + q2 + q3) - dq2*ry3*cos(q0 + q1 + q2 + q3) - dq3*ry3*cos(q0 + q1 + q2 + q3) - L1*dq0*sin(q0 + q1) - L1*dq1*sin(q0 + q1) - dq0*rx3*sin(q0 + q1 + q2 + q3) - dq1*rx3*sin(q0 + q1 + q2 + q3) - dq2*rx3*sin(q0 + q1 + q2 + q3) - dq3*rx3*sin(q0 + q1 + q2 + q3);
	dAdx[2][3][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - L2*dys*sin(q0 + q1 + q2) - L1*dxs*cos(q0 + q1) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - L1*dys*sin(q0 + q1) - L2*dxs*cos(q0 + q1 + q2) - dys*rx3*sin(q0 + q1 + q2 + q3) + 2*L2*dq0*pmy*cos(q1 + q2) + L2*dq1*pmy*cos(q1 + q2) + L2*dq2*pmy*cos(q1 + q2) - 2*L2*dq0*pmx*sin(q1 + q2) - L2*dq1*pmx*sin(q1 + q2) - L2*dq2*pmx*sin(q1 + q2) + 2*L1*dq0*pmy*cos(q1) + L1*dq1*pmy*cos(q1) - 2*L1*dq0*pmx*sin(q1) - L1*dq1*pmx*sin(q1) + 2*dq0*pmy*rx3*cos(q1 + q2 + q3) + dq1*pmy*rx3*cos(q1 + q2 + q3) + dq2*pmy*rx3*cos(q1 + q2 + q3) + dq3*pmy*rx3*cos(q1 + q2 + q3) - 2*dq0*pmx*ry3*cos(q1 + q2 + q3) - dq1*pmx*ry3*cos(q1 + q2 + q3) - dq2*pmx*ry3*cos(q1 + q2 + q3) - dq3*pmx*ry3*cos(q1 + q2 + q3) - 2*dq0*pmx*rx3*sin(q1 + q2 + q3) - dq1*pmx*rx3*sin(q1 + q2 + q3) - dq2*pmx*rx3*sin(q1 + q2 + q3) - dq3*pmx*rx3*sin(q1 + q2 + q3) - 2*dq0*pmy*ry3*sin(q1 + q2 + q3) - dq1*pmy*ry3*sin(q1 + q2 + q3) - dq2*pmy*ry3*sin(q1 + q2 + q3) - dq3*pmy*ry3*sin(q1 + q2 + q3);
	dAdx[3][3][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - L2*dys*sin(q0 + q1 + q2) - L1*dxs*cos(q0 + q1) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - L1*dys*sin(q0 + q1) - L2*dxs*cos(q0 + q1 + q2) - dys*rx3*sin(q0 + q1 + q2 + q3) + L2*dq0*pmy*cos(q1 + q2) - L2*dq0*pmx*sin(q1 + q2) + L1*dq0*pmy*cos(q1) - L1*dq0*pmx*sin(q1) + dq0*pmy*rx3*cos(q1 + q2 + q3) - dq0*pmx*ry3*cos(q1 + q2 + q3) - dq0*pmx*rx3*sin(q1 + q2 + q3) - dq0*pmy*ry3*sin(q1 + q2 + q3);
	dAdx[4][3][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - L2*dys*sin(q0 + q1 + q2) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - L2*dxs*cos(q0 + q1 + q2) - dys*rx3*sin(q0 + q1 + q2 + q3) + L2*dq0*pmy*cos(q1 + q2) - L2*dq0*pmx*sin(q1 + q2) + dq0*pmy*rx3*cos(q1 + q2 + q3) - dq0*pmx*ry3*cos(q1 + q2 + q3) - dq0*pmx*rx3*sin(q1 + q2 + q3) - dq0*pmy*ry3*sin(q1 + q2 + q3);
	dAdx[5][3][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*rx3*sin(q0 + q1 + q2 + q3) + dq0*pmy*rx3*cos(q1 + q2 + q3) - dq0*pmx*ry3*cos(q1 + q2 + q3) - dq0*pmx*rx3*sin(q1 + q2 + q3) - dq0*pmy*ry3*sin(q1 + q2 + q3);

	dAdx[0][4][3] = dq0*ry3*sin(q0 + q1 + q2 + q3) - L2*dq1*cos(q0 + q1 + q2) - L2*dq2*cos(q0 + q1 + q2) - dq0*rx3*cos(q0 + q1 + q2 + q3) - dq1*rx3*cos(q0 + q1 + q2 + q3) - dq2*rx3*cos(q0 + q1 + q2 + q3) - dq3*rx3*cos(q0 + q1 + q2 + q3) - L2*dq0*cos(q0 + q1 + q2) + dq1*ry3*sin(q0 + q1 + q2 + q3) + dq2*ry3*sin(q0 + q1 + q2 + q3) + dq3*ry3*sin(q0 + q1 + q2 + q3);
	dAdx[1][4][3] = - L2*dq0*sin(q0 + q1 + q2) - L2*dq1*sin(q0 + q1 + q2) - L2*dq2*sin(q0 + q1 + q2) - dq0*ry3*cos(q0 + q1 + q2 + q3) - dq1*ry3*cos(q0 + q1 + q2 + q3) - dq2*ry3*cos(q0 + q1 + q2 + q3) - dq3*ry3*cos(q0 + q1 + q2 + q3) - dq0*rx3*sin(q0 + q1 + q2 + q3) - dq1*rx3*sin(q0 + q1 + q2 + q3) - dq2*rx3*sin(q0 + q1 + q2 + q3) - dq3*rx3*sin(q0 + q1 + q2 + q3);
	dAdx[2][4][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - L2*dys*sin(q0 + q1 + q2) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - L2*dxs*cos(q0 + q1 + q2) - dys*rx3*sin(q0 + q1 + q2 + q3) + 2*L2*dq0*pmy*cos(q1 + q2) + L2*dq1*pmy*cos(q1 + q2) + L2*dq2*pmy*cos(q1 + q2) - 2*L1*dq0*ry3*cos(q2 + q3) - 2*L1*dq1*ry3*cos(q2 + q3) - L1*dq2*ry3*cos(q2 + q3) - L1*dq3*ry3*cos(q2 + q3) - 2*L2*dq0*pmx*sin(q1 + q2) - L2*dq1*pmx*sin(q1 + q2) - L2*dq2*pmx*sin(q1 + q2) - 2*L1*dq0*rx3*sin(q2 + q3) - 2*L1*dq1*rx3*sin(q2 + q3) - L1*dq2*rx3*sin(q2 + q3) - L1*dq3*rx3*sin(q2 + q3) - 2*L1*L2*dq0*sin(q2) - 2*L1*L2*dq1*sin(q2) - L1*L2*dq2*sin(q2) + 2*dq0*pmy*rx3*cos(q1 + q2 + q3) + dq1*pmy*rx3*cos(q1 + q2 + q3) + dq2*pmy*rx3*cos(q1 + q2 + q3) + dq3*pmy*rx3*cos(q1 + q2 + q3) - 2*dq0*pmx*ry3*cos(q1 + q2 + q3) - dq1*pmx*ry3*cos(q1 + q2 + q3) - dq2*pmx*ry3*cos(q1 + q2 + q3) - dq3*pmx*ry3*cos(q1 + q2 + q3) - 2*dq0*pmx*rx3*sin(q1 + q2 + q3) - dq1*pmx*rx3*sin(q1 + q2 + q3) - dq2*pmx*rx3*sin(q1 + q2 + q3) - dq3*pmx*rx3*sin(q1 + q2 + q3) - 2*dq0*pmy*ry3*sin(q1 + q2 + q3) - dq1*pmy*ry3*sin(q1 + q2 + q3) - dq2*pmy*ry3*sin(q1 + q2 + q3) - dq3*pmy*ry3*sin(q1 + q2 + q3);
	dAdx[3][4][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - L2*dys*sin(q0 + q1 + q2) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - L2*dxs*cos(q0 + q1 + q2) - dys*rx3*sin(q0 + q1 + q2 + q3) + L2*dq0*pmy*cos(q1 + q2) - 2*L1*dq0*ry3*cos(q2 + q3) - 2*L1*dq1*ry3*cos(q2 + q3) - L1*dq2*ry3*cos(q2 + q3) - L1*dq3*ry3*cos(q2 + q3) - L2*dq0*pmx*sin(q1 + q2) - 2*L1*dq0*rx3*sin(q2 + q3) - 2*L1*dq1*rx3*sin(q2 + q3) - L1*dq2*rx3*sin(q2 + q3) - L1*dq3*rx3*sin(q2 + q3) - 2*L1*L2*dq0*sin(q2) - 2*L1*L2*dq1*sin(q2) - L1*L2*dq2*sin(q2) + dq0*pmy*rx3*cos(q1 + q2 + q3) - dq0*pmx*ry3*cos(q1 + q2 + q3) - dq0*pmx*rx3*sin(q1 + q2 + q3) - dq0*pmy*ry3*sin(q1 + q2 + q3);
	dAdx[4][4][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - L2*dys*sin(q0 + q1 + q2) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - L2*dxs*cos(q0 + q1 + q2) - dys*rx3*sin(q0 + q1 + q2 + q3) + L2*dq0*pmy*cos(q1 + q2) - L1*dq0*ry3*cos(q2 + q3) - L1*dq1*ry3*cos(q2 + q3) - L2*dq0*pmx*sin(q1 + q2) - L1*dq0*rx3*sin(q2 + q3) - L1*dq1*rx3*sin(q2 + q3) - L1*L2*dq0*sin(q2) - L1*L2*dq1*sin(q2) + dq0*pmy*rx3*cos(q1 + q2 + q3) - dq0*pmx*ry3*cos(q1 + q2 + q3) - dq0*pmx*rx3*sin(q1 + q2 + q3) - dq0*pmy*ry3*sin(q1 + q2 + q3);
	dAdx[5][4][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*rx3*sin(q0 + q1 + q2 + q3) - L1*dq0*ry3*cos(q2 + q3) - L1*dq1*ry3*cos(q2 + q3) - L1*dq0*rx3*sin(q2 + q3) - L1*dq1*rx3*sin(q2 + q3) + dq0*pmy*rx3*cos(q1 + q2 + q3) - dq0*pmx*ry3*cos(q1 + q2 + q3) - dq0*pmx*rx3*sin(q1 + q2 + q3) - dq0*pmy*ry3*sin(q1 + q2 + q3);

	dAdx[0][5][3] = dq0*ry3*sin(q0 + q1 + q2 + q3) - dq1*rx3*cos(q0 + q1 + q2 + q3) - dq2*rx3*cos(q0 + q1 + q2 + q3) - dq3*rx3*cos(q0 + q1 + q2 + q3) - dq0*rx3*cos(q0 + q1 + q2 + q3) + dq1*ry3*sin(q0 + q1 + q2 + q3) + dq2*ry3*sin(q0 + q1 + q2 + q3) + dq3*ry3*sin(q0 + q1 + q2 + q3);
	dAdx[1][5][3] = - dq0*ry3*cos(q0 + q1 + q2 + q3) - dq1*ry3*cos(q0 + q1 + q2 + q3) - dq2*ry3*cos(q0 + q1 + q2 + q3) - dq3*ry3*cos(q0 + q1 + q2 + q3) - dq0*rx3*sin(q0 + q1 + q2 + q3) - dq1*rx3*sin(q0 + q1 + q2 + q3) - dq2*rx3*sin(q0 + q1 + q2 + q3) - dq3*rx3*sin(q0 + q1 + q2 + q3);
	dAdx[2][5][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*rx3*sin(q0 + q1 + q2 + q3) - 2*L1*dq0*ry3*cos(q2 + q3) - 2*L1*dq1*ry3*cos(q2 + q3) - L1*dq2*ry3*cos(q2 + q3) - L1*dq3*ry3*cos(q2 + q3) - 2*L1*dq0*rx3*sin(q2 + q3) - 2*L1*dq1*rx3*sin(q2 + q3) - L1*dq2*rx3*sin(q2 + q3) - L1*dq3*rx3*sin(q2 + q3) - 2*L2*dq0*ry3*cos(q3) - 2*L2*dq1*ry3*cos(q3) - 2*L2*dq2*ry3*cos(q3) - L2*dq3*ry3*cos(q3) - 2*L2*dq0*rx3*sin(q3) - 2*L2*dq1*rx3*sin(q3) - 2*L2*dq2*rx3*sin(q3) - L2*dq3*rx3*sin(q3) + 2*dq0*pmy*rx3*cos(q1 + q2 + q3) + dq1*pmy*rx3*cos(q1 + q2 + q3) + dq2*pmy*rx3*cos(q1 + q2 + q3) + dq3*pmy*rx3*cos(q1 + q2 + q3) - 2*dq0*pmx*ry3*cos(q1 + q2 + q3) - dq1*pmx*ry3*cos(q1 + q2 + q3) - dq2*pmx*ry3*cos(q1 + q2 + q3) - dq3*pmx*ry3*cos(q1 + q2 + q3) - 2*dq0*pmx*rx3*sin(q1 + q2 + q3) - dq1*pmx*rx3*sin(q1 + q2 + q3) - dq2*pmx*rx3*sin(q1 + q2 + q3) - dq3*pmx*rx3*sin(q1 + q2 + q3) - 2*dq0*pmy*ry3*sin(q1 + q2 + q3) - dq1*pmy*ry3*sin(q1 + q2 + q3) - dq2*pmy*ry3*sin(q1 + q2 + q3) - dq3*pmy*ry3*sin(q1 + q2 + q3);
	dAdx[3][5][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*rx3*sin(q0 + q1 + q2 + q3) - 2*L1*dq0*ry3*cos(q2 + q3) - 2*L1*dq1*ry3*cos(q2 + q3) - L1*dq2*ry3*cos(q2 + q3) - L1*dq3*ry3*cos(q2 + q3) - 2*L1*dq0*rx3*sin(q2 + q3) - 2*L1*dq1*rx3*sin(q2 + q3) - L1*dq2*rx3*sin(q2 + q3) - L1*dq3*rx3*sin(q2 + q3) - 2*L2*dq0*ry3*cos(q3) - 2*L2*dq1*ry3*cos(q3) - 2*L2*dq2*ry3*cos(q3) - L2*dq3*ry3*cos(q3) - 2*L2*dq0*rx3*sin(q3) - 2*L2*dq1*rx3*sin(q3) - 2*L2*dq2*rx3*sin(q3) - L2*dq3*rx3*sin(q3) + dq0*pmy*rx3*cos(q1 + q2 + q3) - dq0*pmx*ry3*cos(q1 + q2 + q3) - dq0*pmx*rx3*sin(q1 + q2 + q3) - dq0*pmy*ry3*sin(q1 + q2 + q3);
	dAdx[4][5][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*rx3*sin(q0 + q1 + q2 + q3) - L1*dq0*ry3*cos(q2 + q3) - L1*dq1*ry3*cos(q2 + q3) - L1*dq0*rx3*sin(q2 + q3) - L1*dq1*rx3*sin(q2 + q3) - 2*L2*dq0*ry3*cos(q3) - 2*L2*dq1*ry3*cos(q3) - 2*L2*dq2*ry3*cos(q3) - L2*dq3*ry3*cos(q3) - 2*L2*dq0*rx3*sin(q3) - 2*L2*dq1*rx3*sin(q3) - 2*L2*dq2*rx3*sin(q3) - L2*dq3*rx3*sin(q3) + dq0*pmy*rx3*cos(q1 + q2 + q3) - dq0*pmx*ry3*cos(q1 + q2 + q3) - dq0*pmx*rx3*sin(q1 + q2 + q3) - dq0*pmy*ry3*sin(q1 + q2 + q3);
	dAdx[5][5][3] = dxs*ry3*sin(q0 + q1 + q2 + q3) - dys*ry3*cos(q0 + q1 + q2 + q3) - dxs*rx3*cos(q0 + q1 + q2 + q3) - dys*rx3*sin(q0 + q1 + q2 + q3) - L1*dq0*ry3*cos(q2 + q3) - L1*dq1*ry3*cos(q2 + q3) - L1*dq0*rx3*sin(q2 + q3) - L1*dq1*rx3*sin(q2 + q3) - L2*dq0*ry3*cos(q3) - L2*dq1*ry3*cos(q3) - L2*dq2*ry3*cos(q3) - L2*dq0*rx3*sin(q3) - L2*dq1*rx3*sin(q3) - L2*dq2*rx3*sin(q3) + dq0*pmy*rx3*cos(q1 + q2 + q3) - dq0*pmx*ry3*cos(q1 + q2 + q3) - dq0*pmx*rx3*sin(q1 + q2 + q3) - dq0*pmy*ry3*sin(q1 + q2 + q3);

}

// Create augmented jacobians from kinematic jacobians of the manipulator and the satellite
void AugmentedJacobians(float (*kinJac)[9], float (*derKinJac)[9],
						float (*satJac)[9], float (*derSatJac)[9],
						float (*augmJac)[9], float (*derAugmJac)[9]) {

	InitiateMatrixWithZeros(6,6,augmJac);
	InitiateMatrixWithZeros(6,6,derAugmJac);

	augmJac[0][0] = 1;
	augmJac[1][1] = 1;
	augmJac[2][2] = 1;

	int i, j;
	for (i = 3; i < 6; i++) {
		for (j = 0; j < 3; j++) {
			augmJac[i][j] = satJac[i-3][j];
			augmJac[i][j+3] = kinJac[i-3][j];
			derAugmJac[i][j] = derSatJac[i-3][j];
			derAugmJac[i][j+3] = derKinJac[i-3][j];
		}
	}


}

// Calculate intermediate control and its time derivative (demanded joint velocities and accelerations)
void IntermediateControl(float (*eo)[9], float (*deo)[9], float (*dPo)[9], float (*dPod)[9], float (*ddPod)[9],
						 float (*J)[9], float (*dJ)[9], float (*Ko)[9],
						 float (*xvd)[9], float (*dxvd)[9]) {

	float invJ[9][9], dinvJ[9][9], dJ_invJ[9][9], inverseTerm[9][9], Ko_eo[9][9], Ko_deo[9][9];
	float xvd_parenthesis[9][9], dxvd_1[9][9], dxvd_2[9][9];
	float dxvd_parenthesis_1[9][9], dxvd_parenthesis_2[9][9];

	MatrixInverse(J,6,invJ);
	MatrixMultiplication(Ko,eo,Ko_eo,6,6,1);
	MatrixSum(dPo,Ko_eo,xvd_parenthesis,6,1);
	MatrixMultiplication(invJ,xvd_parenthesis,xvd,6,6,1);

	MatrixMultiplication(dJ,invJ,dJ_invJ,6,6,6);
	MatrixMultiplication(invJ,dJ_invJ,dinvJ,6,6,6);
	ValMatMultiplication(-1,dinvJ,inverseTerm,6,6);
	MatrixSum(dPod,Ko_eo,dxvd_parenthesis_1,6,1);
	MatrixMultiplication(inverseTerm,dxvd_parenthesis_1,dxvd_1,6,6,1);

	MatrixMultiplication(Ko,deo,Ko_deo,6,6,1);
	MatrixSum(ddPod,Ko_deo,dxvd_parenthesis_2,6,1);
	MatrixMultiplication(invJ,dxvd_parenthesis_2,dxvd_2,6,6,1);

	MatrixSum(dxvd_1,dxvd_2,dxvd,6,1);

}

void basisRegressionMatrixFunctions(float (*A)[9][9], float (*B)[9][9],
									float (*dA)[9][9], float (*dB)[9][9],
									float (*dAdx)[9][9], float (*dBdx)[9][9],
									float (*ej)[9], float (*dxvd)[9], float (*xv)[9],
									float (*a)[9], float (*b)[9]) {

		float A0[9][9], A1[9][9], A2[9][9], A3[9][9];
		float B0[9][9], B1[9][9], B2[9][9], B3[9][9];
		float dA0[9][9], dA1[9][9], dA2[9][9], dA3[9][9];
		float dB0[9][9], dB1[9][9], dB2[9][9], dB3[9][9];
		float dA0dx[9][9], dA1dx[9][9], dA2dx[9][9], dA3dx[9][9];
		float dB0dx[9][9], dB1dx[9][9], dB2dx[9][9], dB3dx[9][9];
		// Auxiliary variables
		float dA0T[9][9], dA1T[9][9], dA2T[9][9], dA3T[9][9];
		float dB0T[9][9], dB1T[9][9], dB2T[9][9], dB3T[9][9];
		float dA0dxT[9][9], dA1dxT[9][9], dA2dxT[9][9], dA3dxT[9][9];
		float dB0dxT[9][9], dB1dxT[9][9], dB2dxT[9][9], dB3dxT[9][9];
		float half_dA0[9][9], half_dA1[9][9], half_dA2[9][9], half_dA3[9][9];
		float half_dB0[9][9], half_dB1[9][9], half_dB2[9][9], half_dB3[9][9];
		float half_dA0dxT[9][9], half_dA1dxT[9][9], half_dA2dxT[9][9], half_dA3dxT[9][9];
		float half_dB0dxT[9][9], half_dB1dxT[9][9], half_dB2dxT[9][9], half_dB3dxT[9][9];
		float term1_m0[9][9], term2_m0[9][9], term3_m0[9][9], term4_m0[9][9];
		float term1_m1[9][9], term2_m1[9][9], term3_m1[9][9], term4_m1[9][9];
		float term1_m2[9][9], term2_m2[9][9], term3_m2[9][9], term4_m2[9][9];
		float term1_m3[9][9], term2_m3[9][9], term3_m3[9][9], term4_m3[9][9];
		float term1_I0[9][9], term2_I0[9][9], term3_I0[9][9], term4_I0[9][9];
		float term1_I1[9][9], term2_I1[9][9], term3_I1[9][9], term4_I1[9][9];
		float term1_I2[9][9], term2_I2[9][9], term3_I2[9][9], term4_I2[9][9];
		float term1_I3[9][9], term2_I3[9][9], term3_I3[9][9], term4_I3[9][9];
		float subsum1_m0[9][9], subsum2_m0[9][9];
		float subsum1_m1[9][9], subsum2_m1[9][9];
		float subsum1_m2[9][9], subsum2_m2[9][9];
		float subsum1_m3[9][9], subsum2_m3[9][9];
		float subsum1_I0[9][9], subsum2_I0[9][9];
		float subsum1_I1[9][9], subsum2_I1[9][9];
		float subsum1_I2[9][9], subsum2_I2[9][9];
		float subsum1_I3[9][9], subsum2_I3[9][9];
		float a0[9][9], a1[9][9], a2[9][9], a3[9][9];
		float b0[9][9], b1[9][9], b2[9][9], b3[9][9];

		Extract2DMatrixFrom3DMatrix(6,6,0,A,A0);
		Extract2DMatrixFrom3DMatrix(6,6,1,A,A1);
		Extract2DMatrixFrom3DMatrix(6,6,2,A,A2);
		Extract2DMatrixFrom3DMatrix(6,6,3,A,A3);

		Extract2DMatrixFrom3DMatrix(6,6,0,B,B0);
		Extract2DMatrixFrom3DMatrix(6,6,1,B,B1);
		Extract2DMatrixFrom3DMatrix(6,6,2,B,B2);
		Extract2DMatrixFrom3DMatrix(6,6,3,B,B3);

		Extract2DMatrixFrom3DMatrix(6,6,0,dA,dA0);
		Extract2DMatrixFrom3DMatrix(6,6,1,dA,dA1);
		Extract2DMatrixFrom3DMatrix(6,6,2,dA,dA2);
		Extract2DMatrixFrom3DMatrix(6,6,3,dA,dA3);

		Extract2DMatrixFrom3DMatrix(6,6,0,dB,dB0);
		Extract2DMatrixFrom3DMatrix(6,6,1,dB,dB1);
		Extract2DMatrixFrom3DMatrix(6,6,2,dB,dB2);
		Extract2DMatrixFrom3DMatrix(6,6,3,dB,dB3);

		Extract2DMatrixFrom3DMatrix(6,6,0,dAdx,dA0dx);
		Extract2DMatrixFrom3DMatrix(6,6,1,dAdx,dA1dx);
		Extract2DMatrixFrom3DMatrix(6,6,2,dAdx,dA2dx);
		Extract2DMatrixFrom3DMatrix(6,6,3,dAdx,dA3dx);

		Extract2DMatrixFrom3DMatrix(6,6,0,dBdx,dB0dx);
		Extract2DMatrixFrom3DMatrix(6,6,1,dBdx,dB1dx);
		Extract2DMatrixFrom3DMatrix(6,6,2,dBdx,dB2dx);
		Extract2DMatrixFrom3DMatrix(6,6,3,dBdx,dB3dx);

		// a0
		InitiateMatrixWithZeros(6,1,a0);
		MatrixMultiplication(A0,dxvd,term1_m0,6,6,1);
		MatrixTranspose(dA0,dA0T,6,6);
		MatrixMultiplication(dA0T,xv,term2_m0,6,6,1);
		MatrixTranspose(dA0dx,dA0dxT,6,6);
		ValMatMultiplication(0.5,dA0dxT,half_dA0dxT,6,6);
		MatrixMultiplication(half_dA0dxT,xv,term3_m0,6,6,1);
		ValMatMultiplication(0.5,dA0,half_dA0,6,6);
		MatrixMultiplication(half_dA0,ej,term4_m0,6,6,1);
		MatrixSum(term1_m0,term2_m0,subsum1_m0,6,1);
		MatrixSubtract(subsum1_m0,term3_m0,subsum2_m0,6,1);
		MatrixSum(subsum2_m0,term4_m0,a0,6,1);

		// a1
		InitiateMatrixWithZeros(6,1,a1);
		MatrixMultiplication(A1,dxvd,term1_m1,6,6,1);
		MatrixTranspose(dA1,dA1T,6,6);
		MatrixMultiplication(dA1T,xv,term2_m1,6,6,1);
		MatrixTranspose(dA1dx,dA1dxT,6,6);
		ValMatMultiplication(0.5,dA1dxT,half_dA1dxT,6,6);
		MatrixMultiplication(half_dA1dxT,xv,term3_m1,6,6,1);
		ValMatMultiplication(0.5,dA1,half_dA1,6,6);
		MatrixMultiplication(half_dA1,ej,term4_m1,6,6,1);
		MatrixSum(term1_m1,term2_m1,subsum1_m1,6,1);
		MatrixSubtract(subsum1_m1,term3_m1,subsum2_m1,6,1);
		MatrixSum(subsum2_m1,term4_m1,a1,6,1);

		// a2
		InitiateMatrixWithZeros(6,1,a2);
		MatrixMultiplication(A2,dxvd,term1_m2,6,6,1);
		MatrixTranspose(dA2,dA2T,6,6);
		MatrixMultiplication(dA2T,xv,term2_m2,6,6,1);
		MatrixTranspose(dA2dx,dA2dxT,6,6);
		ValMatMultiplication(0.5,dA2dxT,half_dA2dxT,6,6);
		MatrixMultiplication(half_dA2dxT,xv,term3_m2,6,6,1);
		ValMatMultiplication(0.5,dA2,half_dA2,6,6);
		MatrixMultiplication(half_dA2,ej,term4_m2,6,6,1);
		MatrixSum(term1_m2,term2_m2,subsum1_m2,6,1);
		MatrixSubtract(subsum1_m2,term3_m2,subsum2_m2,6,1);
		MatrixSum(subsum2_m2,term4_m2,a2,6,1);

		// a3
		InitiateMatrixWithZeros(6,1,a3);
		MatrixMultiplication(A3,dxvd,term1_m3,6,6,1);
		MatrixTranspose(dA3,dA3T,6,6);
		MatrixMultiplication(dA3T,xv,term2_m3,6,6,1);
		MatrixTranspose(dA3dx,dA3dxT,6,6);
		ValMatMultiplication(0.5,dA3dxT,half_dA3dxT,6,6);
		MatrixMultiplication(half_dA3dxT,xv,term3_m3,6,6,1);
		ValMatMultiplication(0.5,dA3,half_dA3,6,6);
		MatrixMultiplication(half_dA3,ej,term4_m3,6,6,1);
		MatrixSum(term1_m3,term2_m3,subsum1_m3,6,1);
		MatrixSubtract(subsum1_m3,term3_m3,subsum2_m3,6,1);
		MatrixSum(subsum2_m3,term4_m3,a3,6,1);

		// b0
		InitiateMatrixWithZeros(6,1,b0);
		MatrixMultiplication(B0,dxvd,term1_I0,6,6,1);
		MatrixTranspose(dB0,dB0T,6,6);
		MatrixMultiplication(dB0T,xv,term2_I0,6,6,1);
		MatrixTranspose(dB0dx,dB0dxT,6,6);
		ValMatMultiplication(0.5,dB0dxT,half_dB0dxT,6,6);
		MatrixMultiplication(half_dB0dxT,xv,term3_I0,6,6,1);
		ValMatMultiplication(0.5,dB0,half_dB0,6,6);
		MatrixMultiplication(half_dB0,ej,term4_I0,6,6,1);
		MatrixSum(term1_I0,term2_I0,subsum1_I0,6,1);
		MatrixSubtract(subsum1_I0,term3_I0,subsum2_I0,6,1);
		MatrixSum(subsum2_I0,term4_I0,b0,6,1);

		// b1
		InitiateMatrixWithZeros(6,1,b1);
		MatrixMultiplication(B1,dxvd,term1_I1,6,6,1);
		MatrixTranspose(dB1,dB1T,6,6);
		MatrixMultiplication(dB1T,xv,term2_I1,6,6,1);
		MatrixTranspose(dB1dx,dB1dxT,6,6);
		ValMatMultiplication(0.5,dB1dxT,half_dB1dxT,6,6);
		MatrixMultiplication(half_dB1dxT,xv,term3_I1,6,6,1);
		ValMatMultiplication(0.5,dB1,half_dB1,6,6);
		MatrixMultiplication(half_dB1,ej,term4_I1,6,6,1);
		MatrixSum(term1_I1,term2_I1,subsum1_I1,6,1);
		MatrixSubtract(subsum1_I1,term3_I1,subsum2_I1,6,1);
		MatrixSum(subsum2_I1,term4_I1,b1,6,1);

		// b2
		InitiateMatrixWithZeros(6,1,b2);
		MatrixMultiplication(B2,dxvd,term1_I2,6,6,1);
		MatrixTranspose(dB2,dB2T,6,6);
		MatrixMultiplication(dB2T,xv,term2_I2,6,6,1);
		MatrixTranspose(dB2dx,dB2dxT,6,6);
		ValMatMultiplication(0.5,dB2dxT,half_dB2dxT,6,6);
		MatrixMultiplication(half_dB2dxT,xv,term3_I2,6,6,1);
		ValMatMultiplication(0.5,dB2,half_dB2,6,6);
		MatrixMultiplication(half_dB2,ej,term4_I2,6,6,1);
		MatrixSum(term1_I2,term2_I2,subsum1_I2,6,1);
		MatrixSubtract(subsum1_I2,term3_I2,subsum2_I2,6,1);
		MatrixSum(subsum2_I2,term4_I2,b2,6,1);

		// b3
		InitiateMatrixWithZeros(6,1,b3);
		MatrixMultiplication(B3,dxvd,term1_I3,6,6,1);
		MatrixTranspose(dB3,dB3T,6,6);
		MatrixMultiplication(dB3T,xv,term2_I3,6,6,1);
		MatrixTranspose(dB3dx,dB3dxT,6,6);
		ValMatMultiplication(0.5,dB3dxT,half_dB3dxT,6,6);
		MatrixMultiplication(half_dB3dxT,xv,term3_I3,6,6,1);
		ValMatMultiplication(0.5,dB3,half_dB3,6,6);
		MatrixMultiplication(half_dB3,ej,term4_I3,6,6,1);
		MatrixSum(term1_I3,term2_I3,subsum1_I3,6,1);
		MatrixSubtract(subsum1_I3,term3_I3,subsum2_I3,6,1);
		MatrixSum(subsum2_I3,term4_I3,b3,6,1);

		// Assign regression matrix basis functions to matrixes
		InitiateMatrixWithZeros(6,4,a);
		AssignVectorTo2DMatrix(6,0,a0,a);
		AssignVectorTo2DMatrix(6,1,a1,a);
		AssignVectorTo2DMatrix(6,2,a2,a);
		AssignVectorTo2DMatrix(6,3,a3,a);

		InitiateMatrixWithZeros(6,4,b);
		AssignVectorTo2DMatrix(6,0,b0,b);
		AssignVectorTo2DMatrix(6,1,b1,b);
		AssignVectorTo2DMatrix(6,2,b2,b);
		AssignVectorTo2DMatrix(6,3,b3,b);

}

// Calculate adaptation laws for masses and moments of inertia
void adaptationLaws(float (*gamma)[9],
					float (*a)[9], float (*b)[9],
					float (*ej)[9], float (*dxvd)[9], float (*xv)[9],
					float (*dphi_hat)[9]) {

	float gamma_m0, gamma_m1, gamma_m2, gamma_m3;
	float gamma_I0, gamma_I1, gamma_I2, gamma_I3;
	// Auxiliary variables
	float ejT[9][9];
	float left_m0[9][9], left_m1[9][9], left_m2[9][9], left_m3[9][9];
	float left_I0[9][9], left_I1[9][9], left_I2[9][9], left_I3[9][9];
	float dm0_hat[9][9], dm1_hat[9][9], dm2_hat[9][9], dm3_hat[9][9];
	float dI0_hat[9][9], dI1_hat[9][9], dI2_hat[9][9], dI3_hat[9][9];
	float a0[9][9], a1[9][9], a2[9][9], a3[9][9];
	float b0[9][9], b1[9][9], b2[9][9], b3[9][9];


	gamma_m0 = gamma[0][0];
	gamma_m1 = gamma[1][0];
	gamma_m2 = gamma[2][0];
	gamma_m3 = gamma[3][0];
	gamma_I0 = gamma[4][0];
	gamma_I1 = gamma[5][0];
	gamma_I2 = gamma[6][0];
	gamma_I3 = gamma[7][0];

	MatrixTranspose(ej,ejT,6,1);

	ExtractVectorFrom2DMatrix(6,0,a,a0);
	ExtractVectorFrom2DMatrix(6,1,a,a1);
	ExtractVectorFrom2DMatrix(6,2,a,a2);
	ExtractVectorFrom2DMatrix(6,3,a,a3);

	ExtractVectorFrom2DMatrix(6,0,b,b0);
	ExtractVectorFrom2DMatrix(6,1,b,b1);
	ExtractVectorFrom2DMatrix(6,2,b,b2);
	ExtractVectorFrom2DMatrix(6,3,b,b3);

	// dm0_hat
	ValMatMultiplication(gamma_m0,ejT,left_m0,1,6);
	MatrixMultiplication(left_m0,a0,dm0_hat,1,6,1);


	// dm1_hat
	ValMatMultiplication(gamma_m1,ejT,left_m1,1,6);
	MatrixMultiplication(left_m1,a1,dm1_hat,1,6,1);


	// dm2_hat
	ValMatMultiplication(gamma_m2,ejT,left_m2,1,6);
	MatrixMultiplication(left_m2,a2,dm2_hat,1,6,1);

	// dm3_hat
	ValMatMultiplication(gamma_m3,ejT,left_m3,1,6);
	MatrixMultiplication(left_m3,a3,dm3_hat,1,6,1);

	// dI0_hat
	ValMatMultiplication(gamma_I0,ejT,left_I0,1,6);
	MatrixMultiplication(left_I0,b0,dI0_hat,1,6,1);

	// dI1_hat
	ValMatMultiplication(gamma_I1,ejT,left_I1,1,6);
	MatrixMultiplication(left_I1,b1,dI1_hat,1,6,1);

	// dI2_hat
	ValMatMultiplication(gamma_I2,ejT,left_I2,1,6);
	MatrixMultiplication(left_I2,b2,dI2_hat,1,6,1);

	// dI3_hat
	ValMatMultiplication(gamma_I3,ejT,left_I3,1,6);
	MatrixMultiplication(left_I3,b3,dI3_hat,1,6,1);


	// dphi_hat
	dphi_hat[0][0] = dm0_hat[0][0];
	dphi_hat[1][0] = dm1_hat[0][0];
	dphi_hat[2][0] = dm2_hat[0][0];
	dphi_hat[3][0] = dm3_hat[0][0];
	dphi_hat[4][0] = dI0_hat[0][0];
	dphi_hat[5][0] = dI1_hat[0][0];
	dphi_hat[6][0] = dI2_hat[0][0];
	dphi_hat[7][0] = dI3_hat[0][0];

}

// Calculate desired joint torques (control law)
void controlLaw(float (*a)[9], float (*b)[9], float (*J)[9],
				float (*eo)[9], float (*ej)[9], float (*dxvd)[9], float (*xv)[9],
				float (*Kj)[9], float (*phi_hat)[9], float (*tau)[9]) {


	float a0[9][9], a1[9][9], a2[9][9], a3[9][9];
	float b0[9][9], b1[9][9], b2[9][9], b3[9][9];
	float JT[9][9];
	float m0_hat, m1_hat, m2_hat, m3_hat;
	float I0_hat, I1_hat, I2_hat, I3_hat;
	// Auxiliary variables
	float term_m0[9][9], term_m1[9][9], term_m2[9][9], term_m3[9][9];
	float term_I0[9][9], term_I1[9][9], term_I2[9][9], term_I3[9][9];
	float term_Jac[9][9], term_joint[9][9];
	float subsum_mass_01[9][9], subsum_mass_012[9][9], subsum_mass_0123[9][9];
	float subsum_inertia_01[9][9], subsum_inertia_012[9][9], subsum_inertia_0123[9][9];
	float subsum_tau_12[9][9], subsum_tau_123[9][9];


	ExtractVectorFrom2DMatrix(6,0,a,a0);
	ExtractVectorFrom2DMatrix(6,1,a,a1);
	ExtractVectorFrom2DMatrix(6,2,a,a2);
	ExtractVectorFrom2DMatrix(6,3,a,a3);

	ExtractVectorFrom2DMatrix(6,0,b,b0);
	ExtractVectorFrom2DMatrix(6,1,b,b1);
	ExtractVectorFrom2DMatrix(6,2,b,b2);
	ExtractVectorFrom2DMatrix(6,3,b,b3);

	m0_hat = phi_hat[0][0];
	m1_hat = phi_hat[1][0];
	m2_hat = phi_hat[2][0];
	m3_hat = phi_hat[3][0];
	I0_hat = phi_hat[4][0];
	I1_hat = phi_hat[5][0];
	I2_hat = phi_hat[6][0];
	I3_hat = phi_hat[7][0];

	// Evaluation of each term in tau

	// Regression matrix basis functions with adapted parameters
	ValMatMultiplication(m0_hat,a0,term_m0,6,1);
	ValMatMultiplication(m1_hat,a1,term_m1,6,1);
	ValMatMultiplication(m2_hat,a2,term_m2,6,1);
	ValMatMultiplication(m3_hat,a3,term_m3,6,1);
	ValMatMultiplication(I0_hat,b0,term_I0,6,1);
	ValMatMultiplication(I1_hat,b1,term_I1,6,1);
	ValMatMultiplication(I2_hat,b2,term_I2,6,1);
	ValMatMultiplication(I3_hat,b3,term_I3,6,1);

	// Jacobian term
	MatrixTranspose(J,JT,6,6);
	MatrixMultiplication(JT,eo,term_Jac,6,6,1);

	// Joint-space control term
	MatrixMultiplication(Kj,ej,term_joint,6,6,1);


	// Sub-sums of tau containing adapted masses
	MatrixSum(term_m0,term_m1,subsum_mass_01,6,1);
	MatrixSum(subsum_mass_01,term_m2,subsum_mass_012,6,1);
	MatrixSum(subsum_mass_012,term_m3,subsum_mass_0123,6,1);

	// Sub-sums of tau containing adapted inertias
	MatrixSum(term_I0,term_I1,subsum_inertia_01,6,1);
	MatrixSum(subsum_inertia_01,term_I2,subsum_inertia_012,6,1);
	MatrixSum(subsum_inertia_012,term_I3,subsum_inertia_0123,6,1);

	// Control law - adding mass-adapted term, inertia-adapted term, Jacobian term and joint-space control term
	MatrixSum(subsum_mass_0123,subsum_inertia_0123,subsum_tau_12,6,1);
	MatrixSum(subsum_tau_12,term_Jac,subsum_tau_123,6,1);
	MatrixSum(subsum_tau_123,term_joint,tau,6,1);

}

// Create 6D vector from given scalar values
void Create6DVector(float a1, float a2, float a3, float a4, float a5, float a6, float(*vector)[9]) {

	InitiateMatrixWithZeros(6,1,vector);
	vector[0][0] = a1;
	vector[1][0] = a2;
	vector[2][0] = a3;
	vector[3][0] = a4;
	vector[4][0] = a5;
	vector[5][0] = a6;

}


// Initiate mxn matrix with zeros
void InitiateMatrixWithZeros(int m, int n, float (*matrix)[9]) {
	int i, j;

	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			matrix[i][j] = 0;
		}
	}
}


// Initiate mxnxl matrix with zeros
void Initiate3DMatrixWithZeros(int m, int n, int l, float (*matrix)[9][9]) {
	int i, j, k;

	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			for (k = 0; k < l; k++) {
				matrix[i][j][k] = 0;
			}
		}
	}
}

// Assign a mx1 vector to the 2D matrix on index n
void AssignVectorTo2DMatrix(int m, int n, float (*vector)[9], float (*result)[9]) {
	int i;

	for (i = 0; i < m; i++) {
			result[i][n] = vector[i][0];
	}

}

// Assign a mxn matrix to the 3D matrix on index l
void Assign2DMatrixTo3DMatrix(int m, int n, int l, float (*matrix)[9], float (*result)[9][9]) {
	int i, j;

	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			result[i][j][l] = matrix[i][j];
		}
	}
}

// Extract a mx1 vector from the 2D matrix on index n
void ExtractVectorFrom2DMatrix(int m, int n, float (*matrix)[9], float (*result)[9]) {
	int i;

	for (i = 0; i < m; i++) {
			result[i][0] = matrix[i][n];
	}
}


// Extract a mxn matrix from the 3D matrix on index l
void Extract2DMatrixFrom3DMatrix(int m, int n, int l, float (*matrix)[9][9], float (*result)[9]) {
	int i, j;

	for (i = 0; i < m; i++) {
		for (j = 0; j < n; j++) {
			result[i][j] = matrix[i][j][l];
		}
	}
}
