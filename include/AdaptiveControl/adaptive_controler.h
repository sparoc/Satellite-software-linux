/*
 * adaptive_controler.h
 *
 *  Created on: 5 sty 2024
 *      Author: Mateusz Wojtunik
 */

#ifndef ADAPTIVE_CONTROLER_H_
#define ADAPTIVE_CONTROLER_H_

#include <stdio.h>
#include <math.h>

void KinematicJacobians(float, float, float, float, float, float, float, float,
		float (*)[9], float (*)[9], float (*)[9], float (*)[9]);
void PartialJacobians(float, float, float, float,
                      float, float, float, float, float, float,
					  float (*)[9][9], float (*)[9][9],
					  float (*)[9][9], float (*)[9][9],
					  float (*)[9][9], float (*)[9][9]);
void AugmentedJacobians(float (*)[9], float (*)[9], float (*)[9], float (*)[9], float (*)[9], float (*)[9]);
void IntermediateControl(float (*)[9], float (*)[9], float (*)[9], float (*)[9], float (*)[9],
						 float (*)[9], float (*)[9], float (*)[9],
						 float (*)[9], float (*)[9]);
void basisRegressionMatrixFunctions(float (*)[9][9], float (*)[9][9],
									float (*)[9][9], float (*)[9][9],
									float (*)[9][9], float (*)[9][9],
									float (*)[9], float (*)[9], float (*)[9],
									float (*)[9], float (*)[9]);
void adaptationLaws(float (*)[9],
					float (*)[9], float (*)[9],
					float (*)[9], float (*)[9], float (*)[9],
					float (*)[9]);
void controlLaw(float (*)[9], float (*)[9], float (*)[9],
				float (*)[9], float (*)[9], float (*)[9], float (*)[9],
				float (*)[9], float (*)[9], float (*)[9]);
void Create6DVector(float, float, float, float, float, float, float(*)[9]);
void InitiateMatrixWithZeros(int, int, float (*)[9]);
void Initiate3DMatrixWithZeros(int, int, int, float (*)[9][9]);
void AssignVectorTo2DMatrix(int, int, float (*)[9], float (*)[9]);
void Assign2DMatrixTo3DMatrix(int, int, int, float (*)[9], float (*)[9][9]);
void ExtractVectorFrom2DMatrix(int, int, float(*)[9], float(*)[9]);
void Extract2DMatrixFrom3DMatrix(int, int, int, float (*)[9][9], float (*)[9]);

#endif /* ADAPTIVE_CONTROLER_H_ */
