#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <semaphore.h>
#include <sys/types.h>
#include <pthread.h>

#include "main.h"

#define TRUE		1
#define FALSE		0

#define RAD_TO_DEG_IN_ENC_UNIT	((180.0 / SAT_PI) * 186413.51111111111)

#define JACOBIAN_CONTROLER_GAIN 3.0

void Log_SensModuleData(void);
float unwrap_rad(float error);

int g1HzLoopCounter = 0;
int g10HzLoopCounter = 0;
int g100HzLoopCounter = 0;

char debugBuffer[256];
//char comBufferCan[256];

void *lanServerThread(void *arg) {
	int size = sizeof(struct sockaddr_in);
	int client_sock;
	int *new_sock;
	struct sockaddr_in client;

	while ((client_sock = accept(gLanSocketDesc, (struct sockaddr *) &client, (socklen_t*) &size))) {
		puts("LAN Client connection accepted");

		pthread_t lanClientThread;
		new_sock = malloc(1);
		*new_sock = client_sock;

		if (pthread_create(&lanClientThread, NULL, lanClientConnectionHandler, (void*) new_sock) != 0) {
			perror("main.c - StartLanSocket(): Unable to create thread - ");
			return NULL;
		}
	}

	if (client_sock < 0) {
		perror("main.c - lanServerThread(): Client acceptance failed - ");
	}

	return NULL;
}

// CBK-SR WinApp data
// This will handle connection for each client
void *lanClientConnectionHandler(void *clientSocketDesc) {
	unsigned char byte = 0;
	int rxSize;
	// Get the socket descriptor
	int socket = *(int*) clientSocketDesc;
	gLanSocket = socket;

	// Receive a message from client
	while ((rxSize = recv(socket, &byte, 1, 0)) > 0) {
		ParseGCSFrame(byte);
		if (sGCSFrame.IsFrameReady) {
			ProcessGCSCommand();
			sGCSFrame.IsFrameReady = 0;
		}
	}

	if (rxSize == 0) {
		puts("LAN Client disconnected");
		fflush(stdout);
	} else if (rxSize == -1) {
		perror("main.c - lanClientConnectionHandler(): Receiving failed - ");
	}

	// Free the socket pointer
	free(clientSocketDesc);

	return 0;
}

// This will handle connection for can0 msg
void *can0Thread(void *arg) {
	sCan0Frame.CanHardwareNumber = 0;
	puts("CAN0 is running ...");

	while (1) {
		memset(sCan0Frame.RxFrame.data, 0, sizeof(sCan0Frame.RxFrame.data));
		int canFrameSize = sizeof(sCan0Frame.RxFrame);
		int rxSize = read(gCan0SocketDesc, &sCan0Frame.RxFrame, canFrameSize);
		sCan0Frame.RxCounter += rxSize;

		if (sCan0Frame.RxFrame.can_id & CAN_ERR_FLAG) {
			ParseCanError(&sCan0Frame);
		} else if (rxSize == canFrameSize) {
			//puts("CAN_MSG parsed");
			ProcessJointCommand();
		} else {
			puts("CAN0 read less bytes ...");
		}
	}

	return NULL;
}

// This will handle connection for can1 msg
void *can1Thread(void *arg) {
	sCan1Frame.CanHardwareNumber = 1;
	puts("CAN1 is running ...");

	while (1) {
		memset(sCan1Frame.RxFrame.data, 0, sizeof(sCan1Frame.RxFrame.data));
		int rxSize = read(gCan1SocketDesc, &sCan1Frame.RxFrame, sizeof(sCan1Frame.RxFrame));
		sCan1Frame.RxCounter += rxSize;

		if (sCan1Frame.RxFrame.can_id & CAN_ERR_FLAG) {
			ParseCanError(&sCan1Frame);
		} else if (rxSize > 0) {
			// CAN Data
			CbkCanId canId;
			GetCanId(&canId, sCan1Frame.RxFrame.can_id);
		}
	}

	return NULL;
}

// CBK-SR WinApp data or OnBoard Vision Computer data
void *rs0RxThread(void *arg) {
	unsigned char byte = 0;
	while (read(gRS0fd, &byte, 1) > 0) {
		ParseGCSFrame(byte);
		if (sGCSFrame.IsFrameReady) {
			ProcessGCSCommand();
			sGCSFrame.IsFrameReady = 0;
		}
	}

	return NULL;
}

// IMU data
void *rsImuRxThread(void *arg) {
	unsigned char byte = 0;
	while (read(gUA0fd, &byte, 1) > 0) {
		ParseImuFrame(byte);
		if (sImuFrame.IsFrameReady) {
			ProcessImuCommand();
			sImuFrame.IsFrameReady = 0;
		}
	}

	return NULL;
}

float FF_forcex[1000], FF_forcey[1000], FF_momentz[1000];

float th1, th2, th3;
float SatXPose, SatYPose, SatOrient;
float EEXPose, EEYPose, EEOrient;

int main(int argc, char **argv) {
	// Sekcja inicjalizacji
	// Tworzenie zmiennych typu thread
	pthread_t rs0Thread = NULL;
	//pthread_t ua0Thread = NULL;
	pthread_t lanSocketThread = NULL;
	pthread_t can0SocketThread = NULL;
	//pthread_t can1SocketThread = NULL;

	// Inicjalizacja danych w poszczególnych plikach
	InitImuProtocolMembers();
	InitGCSMembers();
	InitMainComputerMembers();
	InitJointProtocolMembers();
	InitDataStructMembers();
	InitSatelliteMembers();
	//initSensingModule();
	sleep(1);
	// Inicjalizacja file loggera
	InitFileLogger();
	FlushFileLogger();

	puts("\n##### CBK-SR Main Computer #####\n");
	LogInfo("##### %d. CBK-SR Main Computer ##### ", 1);
	FlushFileLogger();

	// Inicjalizacja komunikacji Ethernet
	if (argc == 1) {
		// Default (no arguments)
		// 192.168.0.120 for CBK-SR
		gLanSocketDesc = StartLanSocket("192.168.0.120", 8899);
	} else {
		// IP taken from argument
		gLanSocketDesc = StartLanSocket(argv[1], 8899);
	}

	// Start LAN socket thread
	if (pthread_create(&lanSocketThread, NULL, &lanServerThread, NULL) != 0) {
		perror("main.c - pthread_create(): Unable to create the thread for LAN Socket - ");
	}

	gCan0SocketDesc = StartCanSocket("can0");

	// Start CAN0 socket thread
	if (pthread_create(&can0SocketThread, NULL, &can0Thread, NULL) != 0) {
		perror("main.c - pthread_create(): Unable to create the thread for CAN0 Socket - ");
	}

//	gCan1SocketDesc = StartCanSocket("can1");

	// Start CAN1 socket thread
//	if (pthread_create(&can1SocketThread, NULL, &can1Thread, NULL) != 0) {
//		perror("main.c - pthread_create(): Unable to create the thread for CAN1 Socket - ");
//	}

	gRS0fd = RS0Open();

	// Start RS0 thread
	if (pthread_create(&rs0Thread, NULL, &rs0RxThread, NULL) != 0) {
		perror("main.c - pthread_create(): Unable to create the thread for ttyS1 (RS0) - ");
	} else {
		puts("Serial port (RS0 label on PCB board) is running ...");
	}

	gUA0fd = UA0Open();

	// Start UA0 thread for IMU
//	if (pthread_create(&ua0Thread, NULL, &rsImuRxThread, NULL) != 0) {
//		perror("main.c - pthread_create(): Unable to create the thread for ttyS5 (UA0) - ");
//	} else {
//		puts("Serial port (UA0 label on PCB board) is running ...");
//	}

	// Start the 100Hz timer
	StartTimer();

	canRequestFeedback(JOINT_1_ID, CAN_FB_ENCODER_1, CAN_FEEDBACK_CONTINUOUS);
	usleep(50);
	canRequestFeedback(JOINT_2_ID, CAN_FB_ENCODER_1, CAN_FEEDBACK_CONTINUOUS);
	usleep(50);
	canRequestFeedback(JOINT_3_ID, CAN_FB_ENCODER_1, CAN_FEEDBACK_CONTINUOUS);
	usleep(50);

	canRequestFeedback(JOINT_1_ID, CAN_FB_CURRENT, CAN_FEEDBACK_CONTINUOUS);
	usleep(50);
	canRequestFeedback(JOINT_2_ID, CAN_FB_CURRENT, CAN_FEEDBACK_CONTINUOUS);
	usleep(50);
	canRequestFeedback(JOINT_3_ID, CAN_FB_CURRENT, CAN_FEEDBACK_CONTINUOUS);
	usleep(50);

	int counter = 0;

	while (1) {
		//-------------------------------------------------------------------------------------------------------------------
		// 100Hz
		//-------------------------------------------------------------------------------------------------------------------

		if (gTc100HzFlag) {
			Calc_VS_Position();
			Read_Encoder_Data();
			//-------------------------------------------------------------------------------------------------------------------
			if (sAppState.RunManualTest1 && (gJointTrajectorySize > 0)) {
				float position = gJointTrajectoryQuasi[sAppState.RunManualJointIndex][gJointTrajectoryIndex];		// + (360 * sJoint1Status.Param.E0 / 67108864);

				if (sAppState.RunManualJointIndex == 0)
					canSetReferencValue(JOINT_1_ID, CAN_SET_POSITION, position);
				else if (sAppState.RunManualJointIndex == 1)
					canSetReferencValue(JOINT_2_ID, CAN_SET_POSITION, position);
				else if (sAppState.RunManualJointIndex == 2)
					canSetReferencValue(JOINT_3_ID, CAN_SET_POSITION, position);

				LogInfo("Actual Joint 1 Position: %f", sAppState.Joint1CurrPosition);
				LogInfo("Actual Joint 2 Position: %f", sAppState.Joint2CurrPosition);
				LogInfo("Actual Joint 3 Position: %f", sAppState.Joint3CurrPosition);

				LogApp(";QT;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f", EEXPose, EEYPose, EEOrient, position, position, position, SatXPose, SatYPose, SatOrient, th1, th2, th3, 0.0, 0.0, 0.0);

				if (gJointTrajectorySize == 1) {
					sAppState.RunManualTest1 = 0;
					free(gJointTrajectoryQuasi);
					gJointTrajectoryQuasi = NULL;
					puts("SR: Trajectory ended");
					//CloseFileLogger();
				}

				gJointTrajectoryIndex++;
				gJointTrajectorySize--;
			}
			//-------------------------------------------------------------------------------------------------------------------
			else if ((sAppState.RunManualTest1All || sAppState.RunManualQuasiTraj) && (gJointTrajectorySize > 0)) {
				float pos1 = gJointTrajectoryQuasi[0][gJointTrajectoryIndex];
				float pos2 = gJointTrajectoryQuasi[1][gJointTrajectoryIndex];
				float pos3 = gJointTrajectoryQuasi[2][gJointTrajectoryIndex];

				LogInfo("jPosDemand: %f\t%f\t%f", pos1, pos2, pos3);
				LogInfo("jPosActual: %f\t%f\t%f", sAppState.Joint1CurrPosition, sAppState.Joint2CurrPosition, sAppState.Joint3CurrPosition);
				LogInfo("Base x y orient: %f\t%f\t%f", SatXPose, SatYPose, SatOrient);
				LogInfo("EE x y orient: %f\t%f\t%f", EEXPose, EEYPose, EEOrient);
				LogApp(";QT;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f", EEXPose, EEYPose, EEOrient, pos1, pos2, pos3, SatXPose, SatYPose, SatOrient, th1, th2, th3, sJoint_1_state.Current, sJoint_2_state.Current, sJoint_3_state.Current);

				canSetReferencValue(JOINT_1_ID, CAN_SET_POSITION, pos1);
				usleep(50);
				canSetReferencValue(JOINT_2_ID, CAN_SET_POSITION, pos2);
				usleep(50);
				canSetReferencValue(JOINT_3_ID, CAN_SET_POSITION, pos3);

				if (gJointTrajectorySize == 1) {
					sAppState.RunManualTest1All = 0;
					sAppState.RunManualQuasiTraj = 0;

					free(gJointTrajectoryQuasi);
					gJointTrajectoryQuasi = NULL;

					puts("SR: Trajectory for all joints ended");
					//CloseFileLogger();
				}

				gJointTrajectoryIndex++;
				gJointTrajectorySize--;
			}
			//-------------------------------------------------------------------------------------------------------------------
//			if ((sAppState.JointResponseState & JOINT_POS_STATE_RESPONDED) == JOINT_POS_STATE_RESPONDED || ((g10msClock >= sAppState.JointResponseTimeout) && (sAppState.JointResponseTimeout != 0))) {
//
//				if ((g10msClock >= sAppState.JointResponseTimeout) && (sAppState.JointResponseTimeout != 0)) {
//					puts("Can0 - receiving data from joints timeout.");
//				}
//
//				SendManual1TestDataToGCS();
//
//				sAppState.JointResponseState = 0;
//				sAppState.JointResponseTimeout = 0;
//			}

			gTc100HzFlag = 0;
		}

		//-------------------------------------------------------------------------------------------------------------------
		// 10Hz
		//-------------------------------------------------------------------------------------------------------------------

		if (gTc10HzFlag) {
			//fuseENC();
			//Log_SensModuleData();
			if (sAppState.ReadEnc2FromElasticJoint == 1) {
				LogInfo("Joint1, Enc2: %f", sAppState.Joint1Enc2Position);
			}
			//-------------------------------------------------------------------------------------------------------------------
			// Move satellite to required position
			if (sAppState.RunManualTest2) {
				float ff_forcex, ff_forcey, ff_momentz;
				ff_forcex = 0;
				ff_forcey = 0;
				ff_momentz = 0;
				CalculateSattelitePWMToStartPosition(ff_forcex, ff_forcey, ff_momentz);   // empty

				RunColdGasValve(CG_VALVE_1_ID, sSatCtrl.PWM[CG_VALVE_1_ID], sSatellitPid.Delta);
				RunColdGasValve(CG_VALVE_2_ID, sSatCtrl.PWM[CG_VALVE_2_ID], sSatellitPid.Delta);
				RunColdGasValve(CG_VALVE_3_ID, sSatCtrl.PWM[CG_VALVE_3_ID], sSatellitPid.Delta);
				RunColdGasValve(CG_VALVE_4_ID, sSatCtrl.PWM[CG_VALVE_4_ID], sSatellitPid.Delta);
				RunColdGasValve(CG_VALVE_5_ID, sSatCtrl.PWM[CG_VALVE_5_ID], sSatellitPid.Delta);
				RunColdGasValve(CG_VALVE_6_ID, sSatCtrl.PWM[CG_VALVE_6_ID], sSatellitPid.Delta);
				RunColdGasValve(CG_VALVE_7_ID, sSatCtrl.PWM[CG_VALVE_7_ID], sSatellitPid.Delta);
				RunColdGasValve(CG_VALVE_8_ID, sSatCtrl.PWM[CG_VALVE_8_ID], sSatellitPid.Delta);

				LogInfo("PWM:\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d", sSatCtrl.PWM[0], sSatCtrl.PWM[1], sSatCtrl.PWM[2], sSatCtrl.PWM[3], sSatCtrl.PWM[4], sSatCtrl.PWM[5], sSatCtrl.PWM[6], sSatCtrl.PWM[7]);

				// Stop manual 2 test
				float xPosError = sVisionSystem[VS_SATEL_MARKER].PosXcog - sSatCtrl.PositionXReq;
				float yPosError = sVisionSystem[VS_SATEL_MARKER].PosYcog - sSatCtrl.PositionYReq;
				float zOrientError = sVisionSystem[VS_SATEL_MARKER].OrientZcog - sSatCtrl.OrientationZReq;

				xPosError = fabs(xPosError);
				yPosError = fabs(yPosError);
				zOrientError = fabs(zOrientError);

				if (xPosError <= MANUAL_TEST_2_XY_POS_ERROR && yPosError <= MANUAL_TEST_2_XY_POS_ERROR && zOrientError <= MANUAL_TEST_2_Z_ORIENT_ERROR) {
					puts("SR: Satellite requested trajectory stopped");
					//CloseFileLogger();
					sAppState.RunManualTest2 = 0;
				}
			}
			//-------------------------------------------------------------------------------------------------------------------
			// Move satellite through generated trajectory
			if ((sAppState.RunManualTest3 || sAppState.RunManualTest4) && (gSatTrajectorySize > 0)) {
				sSatCtrl.PositionXReq = gSatTrajectory[0][gSatTrajectoryIndex];		// [m]
				sSatCtrl.PositionYReq = gSatTrajectory[1][gSatTrajectoryIndex];		// [m]
				sSatCtrl.OrientationZReq = gSatTrajectory[2][gSatTrajectoryIndex];	// [deg]

				if (gSatTrajectoryIndex == 0)
					load_forces_and_moments(FF_forcex, FF_forcey, FF_momentz);

				CalculateSattelitePWMToStartPosition(FF_forcex[gSatTrajectoryIndex], FF_forcey[gSatTrajectoryIndex], FF_momentz[gSatTrajectoryIndex]);

				RunColdGasValve(CG_VALVE_1_ID, sSatCtrl.PWM[CG_VALVE_1_ID], sSatellitPid.Delta);
				RunColdGasValve(CG_VALVE_2_ID, sSatCtrl.PWM[CG_VALVE_2_ID], sSatellitPid.Delta);
				RunColdGasValve(CG_VALVE_3_ID, sSatCtrl.PWM[CG_VALVE_3_ID], sSatellitPid.Delta);
				RunColdGasValve(CG_VALVE_4_ID, sSatCtrl.PWM[CG_VALVE_4_ID], sSatellitPid.Delta);
				RunColdGasValve(CG_VALVE_5_ID, sSatCtrl.PWM[CG_VALVE_5_ID], sSatellitPid.Delta);
				RunColdGasValve(CG_VALVE_6_ID, sSatCtrl.PWM[CG_VALVE_6_ID], sSatellitPid.Delta);
				RunColdGasValve(CG_VALVE_7_ID, sSatCtrl.PWM[CG_VALVE_7_ID], sSatellitPid.Delta);
				RunColdGasValve(CG_VALVE_8_ID, sSatCtrl.PWM[CG_VALVE_8_ID], sSatellitPid.Delta);

				LogInfo("Sily i moment: %f\t%f\t%f", FF_forcex[gSatTrajectoryIndex], FF_forcey[gSatTrajectoryIndex], FF_momentz[gSatTrajectoryIndex]);
				LogInfo("Satellite position and orientation DEMANDED: %f\t%f\t%f", sSatCtrl.PositionXReq, sSatCtrl.PositionYReq, sSatCtrl.OrientationZReq);
				LogInfo("Satellite position and orientation ACTUAL: %f\t%f\t%f", sVisionSystem[VS_SATEL_MARKER].PosXcog, sVisionSystem[VS_SATEL_MARKER].PosYcog, sVisionSystem[VS_SATEL_MARKER].OrientZcog);
				LogInfo("PWM calculated: %d\t%d\t%d\t%d\t%d\t%d\t%d\t%d", sSatCtrl.PWM[0], sSatCtrl.PWM[1], sSatCtrl.PWM[2], sSatCtrl.PWM[3], sSatCtrl.PWM[4], sSatCtrl.PWM[5], sSatCtrl.PWM[6], sSatCtrl.PWM[7]);

				perror(gProtBuffer);

				if (gSatTrajectorySize == 1) {
					free(gSatTrajectory);
					gSatTrajectory = NULL;
					if (sAppState.RunManualTest3)
						puts("SR: Satellite trajectory ended");
					else
						// RunManualTest4
						puts("SR: Satellite trajectory from file ended");
					//CloseFileLogger();
				}

				gSatTrajectoryIndex++;
				gSatTrajectorySize--;
			}

			//-------------------------------------------------------------------------------------------------------------------
			//CONTROL_DYNAMIC
			if ((sAppState.RunManualTest2RA || sAppState.RunManualTest3RA || sAppState.RunManualTest4RA) && (sAppState.ControlAlgorithm == CONTROL_DYNAMIC)) {
				float EETrajDem[3];
				float EEPrim[9][9];
				float DynJac[9][9], invDynJac[9][9];
				float JointsVelDem[9][9];
				float th1_demand_rad, th2_demand_rad, th3_demand_rad;

				EETrajDem[0] = gSatTrajectory[0][gSatTrajectoryIndex];
				EETrajDem[1] = gSatTrajectory[1][gSatTrajectoryIndex];
				EETrajDem[2] = gSatTrajectory[2][gSatTrajectoryIndex];

				EEPrim[0][0] = (EETrajDem[0] - EEXPose) * JACOBIAN_CONTROLER_GAIN;
				EEPrim[1][0] = (EETrajDem[1] - EEYPose) * JACOBIAN_CONTROLER_GAIN;
				EEPrim[2][0] = unwrap_rad(EETrajDem[2] - EEOrient) * JACOBIAN_CONTROLER_GAIN;

				DynamicJacobian(SatOrient, th1, th2, th3, DynJac);

				MatrixInverse(DynJac, 3, invDynJac);

				MatrixMultiplication(invDynJac, EEPrim, JointsVelDem, 3, 3, 1);

				th1_demand_rad = th1 + JointsVelDem[0][0] * 0.1;
				th2_demand_rad = th2 + JointsVelDem[1][0] * 0.1;
				th3_demand_rad = th3 + JointsVelDem[2][0] * 0.1;

				canSetReferencValue(JOINT_1_ID, CAN_SET_POSITION, th1_demand_rad);
				usleep(50);
				canSetReferencValue(JOINT_2_ID, CAN_SET_POSITION, th2_demand_rad);
				usleep(50);
				canSetReferencValue(JOINT_3_ID, CAN_SET_POSITION, th3_demand_rad);
				usleep(50);

				// Data logging
				LogInfo("Indeks \t%d", gSatTrajectoryIndex);
				LogInfo("Size \t%d", gSatTrajectorySize);
				LogInfo("AAA \t%d", gSatTrajectorySize);
				LogInfo("Measured joints positions: \t%f\t%f\t%f", th1, th2, th3);
				LogInfo("Demanded joints positions: \t%f\t%f\t%f", th1_demand_rad, th2_demand_rad, th3_demand_rad);
				LogInfo("Demanded joints velocities: \t%f\t%f\t%f", JointsVelDem[0][0], JointsVelDem[1][0], JointsVelDem[2][0]);
				LogInfo("Measured Satellite position and orientation: \t%f\t%f\t%f", SatXPose, SatYPose, SatOrient);
				LogInfo("Measured EE position and orientation: \t%f\t%f\t%f", EEXPose, EEYPose, EEOrient);
				LogInfo("Demanded EE position and orientation: \t%f\t%f\t%f", EETrajDem[0], EETrajDem[1], EETrajDem[2]);
				LogApp(";DY;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f", EEXPose, EEYPose, EEOrient, EETrajDem[0], EETrajDem[1], EETrajDem[2], SatXPose, SatYPose, SatOrient, th1, th2, th3, th1_demand_rad, th2_demand_rad, th3_demand_rad);

				if (gSatTrajectorySize == 1) {
					free(gSatTrajectory);
					gSatTrajectory = NULL;
					puts("SR: Robotic arm trajectory ended.");
					sAppState.RunManualTest2RA = 0;
					sAppState.RunManualTest3RA = 0;
					sAppState.RunManualTest4RA = 0;
					//CloseFileLogger();
				} else {
					//ReadJointPosition();
				}

				gSatTrajectoryIndex++;
				gSatTrajectorySize--;

			}
			//-------------------------------------------------------------------------------------------------------------------
			//KINEMATIC_WITHOUT_FB
			if ((sAppState.RunManualTest2RA || sAppState.RunManualTest3RA || sAppState.RunManualTest4RA) && (sAppState.ControlAlgorithm == CONTROL_KINEMATIC_WITHOUT_FB)) {
				float EETrajDem[3];
				float Vel[9][9];
				float KinJac[9][9], invKinJac[9][9];
				float JointsVelDem[9][9];
				float th1_demand_rad, th2_demand_rad, th3_demand_rad;

				EETrajDem[0] = gSatTrajectory[0][gSatTrajectoryIndex];
				EETrajDem[1] = gSatTrajectory[1][gSatTrajectoryIndex];
				EETrajDem[2] = gSatTrajectory[2][gSatTrajectoryIndex];

				Vel[0][0] = (EETrajDem[0] - EEXPose) * JACOBIAN_CONTROLER_GAIN;
				Vel[1][0] = (EETrajDem[1] - EEYPose) * JACOBIAN_CONTROLER_GAIN;
				Vel[2][0] = unwrap_rad(EETrajDem[2] - EEOrient) * JACOBIAN_CONTROLER_GAIN;

				float rotmatrix[9][9], VelRot[9][9];
				rotmatrix[0][0] = cos(SatOrient);
				rotmatrix[0][1] = -sin(SatOrient);
				rotmatrix[0][2] = 0;
				rotmatrix[1][0] = sin(SatOrient);
				rotmatrix[1][1] = cos(SatOrient);
				rotmatrix[1][2] = 0;
				rotmatrix[2][0] = 0;
				rotmatrix[2][1] = 0;
				rotmatrix[2][2] = 1;
				MatrixMultiplication(rotmatrix, Vel, VelRot, 3, 3, 1);

				KinematicJacobian(th1, th2, th3, KinJac);
				MatrixInverse(KinJac, 3, invKinJac);
				MatrixMultiplication(invKinJac, VelRot, JointsVelDem, 3, 3, 1);
				th1_demand_rad = th1 + JointsVelDem[0][0] * 0.1;
				th2_demand_rad = th2 + JointsVelDem[1][0] * 0.1;
				th3_demand_rad = th3 + JointsVelDem[2][0] * 0.1;
				canSetReferencValue(JOINT_1_ID, CAN_SET_POSITION, th1_demand_rad);
				usleep(50);
				canSetReferencValue(JOINT_2_ID, CAN_SET_POSITION, th2_demand_rad);
				usleep(50);
				canSetReferencValue(JOINT_3_ID, CAN_SET_POSITION, th3_demand_rad);
				usleep(50);
				// Data logging
				LogInfo("Indeks \t%d", gSatTrajectoryIndex);
				LogInfo("Size \t%d", gSatTrajectorySize);
				LogInfo("Uchyb: \t%f\t%f\t%f", Vel[0][0], Vel[1][0], Vel[2][0]);
				LogInfo("Measured joints positions: \t%f\t%f\t%f", th1, th2, th3);
				LogInfo("Demanded joints positions: \t%f\t%f\t%f", th1_demand_rad, th2_demand_rad, th3_demand_rad);
				LogInfo("Demanded joints velocities: \t%f\t%f\t%f", JointsVelDem[0][0], JointsVelDem[1][0], JointsVelDem[2][0]);
				LogInfo("Measured Satellite position and orientation: \t%f\t%f\t%f", SatXPose, SatYPose, SatOrient);
				LogInfo("Measured EE position and orientation: \t%f\t%f\t%f", EEXPose, EEYPose, EEOrient);
				LogInfo("Demanded EE position and orientation: \t%f\t%f\t%f", EETrajDem[0], EETrajDem[1], EETrajDem[2]);
				LogApp(";K;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f", EEXPose, EEYPose, EEOrient, EETrajDem[0], EETrajDem[1], EETrajDem[2], SatXPose, SatYPose, SatOrient, th1, th2, th3, th1_demand_rad, th2_demand_rad, th3_demand_rad);
				//Log_SensModuleData();

				if (gSatTrajectorySize == 1) {
					free(gSatTrajectory);
					gSatTrajectory = NULL;
					puts("SR: Robotic arm trajectory ended.");
					sAppState.RunManualTest2RA = 0;
					sAppState.RunManualTest3RA = 0;
					sAppState.RunManualTest4RA = 0;
					//CloseFileLogger();
				} else {
					//ReadJointPosition();
				}

				gSatTrajectoryIndex++;
				gSatTrajectorySize--;
				counter++;
			}
// receiving data from joints timeout
			//-------------------------------------------------------------------------------------------------------------------
			//CONTROL_KINEMATIC_WITH_FB
			if ((sAppState.RunManualTest2RA || sAppState.RunManualTest3RA || sAppState.RunManualTest4RA) && (sAppState.ControlAlgorithm == CONTROL_KINEMATIC_WITH_FB)) {
				static float SatXPosePrev, SatYPosePrev, SatOrientPrev;
				float EETrajDem[3];
				float EEPrim[3];
				float KinJac[9][9], invKinJac[9][9];
				float JointsVelDem[9][9];
				float th1_demand_rad, th2_demand_rad, th3_demand_rad;
				float SatVelIne[9][9];
				float RotSatIne[9][9], RotSatIneT[9][9];
				float gamma;
				float SatBaseVBase[9][9];
				float EEPosSatBase[3];
				float WSAT[3];
				float VCross[3];
				float VelNoRotate[9][9], Vel[9][9];

				EETrajDem[0] = gSatTrajectory[0][gSatTrajectoryIndex];
				EETrajDem[1] = gSatTrajectory[1][gSatTrajectoryIndex];
				EETrajDem[2] = gSatTrajectory[2][gSatTrajectoryIndex];

				EEPrim[0] = (EETrajDem[0] - EEXPose) * JACOBIAN_CONTROLER_GAIN;
				EEPrim[1] = (EETrajDem[1] - EEYPose) * JACOBIAN_CONTROLER_GAIN;
				EEPrim[2] = unwrap_rad(EETrajDem[2] - EEOrient) * JACOBIAN_CONTROLER_GAIN;

				gamma = SatOrient + th1 + th2 + th3;

				RotSatIne[0][0] = cos(gamma);
				RotSatIne[0][1] = -sin(gamma);
				RotSatIne[0][2] = 0;
				RotSatIne[1][0] = sin(gamma);
				RotSatIne[1][1] = cos(gamma);
				RotSatIne[1][2] = 0;
				RotSatIne[2][0] = 0;
				RotSatIne[2][1] = 0;
				RotSatIne[2][2] = 1;

				if (counter == 0) {
					SatVelIne[0][0] = 0;
					SatVelIne[1][0] = 0;
					SatVelIne[2][0] = 0;
					SatXPosePrev = SatXPose;
					SatYPosePrev = SatYPose;
					SatOrientPrev = SatOrient;
				} else {
					SatVelIne[0][0] = (SatXPose - SatXPosePrev) / 0.1;
					SatVelIne[1][0] = (SatYPose - SatYPosePrev) / 0.1;
					SatVelIne[2][0] = (SatOrient - SatOrientPrev) / 0.1;
				}

				MatrixMultiplication(RotSatIne, SatVelIne, SatBaseVBase, 3, 3, 1);

				EEPosSatBase[0] = EEXPose - SatXPose;
				EEPosSatBase[1] = EEYPose - SatYPose;
				EEPosSatBase[2] = 0;

				WSAT[0] = 0;
				WSAT[1] = 0;
				WSAT[2] = SatBaseVBase[2][0];

				CrossProd(WSAT, EEPosSatBase, VCross);

				VelNoRotate[0][0] = EEPrim[0] - SatVelIne[0][0] - VCross[0];
				VelNoRotate[1][0] = EEPrim[1] - SatVelIne[1][0] - VCross[1];
				VelNoRotate[2][0] = EEPrim[2] - SatVelIne[2][0] - VCross[2];

				MatrixTranspose(RotSatIne, RotSatIneT, 3, 3);
				MatrixMultiplication(RotSatIneT, VelNoRotate, Vel, 3, 3, 1);
				KinematicJacobian(th1, th2, th3, KinJac);
				MatrixInverse(KinJac, 3, invKinJac);
				MatrixMultiplication(invKinJac, Vel, JointsVelDem, 3, 3, 1);

				th1_demand_rad = th1 + JointsVelDem[0][0] * 0.1;
				th2_demand_rad = th2 + JointsVelDem[1][0] * 0.1;
				th3_demand_rad = th3 + JointsVelDem[2][0] * 0.1;

				canSetReferencValue(JOINT_1_ID, CAN_SET_POSITION, th1_demand_rad);
				usleep(50);
				canSetReferencValue(JOINT_2_ID, CAN_SET_POSITION, th2_demand_rad);
				usleep(50);
				canSetReferencValue(JOINT_3_ID, CAN_SET_POSITION, th3_demand_rad);
				usleep(50);

				SatXPosePrev = SatXPose;
				SatYPosePrev = SatYPose;
				SatOrientPrev = SatOrient;

				// Data logging
				LogInfo("Indeks \t%d", gSatTrajectoryIndex);
				LogInfo("Size \t%d", gSatTrajectorySize);
				LogInfo("Measured joints positions: \t%f\t%f\t%f", th1, th2, th3);
				LogInfo("Demanded joints positions: \t%f\t%f\t%f", th1_demand_rad, th2_demand_rad, th3_demand_rad);
				LogInfo("Demanded joints velocities: \t%f\t%f\t%f", JointsVelDem[0][0], JointsVelDem[1][0], JointsVelDem[2][0]);
				LogInfo("Measured Satellite position and orientation: \t%f\t%f\t%f", SatXPose, SatYPose, SatOrient);
				LogInfo("Measured EE position and orientation: \t%f\t%f\t%f", EEXPose, EEYPose, EEOrient);
				LogInfo("Demanded EE position and orientation: \t%f\t%f\t%f", EETrajDem[0], EETrajDem[1], EETrajDem[2]);
				LogApp(";KF;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f", EEXPose, EEYPose, EEOrient, EETrajDem[0], EETrajDem[1], EETrajDem[2], SatXPose, SatYPose, SatOrient, th1, th2, th3, th1_demand_rad, th2_demand_rad, th3_demand_rad);

				if (gSatTrajectorySize == 1) {
					free(gSatTrajectory);
					gSatTrajectory = NULL;
					puts("SR: Robotic arm trajectory ended.");
					sAppState.RunManualTest2RA = 0;
					sAppState.RunManualTest3RA = 0;
					sAppState.RunManualTest4RA = 0;
					CloseFileLogger();
				} else {
					ReadJointPosition();
				}

				gSatTrajectoryIndex++;
				gSatTrajectorySize--;
				counter++;
			}

			//-------------------------------------------------------------------------------------------------------------------
			//CONTROL_ADAPTIVE_BACK
			if ((sAppState.RunManualTest2RA || sAppState.RunManualTest3RA || sAppState.RunManualTest4RA) && (sAppState.ControlAlgorithm == CONTROL_ADAPTIVE_BACK)) {

				float q0, q1, q2, q3, dq0, dq1, dq2, dq3, dxs, dys, xs, ys;
				float q0_prev, q1_prev, q2_prev, q3_prev, dq0_prev, dxs_prev, dys_prev, xs_prev, ys_prev;
				float ddq0, ddxs, ddys;
				float xee, yee, qee, dxee, dyee, dqee, ddxee, ddyee, ddqee;
				float xee_prev, yee_prev, qee_prev, dxee_prev, dyee_prev, dqee_prev;
				float xee_dem, yee_dem, qee_dem, dxee_dem, dyee_dem, dqee_dem, ddxee_dem, ddyee_dem, ddqee_dem;

				float J0[9][9], JM[9][9], dJ0[9][9], dJM[9][9];
				float A[9][9][9], B[9][9][9], Adot[9][9][9], Bdot[9][9][9], dAdx[9][9][9], dBdx[9][9][9];
				float J[9][9], dJ[9][9];
				float Po[9][9], Pod[9][9], dPo[9][9], dPod[9][9], ddPo[9][9], ddPod[9][9];
				float eo[9][9], deo[9][9], ej[9][9];
				float Ko[9][9], Kj[9][9];
				float xv[9][9], xvd[9][9], dxvd[9][9];
				float a[9][9], b[9][9];
				float gamma[9][9];
				float dphi_hat[9][9], phi_hat[9][9], phi_hat_previous[9][9];
				float tau[9][9], tau1, tau2, tau3;

				float dt;
				float init_m0, init_m1, init_m2, init_m3;
				float init_I0, init_I1, init_I2, init_I3;

				// Sygnały wejściowe (zamiast jedynek odpowiednie odczyty z czujnikow)
				xs = SatXPose; // Pozycja X satelity z systemu wizyjnego [m]
				ys = SatYPose; // Pozycja Y satelity z systemu wizyjnego [m]
				q0 = SatOrient; // Orientacja satelity z systemu wizyjnego [rad]
				q1 = th1; // Pozycja złączowa 1 przegubu z enkodera [rad]
				q2 = th2; // Pozycja złączowa 2 przegubu z enkodera [rad]
				q3 = th3; // Pozycja złączowa 3 przegubu z enkodera [rad]
				xee = EEXPose; // Pozycja X end-effectora z systemu wizyjnego [m]
				yee = EEYPose; // Pozycja Y end-effectora z systemu wizynego [m]
				qee = EEOrient; // Orientacja end-effectora z systemu wizyjnego [rad]

				// Trajektoria zadana ((zamiast jedynek odpowiednie odczyty z trajektorii zadanej)
				// Iterowanie co 3 bo w pliku sa pozycja, predkosc,przyspieszenie jako 3 kolejne linie
				xee_dem = gSatTrajectory[0][gSatTrajectoryIndex]; // Zadana pozycja X end-effectora [m]
				yee_dem = gSatTrajectory[1][gSatTrajectoryIndex]; // Zadana pozycja Y end-effectora [m]
				qee_dem = gSatTrajectory[2][gSatTrajectoryIndex]; // Zadana orientacja end-effectora [rad]
				gSatTrajectoryIndex++;
				dxee_dem = gSatTrajectory[0][gSatTrajectoryIndex]; // Zadana predkosc na X end-effectora [m/s]
				dyee_dem = gSatTrajectory[1][gSatTrajectoryIndex]; // Zadana predkosc na Y end-effectore [m/s]
				dqee_dem = gSatTrajectory[2][gSatTrajectoryIndex]; // Zadana predkosc katowa end-effectora [rad/s]
				gSatTrajectoryIndex++;
				ddxee_dem = gSatTrajectory[0][gSatTrajectoryIndex]; // Zadane przyspieszenie na X end-effectora [m/s^2]
				ddyee_dem = gSatTrajectory[1][gSatTrajectoryIndex]; // Zadane przyspieszenie na Y end-effectora [m/s^2]
				ddqee_dem = gSatTrajectory[2][gSatTrajectoryIndex]; // Zadane przyspieszenie katowe end-effectora [rad/s^2]
				gSatTrajectoryIndex++;

				// Stałe
				InitiateMatrixWithZeros(6, 6, Ko);
				Ko[0][0] = 1;
				Ko[1][1] = 1;
				Ko[2][2] = 1;
				Ko[3][3] = 5.0;
				Ko[4][4] = 5.0;
				Ko[5][5] = 5.0;

				InitiateMatrixWithZeros(6, 6, Kj);
				Kj[0][0] = 1;
				Kj[1][1] = 1;
				Kj[2][2] = 1;
				Kj[3][3] = 5.0;
				Kj[4][4] = 5.0;
				Kj[5][5] = 5.0;

				InitiateMatrixWithZeros(8, 1, gamma);
				gamma[0][0] = 5;
				gamma[1][0] = 5;
				gamma[2][0] = 5;
				gamma[3][0] = 5;
				gamma[4][0] = 5;
				gamma[5][0] = 5;
				gamma[6][0] = 5;
				gamma[7][0] = 5;

				dt = 0.1;

				init_m0 = 58.6858;
				init_m1 = 2.81;
				init_m2 = 2.82;
				init_m3 = 4.64;
				init_I0 = 2.4172;
				init_I1 = 0.0637;
				init_I2 = 0.0635;
				init_I3 = 0.0515;

				// Wyznaczanie prędkości jako pochodnej numerycznej
				if (gSatTrajectoryIndex == 3) {
					dxs = 0;
					dys = 0;
					dq0 = 0;
					dq1 = 0;
					dq2 = 0;
					dq3 = 0;
					dxee = 0;
					dyee = 0;
					dqee = 0;

					xs_prev = xs;
					ys_prev = ys;
					q0_prev = q0;
					q1_prev = q1;
					q2_prev = q2;
					q3_prev = q3;
					xee_prev = xee;
					yee_prev = yee;
					qee_prev = qee;
				} else {
					dxs = (xs - xs_prev) / 0.1;
					dys = (ys - ys_prev) / 0.1;
					dq0 = (q0 - q0_prev) / 0.1;
					dq1 = (q1 - q1_prev) / 0.1;
					dq2 = (q2 - q2_prev) / 0.1;
					dq3 = (q3 - q3_prev) / 0.1;
					dxee = (xee - xee_prev) / 0.1;
					dyee = (yee - yee_prev) / 0.1;
					dqee = (qee - qee_prev) / 0.1;
				}

				// Wyznaczanie przyspieszen jako pochodnej numerycznej
				if (gSatTrajectoryIndex == 6) {
					ddxs = 0;
					ddys = 0;
					ddq0 = 0;
					ddxee = 0;
					ddyee = 0;
					ddqee = 0;

					dxs_prev = dxs;
					dys_prev = dys;
					dq0_prev = dq0;
					dxee_prev = dxee;
					dyee_prev = dyee;
					dqee_prev = dqee;
				} else {
					ddxs = (dxs - dxs_prev) / 0.1;
					ddys = (dys - dys_prev) / 0.1;
					ddq0 = (dq0 - dq0_prev) / 0.1;
					ddxee = (dxee - dxee_prev) / 0.1;
					ddyee = (dyee - dyee_prev) / 0.1;
					ddqee = (dqee - dqee_prev) / 0.1;
				}

				// Obliczenia sterownika adaptacyjnego
				KinematicJacobians(q0, q1, q2, q3, dq0, dq1, dq2, dq3, JM, dJM, J0, dJ0);

				PartialJacobians(q0, q1, q2, q3, dxs, dys, dq0, dq1, dq2, dq3, A, B, Adot, Bdot, dAdx, dBdx);

				AugmentedJacobians(JM, dJM, J0, dJ0, J, dJ);

				Create6DVector(xs, ys, q0, xee, yee, qee, Po);
				Create6DVector(dxs, dys, dq0, dxee, dyee, dqee, dPo);
				Create6DVector(ddxs, ddys, ddq0, ddxee, ddyee, ddqee, ddPo);

				Create6DVector(xs, ys, q0, xee_dem, yee_dem, qee_dem, Pod);
				Create6DVector(dxs, dys, dq0, dxee_dem, dyee_dem, dqee_dem, dPod);
				Create6DVector(ddxs, ddys, ddq0, ddxee_dem, ddyee_dem, ddqee_dem, ddPod);

				MatrixSubtract(Pod, Po, eo, 6, 1);
				MatrixSubtract(dPod, dPo, deo, 6, 1);

				IntermediateControl(eo, deo, dPo, dPod, ddPod, J, dJ, Ko, xvd, dxvd);

				Create6DVector(dxs, dys, dq0, dq1, dq2, dq3, xv);

				MatrixSubtract(xvd, xv, ej, 6, 1);

				basisRegressionMatrixFunctions(A, B, Adot, Bdot, dAdx, dBdx, ej, dxvd, xv, a, b);

				adaptationLaws(gamma, a, b, ej, dxvd, xv, dphi_hat);

				// Parametry adaptowane
				InitiateMatrixWithZeros(8, 1, phi_hat);
				InitiateMatrixWithZeros(8, 1, phi_hat_previous);

				if (gSatTrajectoryIndex == 3) {
					phi_hat[0][0] = init_m0 + dphi_hat[0][0] * dt;
					phi_hat[1][0] = init_m1 + dphi_hat[1][0] * dt;
					phi_hat[2][0] = init_m2 + dphi_hat[2][0] * dt;
					phi_hat[3][0] = init_m3 + dphi_hat[3][0] * dt;
					phi_hat[4][0] = init_I0 + dphi_hat[4][0] * dt;
					phi_hat[5][0] = init_I1 + dphi_hat[5][0] * dt;
					phi_hat[6][0] = init_I2 + dphi_hat[6][0] * dt;
					phi_hat[7][0] = init_I3 + dphi_hat[7][0] * dt;

					phi_hat_previous[0][0] = phi_hat[0][0];
					phi_hat_previous[1][0] = phi_hat[1][0];
					phi_hat_previous[2][0] = phi_hat[2][0];
					phi_hat_previous[3][0] = phi_hat[3][0];
					phi_hat_previous[4][0] = phi_hat[4][0];
					phi_hat_previous[5][0] = phi_hat[5][0];
					phi_hat_previous[6][0] = phi_hat[6][0];
					phi_hat_previous[7][0] = phi_hat[7][0];

				} else {
					phi_hat[0][0] = phi_hat_previous[0][0] + dphi_hat[0][0] * dt;
					phi_hat[1][0] = phi_hat_previous[1][0] + dphi_hat[1][0] * dt;
					phi_hat[2][0] = phi_hat_previous[2][0] + dphi_hat[2][0] * dt;
					phi_hat[3][0] = phi_hat_previous[3][0] + dphi_hat[3][0] * dt;
					phi_hat[4][0] = phi_hat_previous[4][0] + dphi_hat[4][0] * dt;
					phi_hat[5][0] = phi_hat_previous[5][0] + dphi_hat[5][0] * dt;
					phi_hat[6][0] = phi_hat_previous[6][0] + dphi_hat[6][0] * dt;
					phi_hat[7][0] = phi_hat_previous[7][0] + dphi_hat[7][0] * dt;
				}

				// Finalne prawo sterowania
				controlLaw(a, b, J, eo, ej, dxvd, xv, Kj, phi_hat, tau);

				// Syhnały wyjściowe
				tau1 = tau[3][0]; // moment napędowy na 1 złącze [Nm]
				tau2 = tau[4][0]; // moment napędowy na 2 złącze [Nm]
				tau3 = tau[5][0]; // moment napędowy na 3 złącze [Nm]

//				canSetReferencValue(JOINT_1_ID, CAN_SET_CURRENT, tau1*0.5154); //Torqe constatnt 19.4 mNm/A, gear 1:100
//				usleep(50);
//				canSetReferencValue(JOINT_2_ID, CAN_SET_CURRENT, tau2*0.25706); //Torqe constatnt 38.9 mNm/A, gear 1:100
//				usleep(50);
//				canSetReferencValue(JOINT_3_ID, CAN_SET_CURRENT, tau3*0.25706); //Torqe constatnt 38.9 mNm/A, gear 1:100
//				usleep(50);

//				float ref_vel1 = dq1 + tau1*0.01; // Stala przyjeta arbitralnie
//				float ref_vel2 = dq2 + tau2*0.01; // Stala przyjeta arbitralnie
//				float ref_vel3 = dq3 + tau3*0.01; // Stala przyjeta arbitralnie
//
//				float ref_pos1 = th1 + ref_vel1*0.1;
//				float ref_pos2 = th2 + ref_vel2*0.1;
//				float ref_pos3 = th3 + ref_vel3*0.1;

				float ref_pos1 = th1 + tau1*0.02;
				float ref_pos2 = th2 + tau2*0.02;
				float ref_pos3 = th3 + tau3*0.02;

				canSetReferencValue(JOINT_1_ID, CAN_SET_POSITION, ref_pos1);
				usleep(50);
				canSetReferencValue(JOINT_2_ID, CAN_SET_POSITION, ref_pos2);
				usleep(50);
				canSetReferencValue(JOINT_3_ID, CAN_SET_POSITION, ref_pos3);
				usleep(50);

				// Przypisanie poprzednich wartosci do obliczen w kolejnej iteracji
				xs_prev = xs;
				ys_prev = ys;
				q0_prev = q0;
				q1_prev = q1;
				q2_prev = q2;
				q3_prev = q3;
				xee_prev = xee;
				yee_prev = yee;
				qee_prev = qee;

				dxs_prev = dxs;
				dys_prev = dys;
				dq0_prev = dq0;
				dxee_prev = dxee;
				dyee_prev = dyee;
				dqee_prev = dqee;

				phi_hat_previous[0][0] = phi_hat[0][0];
				phi_hat_previous[1][0] = phi_hat[1][0];
				phi_hat_previous[2][0] = phi_hat[2][0];
				phi_hat_previous[3][0] = phi_hat[3][0];
				phi_hat_previous[4][0] = phi_hat[4][0];
				phi_hat_previous[5][0] = phi_hat[5][0];
				phi_hat_previous[6][0] = phi_hat[6][0];
				phi_hat_previous[7][0] = phi_hat[7][0];

				// Data logging
				LogApp(";AB;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f", EEXPose, EEYPose, EEOrient, xee_dem, yee_dem, qee_dem,dxee_dem, dyee_dem, dqee_dem, ddxee_dem, ddyee_dem, ddqee_dem,SatXPose, SatYPose, SatOrient, th1, th2, th3, tau1, tau2, tau3,xvd[3][0],xvd[4][0],xvd[5][0]);

				if (gSatTrajectorySize <= 3) {
					free(gSatTrajectory);
					gSatTrajectory = NULL;
					puts("SR: Robotic arm trajectory ended.");
					sAppState.RunManualTest2RA = 0;
					sAppState.RunManualTest3RA = 0;
					sAppState.RunManualTest4RA = 0;
					CloseFileLogger();
				}

				// Iterowanie co 3 bo w pliku sa pozycja, predkosc,przyspieszenie jako 3 kolejne linie
				gSatTrajectorySize-=3;
			}
			//-------------------------------------------------------------------------------------------------------------------
			//TORQUE TRAJECTORY
			if ((sAppState.RunManualTest4RA) && (sAppState.ControlAlgorithm == CONTROL_TORQUE_TRAJECTORY)) {
				float tau1, tau2, tau3;

				tau1 = gSatTrajectory[0][gSatTrajectoryIndex]; // Zadana pozycja X end-effectora [m]
				tau2 = gSatTrajectory[1][gSatTrajectoryIndex]; // Zadana pozycja Y end-effectora [m]
				tau3 = gSatTrajectory[2][gSatTrajectoryIndex]; // Zadana orientacja end-effectora [rad]

				canSetReferencValue(JOINT_1_ID, CAN_SET_CURRENT, tau1*0.5154); //Torqe constatnt 19.4 mNm/A, gear 1:100
				usleep(50);
				canSetReferencValue(JOINT_2_ID, CAN_SET_CURRENT, tau2*0.25706); //Torqe constatnt 38.9 mNm/A, gear 1:100
				usleep(50);
				canSetReferencValue(JOINT_3_ID, CAN_SET_CURRENT, tau3*0.25706); //Torqe constatnt 38.9 mNm/A, gear 1:100
				usleep(50);

				// Data logging
				LogApp(";TT;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f", EEXPose, EEYPose, EEOrient ,SatXPose, SatYPose, SatOrient, th1, th2, th3, tau1, tau2, tau3, sJoint_1_state.Current, sJoint_2_state.Current,sJoint_3_state.Current);


				if (gSatTrajectorySize == 1) {
					free(gSatTrajectory);
					gSatTrajectory = NULL;
					puts("SR: Robotic arm trajectory ended.");
					sAppState.RunManualTest2RA = 0;
					sAppState.RunManualTest3RA = 0;
					sAppState.RunManualTest4RA = 0;
					CloseFileLogger();
				} else {
					ReadJointPosition();
				}

				gSatTrajectoryIndex++;
				gSatTrajectorySize--;
				counter++;

			}

			g10HzLoopCounter++;
			gTc10HzFlag = 0;
		}

		//-------------------------------------------------------------------------------------------------------------------
		// 1Hz
		//-------------------------------------------------------------------------------------------------------------------
		if (gTc1HzFlag) {
			gTc1HzFlag = 0;
			SendManual1TestDataToGCS();
			SendManual2TestDataToGCS();
		}
	}
	exit(EXIT_SUCCESS);
}

void Read_Encoder_Data(void) {
	// Joint1 angular position
	th1 = sJoint_1_state.Encoder_1;
	th2 = sJoint_2_state.Encoder_1;
	th3 = sJoint_3_state.Encoder_1;

}

void Calc_VS_Position(void) {
	SatXPose = (float) sVisionSystem[VS_SATEL_MARKER].PositionX / 10000.0; // [m]
	SatYPose = (float) sVisionSystem[VS_SATEL_MARKER].PositionY / 10000.0; // [m]
	SatOrient = (float) sVisionSystem[VS_SATEL_MARKER].OrientationZ * SAT_ORIENT_SCALE_FACTOR; // [rad]
	SatOrient = unwrap_rad(SatOrient);
	SatOrient = SatOrient + SAT_PI / 2.0;
	SatOrient = unwrap_rad(SatOrient);

	SatXPose = SatXPose + SAT_COG_C_DISP_FROM_SAT_MARKER * cos(SatOrient + SAT_COG_ALPHA_DISP_FROM_SAT_MARKER);
	SatYPose = SatYPose + SAT_COG_C_DISP_FROM_SAT_MARKER * sin(SatOrient + SAT_COG_ALPHA_DISP_FROM_SAT_MARKER);

	// End-effector
	float marker_center_to_reference = 0.035;
	float joint3_to_marker_center = 0.177;
	float L3 = 0.31025;
	float marker_reference_to_EE = L3 - joint3_to_marker_center + marker_center_to_reference;
	float temp_orient = (float) sVisionSystem[VS_MANIP_MARKER].OrientationZ * SAT_ORIENT_SCALE_FACTOR;
	EEXPose = (float) sVisionSystem[VS_MANIP_MARKER].PositionX / 10000.0 - marker_reference_to_EE * sin(temp_orient);        // [m]
	EEYPose = (float) sVisionSystem[VS_MANIP_MARKER].PositionY / 10000.0 + marker_reference_to_EE * cos(temp_orient);        // [m]

	temp_orient = unwrap_rad(temp_orient);
	temp_orient = temp_orient + SAT_PI / 2;
	EEOrient = unwrap_rad(temp_orient);
}

void Log_SensModuleData(void) {
	float SM_SatPos[5];
	float SM_SatOrient[5];
	float SM_SatSpeed[5];

	getChaserPosition(SM_SatPos);
	getChaserAttitude(SM_SatOrient);
	getChaserVelocity(SM_SatSpeed);

	float SM_EEPos[5];
	float SM_EEOrient[5];

	getEndEffPosition(SM_EEPos);
	getEndEffAttitude(SM_EEOrient);

	LogApp(";SM;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f;%f", SatXPose, SatYPose, SatOrient, EEXPose, EEYPose, EEOrient, SM_SatPos[0], SM_SatPos[1], SM_SatOrient[0], SM_EEPos[0], SM_EEPos[1], SM_EEOrient[0], SM_SatSpeed[0], SM_SatSpeed[1], SM_SatSpeed[2]);
}

float unwrap_rad(float error) {
	if (error > SAT_PI)
		error = error - 2 * SAT_PI;
	else if (error < -SAT_PI)
		error = error + 2 * SAT_PI;
	return error;
}
