# Define project name here
PROJECT = acbk_sr

# DRI Solutions libs directory
DRI_LIBS = drilibs
# ARM lib directory
ARM_LIB = 

# List C source files here which must be compiled in ARM-Mode
SRCARM = main.c
SRCARM += MainComputer.c 
SRCARM += include/Protocols/ImuComReceiver.c
SRCARM += include/Protocols/ImuProtocol.c
SRCARM += include/Protocols/GCSProtocol.c
SRCARM += include/Protocols/GCSComRxProtocol.c
SRCARM += include/Protocols/GCSComTxProtocol.c
SRCARM += include/Protocols/JointProtocol_v2.c
SRCARM += include/Protocols/GripperProtocol.c
SRCARM += include/Protocols/VisionSystemProtocol.c
SRCARM += include/Algorithm/AngleCalculation.c
SRCARM += include/Algorithm/Kinematics.c
SRCARM += include/Algorithm/ManipulatorControl.c
SRCARM += include/Algorithm/TrajectoryPlanning.c
SRCARM += include/Algorithm/ManipulatorCartesianControl.c
SRCARM += include/DataDefinition/DataStruct.c
SRCARM += include/Logger/FileLogger.c
SRCARM += include/Timer/Timers.c
SRCARM += include/Serials/Serials.c
SRCARM += include/Gpio/Gpio.c
SRCARM += include/Sockets/SocketCAN.c
SRCARM += include/Sockets/SocketServerLAN.c
SRCARM += include/Manipulator/ManualMode.c
SRCARM += include/Satellite/SatelliteControlLaw.c
SRCARM += $(DRI_LIBS)/CRC/CRC16.c
SRCARM += $(DRI_LIBS)/BitConvert/Convert.c
SRCARM += include/MatrixOperations/matrix_operations.c
SRCARM += include/DynamicJacobian/dynamic_jacobian.c
SRCARM += include/KinematicJacobian/kinematic_jacobian.c
SRCARM += include/OVF/OVF.c
SRCARM += include/OVF/obstacles/obstacle_generator.c
SRCARM += include/OVF/matrix_operations_wrapper/matrix_operations_wrapper.c
SRCARM += include/OVF/jacobian_ovf/dynamic_jacobian.c
SRCARM += include/OVF/firas/pot_FIRAS.c
SRCARM += include/OVF/firas/repulse_vecFIRAS.c
SRCARM += include/OVF/dynamics/direct5d3dof.c
SRCARM += include/SensingModule/sensingModule.c
SRCARM += include/SensingModule/kalmanFilter.c
SRCARM += include/SensingModule/matrixOp.c
SRCARM += include/SensingModule/structDef.c
SRCARM += include/SensingModule/dynJacCalculate.c
		
# List C source files compiled to thumb mode here
# ARM instructions are 32 bits wide, while humb instructions are 16 wide
#SRC_THUMB = 					

# List C++ source files here.
# use file-extension cpp for C++-files (use extension .cpp)
CPPSRC = 

# List Assembler source files here.
# Make them always end in a capital .S.  Files ending in a lowercase .s
# will not be considered source files but generated files (assembler
# output from the compiler), and will be deleted upon "make clean"!
# Even though the DOS/Win* filesystem matches both .s and .S the same,
# it will preserve the spelling of the filenames, and gcc itself does
# care about how the name is spelled on its command-line.
ASRC = 

# List Assembler source files here which must be assembled in ARM-Mode..
#ASRCARM = board_cstartup.S
#ASRCARM = Cstartup.S

#define where is stored main program during execution
#PROGRAM_MEMORY = SRAM
#PROGRAM_MEMORY = SDRAM

# Optimization level, can be [0, 1, 2, 3, s]. 
OPT = 0

# MCU core
MCU = native

# MCU model
SUBMDL = SAMA5D3X

#allow thumb interworking
#THUMB    = -mthumb
#THUMB_IW = -mthumb-interwork

# Floating point definition
# Specifies which floating-point ABI to use. Permissible values are: �soft�, �softfp� and �hard�. 
# Specifying �soft� causes GCC to generate output containing library calls for floating-point operations. 
# �softfp� allows the generation of code using hardware floating-point instructions, but still uses the soft-float calling conventions. 
# �hard� allows generation of floating-point instructions and uses FPU-specific calling conventions.
FLOATING_POINT = -frounding-math

## Using the Atmel AT91_lib produces warning with
## the default warning-levels. 
## yes - disable these warnings; no - keep default settings
AT91LIBNOWARN = no
#AT91LIBNOWARN = no

## Output format. (can be ihex or binary)
## (binary i.e. for openocd and SAM-BA, hex i.e. for lpc21isp and uVision)
#FORMAT = ihex
FORMAT = binary

# Debugging format.
# Native formats for AVR-GCC's -g are stabs [default], or dwarf-2.
# AVR (extended) COFF requires stabs, plus an avr-objcopy run.
# DEBUG = stabs
DEBUG = dwarf-4

# List any extra directories to look for include files here.
# Each directory must be seperated by a space.
EXTRAINCDIRS = ./include

# List any extra directories to look for library files here.
#     Each directory must be seperated by a space.
#EXTRA_LIBDIRS = ../arm7_efsl_0_2_4
EXTRA_LIBDIRS = 

# Compiler flag to set the C Standard level.
# c89   - "ANSI" C
# gnu89 - c89 plus GCC extensions
# c99   - ISO C99 standard (not yet fully implemented)
# gnu99 - c99 plus GCC extensions
CSTANDARD = -std=gnu99

# Place -D or -U options for C here
CDEFS = -D$(SUBMDL)

# Place -I options here
CINCS =

# Place -D or -U options for ASM here
ADEFS =

# Compiler flags.
# -g*:          generate debugging information
# -O*:          optimization level
# -f...:        tuning, see GCC manual and avr-libc documentation
# -Wall...:     warning level
# -Wa,...:      tell GCC to pass this to the assembler.
# -adhlns...: create assembler listing
#
# -Wcast-align
#	Warn whenever a pointer is cast such that the required alignment of the target is increased. 
#	For example, warn if a char * is cast to an int * on machines where integers can only be accessed at two- or four-byte boundaries.
#
# -Wimplicit (C and Objective-C only)
#	Warn when a declaration does not specify a type. This warning is enabled by -Wall.
#
# -Wpointer-arith
#	Warn about anything that depends on the �size of� a function type or of void. 
#	GNU C assigns these types a size of 1, for convenience in calculations with void * pointers and pointers to functions. 
#	In C++, warn also when an arithmetic operation involves NULL. 
#
# -Wswitch
# 	Warn whenever a switch statement has an index of enumerated type and lacks a case for one or more of the named codes of that enumeration. 
# 	Case labels outside the enumeration range also provoke warnings when this option is used. This warning is enabled by -Wall.
#
# -Wredundant-decls
#	Warn if anything is declared more than once in the same scope, even in cases where multiple declaration is valid and changes nothing.
#
#-Wreturn-type
#	Warn whenever a function is defined with a return type that defaults to int. 
# 	Also warn about any return statement with no return value in a function whose return type is not void
#
# -Wshadow
#	Warn whenever a local variable or type declaration shadows another variable, parameter, type, or class member (in C++), 
#	or whenever a built-in function is shadowed. Note that in C++, the compiler warns if a local variable shadows an explicit typedef, 
#	but not if it shadows a struct/class/enum.
#
# -Wunused
#	Warn whenever a function parameter is assigned to, but otherwise unused (aside from its declaration).
#	Warn whenever a local variable is assigned to, but otherwise unused (aside from its declaration). This warning is enabled by -Wall.
#	Warn whenever a static function is declared but not defined or a non-inline static function is unused. This warning is enabled by -Wall.
#	Warn whenever a label is declared but not used. This warning is enabled by -Wall.
#	Warn when a typedef locally defined in a function is not used. This warning is enabled by -Wall.
# 	Warn whenever a function parameter is unused aside from its declaration.
#	Warn whenever a local variable or non-constant static variable is unused aside from its declaration. This warning is enabled by -Wall.
#	Warn whenever a statement computes a result that is explicitly not used. To suppress this warning cast the unused expression to �void�. 
#	This includes an expression-statement or the left-hand side of a comma expression that contains no side effects. 
#	For example, an expression such as �x[i,j]� causes a warning, while �x[(void)i,j]� does not. This warning is enabled by -Wall.
#
# Flags for C and C++
#CFLAGS = -g$(DEBUG)
CFLAGS += $(CDEFS) $(CINCS)
#CFLAGS += -s -DNDEBUG
CFLAGS += -O$(OPT)
CFLAGS += -Wall -Wcast-align -Wimplicit 
CFLAGS += -Wpointer-arith -Wswitch
CFLAGS += -Wredundant-decls -Wreturn-type -Wshadow -Wunused
#CFLAGS += -Wa,-adhlns=$(subst $(suffix $<),.lst,$<) 
CFLAGS += $(patsubst %,-I%,$(EXTRAINCDIRS))

# flags only for C
# -Wnested-externs (C and Objective-C only)
#	Warn if an extern declaration is encountered within a function.
CONLYFLAGS += -Wnested-externs 
CONLYFLAGS += $(CSTANDARD)

# #AT91-lib warnings with:
# -Wmissing-prototypes (C and Objective-C only)
# 	Warn if a global function is defined without a previous prototype declaration. 
# 	This warning is issued even if the definition itself provides a prototype. 
# 	Use this option to detect global functions that do not have a matching prototype declaration in a header file. 
# 	This option is not valid for C++ because all function declarations provide prototypes
# 	and a non-matching declaration will declare an overload rather than conflict with an earlier declaration. 
# 	Use -Wmissing-declarations to detect missing declarations in C++. 
# -Wstrict-prototypes (C and Objective-C only)
#	Warn if a function is declared or defined without specifying the argument types. 
#	(An old-style function definition is permitted without a warning if preceded by a declaration that specifies the argument types.)
# -Wmissing-declarations
# 	Warn if a global function is defined without a previous declaration. 
# 	Do so even if the definition itself provides a prototype.
# 	Use this option to detect global functions that are not declared in header files. 
# 	In C, no warnings are issued for functions with previous non-prototype declarations; use -Wmissing-prototype to detect missing prototypes. 
# 	In C++, no warnings are issued for function templates, or for inline functions, or for functions in anonymous namespaces.
ifneq ($(AT91LIBNOWARN),yes)
#CFLAGS += -Wcast-qual
CONLYFLAGS += -Wmissing-prototypes 
CONLYFLAGS += -Wstrict-prototypes
CONLYFLAGS += -Wmissing-declarations
endif

# flags only for C++ (arm-elf-g++)
# CPPFLAGS = -fno-rtti -fno-exceptions
CPPFLAGS = 

# Assembler flags.
#  -Wa,...:    tell GCC to pass this to the assembler.
#  -ahlns:     create listing
#  -g$(DEBUG): have the assembler create line number information
#	It is useful when assembly codes and C codes share the header files.
#	Assembler can preprocess #define macros but does not recognize most of
#	C syntaxes such as typedef or function prototypes. 
#	By excluding C specific codes by preprocessor when __ASSEMBLY__ macro is defined, 
#	the header file can be included by both C and assembly codes.
#	__ASSEMBLY__ is used in: AT91SAM9260.h
ASFLAGS = $(ADEFS) -Wa,-adhlns=$(<:.S=.lst),-g$(DEBUG) -D__ASSEMBLY__


#Additional libraries.

# Extra libraries
#    Each library-name must be seperated by a space.
#    To add libxyz.a, libabc.a and libefsl.a: 
#    EXTRA_LIBS = xyz abc efsl
EXTRA_LIBS =

#Support for newlibc-lpc (file: libnewlibc-lpc.a)
#NEWLIBLPC = -lnewlib-lpc

#MATH_LIB = -lm
MATH_LIB = -lm

# CPLUSPLUS_LIB = -lstdc++


# Linker flags.
# -Wl,...:   tell GCC to pass this to linker.
# -Map:      create map file
# --cref:    add cross reference to  map file
# -nostartfiles
#	Do not use the standard system startup files when linking. 
#	The standard system libraries are used normally, unless -nostdlib or -nodefaultlibs is used.
# -l library 
#	Search the library named library when linking. For example -lm search math library.  

LDFLAGS =

#LDFLAGS = -nostartfiles -Wl,-Map=main.map,--cref
#LDFLAGS += -lc
LDFLAGS += $(MATH_LIB)
#LDFLAGS += -lc -lgcc 
#LDFLAGS += $(patsubst %,-L%,$(EXTRA_LIBDIRS))
#LDFLAGS += $(patsubst %,-l%,$(EXTRA_LIBS))

#ifeq ($(PROGRAM_MEMORY),SDRAM)
#LDFLAGS +=-Tinclude/sdram.lds
#else
#ifeq ($(PROGRAM_MEMORY),SRAM)
#LDFLAGS +=-Tinclude/sram.lds
#else
#@echo Program memory not recognized
#endif
#endif

# Define programs and commands.
SHELL = sh
CC = gcc
#OBJCOPY = arm-linux-gnueabihf-objcopy
#OBJDUMP = arm-linux-gnueabihf-objdump
SIZE = ls -lh
#NM = arm-linux-gnueabihf-nm
REMOVE = rm -f
COPY = cp

# Define Messages
MSG_ERRORS_NONE = Errors: none
MSG_BEGIN = "-------- begin --------"
MSG_END = --------  end  --------
MSG_SIZE_BEFORE = Size before: 
MSG_SIZE_AFTER = Size after:
MSG_FLASH = Creating load file for Flash:
MSG_EXTENDED_LISTING = Creating Extended Listing:
MSG_SYMBOL_TABLE = Creating Symbol Table:
MSG_LINKING = Linking:
MSG_COMPILING = Compiling C:
MSG_COMPILING_ARM = "Compiling C (ARM-only):"
MSG_COMPILINGCPP = Compiling C++:
MSG_COMPILINGCPP_ARM = "Compiling C++ (ARM-only):"
MSG_ASSEMBLING = Assembling:
MSG_ASSEMBLING_ARM = "Assembling (ARM-only):"
MSG_CLEANING = Cleaning project:
MSG_FORMATERROR = Can not handle output-format

# Define all object files.
COBJARM   = $(SRCARM:.c=.o)

# Define all listing files.
LST = $(ASRC:.S=.lst) $(SRCARM:.c=.lst)

# Compiler flags to generate dependency files.
# -MD 	Preprocess and compile, generating output file containing dependency information ending with extension .d.
# -MP	Tells the compiler to add a phony target for each dependency.
# -MF	Tells the compiler to generate makefile dependency information in a file.
### GENDEPFLAGS = -Wp,-M,-MP,-MT,$(*F).o,-MF,.dep/$(@F).d
GENDEPFLAGS = -MD -MP -MF .dep/$(@F).d

# Combine all necessary flags and optional flags.
# -x 	Add target processor to flags.
# 	Specify explicitly the language for the following input files. 
# 	This option applies to all following input files until the next -x option. 
# 	Possible values for language are: 
#	  c  c-header  cpp-output
#	  c++  c++-header  c++-cpp-output
#	  objective-c  objective-c-header  objective-c-cpp-output
#	  objective-c++ objective-c++-header objective-c++-cpp-output
#	  assembler  assembler-with-cpp
#	  ada
#	  f77  f77-cpp-input f95  f95-cpp-input
#	  go
#	  java
ALL_CFLAGS  = -march=$(MCU) $(FLOATING_POINT) -I. $(CFLAGS) $(GENDEPFLAGS)
ALL_ASFLAGS = -march=$(MCU) $(FLOATING_POINT) -I. $(ASFLAGS) #-x assembler-with-cpp 

# Default target.
all: begin gccversion sizebefore build sizeafter finished end

#ifeq ($(FORMAT),ihex)
#build: elf hex lss sym
#hex: $(PROJECT).hex
#IMGEXT=hex
#else 
#ifeq ($(FORMAT),binary)
#build: elf bin lss sym
#bin: $(PROJECT).bin
#IMGEXT=bin
#else 
#$(error "$(MSG_FORMATERROR) $(FORMAT)")
#endif
#endif


build: elf
elf: $(PROJECT).elf
#lss: main.lss 
#sym: main.sym

# Eye candy.
begin:
	@echo
	@echo $(MSG_BEGIN)

finished:
	@echo $(MSG_ERRORS_NONE)

end:
	@echo $(MSG_END)
	@echo


# Display size of file.
# -A 	Select output style: SysV
#HEXSIZE = $(SIZE) --target=$(FORMAT) main.hex
#ELFSIZE = $(SIZE) -A orco.elf
ELFSIZE = $(SIZE) -A $(PROJECT).elf
sizebefore:
	@if [ -f $(PROJECT).elf ]; then echo; echo $(MSG_SIZE_BEFORE); $(ELFSIZE); echo; fi 

sizeafter:
	@echo $(MSG_SIZE_AFTER) 
	$(ELFSIZE)


# Display compiler version information.
gccversion : 
	@$(CC) --version


program:
	@echo "Automatic programming not supported for AT91SAM9260"


# Create final output file (.hex) from ELF output file.
# -O	Create an output file in output format specified after this option
#%.hex: %.elf
#	@echo
#	@echo $(MSG_FLASH) $@
#	$(OBJCOPY) -O $(FORMAT) $< $@
	
	
# Create final output file (.bin) from ELF output file.
# -O	Create an output file in output format specified after this option
#%.bin: %.elf
#	@echo
#	@echo $(MSG_FLASH) $@
#	$(OBJCOPY) -O $(FORMAT) $< $@


# Create extended listing file from ELF output file.
# testing: option -C
# -h	Display the contents of the section headers
# -S	Intermix source code with disassembly
#%.lss: %.elf
#	@echo
#	@echo $(MSG_EXTENDED_LISTING) $@
#	$(OBJDUMP) -h -S $< > $@			


# Create a symbol table from ELF output file.
# -n	Sort szmbols numerically by address
#%.sym: %.elf
#	@echo
#	@echo $(MSG_SYMBOL_TABLE) $@
#	$(NM) -n $< > $@


# Link: create ELF output file from object files.
.SECONDARY : main.elf
.PRECIOUS : $(COBJARM)
%.elf: $(COBJARM)
	@echo
	@echo $(MSG_LINKING) $@
	$(CC) $(ALL_CFLAGS) $(COBJARM) -lrt -lpthread --output $@ $(LDFLAGS)

# Compile: create object files from C source files. ARM-only
#$(CC) -c $(ALL_CFLAGS) $(CONLYFLAGS) $< -o $@ 
$(COBJARM) : %.o : %.c
	@echo
	@echo $(MSG_COMPILING_ARM) $<
	$(CC) -c $(ALL_CFLAGS) $(CONLYFLAGS) $< -lrt -o $@



# Target: clean project.
clean: begin clean_list finished end


clean_list :
	@echo
	@echo $(MSG_CLEANING)
	$(REMOVE) main.hex
	$(REMOVE) $(PROJECT).bin
	$(REMOVE) main.obj
	$(REMOVE) $(PROJECT).elf
	$(REMOVE) main.elf
	$(REMOVE) main.map
	$(REMOVE) main.obj
	$(REMOVE) main.a90
	$(REMOVE) main.sym
	$(REMOVE) main.lnk
	$(REMOVE) main.lss
	$(REMOVE) $(COBJARM)
	$(REMOVE) $(LST)
	$(REMOVE) $(SRCARM:.c=.s)
	$(REMOVE) $(SRCARM:.c=.d)
	$(REMOVE) .dep/*


# Include the dependency files.
-include $(shell mkdir .dep 2>/dev/null) $(wildcard .dep/*)

# Listing of phony targets.
.PHONY : all begin finish end sizebefore sizeafter gccversion \
build elf hex bin lss sym clean clean_list program
