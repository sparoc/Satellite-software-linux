#ifndef CRC_16_H
#define CRC_16_H

#define  SYNCH_BYTE          0x7E
#define  ESCAPE_BYTE         0x7D


int CheckCrc(volatile unsigned char *a_arrPackage, int a_nStartIndex, int a_nSize);
int CalculateByte(int a_nCrc, unsigned int a_nValue);
int CalculateCrc(volatile unsigned char *a_arrPacket, int a_nStartIndex, int a_nCount);
#endif // CRC_16_H
