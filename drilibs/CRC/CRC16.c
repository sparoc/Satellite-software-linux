#include "CRC16.h"

// Calculate CRC
int CalculateByte(int a_nCrc, unsigned int a_nValue)
{
    a_nCrc = a_nCrc ^ (int)a_nValue << 8;

    int i;
    for (i = 0; i < 8; i++)
    {
        if ((a_nCrc & 0x8000) == 0x8000)
            a_nCrc = a_nCrc << 1 ^ 0x1021;
        else
            a_nCrc = a_nCrc << 1;
    }
    return a_nCrc & 0xFFFF;
}

// Calculate new CRC as you can compare it with the CRC from the frame
int CalculateCrc(volatile  unsigned char *a_arrPacket, int a_nStartIndex, int a_nCount)
{
   //int i;
   int nCrc = 0;
   
   //for(i = a_nStartIndex; i < (a_nStartIndex + a_nCount); i++)
   while(a_nCount > 0)
   {
      nCrc = CalculateByte( nCrc, *(a_arrPacket + a_nStartIndex++) );
	  a_nCount--;
   }

   return nCrc;
}

// Check CRC. 'a_arrPackage' do not contain SYNCH_BYTEs
int CheckCrc(volatile unsigned char *a_arrPackage, int a_nStartIndex, int a_nSize)
{
    int nCrc = CalculateCrc( a_arrPackage, a_nStartIndex, a_nSize );

    unsigned char bFirstCrcByte = nCrc >> 0;
    unsigned char bSecondCrcByte = nCrc >> 8;
    
    if( bFirstCrcByte == *(a_arrPackage + a_nStartIndex + a_nSize) &&
        bSecondCrcByte == *(a_arrPackage + a_nStartIndex + a_nSize + 1) )
            return 1;
    else
            return 0;
}
