#include "Convert.h"

typedef union
{
   short value;
   unsigned char array[2];
}ShortUnion __attribute__ ((aligned (4)));

typedef union
{
   unsigned short value;
   unsigned char array[2];
}UShortUnion __attribute__ ((aligned (4)));

typedef union
{
   int value;
   unsigned char array[4];
}IntUnion __attribute__ ((aligned (4)));

typedef union
{
   unsigned int value;
   unsigned char array[4];
}UIntUnion __attribute__ ((aligned (4)));

typedef union
{
   float value;
   unsigned char array[4];
}FloatUnion __attribute__ ((aligned (4)));

typedef union
{
   double value;
   unsigned char array[8];
}DoubleUnion __attribute__ ((aligned (4)));

static ShortUnion shortConvert;
static UShortUnion uShortConvert;
static IntUnion intConvert;
static UIntUnion uIntConvert;
static FloatUnion floatConvert;
static DoubleUnion doubleConvert;


short ConvertToLSBShort(unsigned char * a_array, int a_StartIndex)
{
    shortConvert.array[0]  = a_array[a_StartIndex];
    shortConvert.array[1]  = a_array[++a_StartIndex];
    return shortConvert.value;
}

unsigned short ConvertToLSBUShort(unsigned char * a_array, int a_StartIndex)
{
	uShortConvert.array[0]  = a_array[a_StartIndex];
	uShortConvert.array[1]  = a_array[++a_StartIndex];
    return uShortConvert.value;
}

int ConvertToLSBInt(unsigned char * a_array, int a_StartIndex)
{
    intConvert.array[0]  = a_array[a_StartIndex];
    intConvert.array[1]  = a_array[++a_StartIndex];
    intConvert.array[2]  = a_array[++a_StartIndex];
    intConvert.array[3]  = a_array[++a_StartIndex];
    return intConvert.value;
}

float ConvertToLSBSingle(unsigned char * a_array, int a_StartIndex)
{
    floatConvert.array[0]  = a_array[a_StartIndex];
    floatConvert.array[1]  = a_array[++a_StartIndex];
    floatConvert.array[2]  = a_array[++a_StartIndex];
    floatConvert.array[3]  = a_array[++a_StartIndex];
    return floatConvert.value;
}

double ConvertToLSBDouble(unsigned char * a_array, int a_StartIndex)
{
	doubleConvert.array[0]  = a_array[a_StartIndex];
	doubleConvert.array[1]  = a_array[++a_StartIndex];
	doubleConvert.array[2]  = a_array[++a_StartIndex];
	doubleConvert.array[3]  = a_array[++a_StartIndex];
	doubleConvert.array[4]  = a_array[++a_StartIndex];
	doubleConvert.array[5]  = a_array[++a_StartIndex];
	doubleConvert.array[6]  = a_array[++a_StartIndex];
	doubleConvert.array[7]  = a_array[++a_StartIndex];
    return doubleConvert.value;
}


int PutShortIntoLSBArray(unsigned char * a_array, int a_StartIndex, short value)
{
    shortConvert.value = value;
    a_array[a_StartIndex] = shortConvert.array[0];
    a_array[++a_StartIndex] = shortConvert.array[1];
    return ++a_StartIndex;
}

int PutUShortIntoLSBArray(unsigned char * a_array, int a_StartIndex, unsigned short value)
{
	uShortConvert.value = value;
    a_array[a_StartIndex] = uShortConvert.array[0];
    a_array[++a_StartIndex] = uShortConvert.array[1];
    return ++a_StartIndex;
}

int PutIntIntoLSBArray(unsigned char * a_array, int a_StartIndex, int value)
{
    intConvert.value = value;
    a_array[a_StartIndex] = intConvert.array[0];
    a_array[++a_StartIndex] = intConvert.array[1];
    a_array[++a_StartIndex] = intConvert.array[2];
    a_array[++a_StartIndex] = intConvert.array[3];
    return ++a_StartIndex;
}

int PutUIntIntoLSBArray(unsigned char * a_array, int a_StartIndex, unsigned int value)
{
    uIntConvert.value = value;
    a_array[a_StartIndex] = uIntConvert.array[0];
    a_array[++a_StartIndex] = uIntConvert.array[1];
    a_array[++a_StartIndex] = uIntConvert.array[2];
    a_array[++a_StartIndex] = uIntConvert.array[3];
    return ++a_StartIndex;
}

int PutSingleIntoLSBArray(unsigned char * a_array, int a_StartIndex, float value)
{
    floatConvert.value = value;
    a_array[a_StartIndex] = floatConvert.array[0];
    a_array[++a_StartIndex] = floatConvert.array[1];
    a_array[++a_StartIndex] = floatConvert.array[2];
    a_array[++a_StartIndex] = floatConvert.array[3];
    return ++a_StartIndex;
}

int PutDoubleIntoLSBArray(unsigned char * a_array, int a_StartIndex, double value)
{
	doubleConvert.value = value;
    a_array[a_StartIndex] = doubleConvert.array[0];
    a_array[++a_StartIndex] = doubleConvert.array[1];
    a_array[++a_StartIndex] = doubleConvert.array[2];
    a_array[++a_StartIndex] = doubleConvert.array[3];
    a_array[++a_StartIndex] = doubleConvert.array[4];
    a_array[++a_StartIndex] = doubleConvert.array[5];
    a_array[++a_StartIndex] = doubleConvert.array[6];
    a_array[++a_StartIndex] = doubleConvert.array[7];
    return ++a_StartIndex;
}

short ConvertToMSBShort(unsigned char * a_array, int a_StartIndex)
{
    shortConvert.array[0]  = a_array[1 + a_StartIndex];
    shortConvert.array[1]  = a_array[a_StartIndex];
    return shortConvert.value;
}

int ConvertToMSBInt(unsigned char * a_array, int a_StartIndex)
{
    intConvert.array[0]  = a_array[3 + a_StartIndex];
    intConvert.array[1]  = a_array[2 + a_StartIndex];
    intConvert.array[2]  = a_array[1 + a_StartIndex];
    intConvert.array[3]  = a_array[a_StartIndex];
    return intConvert.value;
}

float ConvertToMSBSingle(unsigned char * a_array, int a_StartIndex)
{
    floatConvert.array[0]  = a_array[3 + a_StartIndex];
    floatConvert.array[1]  = a_array[2 + a_StartIndex];
    floatConvert.array[2]  = a_array[1 + a_StartIndex];
    floatConvert.array[3]  = a_array[a_StartIndex];
    return floatConvert.value;
}

int PutShortIntoMSBArray(unsigned char * a_array, int a_StartIndex, short value)
{
    shortConvert.value = value;
    a_array[a_StartIndex] = shortConvert.array[1];
    a_array[++a_StartIndex] = shortConvert.array[0];
    return ++a_StartIndex;
}

int PutIntIntoMSBArray(unsigned char * a_array, int a_StartIndex, int value)
{
    intConvert.value = value;
    a_array[a_StartIndex] = intConvert.array[3];
    a_array[++a_StartIndex] = intConvert.array[2];
    a_array[++a_StartIndex] = intConvert.array[1];
    a_array[++a_StartIndex] = intConvert.array[0];
    return ++a_StartIndex;
}

int PutSingleIntoMSBArray(unsigned char * a_array, int a_StartIndex, float value)
{
    floatConvert.value = value;
    a_array[a_StartIndex] = floatConvert.array[3];
    a_array[++a_StartIndex] = floatConvert.array[2];
    a_array[++a_StartIndex] = floatConvert.array[1];
    a_array[++a_StartIndex] = floatConvert.array[0];
    return ++a_StartIndex;
}

// Convert Integer value to ASCII char array
// Return length of the output array
short ConvertIntegerToString(unsigned char *output, int a_nValue)
{
    unsigned char negFlag = 0;
    unsigned char pos = 0;
    if(a_nValue < 0)
    {
         negFlag = 1;
         a_nValue = -a_nValue;
    }
    while(a_nValue > 0)
    {
         output[pos++] = '0' + a_nValue % 10;
         a_nValue = a_nValue / 10;
    }
    if(negFlag)
        output[pos++] = '-';

    output[pos] = '\0';

    short i;
   	unsigned char ch;
   	for(i = 0; i <= (pos - 1) / 2; i++)
    {
        ch = output[i];
        output[i] = output[pos - 1 - i];
        output[pos-1-i] = ch;
    }

   	return pos;
}

// a_nValue == 0x6A Hex or 106 Dec
// output[0] = 54 - '6'
// output[1] = 65 - 'A'
short ConvertIntegerToHex(unsigned char *output, int a_nValue)
{
	 unsigned char nCounter = 0;
     int mod16[5];
     unsigned char i = 0;
     while(a_nValue > 15)
     {
           mod16[i] = a_nValue % 16;
           a_nValue = a_nValue / 16;
           i++;
           nCounter++;
     }

     mod16[i] = a_nValue;
     unsigned char index = nCounter;
     for (i = 0; i <= nCounter; i++)
     {
    	 if (mod16[i] == 0)
    	    output[index] = 48;			// 0
    	 else if (mod16[i] == 1)
    	    output[index] = 49;			// 1
    	 else if (mod16[i] == 2)
    	    output[index] = 50;			// 2
    	 else if (mod16[i] == 3)
    	    output[index] = 51;			// 3
    	 else if (mod16[i] == 4)
    	    output[index] = 52;			// 4
    	 else if (mod16[i] == 5)
    	    output[index] = 53;			// 5
    	 else if (mod16[i] == 6)
    	    output[index] = 54;			// 6
    	 else if (mod16[i] == 7)
    	    output[index] = 55;			// 7
    	 else if (mod16[i] == 8)
    	    output[index] = 56;			// 8
    	 else if (mod16[i] == 9)
    	    output[index] = 57;			// 9
    	 else if (mod16[i] == 10)
         	output[index] = 65;         // A
         else if (mod16[i] == 11)
            output[index] = 66;         // B
         else if (mod16[i] == 12)
            output[index] = 67;         // C
         else if (mod16[i] == 13)
         	output[index] = 68;         // D
         else if (mod16[i] == 14)
            output[index] = 69;         // E
         else if (mod16[i] == 15)
            output[index] = 70;         // F

    	 index--;
     }

     return ++nCounter;
}
