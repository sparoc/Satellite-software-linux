#ifndef CONVERT_H
#define CONVERT_H

short ConvertToLSBShort(unsigned char * a_array, int a_StartIndex);
unsigned short ConvertToLSBUShort(unsigned char * a_array, int a_StartIndex);
int ConvertToLSBInt(unsigned char * a_array, int a_StartIndex);
float ConvertToLSBSingle(unsigned char * a_array, int a_StartIndex);
double ConvertToLSBDouble(unsigned char * a_array, int a_StartIndex);

int PutShortIntoLSBArray(unsigned char * a_array, int a_StartIndex, short value);
int PutUShortIntoLSBArray(unsigned char * a_array, int a_StartIndex, unsigned short value);
int PutIntIntoLSBArray(unsigned char * a_array, int a_StartIndex, int value);
int PutUIntIntoLSBArray(unsigned char * a_array, int a_StartIndex, unsigned int value);
int PutSingleIntoLSBArray(unsigned char * a_array, int a_StartIndex, float value);
int PutDoubleIntoLSBArray(unsigned char * a_array, int a_StartIndex, double value);

short ConvertToMSBShort(unsigned char * a_array, int a_StartIndex);
int ConvertToMSBInt(unsigned char * a_array, int a_StartIndex);
float ConvertToMSBSingle(unsigned char * a_array, int a_StartIndex);

int PutShortIntoMSBArray(unsigned char * a_array, int a_StartIndex, short value);
int PutIntIntoMSBArray(unsigned char * a_array, int a_StartIndex, int value);
int PutSingleIntoMSBArray(unsigned char * a_array, int a_StartIndex, float value);

short ConvertIntegerToString(unsigned char *output, int a_nValue);
short ConvertIntegerToHex(unsigned char *output, int a_nValue);

#endif
