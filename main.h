/*
 * main.h
 *
 */

#ifndef MAIN_H_
#define MAIN_H_

#include <math.h>
#include "MainComputer.h"
#include "include/Timer/Timers.h"
#include "include/Protocols/ImuProtocol.h"
#include "include/Protocols/GCSProtocol.h"
#include "include/Protocols/GCSComTxProtocol.h"
#include "include/Protocols/GCSComRxProtocol.h"
#include "include/Protocols/GripperProtocol.h"
#include "include/Protocols/ImuComReceiver.h"
#include "include/Protocols/JointProtocol_v2.h"
#include "include/Protocols/VisionSystemProtocol.h"
#include "include/Satellite/SatelliteControlLaw.h"
#include "../drilibs/BitConvert/Convert.h"
#include "include/Serials/Serials.h"
#include "include/Logger/FileLogger.h"
#include "include/Sockets/SocketCAN.h"
#include "include/Sockets/SocketServerLAN.h"
#include "include/Algorithm/Kinematics.h"
#include "include/Algorithm/ManipulatorControl.h"
#include "include/Algorithm/ManipulatorCartesianControl.h"
#include "include/MatrixOperations/matrix_operations.h"
#include "include/DynamicJacobian/dynamic_jacobian.h"
#include "include/KinematicJacobian/kinematic_jacobian.h"
#include "include/OVF/OVF.h"
#include "include/SensingModule/dynJacCalculate.h"
#include "include/SensingModule/kalmanFilter.h"
#include "include/SensingModule/matrixOp.h"
#include "include/SensingModule/structDef.h"
#include "include/SensingModule/sensingModule.h"
#include "include/AdaptiveControl/adaptive_controler.h"

void *rs0RxThread(void *arg);
void *rsImuRxThread(void *arg);
void *can0Thread(void *arg);
void *can1Thread(void *arg);
void *lanServerThread(void *arg);
void *lanClientConnectionHandler(void *clientSocketDesc);
int start_thread_with_priority(pthread_t* thread, void *(*start_routine)(void*), void* arg);

void Calc_VS_Position(void);
void Read_Encoder_Data(void);

#endif /* MAIN_H_ */
