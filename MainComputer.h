/*
 * MainComputer.h
 *
 *  Created on: Oct 5, 2016
 *      Author: ubuntu
 */

#ifndef INCLUDE_PROTOCOLS_MAINCOMPUTER_H_
#define INCLUDE_PROTOCOLS_MAINCOMPUTER_H_

#include "include/DataDefinition/DataStruct.h"

enum WorkingMode
{
	MAIN_COMP_STANDBY = 0x00,
	MAIN_COMP_INIT = 0x01,
	MAIN_COMP_INIT_DONE = 0x02,
	MAIN_COMP_NOMINAL = 0x03,
	MAIN_COMP_NOMINAL_DONE = 0x04,
	MAIN_COMP_MANULA = 0x05,
	MAIN_COMP_MAN_CON_OFF = 0x06,
};

void InitMainComputerMembers(void);

#endif /* INCLUDE_PROTOCOLS_MAINCOMPUTER_H_ */
