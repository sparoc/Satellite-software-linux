/*
 * MainComputer.c
 *
 *  Created on: Oct 5, 2016
 *      Author: ubuntu
 */

#include "MainComputer.h"


void InitMainComputerMembers(void)
{
	sAppState.WorkingMode = MAIN_COMP_STANDBY;
	sAppState.InitProcessDone = MAIN_COMP_STANDBY;
	sAppState.DisableGCSCommands = 0;
	sAppState.LoopCycles100Hz = 0;
	sAppState.LoopCycles10Hz = 0;
	sAppState.LoopCycles1Hz = 0;
}
